# README #

This software suite contains tools for analyzing time trajectrories from particle-based simulations.

The current supported functionalities include:

 1. Clustering analysis, i.e., finding clusters of particles within a certain cutoff
 2. Radial distribution function g(r) between two separate sets of particle types
 3. Structure factor S(q)
 4. Diffraction patterns perpendicular to a given direction
 5. Polymer conformation analysis for multiple chains
 6. Time-correlation functions 
 7. Intermediate scattering functions

The suppoted input data format includes:

 * For analysis of single snapshots/configurations: LAMMPS dump files, and HOOMD XML files
 * For analysis of multiple snapshots: LAMMPS dump files, HOOMD XML files, and DCD trajectories

### Build ###

* Prerequisite: C++ compiler, FFTW3 libraries (recommended)
  After you have installed FFTW3 libraries on your machine, modify the FFTW3_HOME variable in Makefile.cpu
  so that $(FFTW3_HOME)/include and $(FFTW3_HOME)/lib are where fftw3.h and libfftw3.so reside.

* Traditional build: Go to src/, build with the provided Makefile.cpu

`
    make -f Makefile.cpu -j4
`

If successful, separate binaries with explanatory names will be generated within src/. A static library libanalyze.a is also built for use from any driving code.

* CMake build: At the top level, create a build folder and run CMake from that folder

`
    mkdir build;
    cd build/;
    ccmake ../;
    make
`

### Examples ###

Example data sets for analysis are provided in examples/.

The header of the source files src/main-*.cpp also gives some example use cases
for the corresponding binaries.

### Python wrappers ###

The binaries built in src/ can be invoked using the python scripts under wrappers/.

### Contact ###

* Trung Nguyen (ndactrung at gmail dot com)
