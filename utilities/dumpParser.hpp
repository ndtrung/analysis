//
// Created by martin on 2/2/17.
//

#ifndef IONSCLUSTERS_DUMPPARSER_HPP
#define IONSCLUSTERS_DUMPPARSER_HPP

#include <vector>
#include <cstdio>
#include <string>

class dumpParser
{
    FILE* file_ptr = nullptr;

    std::vector<std::string> attribute_names;
    std::vector< std::vector<std::string> > attribute_values;

    int parse_dump_file();

public:
    dumpParser() = default;

    int open_dump(const char* filename);

    std::vector<std::string> get_attr_list(std::string dump_attr_name, int& err);

    int close_file();

    ~dumpParser()
    {
        if(file_ptr)
        {
            close_file();
        }
    }
};

enum LAMMPS_POS_MODE // x, xu, xs, xus in traj file
{
    NORMAL,
    UNWRAPPED,
    SCALED,
    SCALED_UNWRAPPED
};

class lammpstrjParser
{
    FILE* file_ptr = nullptr;

    LAMMPS_POS_MODE coordinate_mod = NORMAL;

    long cframe_location;

    std::size_t nparticles = 0;

    std::size_t current_frame = 0;

    std::size_t current_timestep = 0;

    std::vector<std::string> attribute_names;

    std::vector< std::vector<std::string> > current_frame_values;
public:

    lammpstrjParser() = default;

    int open_traj(const char* filename);

    std::vector<std::string> get_attr_list(std::string attr_name, int& err);

    int get_next_frame(float* box, float* x, float* y, float* z);

    int goto_frame(std::size_t frame);

    int goto_timestep(std::size_t step);

    ~lammpstrjParser();
};

#endif //IONSCLUSTERS_DUMPPARSER_HPP
