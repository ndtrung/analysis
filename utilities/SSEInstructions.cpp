#include "SSEInstructions.hpp"
#include <x86intrin.h>
#include <cstdint>
#include <cstdlib>

// I'm assuming that any computer has SSE / SSE2 extensions at the very least since it dates to 2001. I'm assuming that gcc will use SSE2 - SSE 4.2
// if available when compiled with -march=native; the separation for AVX2 is the use of ymm registers (256b wide vs 128b SSE/AVX)

#ifdef __AVX2__
void sumuint16_from_to( std::uint16_t* __restrict__ array_from,  std::uint16_t* __restrict__ array_into,  std::size_t array_sz)
{
    std::size_t mov_sz = array_sz / 16;
    __m256i* m256_ptr_fr = (__m256i*) array_from;
    __m256i* m256_ptr_to = (__m256i*) array_into;
    for(std::size_t sse_idx = 0; sse_idx != mov_sz; sse_idx++)
    {
        __m256i from = _mm256_load_si256(m256_ptr_fr++);
        __m256i into = _mm256_load_si256(m256_ptr_to);
        into = _mm256_add_epi16(from, into);
        _mm256_stream_si256(m256_ptr_to++, into);
    }
}

void sumf_from_to( float* __restrict__ array_from,  float* __restrict__ array_into,  std::size_t array_sz)
{
    std::size_t mov_sz = array_sz / 8;
    __m256* m256_ptr_fr = (__m256*) array_from;
    __m256* m256_ptr_to = (__m256*) array_into;
    for(std::size_t sse_idx = 0; sse_idx != mov_sz; sse_idx++)
    {
        __m256 from = _mm256_load_ps((float*) m256_ptr_fr++);
        __m256 into = _mm256_load_ps((float*) m256_ptr_to);
        into = _mm256_add_ps(from, into);
        _mm256_stream_ps((float*) (m256_ptr_to++), into);
    }
}


void float_div_by_int( float* __restrict__ array,  std::uint16_t val,  std::size_t array_sz)
{
    std::size_t mov_sz = array_sz/8;
    __m256* m256_arr_ptr = (__m256*) array;
    __m256 m256_div = _mm256_set1_ps(1.0f / (float) val);
    for(std::size_t sse_idx = 0; sse_idx != mov_sz; sse_idx++)
    {
        __m256 m256_array = _mm256_load_ps((float*) m256_arr_ptr);
        m256_array = _mm256_mul_ps(m256_array, m256_div);
        _mm256_stream_ps((float*) m256_arr_ptr++, m256_array);
    }
}

void int_div_by_int_cast( std::uint16_t* __restrict__ array,  float* __restrict__ array_return,  std::uint16_t val,  std::size_t array_sz)
{
    std::size_t mov_sz = array_sz / 16;
    __m256* m256f_array_ptr = (__m256*) array_return;
    __m256i* m256i_arr_ptr = (__m256i*) array;

    __m256 m256_div = _mm256_set1_ps(1.0f / (float) val);
    for(std::size_t sse_idx = 0; sse_idx != mov_sz; sse_idx++)
    {
        __m256i m256_short_array = _mm256_load_si256(m256i_arr_ptr++);
        __m256i m256_int_lo = _mm256_unpacklo_epi16(m256_short_array, _mm256_set1_epi16(0));
        __m256i m256_int_hi = _mm256_unpackhi_epi16(m256_short_array, _mm256_set1_epi16(0));
        __m256 fl_lo = _mm256_cvtepi32_ps(m256_int_lo);
        __m256 fl_hi = _mm256_cvtepi32_ps(m256_int_hi);
        fl_lo = _mm256_mul_ps(fl_lo, m256_div);
        fl_hi = _mm256_mul_ps(fl_hi, m256_div);
        _mm256_stream_ps((float*) m256f_array_ptr++, fl_lo);
        _mm256_stream_ps((float*) m256f_array_ptr++, fl_hi);
    }
}

void memset_uint16_to_zero(std::uint16_t* __restrict__ array,  std::size_t size)
{
    std::size_t mov_sz = size / 16;
    __m256i* m256i_ptr = (__m256i*) array;
    __m256i setv = _mm256_setzero_si256();
    for(std::size_t sse_idx = 0; sse_idx != mov_sz; sse_idx++)
    {
        _mm256_stream_si256(m256i_ptr++, setv);
    }
}

void adjust_1dim(float* __restrict__ dim_array, float dim_length, const std::size_t arr_sz)
{
    __m256 m256_half_box = _mm256_set1_ps(dim_length/2.0);
    __m256 m256_half_box_neg = _mm256_set1_ps(-1.0f*dim_length/2.0);
    __m256 m256_box = _mm256_set1_ps(dim_length);


    // fix the alignment to the next aligned to 32
    unsigned long ptr_int = (unsigned long) dim_array;
    unsigned long next_aligned_index = (ptr_int + 31) & ~(31);
    unsigned long padding_size = (next_aligned_index - ptr_int) / 4;

    // calculate PBC for unaligned sectioon
    for(auto idx = 0; idx != padding_size; idx++)
    {
        dim_array[idx] -= (dim_array[idx] >= dim_length / 2.0) ? (dim_length) : 0.0;
        dim_array[idx] += (dim_array[idx] < - dim_length / 2.0) ? (dim_length) : 0.0;
    }

    // AVX2 instructions on the aligned section
    __m256* arr_ptr = (__m256*) (dim_array + padding_size);
    std::size_t sse_idx;
    for(sse_idx = 0; sse_idx != (arr_sz - padding_size) / 8; sse_idx++)
    {
        __m256 position_array = _mm256_load_ps((float*) arr_ptr);

        __m256 larger_than = _mm256_cmp_ps(position_array, m256_half_box, 0x1d); // x > box / 2 ? 0xF : 0x0
        __m256 correction_plus = _mm256_and_ps(larger_than, m256_box); // box & x > box/2
        position_array = _mm256_sub_ps(position_array, correction_plus); // x - box & x>box/2

        __m256 smaller_than = _mm256_cmp_ps(position_array, m256_half_box_neg, 0x11); // x < -box/2 ?
        correction_plus = _mm256_and_ps(smaller_than, m256_box);
        position_array = _mm256_add_ps(position_array, correction_plus); // x + (box & x < -box/2)

        _mm256_stream_ps((float*) arr_ptr++, position_array);
    }

    // leftovers
    for(std::size_t idx = padding_size + 8* sse_idx; idx < arr_sz; idx++)
    {
        dim_array[idx] -= (dim_array[idx] >= dim_length / 2.0) ? (dim_length) : 0.0;
        dim_array[idx] += (dim_array[idx] < - dim_length / 2.0) ? (dim_length) : 0.0;
    }
}

void adjust_gr(float* __restrict__ gr_ptr, float* __restrict__ r_ptr, float av_density, float dr, std::size_t array_length)
{
	// load g(r) and divide it by 4 pi r^2 dr
	__m256 m256_inv_4pi_rho_dr = _mm256_set1_ps(1.0f/(dr * 4 * 3.1415926535897 * av_density));
	__m256* m256_gr_ptr = (__m256*) gr_ptr;
	__m256* m256_r_ptr = (__m256*) r_ptr;

	for(auto sse_idx = 0; sse_idx != array_length / 8; sse_idx++)
	{
		__m256 rvec = _mm256_load_ps((float*) m256_r_ptr++);
		rvec = _mm256_mul_ps(rvec, rvec);

		__m256 gr_vec = _mm256_load_ps((float*) m256_gr_ptr);
		gr_vec = _mm256_div_ps(gr_vec, rvec);
		gr_vec = _mm256_mul_ps(gr_vec, m256_inv_4pi_rho_dr);
		_mm256_stream_ps((float*) m256_gr_ptr++, gr_vec);
	}
}

#else

void sumuint16_from_to( std::uint16_t* __restrict__ array_from,  std::uint16_t* __restrict__ array_into,  std::size_t array_sz)
{
    std::size_t mov_sz = array_sz / 8;
    __m128i* m128_ptr_fr = (__m128i*) array_from;
    __m128i* m128_ptr_to = (__m128i*) array_into;
    for(std::size_t sse_idx = 0; sse_idx != mov_sz; sse_idx++)
    {
        __m128i from = _mm_load_si128(m128_ptr_fr++);
        __m128i into = _mm_load_si128(m128_ptr_to);
        into = _mm_add_epi16(from, into);
        _mm_stream_si128(m128_ptr_to++, into);
    }
}

void sumf_from_to( float* __restrict__ array_from,  float* __restrict__ array_into,  std::size_t array_sz)
{
    std::size_t mov_sz = array_sz / 4;
    __m128* m128_ptr_fr = (__m128*) array_from;
    __m128* m128_ptr_to = (__m128*) array_into;
    for(std::size_t sse_idx = 0; sse_idx != mov_sz; sse_idx++)
    {
        __m128 from = _mm_load_ps((float*) m128_ptr_fr++);
        __m128 into = _mm_load_ps((float*) m128_ptr_to);
        into = _mm_add_ps(from, into);
        _mm_stream_ps((float*) (m128_ptr_to++), into);
    }
}


void float_div_by_int( float* __restrict__ array, std::uint16_t val, std::size_t array_sz)
{
    std::size_t mov_sz = array_sz / 4;
    __m128* m128_arr_ptr = (__m128*) array;
    __m128 m128_div = _mm_set1_ps(1.0/ (float) val);
    for(std::size_t sse_idx = 0; sse_idx != mov_sz; sse_idx++)
    {
        __m128 m128_array = _mm_load_ps((float*) m128_arr_ptr);
        m128_array = _mm_mul_ps(m128_array, m128_div);
        _mm_stream_ps((float*) m128_arr_ptr++, m128_array);
    }
}

void int_div_by_int_cast(std::uint16_t* __restrict__ array, float* __restrict__ array_return, std::uint16_t val, std::size_t array_sz)
{
    std::size_t mov_sz = array_sz/8;
    __m128* m128f_array_ptr = (__m128*) array_return;
    __m128i* m128i_arr_ptr = (__m128i*) array;
    __m128 m128_div = _mm_set1_ps(1.0f / static_cast<float>(val));

    for(std::size_t sse_idx = 0; sse_idx != mov_sz; sse_idx++)
    {
        __m128i m128_short_array = _mm_load_si128(m128i_arr_ptr++);
        __m128i m128_int_low = _mm_unpacklo_epi16(m128_short_array, _mm_set1_epi16(0));
        __m128i m128_int_high = _mm_unpackhi_epi16(m128_short_array, _mm_set1_epi16(0));
        __m128 fl_lo = _mm_cvtepi32_ps(m128_int_low);
        __m128 fl_hi = _mm_cvtepi32_ps(m128_int_high);
        fl_lo = _mm_mul_ps(fl_lo, m128_div);
        fl_hi = _mm_mul_ps(fl_hi, m128_div);
        _mm_stream_ps((float*) m128f_array_ptr++, fl_lo);
        _mm_stream_ps((float*) m128f_array_ptr++, fl_hi);
    }
}

void memset_uint16_to_zero(std::uint16_t* __restrict__ array,  std::size_t size)
{
    std::size_t mov_sz = size/8;
    __m128i* m128i_ptr = (__m128i*) array;
    __m128i setv = _mm_setzero_si128();
    for(std::size_t sse_idx = 0; sse_idx != mov_sz; sse_idx++)
    {
        _mm_stream_si128(m128i_ptr++, setv);
    }
}

// adjusts orthorhombic box dimensions to first image under convention (-0.5, 0.5). this is done with
// a = x > box / 2 ? 0b(0...0) : 0b(1...1);
// x - a & box;
// a = x < -box / 2 ? 0b(0...0) : 0b(1...1);
// x + a & box;
void adjust_1dim(float* __restrict__ dim_array, float dim_length, const std::size_t arr_sz)
{
    __m128 m128_half_box = _mm_set1_ps(dim_length/2.0f);
    __m128 m128_half_box_neg = _mm_set1_ps(-dim_length/2.0f);
    __m128 m128_box = _mm_set1_ps(dim_length);

    // fix the alignment to the next aligned to 16
    unsigned long ptr_int = (unsigned long) dim_array;
    unsigned long next_aligned_index = (ptr_int + 15) & ~(15);
    unsigned long padding_size = (next_aligned_index - ptr_int) / 4;

    // calculate PBC for unaligned sectioon
    for(auto idx = 0; idx != padding_size; idx++)
    {
	    dim_array[idx] += (dim_array[idx] < -dim_length / 2.0f) ? (dim_length) : 0.0f;
	    dim_array[idx] -= (dim_array[idx] >= dim_length / 2.0f) ? (dim_length) : 0.0f;
    }

    // use SSE instruction on aligned section
    __m128* arr_ptr = (__m128*) (dim_array + padding_size);
    std::size_t sse_idx;
    for(sse_idx = 0; sse_idx != (arr_sz - padding_size) / 4; sse_idx++)
    {
        __m128 position_array = _mm_load_ps((float*) arr_ptr);

	    __m128 smaller_than = _mm_cmplt_ps(position_array, m128_half_box_neg); // x < -box/2 ?
	    __m128 correction_plus = _mm_and_ps(smaller_than, m128_box);
	    position_array = _mm_add_ps(position_array, correction_plus); // x + box & x < -box/2

        __m128 larger_than = _mm_cmpge_ps(position_array, m128_half_box); // x >= box / 2 ? 0xF : 0x0
        correction_plus = _mm_and_ps(larger_than, m128_box); // box & 0 = 0, box & 1 = box
        position_array = _mm_sub_ps(position_array, correction_plus); // x - box & x>box/2

        _mm_stream_ps((float*) arr_ptr++, position_array);
    }
    for(std::size_t idx = padding_size+4*sse_idx; idx < arr_sz; idx++)
    {
	    dim_array[idx] += (dim_array[idx] < -dim_length / 2.0f) ? (dim_length) : 0.0f;
	    dim_array[idx] -= (dim_array[idx] >= dim_length / 2.0f) ? (dim_length) : 0.0f;
    }
}

void adjust_gr(float* __restrict__ gr_ptr, float* __restrict__ r_ptr, float av_density, float dr, std::size_t array_length)
{
	// load g(r) and divide it by 4 pi r^2 dr
	__m128 m128_inv_4pi_rho_dr = _mm_set1_ps(1.0f/(dr * 4 * 3.1415926535897 * av_density));
	__m128* m128_gr_ptr = (__m128*) gr_ptr;
	__m128* m128_r_ptr = (__m128*) r_ptr;

	for(auto sse_idx = 0; sse_idx != array_length / 4; sse_idx++)
	{
		__m128 rvec = _mm_load_ps((float*) m128_r_ptr++);
		rvec = _mm_mul_ps(rvec, rvec);

		__m128 gr_vec = _mm_load_ps((float*) m128_gr_ptr);
		gr_vec = _mm_div_ps(gr_vec, rvec);
		gr_vec = _mm_mul_ps(gr_vec, m128_inv_4pi_rho_dr);
		_mm_stream_ps((float*) m128_gr_ptr++, gr_vec);
	}
}

#endif
