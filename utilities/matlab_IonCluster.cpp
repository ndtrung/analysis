#include <mex.h>
#include <vector>
#include <string>
#include <cstdio>
#include <thread>
#include "Utility.hpp"
#include "SynchronizedIO.hpp"
#include "matrix.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    // input arguments are {DCD files}, {XML files}, [grid sizes], [dr], {particle types},
    // {update_type}, [max_sz, max_charge], transition_type = 0 (none), first_frame = 0, cutoff = 0.5, unit charge = 1, nthreads = 1

    if(nlhs > 1){mexErrMsgTxt("Unexpected number of return arguments, aborting");}

    if(nrhs > 12 || nrhs < 8){mexErrMsgTxt("Unexpected number of input arguments, aborting");}




    mxArray* pdcd_name;
    mxArray* pxml_name;
    std::size_t cellidx = 0;

    std::vector<ClusterFile> files;
    auto dims1 = *mxGetDimensions(prhs[0]);
    auto dims2 = *mxGetDimensions(prhs[1]);

    if(dims1 != dims2)
    {
        std::cout << "file list has dimensions of " << dims1 << " for trajectory and " << dims2 << " for topology " << std::endl;
        mexErrMsgTxt("Error in matching file list arguments between topology and trajectory");
    }

    std::cout << " calculating " << dims1 << " files " << std::endl;

    if(dims1 != 0) // empty cells cause segmentation faults
    {
        while (pdcd_name = mxGetCell(prhs[0], cellidx), pxml_name = mxGetCell(prhs[1], cellidx), cellidx++, pdcd_name &&
                                                                                                            pxml_name)
        {
            ClusterFile _f;
            _f.topology_filepath = std::string(mxArrayToString(pxml_name));
            _f.trajectory_filepath = std::string(mxArrayToString(pdcd_name));

            if(_f.topology_filepath.find(".dump") != std::string::npos)
            {
                _f.MD_type = LAMMPS_FILE;
            }
            else
            {
                _f.MD_type = XML_FILE;
            }

            files.push_back(_f);
        }
    }
    else
    {
        std::cout << " Warning : empty file list" << std::endl;
    }

    double* pgrid = mxGetPr(prhs[2]);
    if(pgrid[0] > static_cast<double>(UINT16_MAX) || pgrid[1] > static_cast<double>(UINT16_MAX) || pgrid[2] > static_cast<double>(UINT16_MAX))
    {
        mexErrMsgTxt("number of grid points larger than C limit on uint16_t (65535)");
    }
    if(pgrid[0] < 0.0 || pgrid[1] < 0.0 || pgrid[2] < 0.0)
    {
        mexErrMsgTxt("negative number of grid points");
    }
    std::uint16_t grid[3];
    grid[0] = static_cast<std::uint16_t>(pgrid[0]);
    grid[1] = static_cast<std::uint16_t>(pgrid[1]);
    grid[2] = static_cast<std::uint16_t>(pgrid[2]);

    double* pdr = mxGetPr(prhs[3]);
    std::size_t Ndr = *mxGetDimensions(prhs[3]);
	bool supplied_vector;
	std::vector<double> drvector;
	double max_dr_val;
	std::size_t npoints;

    if(Ndr == 2) // max_value, num_points
    {
	    supplied_vector = false;
	    max_dr_val = pdr[0];
	    npoints = static_cast<std::size_t>(pdr[1]);
    }
    else // supplied vector
    {
	    supplied_vector = true;
	    npoints = Ndr;
	    drvector.assign(pdr, pdr + Ndr);
	    max_dr_val = drvector[Ndr - 1];
    }

    std::vector<std::string> particle_types;
    mxArray* ptypes;
    cellidx = 0;
    auto ntypes = *mxGetDimensions(prhs[4]);
    if(ntypes < 1 )
    {
        mexErrMsgTxt("Empty list of  particle types supplied for clustering");
    }
    while(ptypes = mxGetCell(prhs[4], cellidx), cellidx++, ptypes)
    {
        particle_types.push_back(std::string(mxArrayToString(ptypes)));
    }

    mxArray* pupdates;
    std::vector<std::string> update_types;
    cellidx = 0;
    auto n_types_updates = *mxGetDimensions(prhs[5]); // empty cells cause segmentation errors
    if(n_types_updates > 0)
    {
        while (pupdates = mxGetCell(prhs[5], cellidx), cellidx++, pupdates)
        {
            update_types.push_back(std::string(mxArrayToString(pupdates)));
        }
    }

    double* plimits = mxGetPr(prhs[6]);
    std::uint16_t max_cluster_size = static_cast<std::uint16_t>(plimits[0]);
    std::int16_t max_cluster_charge = static_cast<std::int16_t>(plimits[1]);

    lifetime life;
    if(nrhs > 7)
    {
        life = static_cast<lifetime>(*mxGetPr(prhs[7]));
    }
    else
    {
        life = OFF;
    }

    std::size_t initial_frame = 0;
    if(nrhs > 8)
    {
        initial_frame = static_cast<std::size_t>(*mxGetPr(prhs[8]));
    }

    float cluster_cutoff = 0.5;
    if(nrhs > 9)
    {
        cluster_cutoff = static_cast<float>(*mxGetPr(prhs[9]));
    }

    float unit_charge = 1.0;
    if(nrhs > 10)
    {
        unit_charge = static_cast<float>(*mxGetPr(prhs[10]));
    }

    std::size_t nthreads = 1;
    if(nrhs > 11)
    {
        nthreads = static_cast<std::size_t>(*mxGetPr(prhs[11]));
    }
    std::cout << "system reports " << std::thread::hardware_concurrency() << " concurrent threads supported" << std::endl;

    if(nthreads > std::thread::hardware_concurrency() && !std::thread::hardware_concurrency())
    {
        mexErrMsgTxt("Using more threads than system reports as concurrently usable, aborting");
    }

    // set all options into structure
    cluster_options opts;
    opts.clustering_cutoff = cluster_cutoff;
	opts.max_r_value_gofr_calculation = static_cast<float>(max_dr_val);
	opts.nbins_gofr_calculation = npoints;
    //opts.dr_all_vector = std::vector<float>(drvector.begin(), drvector.end());
    //opts.dr_charge_vector = std::vector<float>(drvector.begin(), drvector.end());
    //opts.dr_sz_vector = std::vector<float>(drvector.begin(), drvector.end());
    opts.first_frame = initial_frame;
    for(auto idx = 0; idx != 3; idx++)
    {
        opts.grid_size[idx] = grid[idx];
    }
    opts.ID = std::string(" ");
    opts.ions_types = particle_types;
    opts.update_on_coord_change_type = update_types;
    opts.transition_matrix = life;
    opts.unit_charge = unit_charge;
    opts.max_cluster_charge = max_cluster_charge;
    opts.max_cluster_size = max_cluster_size;

    //! output values to screen

    std::cout << "batch multithread implementation of clustering code using " << nthreads << " threads" << std::endl;
    std::cout << "Running with following options " << std::endl;
    std::cout << "initial DCD frame number = " << initial_frame << std::endl;
    std::cout << "clustering cutoff  = " << cluster_cutoff << std::endl;
    std::cout << "gridding clusters on " << grid[0] << " X " << grid[1] << " X " << grid[2] << " points" << std::endl;
    std::cout << "limits on size = " << max_cluster_size <<  " and on charge (absolute value)  = " << max_cluster_charge << std::endl;
    std::cout << "------------------------" << std::endl;


    //OutputStack<IonClusters>* outptr = (OutputStack<IonClusters>*) mxMalloc(sizeof(OutputStack<IonClusters>));//new OutputStack<IonClusters>;
    std::vector<IonClusters> results;
    for(std::size_t fileidx = 0; fileidx != files.size(); fileidx++)
    {
        std::cout << "Running on file : " << std::endl;
        std::cout << "trajectory : " << files[fileidx].trajectory_filepath << std::endl;
        std::cout << "topology : " << files[fileidx].topology_filepath << std::endl;


        opts.ID = files[fileidx].topology_filepath;
        batch_cluster_handle* handle = new batch_cluster_handle(files[fileidx], nthreads, opts, &results); //outptr);
        std::cout << "handle pointer " << handle << std::endl;
        if(!handle->valid_dr_sz)
        {
            mexErrMsgTxt("violated alignment requirements of dr vector (size%8 = 0) ");
        }
        if(!handle->valid_grid_sz)
        {
            mexErrMsgTxt("violated alignment requirements of grid sizes (size%16 = 0)");
        }

        if(!handle->valid_files)
        {
            std::cout << "handle reported error # " << handle->last_error_code << std::endl;
            mexErrMsgTxt("DCDHandle returned invalid files in batch handle");
        }
        std::cout << "batch object created, starting clustering" << std::endl;
        std::cout << "------------------------" << std::endl;
        while (!handle->memory_load_frames())
        {
            std::cout << "current file at " << std::endl;
            std::cout << "# of frames for current average : " << handle->get_current_nframe_avg() << std::endl;
            std::cout << "current frame position in trajectory : " << handle->get_current_traj_frame() << std::endl;
        }
        std::cout << " destroying batch handle" << std::endl;
        delete handle;
        //handle = nullptr;
    }

    std::cout << "Calculations finished" << std::endl;


    //std::vector<IonClusters> results = outptr->get_results();
    std::cout << "creating output structure of size : " << results.size() << std::endl;
    //delete outptr;



    const char* fields[9];
    for(auto i = 0; i != 9; i++)
    {
        fields[i] = (char*) mxMalloc(20);
    }
    memcpy((void*) fields[0], "ID", sizeof("ID"));
    memcpy((void*) fields[1], "update_pos", sizeof("update_pos"));

    memcpy((void*) fields[2], "dr", sizeof("dr"));
    memcpy((void*) fields[3], "gr_all", sizeof("gr_all"));
    memcpy((void*) fields[4], "gr_charge", sizeof("gr_charge"));
    memcpy((void*) fields[5], "gr_size", sizeof("gr_size"));

    memcpy((void*) fields[6], "grid points", sizeof("grid points"));
    memcpy((void*) fields[7], "grid_charge", sizeof("grid_charge"));
    memcpy((void*) fields[8], "grid_size", sizeof("grid_size"));

    plhs[0] = mxCreateStructMatrix(results.size(), 1, 9, fields);

    for(auto i = 1; i != 9; i++)
    {
        mxFree((char*) fields[i]);
    }

    std::cout << "moving values from results into structure " << std::endl;

    for(auto i = 0; i != results.size(); i++)
    {
        mxArray* id = mxCreateString(results[i].ID.c_str());

        mxArray* upt_p = mxCreateDoubleMatrix(1, results[i].update_coord_values.size(), mxREAL);
        double* pupt_p = mxGetPr(upt_p);
        for(auto idx = 0; idx != results[i].update_coord_values.size(); idx++)
        {
            pupt_p[idx] = static_cast<double>(results[i].update_coord_values[idx]);
        }
        std::cout << " moving r, g(r) " << std::endl;
        mxArray* dr = mxCreateDoubleMatrix(1, results[i].dr_all.size(), mxREAL);
        double* pdr = mxGetPr(dr);
        mxArray* gr_al = mxCreateDoubleMatrix(1, results[i].gr_all.size(), mxREAL);
        double* pgr_al = mxGetPr(gr_al);

        mxArray* gr_ch;
        double* pgr_ch;
        if(results[i].gr_charge.size() > 0)
        {
            gr_ch = mxCreateDoubleMatrix(1, results[i].gr_charge[0].size() * results[i].gr_charge.size(), mxREAL);
            pgr_ch = mxGetPr(gr_ch);
        }
        else
        {
            gr_ch = mxCreateDoubleMatrix(1,1,mxREAL);
            pgr_ch = nullptr;
        }

        mxArray* gr_sz;
        double* pgr_sz;
        if(results[i].gr_size.size() > 0)
        {
            gr_sz = mxCreateDoubleMatrix(1, results[i].gr_size[0].size() * results[i].gr_size.size(), mxREAL);
            pgr_sz = mxGetPr(gr_sz);
        }
        else
        {
            gr_sz = mxCreateDoubleMatrix(1,1,mxREAL);
            pgr_sz = nullptr;
        }


        std::cout << " moving dr, gr all " << std::endl;
        for(auto ii = 0; ii != results[i].dr_all.size(); ii++)
        {
            pdr[ii] = static_cast<double>(results[i].dr_all[ii]);
            pgr_al[ii] = static_cast<double>(results[i].gr_all[ii]);
        }

        std::cout << "moving charge" << std::endl;
        if(results[i].gr_charge.size() > 0 && pgr_ch)
        {
            for (auto dr_idx = 0; dr_idx != results[i].gr_charge.size(); dr_idx++)
            {
                for (auto ii = 0; ii != results[i].gr_charge[dr_idx].size(); ii++)
                {
                    if(results[i].gr_charge[dr_idx].size() != results[i].gr_charge[0].size())
                    {
                        mexErrMsgTxt("Incompatible sizes in g(r) vectors [charges]");
                    }
                    pgr_ch[ii + dr_idx * results[i].gr_charge[0].size()]
                            = static_cast<double>(results[i].gr_charge[dr_idx][ii]);
                }
            }
        }

        std::cout << "moving size " << std::endl;
        if(results[i].gr_size.size() > 0 && pgr_sz)
        {
            for (auto dr_idx = 0; dr_idx != results[i].gr_size.size(); dr_idx++)
            {
                for (auto ii = 0; ii != results[i].gr_size[dr_idx].size(); ii++)
                {
                    if(results[i].gr_size[0].size() != results[i].gr_size[dr_idx].size())
                    {
                        mexErrMsgTxt("Incompatible sizes in g(r) vector [size]");
                    }
                    pgr_sz[ii + dr_idx * results[i].gr_size.size()]
                            = static_cast<double>(results[i].gr_size[dr_idx][ii]);
                }
            }
        }
        std::cout << "moving grid " << std::endl;
        mxArray* gridpts = mxCreateDoubleMatrix(results[i].grid_pts_charge.size(), 3, mxREAL);
        double* pgridpts = mxGetPr(gridpts);

        std::size_t grid_sz = grid[0] * grid[1] * grid[2];
        for(auto ii = 0; ii != 3; ii++)
        {
            for(auto grididx = 0; grididx != results[i].grid_pts_charge.size(); grididx++)
            {
                pgridpts[ii + grididx * 3] = static_cast<double>(results[i].grid_pts_charge[grididx][ii]);
            }
        }
        std::cout << " creating grid_charge & grid_size, " << grid_sz << " points per grid, numel(charge) : "
                  << results[i].grid_values_charge.size() * grid_sz << "numel(size) : " << results[i].grid_values_size.size() * grid_sz << std::endl;
        mxArray* gridch = mxCreateDoubleMatrix(1, results[i].grid_values_charge.size() * grid_sz, mxREAL);
        double *pgridch = mxGetPr(gridch);
        mxArray* gridsz = mxCreateDoubleMatrix(1, results[i].grid_values_size.size() * grid_sz, mxREAL);
        double* pgridsz = mxGetPr(gridsz);

        std::cout << " assinging values" << std::endl;
        for(auto grid_idx = 0; grid_idx!= results[i].grid_values_charge.size(); grid_idx++)
        {
            if(results[i].grid_values_charge[grid_idx].size() != grid_sz)
            {
                mexErrMsgTxt("Invalid number of grid points obtained from calculator [charge]");
            }
            for(auto iidx = 0; iidx != grid_sz; iidx++)
            {
                pgridch[iidx + grid_idx * grid_sz] = static_cast<double>(results[i].grid_values_charge[grid_idx][iidx]);
            }
        }

        for(auto grid_idx = 0; grid_idx != results[i].grid_values_size.size(); grid_idx++)
        {
            if(results[i].grid_values_size[grid_idx].size() != grid_sz)
            {
                mexErrMsgTxt("invalid number of grid points produced by calculator [size]");
            }
            for(auto iidx = 0; iidx != grid_sz; iidx++)
            {
                pgridsz[iidx + grid_idx * grid_sz] = static_cast<double>(results[i].grid_values_size[grid_idx][iidx]);
            }
        }

        mxSetFieldByNumber(plhs[0], i, 0, id);
        mxSetFieldByNumber(plhs[0], i, 1, upt_p);
        mxSetFieldByNumber(plhs[0], i, 2, dr);
        mxSetFieldByNumber(plhs[0], i, 3, gr_al);
        mxSetFieldByNumber(plhs[0], i, 4, gr_ch);
        mxSetFieldByNumber(plhs[0], i, 5, gr_sz);
        mxSetFieldByNumber(plhs[0], i, 6, gridpts);
        mxSetFieldByNumber(plhs[0], i, 7, gridch);
        mxSetFieldByNumber(plhs[0], i, 8, gridsz);
    }
}
