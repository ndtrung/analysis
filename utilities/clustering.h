#ifndef __CLUSTERING
#define __CLUSTERING

#include <vector>
#include <string>
#include <array>
#include "buildingblocks.h"

namespace Clusters {

    enum MAX_VALUES_BEHAVIOR
    {
	    CLAMP_ALL = 0,
	    IGNORE_ALL,
	    CLAMP_SIZE,
	    CLAMP_CHARGE
    };

    class Clustering
    {
    public:
        Clustering(float _cutoff, int _periodic_wrapped=1, int _nNumBeadsInParticle=1,
                   int _threshold=5, int _write_clusters=0, int _bin_size=5,
                   std::uint32_t _nparticles = 0, int _nx=1, int _ny=1, int _nz=1);
        ~Clustering();

        int find_clusters(vector<Cluster>& clusterList);

        ///! Bead list for clustering
        //std::vector<Bead> beadList;



        ///! functions for batch handles
        void set_offsets(std::size_t offset, std::size_t offset_prev)
        {
            working_offset = offset;
            previous_offset = offset_prev;
        }

        static void wrapper_execute(Clustering* obj_ptr, std::size_t thread_index)
        {
            if(obj_ptr)
            {
                obj_ptr->execute();
            }
            else
            {
                std::cout << "executing wrapper got null pointer " << std::endl;
                return;
            }
//#ifdef DEBUGGING
            std::cout << "exiting thread index " << thread_index << " using offset " << obj_ptr->working_offset << std::endl;
//#endif
        }

        void execute();

        void set_buffers(std::uint16_t* __restrict__ pgrid_charge, std::size_t grid_charge_sz,
                         std::uint16_t* __restrict__ pgrid_sz, std::size_t grid_sz_sz,
                         float* __restrict__ pdr, std::size_t _dr_sz,
                         float* __restrict__ pgr_sz, std::size_t gr_sz_sz,
                         float* __restrict__ pgr_charge, std::size_t gr_charge_sz,
                         float* __restrict__ pgr_all, std::size_t gr_all_sz,
                         float* __restrict__ coord_workspace, std::size_t coord_size,
                         float* __restrict__ box_workspace, bool* __restrict__ _use_index, std::int16_t* __restrict__ _charge_index);

        void set_extra_buffers(std::uint16_t* __restrict__ p_tr_mat_sz, std::size_t tr_mat_sz_sz,
                               std::uint16_t* __restrict__ p_tr_mat_charge, std::size_t tr_mat_charge_sz,
                                int matrix_type);

        void set_limits(std::int16_t mcharge, std::uint16_t msize) {max_charge = mcharge; max_size = msize;}

    protected:

        void make_cluster(int i, Cluster& cluster, int nNumBeadsInParticle);

        void arrange(std::vector<Particle>& particleList, Cluster& cluster, int nNumBeadsPerParticle);


        ///! cluster analysis

        float asphericity_param(Cluster& cluster, float* major_eigenvector, float* R2, float& Rg, float* xm);

        int binning(float xm, float ym, float zm);

        void center_of_mass(std::vector<float>* c, float* xm, int period_wrapped);

        void calculate_gofr();

	    void calculate_sofq();

        ///! cluster shapes
        void rotate(float **matrix, int i, int j, int k, int l, float s, float tau);

        int diagonalize(float **matrix, float *evalues, float **evectors);

        void gyration_tensor(std::vector<float>* c, float* xm, int periodic_wrapped, float** gyration, float* R2, float* major_eigenvector);

        ///! create neighbor lists
        void create_cell_list();

	    void create_cell_list_gofr();

        void neighboring();

	    void neighboring_gofr();

	    void gofr_idx1_idx2(std::size_t idx_i, std::size_t idx_j, float rij);

	    static inline std::size_t upper_triangular_ij_to_lin(std::size_t, std::size_t, std::size_t);
    protected:

        float cutoff;            //! Maximum distance between two beads to be in a cluster
        int periodic_wrapped;     //! 1 if the clusters are wrapped by the PBC
        int nNumBeadsInParticle;  //! Number of beads per particle/molecule
        int threshold;            //! Exclude clusters with fewer than threshold particles
        int write_clusters;       //! 1 if the clusters are written to files, 0 otherwise
        int bin_size;             //! Bin size for the cluster size histogram (number of beads)
        std::uint32_t nparticles; //! real number of particles in file
        int nNumTotalBeads;       //! Number of beads per snapshot for clustering
        //float Lx, Ly, Lz;        //! Box dimensions / replaced by float* box
        //int timestep;             //! Time step read from the config file

        int *grid = nullptr;                //! grid for spatial binning
        int nx, ny, nz;           //! Number of bins in x, y and z directions
        int nbins;
        int spatial_binning;      //! 1 if do the spatial binning, 0 otherwise

        int file_type;            //! Config file type: XYZ, DUMP (LAMMPS) or XML (HOOMD)
        string base, ext;         //! Config files are in the form: base.ext
        int max_neigh;
	    int g_max_neigh;
        //! maximum values for cluster charge / size for statistics
        std::int16_t max_charge = 0;
        std::uint16_t max_size = 0;
		MAX_VALUES_BEHAVIOR CLUSTER_MAX_VALUES_BEHAVIOR = CLAMP_ALL;

        //! buffer addresses

        std::size_t working_offset = 0; //! current working offset set by execute(offset)
        std::size_t previous_offset = 0;

        std::uint16_t* __attribute__((aligned(32))) buf_grid_charge;
        std::size_t grid_charge_sz;

        std::uint16_t* __attribute__((aligned(32))) buf_grid_sz;
        std::size_t grid_sz_sz;

        float* __attribute__((aligned(32))) buf_dr;
        std::size_t dr_sz;

        float* __attribute__((aligned(32))) buf_gr_charge;
        std::size_t gr_charge_sz;

        float* __attribute__((aligned(32))) buf_gr_sz;
        std::size_t gr_sz_sz;

        float* __attribute__((aligned(32))) buf_gr_all;
        std::size_t gr_all_sz;

        float* coordinates;
        std::size_t coordinates_sz;

	    float* __attribute__((aligned(32))) buf_dq; //! q vectors
	    float* __attribute__((aligned(32))) buf_sofq; //! results
	    std::size_t dq_sz;

        float* box;

        bool* use_idx = nullptr;
        std::int16_t* charge_idx;
        bool* visited = nullptr;
        std::uint16_t* cluster_size_array = nullptr;

        std::vector<Particle> particleList;

        //! extra buffers for transition matrix
        std::uint16_t* __attribute__((aligned(32))) buf_tr_mat_sz = nullptr;
        std::size_t tr_mat_sz_sz;

        std::uint16_t* __attribute__((aligned(32))) buf_tr_mat_charge = nullptr;
        std::size_t tr_mat_charge_sz;

        int matrix_type = 0;
        ///! Neighbor list info for other accesses
        int** neighbors = nullptr;   ///! neighbors[i][j] gives the index of the jth neighbor of bead i
        int* num_neigh = nullptr;    ///! num_neigh[i] gives the number of neighbors of bead i

        //! current clusters
        std::vector<Cluster> clusterList;

        //! cell data to avoid multiple creation / destructions
        int** cells = nullptr;
        int** neighbor_cells = nullptr;
        int* num_neighbor_cells = nullptr;
        int* num_beads_in_cell = nullptr;
        int ncellx = 0, ncelly = 0, ncellz = 0, ncellxy = 0, ncells = 0;

        //! cell data for g(r) calculation
	    bool* particle_visited = nullptr;
	    std::size_t size_particle_visited = 0;
        float g_cutoff = 0.0f;
        int** g_cells = nullptr;
        int** g_neighbor_cells = nullptr;
        int* g_num_neighbor_cells = nullptr;
        int* g_num_beads_in_cell = nullptr;

	    int** g_neighbors = nullptr;
	    std::uint32_t* g_num_neigh = nullptr;
	    std::size_t sz_g_num_neigh = 0;

        int g_ncellx = 0, g_ncelly = 0, g_ncellz = 0, g_ncellxy = 0, g_ncells = 0;
    };

} // namespace Clusters

#endif
