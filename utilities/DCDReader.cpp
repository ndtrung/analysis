//
// Created by martin on 10/19/16.
//

#include "DCDReader.hpp"
#include <cstdint>
#include <iostream>
#include <cstdio>
#include <cmath>

bool DCDHandle::is_reading()
{
    if(!ptr_file)
    {
        return false;
    }
}

int DCDHandle::open_DCD(const char *filename)
{
    // if already reading something, close it
    if(ptr_file){ std::fclose(ptr_file);}

    ptr_file = std::fopen(filename, "rb");
    // unable to open file, fopen returned NULL
    if(!ptr_file)
    {
        return -1;
    }

    // unable to seek end of file
    if (!std::fseek(ptr_file, 0, SEEK_END))
    {
        filesize = std::ftell(ptr_file);
        if (std::fseek(ptr_file, 0, SEEK_SET)) { // unable to go back to beginning, fseek returns nonzero
            std::fclose(ptr_file);
            return -2;
        }
    }
    else
    {
        std::fclose(ptr_file);
        return -2;
    }

    std::size_t byteread;
    bool read_error = false;
    byteread = std::fread(&head.blocksize1, sizeof(head.blocksize1), 1, ptr_file);
    read_error = read_error || byteread != 1;

    if(head.blocksize1 != 84) // bad magic number, one should try different endianess here
    {
        std::fclose(ptr_file);
        return -4;
    }

    byteread = std::fread(&head.hdr, sizeof(char), 4, ptr_file);
    read_error = read_error || byteread != 4;

    byteread = std::fread(&head.nset, sizeof(head.nset), 1, ptr_file);
    read_error = read_error || byteread != 1;

    byteread = std::fread(&head.istart, sizeof(head.istart), 1, ptr_file);
    read_error = read_error || byteread != 1;

    byteread = std::fread(&head.nsavc, sizeof(head.nsavc), 1, ptr_file);
    read_error = read_error || byteread != 1;

    byteread = std::fread(&head.nstep, sizeof(head.nstep), 1, ptr_file);
    read_error = read_error || byteread != 1;

    byteread = std::fread(&head.null4, sizeof(std::int32_t), 4, ptr_file);
    read_error = read_error || byteread != 4 ;

    byteread = std::fread(&head.nfreat, sizeof(head.nfreat), 1, ptr_file);
    read_error = read_error || byteread != 1;

    float buf;
    byteread = std::fread(&buf, sizeof(buf), 1, ptr_file);
    read_error = read_error || byteread != 1;
    head.delta = (double) buf;

    byteread = std::fread(&head.null9, sizeof(std::int32_t), 9, ptr_file);
    read_error = read_error || byteread != 9;

    byteread = std::fread(&head.version, sizeof(head.version), 1, ptr_file);
    read_error = read_error || byteread != 1;

    if(read_error)
    {
        return -5;
    }


    // ensure version of DCD file is correct
    if(head.version > 0)
    {
        head.ischarmm = true;
    }
    else
    {
        long cof = std::ftell(ptr_file);
        if(!std::fseek(ptr_file, 44, SEEK_SET))
        {
            std::fclose(ptr_file);
            return -2;
        }
        byteread = std::fread(&head.delta, sizeof(head.delta), 1, ptr_file);
        read_error = read_error || byteread != 1;
        if(!std::fseek(ptr_file, cof, SEEK_SET))
        {
            std::fclose(ptr_file);
            return -2;
        }
    }

    if(head.ischarmm)
    {
        long cof = std::ftell(ptr_file);
        std::fseek(ptr_file, 48, SEEK_SET);
        std::int32_t n;
        std::fread(&n, sizeof(n), 1, ptr_file);
        if(n == 1){head.ischarmm_xtra_block = true;}

        std::fseek(ptr_file, 52, SEEK_SET);
        std::fread(&n, sizeof(n), 1, ptr_file);
        if(n == 1){head.ischarmm_4dims = true;}

        std::fseek(ptr_file, cof, SEEK_SET);
    }

    std::int32_t blocksize1;
    std::fread(&blocksize1, sizeof(blocksize1), 1, ptr_file);
    if(blocksize1 != head.blocksize1){fclose(ptr_file); return -5;}
    std::fread(&head.blocksize2, sizeof(head.blocksize2), 1, ptr_file);
    std::fread(&head.ntitle, sizeof(head.ntitle), 1, ptr_file);
    head.title = (char *) std::malloc(head.ntitle*80 * sizeof(char));
    std::fread(head.title, sizeof(char), 80 * head.ntitle * sizeof(char), ptr_file);
    std::int32_t blocksize2;
    std::fread(&blocksize2, sizeof(blocksize2), 1, ptr_file);
    if(blocksize2 != head.blocksize2){fclose(ptr_file); return -5;}


    std::fread(&head.blocksize3, sizeof(head.blocksize3), 1, ptr_file);
    std::fread(&n_particles, sizeof(n_particles), 1, ptr_file);
    std::int32_t blocksize3;
    std::fread(&blocksize3, sizeof(blocksize3), 1, ptr_file);
    if(head.blocksize3 != blocksize3){fclose(ptr_file); return -5;}

    headersize = std::ftell(ptr_file);

    extra_block_size = head.ischarmm_xtra_block ? 4*2 + 8*6 : 0;
    coord_block_size = (4*2 + 4*n_particles)*3;
    nframes = (filesize - headersize) / (extra_block_size + coord_block_size);
    current_frame = 0;

    return 0;
}

int DCDHandle::get_frame(float *box, float *x, float *y, float *z)
{
    std::size_t objcount;
    if(current_frame == nframes)
    {
        return -1;
    }

    if(!ptr_file)
    {return -2;}
    long cof = std::ftell(ptr_file);
    std::int32_t blksiz;
    if(head.ischarmm_xtra_block)
    {
        double dummy[6];
        std::fread(&blksiz, sizeof(blksiz), 1, ptr_file);
        std::fread(&dummy, sizeof(double), 6, ptr_file);
        std::fread(&blksiz, sizeof(blksiz), 1, ptr_file);
        float dummy2[6];

        for(int idx = 0; idx < 6; idx ++)
        {
            dummy2[idx] = (float) dummy[idx];
        }

        box[0] = dummy2[0]; box[1] = dummy2[2]; box[2] = dummy2[5];
        box[3] = std::acos(dummy2[4]); box[4] = std::acos(dummy2[3]); box[5] = std::acos(dummy2[1]); //charmm to XYZ cos a, cos b, cos g
    }

    // read x
    std::fread(&blksiz, sizeof(blksiz), 1, ptr_file);
    if(blksiz/4 != (std::int32_t)n_particles)
    {
        return -3;
    }
    std::fread(x, sizeof(float), blksiz/4, ptr_file);
    std::fread(&blksiz, sizeof(blksiz), 1, ptr_file);

    // read y
    std::fread(&blksiz, sizeof(blksiz), 1, ptr_file);
    if(blksiz/4 != (std::int32_t)n_particles)
    {
        return -3;
    }
    std::fread(y, sizeof(float), blksiz/4, ptr_file);
    std::fread(&blksiz, sizeof(blksiz), 1, ptr_file);

    // read z
    std::fread(&blksiz, sizeof(blksiz), 1, ptr_file);
    if(blksiz/4 != (std::int32_t)n_particles)
    {
        return -3;
    }
    std::fread(z, sizeof(float), blksiz/4, ptr_file);
    std::fread(&blksiz, sizeof(blksiz), 1, ptr_file);

    if(head.ischarmm_4dims) // skip this stuff
    {
        std::fread(&blksiz, sizeof(blksiz), 1 ,ptr_file);
        std::fseek(ptr_file, (long) blksiz, SEEK_CUR);
        std::fread(&blksiz, sizeof(blksiz), 1, ptr_file);
    }
    if(std::ferror(ptr_file) != 0)
    {
        return -4;
    }

    framesize = std::ftell(ptr_file) - cof ;
    current_frame++;
    
    return 0;
}

int DCDHandle::goto_frame(std::size_t frame)
{
    if(!ptr_file || frame > nframes)
    {
        return -1;
    }
    long offset = framesize * frame + headersize;
    if(offset > filesize)
    {
        return -2;
    }

    std::fseek(ptr_file, offset, SEEK_SET);
    current_frame = static_cast<int>(frame);
}