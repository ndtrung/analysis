//
// Created by martin on 7/15/16.
//

#ifndef CLIBS_UTILITY_HPP
#define CLIBS_UTILITY_HPP

#include <vector>
#include <cstdlib>
#include <memory>
#include <array>
#include <numeric>
#include <algorithm>
#include <mutex>
#include <queue>

template <typename T>
std::vector<std::size_t> argsort(const std::vector<T> &v)
{
    std::vector<std::size_t> idx(v.size());
    std::iota(idx.begin(), idx.end(), 0);

    std::sort(idx.begin(), idx.end(), [&v](std::size_t idx1, std::size_t idx2){return v[idx1]<v[idx2];});
    return idx;
}

template <typename T>
T Angle(const std::vector<T> v1, const std::vector<T> v2)
{
    T inner = std::inner_product(v1.begin(), v1.end(), v2.begin(), 0.0);
    T norm1 = std::inner_product(v1.begin(), v1.end(), v1.begin(), 0.0);
    T norm2 = std::inner_product(v2.begin(), v2.end(), v2.begin(), 0.0);
    return acos(inner / std::sqrt(norm1 * norm2));
}

template <typename T, const std::size_t N>
T Angle(const std::array<T, N> v1, const std::array<T, N> v2)
{
    T inner = std::inner_product(v1.begin(), v1.end(), v2.begin(), 0.0);
    T norm1 = std::inner_product(v1.begin(), v1.end(), v1.begin(), 0.0);
    T norm2 = std::inner_product(v2.begin(), v2.end(), v2.begin(), 0.0);
    return acos(inner / std::sqrt(norm1 * norm2));
}

template<typename T>
class synchronized_queue
{
    std::queue<T> q;
    std::mutex mtx;

public:

    std::size_t size()
    {
        std::unique_lock<std::mutex> lck(mtx);
        return q.size();
    }

    void push(T elem)
    {
        std::unique_lock<std::mutex> lck(mtx);
        q.push(elem);
    }

    T pop()
    {
        std::unique_lock<std::mutex> lck(mtx);
        T elem = q.front();
        q.pop();
        return elem;
    }

    void pop(T* &T_ptr)
    {
        std::unique_lock<std::mutex> lck(mtx);
        if(q.size()==0)
        {
            std::free(T_ptr);
            T_ptr = nullptr;
            return;
        }
        T elem = q.front();
        q.pop();
        *T_ptr = elem;
    }

    void pop(std::unique_ptr<T> &T_ptr)
    {
        std::unique_lock<std::mutex> lck(mtx);
        if(q.size() == 0)
        {
            T_ptr.release();
            return;
        }
        T elem = q.front();
        q.pop();
        *T_ptr = elem;
    }

    bool pop(std::shared_ptr<T> T_ptr)
    {
        std::unique_lock<std::mutex> lck(mtx);
        if(q.size() == 0)
        {
            return false;
        }
        T elem = q.front();
        q.pop();
        *T_ptr = elem;
        return true;
    }
};

template<typename T>
class OutputStack
{
    std::vector<T> outputs;
    std::mutex mtx;
public:
    void push(T elem)
    {
        std::unique_lock<std::mutex> lck(mtx);
        outputs.push_back(elem);
    }
    std::vector<T> get_results()
    {
        std::unique_lock<std::mutex> lck(mtx);
        return outputs;
    }
};


#endif //CLIBS_UTILITY_HPP
