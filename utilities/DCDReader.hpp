#ifndef IONSCLUSTERS_DCDREADER_HPP
#define IONSCLUSTERS_DCDREADER_HPP
#include <cstdint>
#include <iostream>
struct DCDHeader
{
    bool ischarmm;
    bool ischarmm_xtra_block;
    bool ischarmm_4dims;
    bool double_delta;
    std::int32_t blocksize1, blocksize2, blocksize3, nset, istart, nsavc, nstep, null4[4],
            nfreat, null9[9], version, ntitle;
    double delta;
    char* title;
    char hdr[4];
};

class DCDHandle
{
    std::int32_t n_particles;
    long filesize, headersize, framesize;
    DCDHeader head;
    bool head_read = false;
    FILE* ptr_file = nullptr;
    int extra_block_size, coord_block_size, current_frame, nframes;

public:
    long get_frame_size(){return framesize;}
    int get_current_frame(){return current_frame;}
    int open_DCD(const char* filename);
    int get_frame(float* box, float* x, float* y, float* z);
    int goto_frame(std::size_t frame);
    DCDHeader get_head(){return head;}
    std::size_t get_natoms(){return (std::size_t) n_particles;}
    long fptr_tell(){return std::ftell(ptr_file);}
    std::size_t get_nframes(){return (std::size_t) nframes;}
    bool is_reading();
    int close_dcd(){return std::fclose(ptr_file);}
    ~DCDHandle()
    {
        if(!ptr_file){close_dcd();}
    }
};

#endif //IONSCLUSTERS_DCDREADER_HPP
