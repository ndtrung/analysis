
#ifndef IONSCLUSTERS_SYNCHRONIZEDIO_HPP
#define IONSCLUSTERS_SYNCHRONIZEDIO_HPP

#include "gsd.h"
#include "DCDReader.hpp"
#include "XMLParser.hpp"
#include "dumpParser.hpp"
#include "Utility.hpp"
#include <cstdint>
#include "clustering.h"

enum file_type
{
    GSD_FILE, // topology & trajectory from GSD file
    XML_FILE, // hoomd XML + DCD
    LAMMPS_FILE, // lammps topology + DCD
    XYZ_FILE, // topology from XYZ, trajectory from DCD
    LAMMPSTRJ_FILE // topology & trajectory from plain text lammps trj
};

enum lifetime
{
    OFF=0,
    AVERAGE=1,
    SPATIAL=2
};

enum MODE_TRACKED_POSITION
{
	PUSH_NEW_GRIDS = 0,
	EXIT_FILE = 1
};

struct ClusterFile
{
    std::string trajectory_filepath;
    std::string topology_filepath;
    file_type MD_type;
};


struct cluster_options
{
    std::string ID;
    std::vector<std::string> ions_types;
    std::uint32_t first_frame;
    std::uint16_t grid_size[3];

    float clustering_cutoff;

	float max_r_value_gofr_calculation = 0.0;
	std::uint32_t nbins_gofr_calculation = 0;

    /*
	std::vector<float> dr_sz_vector;
    std::vector<float> dr_charge_vector;
    std::vector<float> dr_all_vector;
     */

    float unit_charge;

    lifetime transition_matrix;

    std::int16_t max_cluster_charge;
    std::uint16_t max_cluster_size;

	MODE_TRACKED_POSITION update_coord_type = EXIT_FILE;
    std::vector<std::string> update_on_coord_change_type;
    std::vector<std::string> additional_gofr_types;
};

struct IonClusters
{
    std::string ID;
    std::vector<float> update_coord_values;

    std::vector<float> dr_all;
    std::vector<float> gr_all;

    std::uint16_t max_cluster_size;
    std::uint16_t max_cluster_charge;

    std::vector<float> dr_size;
    std::vector< std::vector<float> > gr_size;

    std::vector<float> dr_charge;
    std::vector< std::vector<float> > gr_charge;

    std::vector< std::array<float, 3> > grid_pts_charge;
    std::vector< std::vector<float> > grid_values_charge;

    std::vector< std::array<float, 3> > grid_pts_size;
    std::vector< std::vector<float> > grid_values_size;
};

struct constant_simulation_values
{
    std::uint32_t n_frames;
    std::uint32_t n_particles;
    float box[6];
};


class batch_cluster_handle
{
    //! Base MD values
    file_type MD_type;
    std::string file_identifier;
    float* box_workspace = nullptr;
    std::size_t unit_box_workspace_sz = 6;

    float* coord_workspace = nullptr;
    std::size_t unit_coord_workspace_sz;

    uint32_t* frame_number = nullptr; // current frame # for averaging. Can be used to avoid calculation of lifetime at frame #0
    std::size_t unit_frame_workspace_sz = 1;
    std::size_t prod_grid_size;

    //! clamp values of clusters for charge / size
    std::int16_t max_cluster_charge;
    std::uint16_t max_cluster_size;

    //! workspace for workers, grid_size[0] * grid_size[1] * grid_size[2] * (2 * max_cluster_charge + 1)
    // this is the number of clusters in each grid for each cluster charge
    std::uint16_t* grid_charge_workspace = nullptr;
    std::size_t unit_grid_charge_workspace_sz;
    std::uint16_t* grid_charge_av = nullptr;

    //! workspace for number of cluster in each grid pt for each cluster size prod(grid_size) * (max_cluster_size)
    std::uint16_t* grid_sz_workspace = nullptr;
    std::size_t unit_grid_sz_workspace_sz;
    std::uint16_t* grid_sz_av = nullptr;

    //! workspace for g(r) for pairs of clusters sizes (dr grid size * max_cluster_size * (max_custer_size - 1)  / 2 )
    //float* dr_sz_workspace = nullptr;
    //std::size_t unit_dr_sz_sz = 0;
    float* gr_sz_workspace = nullptr;
    float* gr_sz_av = nullptr;
    std::size_t unit_gr_sz_workspace_sz;

    //! workspace for g(r) for pairs of cluster charges(dr grid size * (2*max_cluster_charge+1)*max_cluster_charge
    //float* dr_charge_workspace = nullptr;
    //std::size_t unit_dr_charge_sz = 0;
    float* gr_charge_workspace = nullptr;
    float* gr_charge_av = nullptr;
    std::size_t unit_gr_charge_workspace_sz;


    //! workspace for g(r) of all clusters
    float* dr_all_workspace = nullptr;
    std::size_t unit_dr_all_sz;
    float* gr_all_workspace = nullptr;
    float* gr_all_av = nullptr;
    std::size_t unit_gr_all_workspace_sz;


    lifetime transition_matrix_type;
    //! workspace for transition matrix, by size and charge
    std::uint16_t* tr_matrix_sz_workspace = nullptr;
    std::uint16_t* tr_matrix_sz_av = nullptr;
    std::size_t tr_matrix_sz_sz; // size of all the matrixes
    std::size_t tr_matrix_sz_spatial_sz; // size of each spatial matrix

    std::uint16_t* tr_matrix_charge_workspace = nullptr;
    std::uint16_t* tr_matrix_charge_av = nullptr;
    std::size_t tr_matrix_charge_sz;
    std::size_t tr_matrix_charge_spatial_sz;

    //! handles for IO
    DCDHandle* handle_dcd = nullptr;
    HOOMD_XML_parser* handle_xml = nullptr;
    gsd_handle* handle_gsd = nullptr;
    dumpParser* handle_lammps = nullptr;

    constant_simulation_values const_vals;

    //! internal dataand pointers
    std::uint16_t n_avg_traj_frames;
    std::size_t nthreads;
    std::size_t cyclic_idx_memory_load = 0;
    std::vector<IonClusters>* output_ptr = nullptr;
    std::vector<Clusters::Clustering*> cluster_calculators;

    //! coordinates of the push on change particles
    float* m_p_update_on_change = nullptr;
    std::uint32_t* update_on_change_idx = nullptr;
    std::size_t n_updates_on_change;

    //! whether to use the index to calculate clusters or not and the charge of the particle
    bool* use_index = nullptr;
    std::int16_t* charge_index = nullptr;

    //! if additional types are supplied for g(r) calculations, i.e., DNA - cluster calculations
    bool** add_gofr_use_index = nullptr;
    std::uint8_t Nadd_gofr = 0;
    std::vector<std::string> Nadd_gofr_names;


    float cluster_cutoff;
    std::uint16_t grids[3];
    std::uint32_t current_traj_frame = 0;
#ifdef DEBUGGING
    bool verbose = true;
#else
    bool verbose = false;
#endif

	MODE_TRACKED_POSITION update_mode;

    //! inner subfunctions for simplicity
    int parse_topology(ClusterFile, cluster_options);
public:
    batch_cluster_handle(ClusterFile _file, std::size_t n_threads, cluster_options _opts, std::vector<IonClusters>* outptr);//OutputStack<IonClusters> *outptr);

    bool valid_files = false;
    bool valid_grid_sz = false;
    bool valid_dr_sz = false;

    int memory_load_frames();
    int compile_current_data();
    int compile_current_data(std::size_t up_to_frame);
    int push_data_out();
    int reset_state(bool chk_update = false);

    std::uint16_t get_current_nframe_avg()
    {
        return n_avg_traj_frames;
    }

    std::uint32_t get_current_traj_frame()
    {
        return current_traj_frame;
    }

    int last_error_code = 0;

    ~batch_cluster_handle(); // clean up memory on deletion
};









#endif //IONSCLUSTERS_SYNCHRONIZEDIO_HPP
