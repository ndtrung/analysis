
#include <algorithm>
#include <fstream>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <string>
#include "DCDReader.hpp"
#include "memory.h"
#include "clustering.h"
#include "SSEInstructions.hpp"

using namespace std;
using namespace Clusters;

#define anint(x) ((x >= 0.5) ? (1.0) : (x < -0.5) ? (-1.0) : (0.0))
#define MAX_NEIGHBORS        64
#define MAX_BEADS_IN_CELL    128

#define MAX_NEIGHBORS_GOFR 512
#define MAX_BEADS_IN_GOFR_CELL 512

enum {XYZ=0, XML=1, DUMP=2, DCD=3};

Clustering::Clustering(float _cutoff, int _periodic_wrapped, int _nNumBeadsInParticle,
                       int _threshold, int _write_clusters, int _bin_size,
                       std::uint32_t _nparticles, int _nx, int _ny, int _nz)
{
    cutoff = _cutoff;
    periodic_wrapped = _periodic_wrapped;
    nNumBeadsInParticle = _nNumBeadsInParticle;
    threshold = _threshold;
    write_clusters = _write_clusters;
    bin_size = _bin_size;
    nparticles = _nparticles;
    nx = _nx;
    ny = _ny;
    nz = _nz;
    grid = NULL;
    neighbors = NULL;
    num_neigh = NULL;
    //io_handle = NULL;
    max_neigh = MAX_NEIGHBORS;
	g_max_neigh = MAX_NEIGHBORS_GOFR;


    nbins = nx * ny * nz;
    create(grid, nbins);
    memset(grid, 0, nbins*sizeof(int));

    visited = new bool[nparticles];
    std::fill_n(visited, nparticles, false);

    cluster_size_array = new std::uint16_t[nparticles];
    memset(cluster_size_array, 0, sizeof(std::uint16_t) * nparticles);
}

Clustering::~Clustering()
{
    //if (io_handle) delete io_handle;
    destroy(grid);
    destroy(num_neigh);
    destroy(neighbors);
    destroy(cells);
    destroy(num_beads_in_cell);
    destroy(neighbor_cells);
    destroy(num_neighbor_cells);

	_mm_free(g_num_neigh);
	destroy(g_neighbors);
	destroy(g_cells);
	destroy(g_num_beads_in_cell);
	destroy(g_neighbor_cells);
	destroy(g_num_neighbor_cells);

    delete[] visited;
    delete[] cluster_size_array;
}


// Clustering workhorse
// return a vector of clusters, each is essentially a vector of bead indices in beadList
int Clustering::find_clusters(vector<Cluster>& clusterList)
{
#ifdef DEBUGGING
    std::cout << " in find_clusters(std::vector<>&)" << std::endl;
#endif
    if (nparticles == 0) return 0;

    int i, nNumBeads, nNumParticles;
    float cutoff2 = cutoff * cutoff;

    nNumParticles = nparticles / nNumBeadsInParticle;

    // reset the cluster list
    clusterList.clear();

    // Update the new number of beads
    nNumBeads = nparticles;

    // Finding neighbors
    // using the cell-list algorithm
    neighboring();
#ifdef DEBUGGING
    std::cout << "ptr to cluster_size_array" << cluster_size_array << std::endl;
    std::cout << "ptr to num_neigh " << num_neigh << std::endl;
    std::cout << "ptr to neighbors " << neighbors << std::endl;
    std::cout << " ptr to 1st el" << neighbors[0] << std::endl;
    std::cout << " making clusters" << std::endl;
#endif
    // Loop through all the beads of the specified type
    int nNumBeadsInParticleAfterFiltered = nparticles / nNumParticles;
    for (i = 0; i < nNumBeads; i++) {
#ifdef DEEP_DEBUGGING
        std::cout << "i-idx : " << i << std::endl;
        std::cout << "visited : " << visited[i] << std::endl;
        std::cout << "use_idx : " << use_idx[i] << std::endl;
#endif
        vector<int> cluster;
        make_cluster(i, cluster, nNumBeadsInParticleAfterFiltered);

        if (cluster.size() > 0)
            clusterList.push_back(cluster);
    }
#ifdef DEBUGGING
    std::cout << " finished making clusters " << std::endl;
#endif
    float As, major[3], R2[3], xm[3], Rg;
    int nNumParticlesInCluster;
    int nTotalParticlesinCluster = 0;
    int nClusters = 0; // number of clusters bigger than threshold
    int numClusters = clusterList.size();
    vector<float> Rg_list;

    particleList.clear();

#ifdef DEBUGGING
    std::cout << "making particleList from N = " << clusterList.size() << " clusters " << std::endl;
    std::cout << "box pointers report : " << *(box + working_offset * 6) << ", " << *(1+box + working_offset * 6) << ", " << *(2+box + working_offset * 6) << std::endl;
#endif
    for (i = 0; i < numClusters; i++)
    {
#ifdef DEEP_DEBUGGING
        std::cout << "getting numBeads " << std::endl;
#endif
        int numBeads = clusterList[i].size();
        nNumParticlesInCluster =  numBeads / nNumBeadsInParticleAfterFiltered;
#ifdef DEEP_DEBUGGING
        std::cout << "setting cluster_size_array" << std::endl;
#endif
        for (int j = 0; j < numBeads; j++)
            cluster_size_array[clusterList[i][j]] = numBeads;

        // NOTE: threshold is the number of particles, not beads
        // exclude the clusters smaller than threshold
        if (nNumParticlesInCluster < threshold) continue;
#ifdef DEEP_DEBUGGING
        std::cout << "sorting" << std::endl;
#endif
        // Ascending sorting is required to ensure beads in a particle sit next to each other
        sort(clusterList[i].begin(), clusterList[i].end());

        /* this currently doesnt work
        std::cout << "As" << std::endl;
        As = asphericity_param(clusterList[i], major, R2, Rg, xm);
        Rg_list.push_back(Rg);
        */
        //std::cout << "arrange" << std::endl;
        // Arrange the particles based on the distance from their COM to the bottom left corner of the box
        arrange(particleList, clusterList[i], nNumBeadsInParticleAfterFiltered);

        nTotalParticlesinCluster += nNumParticlesInCluster;
        nClusters++;
    }
#ifdef DEBUGGING
    std::cout << "gridding N = " << particleList.size() << " clusters" << std::endl;
    std::cout << "nx = " << nx << " ny = " << ny << "nz = " << nz << std::endl;
#endif

    // this is the gridding section, take each cluster and put them on the grid defined by nx, ny, nz and pointers buf_x
    for(std::size_t particle_idx = 0; particle_idx != particleList.size(); particle_idx++)
    {

        int grid_idx = binning(particleList[particle_idx].xm, particleList[particle_idx].ym, particleList[particle_idx].zm);
#ifdef DEBUGGING
        if(grid_idx < 0 || grid_idx >= nx*ny*nz)
        {
            std::cout << "error at idx " << particle_idx << std::endl;
            std::cout << "particle location : (" << particleList[particle_idx].xm << ", "
                      << particleList[particle_idx].ym << ", " << particleList[particle_idx].zm << ") " << std::endl;

            std::cout << "box sizes : (" << box[0 + working_offset * 6] << ", " << box[1 + working_offset * 6] << ", "
                      << box[2 + working_offset * 6] << ")" << std::endl;

            std::cout << " charge : " << particleList[particle_idx].charge << std::endl;
            std::cout << " size : " << particleList[particle_idx].x_list.size() << std::endl;
            std::cout << "coordinates : " << std::endl;
            for(auto i = 0; i!= particleList[particle_idx].x_list.size(); i++)
            {
                std::cout << particleList[particle_idx].x_list[i];
                std::cout << ", ";
                std::cout << particleList[particle_idx].y_list[i];
                std::cout << ", ";
                std::cout << particleList[particle_idx].z_list[i];
                std::cout << std::endl;
            }
            float gridSizex = box[0 + working_offset * 6] / nx;
            float gridSizey = box[1 + working_offset * 6] / ny;
            float gridSizez = box[2 + working_offset * 6] / nz;
            std::cout << " grid sizes reported " << gridSizex << ", " << gridSizey << ", " << gridSizez << std::endl;
            std::cout << " first offset " << (particleList[particle_idx].zm + 0.5*box[2 + working_offset * 6]) / gridSizez << std::endl;
            std::cout << " second offset " << (particleList[particle_idx].ym + 0.5*box[1 + working_offset * 6]) / gridSizey << std::endl;
            std::cout << " third offset " << (particleList[particle_idx].xm + 0.5*box[0 + working_offset * 6]) / gridSizex << std::endl;
            std::abort();
        }
#endif

        std::int16_t ccharge = particleList[particle_idx].charge;

        if(ccharge < -max_charge)
        {
            ccharge = -max_charge;
        }
        else if (ccharge > max_charge)
        {
            ccharge = max_charge;
        }
        std::uint16_t csize = particleList[particle_idx].x_list.size();
        if(csize > max_size)
        {
	        csize = max_size;
        }
	    particleList[particle_idx].clamped_size = csize;
	    particleList[particle_idx].clamped_charge = ccharge;
#ifdef DEEP_DEBUGGING
        std::cout << " particle index " << particle_idx << " grid index " << grid_idx << std::endl;
#endif
#ifdef DEBUGGING
        if(nx*ny*nz * (ccharge + max_charge) + grid_idx > grid_charge_sz)
        {
            std::cout << "index out of bounds in charge grid, supplied index " <<
                      nx*ny*nz * (ccharge + max_charge) + grid_idx << " while grid size is of " << grid_charge_sz << std::endl;
            std::cout << "system base grid size : " << nx*ny*nz << std::endl;
            std::cout << "cluster charge : " << ccharge << std::endl;
            std::cout << "system maximum cluster charge " << max_charge << std::endl;
            std::cout << "provided grid index by binning() " << grid_idx << std::endl;
            std::abort();
        }
        if(nx*ny*nz * (csize - 1) + grid_idx > grid_sz_sz)
        {
            std::cout << "index out of bounds in size grid, supplied index " << nx*ny*nz * (csize) + grid_idx << " while grid size is " << grid_sz_sz << std::endl;
            std::cout << "system base grid size : " << nx*ny*nz << std::endl;
            std::cout << "cluster size : " << csize << std::endl;
            std::cout << "system maximum cluster size " << max_size << std::endl;
            std::cout << "provided grid index by binning() " << grid_idx << std::endl;
            std::abort();
        }
#endif
        buf_grid_charge[grid_charge_sz * working_offset + nx*ny*nz * (ccharge + max_charge) + grid_idx]++;
        buf_grid_sz[grid_sz_sz * working_offset + nx*ny*nz * (csize - 1) + grid_idx]++;
    }
#ifdef DEBUGGING
    std::cout << "finished gridding" << std::endl;
#endif
    return 0;
}

__attribute__((optimize("unroll-loops")))
void Clustering::create_cell_list()
{
    int i, j, k, c, cn, x, y, z, ncell2;

    ncell2 = ncellxy;

    for (c = 0; c < ncells; c++)
    {
        num_neighbor_cells[c] = 0;
    }

    // loop over all cells
    for (i = 0; i < ncellz; i++) {
        for (j = 0; j < ncelly; j++) {
            for (k = 0; k < ncellx; k++) {

                // identify current cell
                c = i*ncell2 + j*ncellx + k;

                // always interact within cell
                neighbor_cells[c][num_neighbor_cells[c]] = c;
                num_neighbor_cells[c]++;


                // collect near neighbors of cell c
                x = 1;
                for (y = -1; y <= 1; y++) {
                    for (z = -1; z <= 1; z++) {
                        cn = ((i + z + ncellz) % ncellz) * ncell2 +
                             ((j + y + ncelly) % ncelly) * ncellx +
                             (k + x + ncellx) % ncellx;
                        neighbor_cells[c][num_neighbor_cells[c]] = cn;
                        num_neighbor_cells[c]++;
                    }
                }

                x = 0;
                y = 1;
                for (z = -1; z <= 1; z++) {
                    cn = ((i + z + ncellz) % ncellz) * ncell2 +
                         ((j + y + ncelly) % ncelly) * ncellx +
                         (k + x + ncellx) % ncellx;
                    neighbor_cells[c][num_neighbor_cells[c]] = cn;
                    num_neighbor_cells[c]++;
                }

                y = 0;
                z = 1;
                cn = ((i + z + ncellz) % ncellz) * ncell2 +
                     ((j + y + ncelly) % ncelly) * ncellx +
                     (k + x + ncellx) % ncellx;
                neighbor_cells[c][num_neighbor_cells[c]] = cn;
                num_neighbor_cells[c]++;
            }
        }
    } // end loop over cells
}


__attribute__((optimize("unroll-loops")))
void Clustering::create_cell_list_gofr()
{
	int i, j, k, c, cn, x, y, z, ncell2;

	ncell2 = g_ncellxy;

	for (c = 0; c < g_ncells; c++)
	{
		g_num_neighbor_cells[c] = 0;
	}

	// loop over all cells
	for (i = 0; i < g_ncellz; i++) {
		for (j = 0; j < g_ncelly; j++) {
			for (k = 0; k < g_ncellx; k++) {

				// identify current cell
				c = i*ncell2 + j*g_ncellx + k;

				// always interact within cell
				g_neighbor_cells[c][g_num_neighbor_cells[c]] = c;
				g_num_neighbor_cells[c]++;


				// collect near neighbors of cell c
				x = 1;
				for (y = -1; y <= 1; y++) {
					for (z = -1; z <= 1; z++) {
						cn = ((i + z + g_ncellz) % g_ncellz) * ncell2 +
						     ((j + y + g_ncelly) % g_ncelly) * g_ncellx +
						     (k + x + g_ncellx) % g_ncellx;
						g_neighbor_cells[c][g_num_neighbor_cells[c]] = cn;
						g_num_neighbor_cells[c]++;
					}
				}

				x = 0;
				y = 1;
				for (z = -1; z <= 1; z++) {
					cn = ((i + z + g_ncellz) % g_ncellz) * ncell2 +
					     ((j + y + g_ncelly) % g_ncelly) * g_ncellx +
					     (k + x + g_ncellx) % g_ncellx;
					g_neighbor_cells[c][g_num_neighbor_cells[c]] = cn;
					g_num_neighbor_cells[c]++;
				}

				y = 0;
				z = 1;
				cn = ((i + z + g_ncellz) % g_ncellz) * ncell2 +
				     ((j + y + g_ncelly) % g_ncelly) * g_ncellx +
				     (k + x + g_ncellx) % g_ncellx;
				g_neighbor_cells[c][g_num_neighbor_cells[c]] = cn;
				g_num_neighbor_cells[c]++;
			}
		}
	} // end loop over cells
}

// Build the neighbor list of the beads
// Note: To restrict the clusters to contain the specified types only,
//       modify how the beads are added to the neighbor list
void Clustering::neighboring()
{
    int i, j, _ncellx, _ncelly, _ncellz, _ncellxy, _ncells, n, cn;
    int HALF_NEIGH=14;
    int nNumBeads, indexi, indexj;
    float xtmp, ytmp, ztmp, dx, dy, dz, r2;

    nNumBeads = nparticles;

    float cutoff2 = cutoff * cutoff;

    // Divide the box into cells of which the size is cutoff
    _ncellx = ceil(box[0 + working_offset * 6] / cutoff);
    _ncelly = ceil(box[1 + working_offset * 6] / cutoff);
    _ncellz = ceil(box[2 + working_offset * 6] / cutoff);



    // check memory corruption
    if((!cells || !num_beads_in_cell || !neighbor_cells || !num_neighbor_cells) && (cells || num_beads_in_cell || neighbor_cells || num_neighbor_cells))
    {
        std::abort();
    }

    if(!cells)
    {
        ncellx = _ncellx;
        ncelly = _ncelly;
        ncellz = _ncellz;

        ncellxy = ncellx * ncelly;
        ncells = ncellx * ncelly * ncellz;

        create(cells, ncells, MAX_BEADS_IN_CELL);
        create(num_beads_in_cell, ncells);
        create(neighbor_cells, ncells, HALF_NEIGH);
        create(num_neighbor_cells, ncells);
    }
    else if (ncellx != _ncellx || ncelly != _ncelly || ncellz != _ncellz)
    {
        destroy(cells);
        destroy(num_beads_in_cell);
        destroy(neighbor_cells);
        destroy(num_neighbor_cells);

        ncellx = _ncellx;
        ncelly = _ncelly;
        ncellz = _ncellz;

        ncellxy = ncellx * ncelly;
        ncells = ncellx * ncelly * ncellz;

        create(cells, ncells, MAX_BEADS_IN_CELL);
        create(num_beads_in_cell, ncells);
        create(neighbor_cells, ncells, HALF_NEIGH);
        create(num_neighbor_cells, ncells);
    }


    memset(num_beads_in_cell, 0, ncells*sizeof(int));
#ifdef DEBUGGING
    std::cout << "create and memset num_neigh" << std::endl;
#endif
    if (num_neigh == NULL) create(num_neigh, nNumBeads);

    memset(num_neigh, 0, nNumBeads*sizeof(int));

#ifdef DEBUGGING
    std::cout << " create neighbors and call create_cell_list" << std::endl;
#endif

    if (neighbors == NULL) create(neighbors, nNumBeads, max_neigh);
    // Create the cell list
    create_cell_list();//ncellx, ncelly, ncellz, neighbor_cells, num_neighbor_cells);

    // Bin the beads into cells
    int idxx, idxy, idxz, c;
    float cellSizex, cellSizey, cellSizez, halfboxx, halfboxy, halfboxz;
    halfboxx = box[0 + working_offset * 6] / 2.0;
    halfboxy = box[1 + working_offset * 6] / 2.0;
    halfboxz = box[2 + working_offset * 6] / 2.0;
    cellSizex = box[0 + working_offset * 6] / ncellx;
    cellSizey = box[1 + working_offset * 6] / ncelly;
    cellSizez = box[2 + working_offset * 6] / ncellz;

#ifdef DEBUGGING
    std::cout << "calculate number of beads in each cell" << std::endl;
#endif

    for (i = 0; i < nNumBeads; i++)
    {
        xtmp = coordinates[i + working_offset * 3 * nparticles];
        ytmp = coordinates[i + (working_offset * 3 + 1) * nparticles];
        ztmp = coordinates[i + (working_offset * 3 + 2) * nparticles];

        // if the particle ends exactly on the PBC, it produces an out of bounds error
        // overflow fixed by restraining box to [-L/2, L/2 )
        idxz = static_cast<int>((ztmp + halfboxz) / cellSizez) % ncellz;
        idxy = static_cast<int>((ytmp + halfboxy) / cellSizey) % ncelly;
        idxx = static_cast<int>((xtmp + halfboxx) / cellSizex) % ncellx;


        c = idxz * ncellxy + idxy * ncellx + idxx;
#ifdef DEBUGGING
        if(c >= ncells)
        {
            std::cout << " index c : " << c << " is larger than number of cells " << ncells << std::endl;
            std::cout << " box : (" << box[0 + working_offset * 6] << ", "
                                    << box[1 + working_offset * 6] << ", "
                                    << box[2 + working_offset * 6] << ", "
                                    << box[3 + working_offset * 6] << ", "
                                    << box[4 + working_offset * 6] << ", "
                                    << box[5 + working_offset * 6] << ")" << std::endl;
            std::cout << "cell index Z : " << (ztmp + halfboxz) / cellSizez << ", out of ncellz " << ncellz << std::endl;
            std::cout << "cell index Y : " << (ytmp + halfboxy) / cellSizey << ", out of ncelly " << ncelly << std::endl;
            std::cout << "cell index X : " << (xtmp + halfboxx) / cellSizex << ", out of ncellx " << ncellx << std::endl;
            std::abort();
        }
        if(c < 0)
        {
            std::cout << "index c : " << c << " is negative " << std::endl;
            std::abort();
        }

        if(num_beads_in_cell[c] >= MAX_BEADS_IN_CELL)
        {
            std::cout << "more beads in cell than defined macro MAX_BEADS_IN_CELL" << std::endl;
            std::abort();
        }
#endif
        // put the index of the bead into the list of molecules in this cell
        cells[c][num_beads_in_cell[c]] = i;
        num_beads_in_cell[c]++;
    }
#ifdef DEBUGGING
    std::cout << "build the neighbor list of each bead of the specified type" << std::endl;
#endif
    // Neighboring
    // build the neighbor list of each bead of the specified type
    for (c = 0; c < ncells; c++)
    {

        if (num_beads_in_cell[c] == 0) continue;

        // find neighbors within a cell
        for (i = 0; i < num_beads_in_cell[c] - 1; i++)
        {
            indexi = cells[c][i];

            if(!use_idx[indexi]){continue;}

            xtmp = coordinates[indexi + working_offset * 3 * nparticles];
            ytmp = coordinates[indexi + (working_offset * 3 + 1) * nparticles];
            ztmp = coordinates[indexi + (working_offset * 3 + 2) * nparticles];

            for (j = i + 1; j<num_beads_in_cell[c]; j++) {

                indexj = cells[c][j];
                if(!use_idx[indexj]){continue;}
                // find the distance between bead i and bead j
                dx  = xtmp - coordinates[indexj + working_offset * 3 * nparticles];
                dy  = ytmp - coordinates[indexj + (working_offset * 3 + 1) * nparticles];
                dz  = ztmp - coordinates[indexj + (working_offset * 3 + 2) * nparticles];

                dx -= box[0 + working_offset * 6] * anint(dx / box[0 + working_offset * 6]);
                dy -= box[1 + working_offset * 6] * anint(dy / box[1 + working_offset * 6]);
                dz -= box[2 + working_offset * 6] * anint(dz / box[2 + working_offset * 6]);

                r2 = dx * dx + dy * dy + dz * dz;

                if (r2 <= cutoff2) {
                    // add bead indexj into the neighbor list of bead indexi and vice versa
                    neighbors[indexi][num_neigh[indexi]++] = indexj;
                    neighbors[indexj][num_neigh[indexj]++] = indexi;
                }
            }
        }  // end within a cell
        // now find neighbors in bordering cells  NBRS = 14;
        for (n = 1; n < num_neighbor_cells[c]; n++) {
            cn = neighbor_cells[c][n];

            // this is similar to that above
            for (i = 0; i < num_beads_in_cell[c]; i++) {
                indexi = cells[c][i];
                if(!use_idx[indexi]){continue;}
                xtmp = coordinates[indexi + working_offset * 3 * nparticles];
                ytmp = coordinates[indexi + (working_offset * 3 + 1) * nparticles];
                ztmp = coordinates[indexi + (working_offset * 3 + 2) * nparticles];

                // here is the loop through each bead in the cell[nbr]
                for (j = 0; j < num_beads_in_cell[cn]; j++) {
                    indexj = cells[cn][j];
                    if(!use_idx[indexj]){continue;}

                    // find the distance between bead i and bead j
                    dx  = xtmp - coordinates[indexj + working_offset * 3 * nparticles];
                    dy  = ytmp - coordinates[indexj + (working_offset * 3 + 1) * nparticles];
                    dz  = ztmp - coordinates[indexj + (working_offset * 3 + 2) * nparticles];

                    dx -= box[0 + working_offset * 6] * anint(dx / box[0 + working_offset * 6]);
                    dy -= box[1 + working_offset * 6] * anint(dy / box[1 + working_offset * 6]);
                    dz -= box[2 + working_offset * 6] * anint(dz / box[2 + working_offset * 6]);


                    r2 = dx * dx + dy * dy + dz * dz;

                    if (r2 <= cutoff2)
                    {
                        // add bead indexj into the neighbor list of bead indexi and vice versa
                        neighbors[indexi][num_neigh[indexi]++] = indexj;
                        neighbors[indexj][num_neigh[indexj]++] = indexi;
                    }
                }
            }
        }   // end loops over neighbor cells
    }  // end loop over cells
#ifdef DEBUGGING
    std::cout << "deallocating cells" << std::endl;
#endif

    // deallocate

}

void Clustering::neighboring_gofr()
{
	int i, j, _ncellx, _ncelly, _ncellz, _ncellxy, _ncells, n, cn;
	int HALF_NEIGH=14;
	int indexi, indexj;
	float xtmp, ytmp, ztmp, dx, dy, dz, r2;

	std::size_t nNumBeads = particleList.size();

	if(g_cutoff <= 0.0f)
	{
		std::abort();
	}

	float g_cutoff2 = g_cutoff * g_cutoff;

	// Divide the box into cells of which the size is cutoff
	int _g_ncellx = ceil(box[0 + working_offset * 6] / g_cutoff);
	int _g_ncelly = ceil(box[1 + working_offset * 6] / g_cutoff);
	int _g_ncellz = ceil(box[2 + working_offset * 6] / g_cutoff);



	// check memory corruption
	if((!g_cells || !g_num_beads_in_cell || !g_neighbor_cells || !g_num_neighbor_cells) &&
			(g_cells || g_num_beads_in_cell || g_neighbor_cells || g_num_neighbor_cells))
	{
		std::abort();
	}
#ifdef DEBUGGING
	std::cout << "checking neighbouring_gofr memory allocation sizes" << std::endl;
#endif

	if(!g_cells)
	{
		g_ncellx = _g_ncellx;
		g_ncelly = _g_ncelly;
		g_ncellz = _g_ncellz;

		g_ncellxy = g_ncellx * g_ncelly;
		g_ncells = g_ncellx * g_ncelly * g_ncellz;

		create(g_cells, g_ncells, MAX_BEADS_IN_GOFR_CELL);
		create(g_num_beads_in_cell, g_ncells);
		create(g_neighbor_cells, g_ncells, HALF_NEIGH);
		create(g_num_neighbor_cells, g_ncells);
	}
	else if (g_ncellx != _g_ncellx || g_ncelly != _g_ncelly || g_ncellz != _g_ncellz)
	{
		destroy(g_cells);
		destroy(g_num_beads_in_cell);
		destroy(g_neighbor_cells);
		destroy(g_num_neighbor_cells);

		g_ncellx = _g_ncellx;
		g_ncelly = _g_ncelly;
		g_ncellz = _g_ncellz;

		g_ncellxy = g_ncellx * g_ncelly;
		g_ncells = g_ncellx * g_ncelly * g_ncellz;

		create(g_cells, g_ncells, MAX_BEADS_IN_GOFR_CELL);
		create(g_num_beads_in_cell, g_ncells);
		create(g_neighbor_cells, g_ncells, HALF_NEIGH);
		create(g_num_neighbor_cells, g_ncells);
	}
	memset(g_num_beads_in_cell, 0, g_ncells*sizeof(int));
#ifdef DEBUGGING
	std::cout << "binning " << nNumBeads << " into g(r) neighbouring calculation" << std::endl;
#endif

	// check memory corruption
	if((!g_num_neigh && g_neighbors) || (g_num_neigh && !g_neighbors))
	{
		std::cout << "pointer corruption in g_num_nei / g_nei pointers" << std::endl;
		std::abort();
	}

	if(!g_num_neigh)
	{
		g_num_neigh = (std::uint32_t*) _mm_malloc(nNumBeads*sizeof(std::uint32_t), 32);
		create(g_neighbors, nNumBeads, g_max_neigh);
		sz_g_num_neigh = nNumBeads;
	}
	else if(nNumBeads > sz_g_num_neigh) // numbers of clusters can change from one frame to the next; reallocate if size(new) > size(old)
	{
		_mm_free(g_num_neigh);
		g_num_neigh = (std::uint32_t*) _mm_malloc(nNumBeads*sizeof(std::uint32_t), 32);

		destroy(g_neighbors);
		create(g_neighbors, nNumBeads, g_max_neigh);

		sz_g_num_neigh = nNumBeads;
	}
	memset(g_num_neigh, 0, nNumBeads*sizeof(std::uint32_t));
	// Create the cell list
	create_cell_list_gofr();


#ifdef DEBUGGING
	std::cout << " binning the particles into cells" << std::endl;
#endif

	// Bin the beads into cells
	int idxx, idxy, idxz, c;
	float cellSizex, cellSizey, cellSizez, halfboxx, halfboxy, halfboxz;
	halfboxx = box[0 + working_offset * 6] / 2.0;
	halfboxy = box[1 + working_offset * 6] / 2.0;
	halfboxz = box[2 + working_offset * 6] / 2.0;
	cellSizex = box[0 + working_offset * 6] / g_ncellx;
	cellSizey = box[1 + working_offset * 6] / g_ncelly;
	cellSizez = box[2 + working_offset * 6] / g_ncellz;

	for (i = 0; i < nNumBeads; i++)
	{
		xtmp = particleList[i].xm;
		ytmp = particleList[i].ym;
		ztmp = particleList[i].zm;

		// if the particle ends exactly on the PBC, it produces an out of bounds error
		idxz = static_cast<int>((ztmp + halfboxz) / cellSizez) % g_ncellz;
		idxy = static_cast<int>((ytmp + halfboxy) / cellSizey) % g_ncelly;
		idxx = static_cast<int>((xtmp + halfboxx) / cellSizex) % g_ncellx;

		c = idxz * g_ncellxy + idxy * g_ncellx + idxx;

		/*
		 * debug checking macros
		 */

#ifdef DEBUGGING
		if(c >= g_ncells)
        {
            std::cout << " index c : " << c << " is larger than number of cells " << g_ncells << std::endl;
            std::cout << " box : (" << box[0 + working_offset * 6] << ", "
                                    << box[1 + working_offset * 6] << ", "
                                    << box[2 + working_offset * 6] << ", "
                                    << box[3 + working_offset * 6] << ", "
                                    << box[4 + working_offset * 6] << ", "
                                    << box[5 + working_offset * 6] << ")" << std::endl;
            std::cout << "cell index Z : " << (ztmp + halfboxz) / cellSizez << ", out of ncellz " << g_ncellz << std::endl;
            std::cout << "cell index Y : " << (ytmp + halfboxy) / cellSizey << ", out of ncelly " << g_ncelly << std::endl;
            std::cout << "cell index X : " << (xtmp + halfboxx) / cellSizex << ", out of ncellx " << g_ncellx << std::endl;
            std::abort();
        }
        if(c < 0)
        {
            std::cout << "index c : " << c << " is negative " << std::endl;
            std::abort();
        }
		if(g_num_beads_in_cell[c] == MAX_BEADS_IN_GOFR_CELL)
		{
			std::cout << "number of beads in current cell larger than macro MAX_BEADS_IN_GOFR_CELL; increase macro value or reduce max value of r" << std::endl;
			std::abort();
		}
#endif
		// put the index of the bead into the list of molecules in this cell
		g_cells[c][g_num_beads_in_cell[c]] = i;
		g_num_beads_in_cell[c]++;
	}

#ifdef DEBUGGING
	std::cout << " building neighbouring list" << std::endl;
#endif

	// Neighboring
	// build the neighbor list of each bead of the specified type
	for (c = 0; c < g_ncells; c++)
	{
		if (g_num_beads_in_cell[c] == 0) continue;

		// find neighbors within a cell
		for (i = 0; i < g_num_beads_in_cell[c] - 1; i++)
		{
			indexi = g_cells[c][i];
#ifdef DEBUGGING // checking particleList bounds against indexi
			if(indexi >= particleList.size() || indexi < 0)
				{
					std::cout << "index i : " << indexi << " is larger than size of particleList : " << particleList.size() << std::endl;
				}
#endif
			xtmp = particleList[indexi].xm;
			ytmp = particleList[indexi].ym;
			ztmp = particleList[indexi].zm;

			for (j = i + 1; j < g_num_beads_in_cell[c]; j++) {

				indexj = g_cells[c][j];
#ifdef DEBUGGING // particleList bounds against indexj
				if(indexj >= particleList.size() || indexj < 0)
					{
						std::cout << "index j : " << indexj << " is larger than size of particleList : " << particleList.size() << std::endl;
					}
#endif
				// find the distance between bead i and bead j
				dx  = xtmp - particleList[indexj].xm;
				dy  = ytmp - particleList[indexj].ym;
				dz  = ztmp - particleList[indexj].zm;

				dx -= box[0 + working_offset * 6] * anint(dx / box[0 + working_offset * 6]);
				dy -= box[1 + working_offset * 6] * anint(dy / box[1 + working_offset * 6]);
				dz -= box[2 + working_offset * 6] * anint(dz / box[2 + working_offset * 6]);

				r2 = dx * dx + dy * dy + dz * dz;
#ifdef DEBUGGING // debugging check for hit against macro MAX_NEIGHBORS_GOFR
				if(g_num_neigh[indexi] >= MAX_NEIGHBORS_GOFR || g_num_neigh[indexj] >= MAX_NEIGHBORS_GOFR)
					{
						std::cout << "number of neighbours for g(r) calculation larger than macro MAX_NEIGHBORS_GOFR, increase macro value or reduce max r" << std::endl;
						std::abort();
					}
#endif
				if (r2 <= g_cutoff2)
				{
					// add bead indexj into the neighbor list of bead indexi and vice versa
					g_neighbors[indexi][g_num_neigh[indexi]++] = indexj;
					g_neighbors[indexj][g_num_neigh[indexj]++] = indexi;
				}
			}
		}  // end within a cell
		// now find neighbors in bordering cells  NBRS = 14;
		for (n = 1; n < g_num_neighbor_cells[c]; n++)
		{
#ifdef DEBUGGING
			if(n >= HALF_NEIGH)
			{
				std::cout << "number of neighbouring cells larger than HALF_NEIGH macro" << std::endl;
				std::abort();
			}
#endif
			cn = g_neighbor_cells[c][n];

			// this is similar to that above
			for (i = 0; i < g_num_beads_in_cell[c]; i++)
			{
				indexi = g_cells[c][i];
#ifdef DEBUGGING // checking particleList bounds against indexi
				if(indexi >= particleList.size() || indexi < 0)
				{
					std::cout << "index i : " << indexi << " is larger than size of particleList : " << particleList.size() << std::endl;
				}
#endif
				xtmp = particleList[indexi].xm;
				ytmp = particleList[indexi].ym;
				ztmp = particleList[indexi].zm;

				// here is the loop through each bead in the cell[nbr]
				for (j = 0; j < g_num_beads_in_cell[cn]; j++)
				{

					indexj = g_cells[cn][j];
#ifdef DEBUGGING // checking particleList bounds against indexj
					if(indexj >= particleList.size() || indexj < 0)
					{
						std::cout << "index j : " << indexj << " is larger than size of particleList : " << particleList.size() << std::endl;
					}
#endif
					// find the distance between bead i and bead j
					dx  = xtmp - particleList[indexj].xm;
					dy  = ytmp - particleList[indexj].ym;
					dz  = ztmp - particleList[indexj].zm;

					dx -= box[0 + working_offset * 6] * anint(dx / box[0 + working_offset * 6]);
					dy -= box[1 + working_offset * 6] * anint(dy / box[1 + working_offset * 6]);
					dz -= box[2 + working_offset * 6] * anint(dz / box[2 + working_offset * 6]);

					r2 = dx * dx + dy * dy + dz * dz;
					if (r2 <= g_cutoff2)
					{
#ifdef DEBUGGING // debugging check for hit against macro MAX_NEIGHBORS_GOFR
						if(g_num_neigh[indexi] == MAX_NEIGHBORS_GOFR || g_num_neigh[indexj] == MAX_NEIGHBORS_GOFR)
						{
							std::cout << "number of neighbours for g(r) calculation larger than macro MAX_NEIGHBORS_GOFR, increase macro value or reduce max r" << std::endl;
							std::abort();
						}
#endif
						// add bead indexj into the neighbor list of bead indexi and vice versa
						g_neighbors[indexi][g_num_neigh[indexi]++] = indexj;
						g_neighbors[indexj][g_num_neigh[indexj]++] = indexi;
					}
				}
			}
		}   // end loops over neighbor cells
	}  // end loop over cells
}


// recursively find clusters for bead i by iterating through i's neighbor list
void Clustering::make_cluster(int i, Cluster& cluster, int nNumBeadsInParticle)
{
    //int j, start, end;

    if (!visited[i] && use_idx[i])
    {
        auto start = i - i % nNumBeadsInParticle;
        auto end = start + nNumBeadsInParticle;
        for (auto j = start; j < end; j++)
        {
            visited[j] = true;
            cluster.push_back(j);
        }
        int nNeighbors = num_neigh[i];

        for (auto j = 0; j < nNeighbors; j++)
        {
#ifdef DEEP_DEBUGGING
            std::cout << "accessing neighbors[" << i << "][" << j << "]" <<std::endl;
#endif
            make_cluster(neighbors[i][j], cluster, nNumBeadsInParticle);
        }
    }
}

// each particle (body) is composed of nNumBeadsPerParticle beads
// Since each cluster holds the list of bead indices,
// which are not in any order with respect to the particles,
// we need to first re-arrange (group) the beads into particles (for visualization purposes).
// Then the particles are arranged based on their distance to some reference point,
// e.g. the bottom corner of the box or the cluster center of mass,
// so that a loop through the particle list has some spatial order
void Clustering::arrange(std::vector<Particle>& particleList, Cluster& cluster, int nNumBeadsPerParticle)
{
    int i, j, nNumBeads, nNumParticles;

    // group beads from the cluster into particles

    nNumBeads = cluster.size();
    nNumParticles = nNumBeads / nNumBeadsPerParticle;
    Particle particle(box+(working_offset*6));

    for (i = 0; i < nNumParticles; i++)
    {
        for (j = 0; j < nNumBeadsPerParticle; j++)
        {
            std::size_t listidx = cluster[i * nNumBeadsPerParticle + j];
            particle.x_list.push_back(coordinates[listidx + working_offset * 3 * nparticles]);
            particle.y_list.push_back(coordinates[listidx + (working_offset * 3 + 1) * nparticles]);
            particle.z_list.push_back(coordinates[listidx + (working_offset * 3 + 2) * nparticles]);
            particle.charge += charge_idx[listidx];
        }
    }
    particle.find_center_of_mass();

	// check for clamping behavior of the clustering code
	if(CLUSTER_MAX_VALUES_BEHAVIOR == IGNORE_ALL && (std::abs(particle.charge) > max_charge || particle.x_list.size() > max_size))
		return;
	else if(CLUSTER_MAX_VALUES_BEHAVIOR == CLAMP_SIZE && std::abs(particle.charge) > max_charge)
		return;
	else if(CLUSTER_MAX_VALUES_BEHAVIOR == CLAMP_CHARGE && particle.x_list.size() > max_size)
		return;

    particleList.push_back(particle);
/*
    // re-arrange the particles based on their distance to some reference point (optional)

    float px, py, pz;
    px = -box[working_offset * 6] / 2.0;
    py = -box[1 + working_offset * 6] / 2.0;
    pz = -box[2 + working_offset * 6] / 2.0;

    for (i = 0; i < nNumParticles; i++) {
        particleList[i].find_center_of_mass();
        particleList[i].Dist2(px, py, pz);
    }

    sort(particleList.begin(), particleList.end());
 */
}


// Gyration tensor and Asphericity parameter helper functions
void Clustering::rotate(float **matrix, int i, int j, int k, int l, float s, float tau)
{
    float g = matrix[i][j];
    float h = matrix[k][l];
    matrix[i][j] = g - s * (h + g * tau);
    matrix[k][l] = h + s * (g - h * tau);
}

// diagonalize a matrix
// return the eigenvalues in evalues and eigenvectors in evectors
#define MAXJACOBI 50
int Clustering::diagonalize(float **matrix, float *evalues, float **evectors)
{
    int i, j, k, iter;
    float tresh, theta, tau, t, sm, s, h, g, c, b[3], z[3];

    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++)
            evectors[i][j] = 0.0;
        evectors[i][i] = 1.0;
    }

    for (i = 0; i < 3; i++) {
        b[i] = evalues[i] = matrix[i][i];
        z[i] = 0.0;
    }

    for (iter = 1; iter <= MAXJACOBI; iter++) {
        sm = 0.0;
        for (i = 0; i < 2; i++)
            for (j=i+1; j<3; j++)
                sm += fabs(matrix[i][j]);
        if (sm == 0.0)
            return 0;

        if (iter < 4)
            tresh = 0.2 * sm / (3 * 3);
        else
            tresh = 0.0;

        for (i = 0; i < 2; i++) {
            for (j = i+1; j < 3; j++) {
                g = 100.0*fabs(matrix[i][j]);
                if (iter > 4 && fabs(evalues[i])+g == fabs(evalues[i])
                    && fabs(evalues[j])+g == fabs(evalues[j]))
                    matrix[i][j] = 0.0;
                else if (fabs(matrix[i][j]) > tresh) {
                    h = evalues[j] - evalues[i];
                    if (fabs(h)+g == fabs(h))
                        t = (matrix[i][j]) / h;
                    else {
                        theta = 0.5 * h / (matrix[i][j]);
                        t = 1.0 / (fabs(theta) + sqrt(1.0 + theta * theta));
                        if (theta < 0.0)
                            t = -t;
                    }

                    c = 1.0 / sqrt(1.0 + t * t);
                    s = t * c;
                    tau = s / (1.0 + c);
                    h = t * matrix[i][j];
                    z[i] -= h;
                    z[j] += h;
                    evalues[i] -= h;
                    evalues[j] += h;
                    matrix[i][j] = 0.0;

                    for (k = 0; k < i; k++)
                        rotate(matrix, k, i, k, j, s, tau);
                    for (k = i+1; k < j; k++)
                        rotate(matrix, i, k, k, j, s, tau);
                    for (k = j+1; k < 3; k++)
                        rotate(matrix, i, k, j, k, s, tau);
                    for (k = 0; k < 3; k++)
                        rotate(evectors, k, i, k, j, s, tau);
                }
            }
        }

        for (i = 0; i < 3; i++) {
            evalues[i] = b[i] += z[i];
            z[i] = 0.0;
        }
    }

    return 1;
}

void Clustering::center_of_mass(std::vector<float>* c, float* xm, const int period_wrapped)
{
    int i, nNumBeads;
    float dx, dy, dz, Lx2, Ly2, Lz2;
    float pivot[3];
    float* data;

    if(!c)
    {
        nNumBeads = nparticles;
        data = coordinates + working_offset * 3 * nparticles;
    }
    else
    {
        nNumBeads = c->size() / 3;
        data = c->data();
    }


    if (period_wrapped == 1) {
        Lx2 = box[0] / 2.0;
        Ly2 = box[1] / 2.0;
        Lz2 = box[2] / 2.0;

        pivot[0] = data[0];
        pivot[1] = data[nNumBeads];
        pivot[2] = data[nNumBeads * 2];

        // Release periodic boundary conditions for all beads in cluster
        for (i = 1; i < nNumBeads; i++) {
            dx = data[i] - pivot[0];
            dy = data[nNumBeads + i] - pivot[1];
            dz = data[nNumBeads * 2 + i] - pivot[2];

            if (dx > Lx2) data[i] -= box[0];
            else if (dx < -Lx2) data[i] += box[0];
            if (dy > Ly2) data[nNumBeads + i] -= box[1];
            else if (dy < -Ly2) data[nNumBeads + i] += box[1];
            if (dz > Lz2) data[nNumBeads * 2 + i] -= box[2];
            else if (dz < -Lz2) data[nNumBeads * 2 + i] += box[2];
        }
    }

    // Compute the center of mass
    xm[0] = xm[1] = xm[2] = 0.0;
    for (i = 0; i < nNumBeads; i++) {
        xm[0] += data[i];
        xm[1] += data[nNumBeads + i];
        xm[2] += data[nNumBeads * 2 + i];
    }

    xm[0] /= nNumBeads;
    xm[1] /= nNumBeads;
    xm[2] /= nNumBeads;
}

void Clustering::gyration_tensor(std::vector<float>* c, float* xm, int periodic_wrapped, float** gyration, float* R2, float* major_eigenvector)
{
    int i, j, k, nNumBeads;
    float* data;
    if(!c)
    {
        nNumBeads = nparticles;
        data = coordinates + working_offset * nparticles * 3;
    }
    else
    {
        nNumBeads = c->size()/3;
        data = c->data();
    }

    for (j = 0; j < 3; j++)
        for (k = 0; k < 3; k++)
            gyration[j][k] = 0.0;

    float xi[3], L[3];
    L[0] = box[0]; L[1] = box[1]; L[2] = box[2];
    for (i = 0; i < nNumBeads; i++) {
        xi[0] = data[i];
        xi[1] = data[i + nNumBeads];
        xi[2] = data[i + 2 * nNumBeads];
        for (j = 0; j < 3; j++) {
            float rj = xi[j] - xm[j];
            if (rj > L[j]/2)
                rj -= L[j];
            else if (rj < -L[j]/2)
                rj += L[j];

            for (k = 0; k < 3; k++) {
                float rk = xi[k] - xm[k];
                if (rk > L[k]/2)
                    rk -= L[k];
                else if (rk < -L[k]/2)
                    rk += L[k];

                gyration[j][k] += rj * rk; //(xi[j] - xm[j]) * (xi[k] - xm[k]);
            }
        }
    }

    for (j = 0; j < 3; j++)
        for (k = 0; k < 3; k++)
            gyration[j][k] /= nNumBeads;

    // Dummy eigen_vectors
    float** eigen_vectors = new float* [3];
    for (i = 0; i < 3; i++)
        eigen_vectors[i] = new float [3];

    // Diagonalize the gyration tensor, yielding the eigenvalues as squares of the radii of gyration
    diagonalize(gyration, R2, eigen_vectors);

    // find the maximum eignvalue
    float max = R2[0];
    int index = 0;
    for (i = 1; i < 3; i++) {
        if (max < R2[i]) {
            max = R2[i];
            index = i;
        }
    }

    major_eigenvector[0] = eigen_vectors[0][index];
    major_eigenvector[1] = eigen_vectors[1][index];
    major_eigenvector[2] = eigen_vectors[2][index];

    for (i = 0; i < 3; i++)
        delete [] eigen_vectors[i];
    delete [] eigen_vectors;
}

// Compute the asphericity parameter of a cluster
// return the asphericity paramter (As), radius of gyration (Rg), center of mass (xm)
float Clustering::asphericity_param(Cluster& cluster, float* major_eigenvector, float* R2, float& Rg, float* xm)
{
    int i;
    float As;
    float **gyration;

    gyration = new float* [3];
    for (i = 0; i < 3; i++)
        gyration[i] = new float [3];

    //BeadList beadsInCluster;
    std::vector<float> cluster_coordinates(3*cluster.size());
    for (i = 0; i < cluster.size(); i++)
        cluster_coordinates[i] = coordinates[cluster[i] + working_offset * nparticles * 3];
    cluster_coordinates[i + cluster.size()] = coordinates[cluster[i] + (working_offset * 3 + 1)* nparticles];
    cluster_coordinates[i + 2*cluster.size()] = coordinates[cluster[i] + (working_offset * 3 + 2)* nparticles];

    // Find the center of mass
    center_of_mass(&cluster_coordinates, xm, periodic_wrapped);

    // Compute the gyration tensor
    gyration_tensor(&cluster_coordinates, xm, periodic_wrapped, gyration, R2, major_eigenvector);

    As = ((R2[2] - R2[1]) * (R2[2] - R2[1]) +
          (R2[1] - R2[0]) * (R2[1] - R2[0]) +
          (R2[2] - R2[0]) * (R2[2] - R2[0])) /
         (2 * (R2[0] + R2[1] + R2[2]) * (R2[0] + R2[1] + R2[2]));

    Rg = sqrt(R2[0] + R2[1] + R2[2]);

    for (i = 0; i < 3; i++)
        delete [] gyration[i];
    delete [] gyration;

    return As;
}


// binning the clusters into the cells
int Clustering::binning(float xm, float ym, float zm)
{
    int nxy = nx * ny;
    float gridSizex, gridSizey, gridSizez;
    gridSizex = box[0 + working_offset * 6] / nx;
    gridSizey = box[1 + working_offset * 6] / ny;
    gridSizez = box[2 + working_offset * 6] / nz;

    int c = static_cast<int>((zm + 0.5*box[2 + working_offset * 6]) / gridSizez) * nxy +
            static_cast<int>((ym + 0.5*box[1 + working_offset * 6]) / gridSizey) * nx +
            static_cast<int>((xm + 0.5*box[0 + working_offset * 6]) / gridSizex);
    return c;
}


void Clustering::execute()
{
    //set visited indexes to zero
    std::fill_n(visited, nparticles, false);

    // reset the current grid
    memset_uint16_to_zero(buf_grid_sz + working_offset * grid_sz_sz, grid_sz_sz);
    memset_uint16_to_zero(buf_grid_charge + working_offset * grid_charge_sz, grid_charge_sz);

#ifdef DEBUGGING
    std::size_t random_index = rand() % nparticles;
    std::cout << " random particle before applying PBC (dimension 0) : " <<
                coordinates[(working_offset * 3 * nparticles) + random_index] << std::endl;
#endif

    // make sure all particles lie inside (-0.5, 0.5)
    for(auto dim = 0; dim != 3; dim++)
    {
        adjust_1dim(coordinates + (working_offset*3 + dim) *nparticles, box[dim + working_offset * 6], nparticles );
    }

#ifdef DEBUGGING
    std::cout << "box limits (first dimension) : ( -" << box[working_offset*6] / 2 << ", " << box[working_offset*6]/2 << ") " << std::endl;
    std::cout << "random particle position in that box : ";
    std::cout << coordinates[(working_offset * 3 * nparticles) + random_index] << std::endl;
#endif

    // calculate clusters
    //std::vector<Cluster> clusterList;
    find_clusters(clusterList);
	// memset the g(r) to zero
	memset(buf_gr_all + gr_all_sz * working_offset, 0, sizeof(float) * gr_all_sz);
	memset(buf_gr_sz + gr_sz_sz * working_offset, 0, sizeof(float) * gr_sz_sz);
	memset(buf_gr_charge + gr_charge_sz * working_offset, 0, sizeof(float) * gr_charge_sz);
	calculate_gofr();
	memset(buf_sofq, 0, sizeof(float) * dq_sz);
	calculate_sofq();
    return;
}

void Clustering::set_buffers(std::uint16_t* __restrict__ pgrid_charge, std::size_t _grid_charge_sz,
                             std::uint16_t* __restrict__ pgrid_sz, std::size_t _grid_sz_sz,
                             float* __restrict__ pdr, std::size_t _dr_sz,
                             float* __restrict__ pgr_sz, std::size_t _gr_sz_sz,
                             float* __restrict__ pgr_charge, std::size_t _gr_charge_sz,
                             float* __restrict__ pgr_all, std::size_t _gr_all_sz,
                             float* __restrict__ coord_workspace, std::size_t coord_size,
                             float* __restrict__ box_workspace, bool* __restrict__ _use_index,
                             std::int16_t* __restrict__ _charge_index)
{
    buf_grid_charge = (std::uint16_t*) __builtin_assume_aligned(pgrid_charge, 32);
    grid_charge_sz = _grid_charge_sz;

    buf_grid_sz = (std::uint16_t*) __builtin_assume_aligned(pgrid_sz, 32);
    grid_sz_sz = _grid_sz_sz;

    buf_dr = (float*) __builtin_assume_aligned(pdr, 32);
    dr_sz = _dr_sz;

    buf_gr_charge = (float*) __builtin_assume_aligned(pgr_charge, 32);
    gr_charge_sz = _gr_charge_sz;

    buf_gr_sz = (float*) __builtin_assume_aligned(pgr_sz, 32);
    gr_sz_sz = _gr_sz_sz;

    buf_gr_all = (float*) __builtin_assume_aligned(pgr_all, 32);
    gr_all_sz = _gr_all_sz;

    coordinates = coord_workspace;
    coordinates_sz = coord_size;

    box = box_workspace;
    use_idx = _use_index;
    charge_idx = _charge_index;

}

void Clustering::set_extra_buffers(std::uint16_t* __restrict__ p_tr_mat_sz, std::size_t _tr_mat_sz_sz,
                                   std::uint16_t* __restrict__ p_tr_mat_charge, std::size_t _tr_mat_charge_sz,
                                   int _matrix_type)
{
    buf_tr_mat_charge = (std::uint16_t*) __builtin_assume_aligned(p_tr_mat_charge, 32);
    tr_mat_charge_sz = _tr_mat_charge_sz;

    buf_tr_mat_sz = (std::uint16_t*) __builtin_assume_aligned(p_tr_mat_sz, 32);
    tr_mat_sz_sz = _tr_mat_sz_sz;

    matrix_type = _matrix_type;
}

void Clustering::calculate_gofr()
{
    /*
     * find the cutoff of g(r) then bin the space for every type of cluster + for additional particles
     */
    g_cutoff = buf_dr[dr_sz-1];
	float mdistsq = g_cutoff * g_cutoff;

#ifdef DEBUGGING
	std::cout << "invoking neighboring_gofr() with cutoff of " << g_cutoff << std::endl;
#endif

	neighboring_gofr();

#ifdef DEBUGGING
	std::cout << "calculating g(r) by looping over pairs" << std::endl;
#endif
	// loop over all clusters and then over its neighbours; this will double count all pairs i-j so only count if j > i
	for (std::size_t pidx = 0; pidx != particleList.size(); pidx++)
	{
		for (std::size_t o_lin_idx = 0; o_lin_idx != g_num_neigh[pidx]; o_lin_idx++)
		{
			std::size_t oidx = g_neighbors[pidx][o_lin_idx];

			if(pidx > oidx) // count only oidx > pidx to avoid double count
				continue;

			auto dx = particleList[pidx].xm - particleList[oidx].xm;
			auto dy = particleList[pidx].ym - particleList[oidx].ym;
			auto dz = particleList[pidx].zm - particleList[oidx].zm;

			dx -= box[0 + working_offset * 6] * anint(dx / box[0 + working_offset * 6]);
			dy -= box[1 + working_offset * 6] * anint(dy / box[1 + working_offset * 6]);
			dz -= box[2 + working_offset * 6] * anint(dz / box[2 + working_offset * 6]);
			auto rsq = dx*dx + dy*dy + dz*dz;
			if(rsq < mdistsq)
				gofr_idx1_idx2(pidx, oidx, std::sqrt(rsq));
		}
	}
}

void Clustering::calculate_sofq()
{
	coordinates[idx + working_offset * 3 *nparticles]; // X coordinate
	buf_dq[q_index + working_offset * dq_sz]; // qindexth q vector
	buf_sofq[q_result_index + working_offset * dq_sz]; // result to store in of the q_result_index
}

void Clustering::gofr_idx1_idx2(std::size_t idx_i, std::size_t idx_j, float rij)
{
	//resolve linear index into dr
	auto bin_sz = buf_dr[dr_sz - 1] / dr_sz;
	std::size_t linear_idx = static_cast<std::size_t>(rij / bin_sz);

	if(linear_idx == dr_sz) // edge case if sqrt(r_ij) = sqrt(bin_sz^2)
	{
		return;
	}

#ifdef DEBUGGING
	if(linear_idx >= dr_sz)
	{
		std::cout << " linear index : " << linear_idx << " larger than buffer size in gofr_idx1_idx2" << std::endl;
		std::abort();
	}
#endif

	//get g(r) buffer index
	std::size_t major_offset_all = working_offset * gr_all_sz;
	std::size_t major_offset_charge = working_offset * gr_charge_sz;
	std::size_t major_offset_size = working_offset * gr_sz_sz;

	std::size_t minor_offset_size = dr_sz * upper_triangular_ij_to_lin(particleList[idx_i].clamped_size - 1,
	                                                           particleList[idx_j].clamped_size - 1, max_size);
	std::size_t minor_offset_charge = dr_sz * upper_triangular_ij_to_lin(particleList[idx_i].clamped_charge + max_charge,
	                                                             particleList[idx_j].clamped_charge + max_charge, 2*max_charge + 1);


#ifdef DEBUGGING
	if(minor_offset_size/dr_sz > max_size * (max_size+1) / 2 || minor_offset_size < 0)
	{
		std::cout << "error in minor offset on size g(r)! minor offset : " << minor_offset_size << std::endl;
		std::cout << "supplied size - 1 : particle[idx_i] : " << particleList[idx_i].x_list.size() -1 << ", particle[idx_j]"<<
		 particleList[idx_j].x_list.size() << std::endl;
		std::abort();
	}
	if(minor_offset_charge/dr_sz > (2* max_charge + 1)*(max_charge+1) || minor_offset_charge < 0)
	{
		std::cout << "error in minor offset on charge g(r)! minor offset : " << minor_offset_charge << std::endl;
		std::cout << "supplied charge+max_charge : particle[idx_i] : " << particleList[idx_i].charge + max_charge << ", particle[idx_j]"<<
		 particleList[idx_j].charge + max_charge << std::endl;
		std::abort();
	}
#endif

	buf_gr_all[major_offset_all + linear_idx]++;
	buf_gr_charge[major_offset_charge + minor_offset_charge + linear_idx]++;
	buf_gr_sz[major_offset_size + minor_offset_size + linear_idx]++;
}

inline std::size_t Clustering::upper_triangular_ij_to_lin(std::size_t i, std::size_t j, std::size_t matrix_sz)
{
	/*
	 * takes the (i,j) index of an upper triangular matrix and returns its linear index assuming we dont count the lower
	 * triangular ones
	 *
	 * shamelessly stolen from Robert T. McGibbon at D. E. Shaw research, found at
	 * http://stackoverflow.com/questions/27086195/linear-index-upper-triangular-matrix
	 */
	return (matrix_sz * (matrix_sz + 1) / 2) - (matrix_sz-i)*(1+matrix_sz-i)/2 + j - i;
}