/*
  generic memory management functions
  support aligned memory allocation
*/

#ifndef __MEMORY
#define __MEMORY


#define MEMALIGN 64

template <typename TYPE>
TYPE* create(TYPE*& array, int nsize)
{
  if (nsize == 0) return NULL;
  int64_t nbytes = nsize * sizeof(TYPE);
#if defined(MEMALIGN)
  void* ptr;
  int error = posix_memalign(&ptr, MEMALIGN, nbytes);
  if (error) array = NULL;
  else array = (TYPE*) ptr;
#else
  array = (TYPE*) malloc(nbytes);
#endif
  return array;
}

template <typename TYPE>
void destroy(TYPE*& array)
{
  if (array == NULL) return;
  free(array);
  array = NULL;
}

template <typename TYPE>
TYPE** create(TYPE**& array, int row, int col)
{
    std::size_t nbytes = (row*col) * sizeof(TYPE);
  if (nbytes == 0) return NULL;

  TYPE* data;
#if defined(MEMALIGN)
  void* ptr;
  int error;
  error = posix_memalign(&ptr, MEMALIGN, nbytes);
  if (error) data = NULL;
  else data = (TYPE*) ptr;
#else
  data = (TYPE*) malloc(nbytes);
#endif

  int np = row * sizeof(TYPE*);
#if defined(MEMALIGN)
  error = posix_memalign(&ptr, MEMALIGN, np);
  if (error) array = NULL;
  else array = (TYPE**) ptr;
#else
  array = (TYPE**) malloc(np);
#endif

  int n = 0;
  for (int i = 0; i < row; i++) {
    array[i] = &data[n];
    n += col;
  }
  return array;
}

template <typename TYPE>
void destroy(TYPE**& array)
{
  if (array == NULL) return;
  free(array[0]);
  free(array);
  array = NULL;
}



#endif
