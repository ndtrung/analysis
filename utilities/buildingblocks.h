#ifndef __BUILDING_BLOCKS
#define __BUILDING_BLOCKS

#include <string.h>
#include <vector>
#include <cstdint>
using namespace std;

namespace Clusters {


    typedef vector<int> Cluster;

    class Particle
    {
        friend bool operator< (const Particle& p1, const Particle& p2);
        const float* box_ptr;
    public:
        Particle(float* box = nullptr) {box_ptr = box;}
        Particle(const Particle&) = default;
        Particle& operator= (const Particle&) = default;


        void Dist2(float px, float py, float pz);
        void find_center_of_mass();

        std::vector<float> x_list, y_list, z_list;
        std::uint16_t clamped_size = 0;
        std::int16_t clamped_charge = 0;
        std::int16_t charge = 0;
        float xm = 0.0f;
        float ym = 0.0f;
        float zm = 0.0f;
        float dist2 = 0.0f;
    };

} // namespace Clusters

#endif
