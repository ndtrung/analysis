// Created by martin on 10/31/16.
//

#ifndef IONSCLUSTERS_SSEINSTRUCTIONS_HPP
#define IONSCLUSTERS_SSEINSTRUCTIONS_HPP

#include <x86intrin.h>
#include <cstdint>
#include <cstdlib>

// sums 2 uint16 arrays : array_to = array_to + array_from, must satisfy array_sz % SSE_sz = 0 (8 on AVX, 16 on AVX2)
void sumuint16_from_to( std::uint16_t* __restrict__ array_from,
                        std::uint16_t* __restrict__ array_into,  std::size_t array_sz);

// sums 2 float arrays : array_to = array_to + array_from, must satisfy array_sz % SSE_sz = 0 (4 on AVX, 8 on AVX2)
void sumf_from_to( float* __restrict__ array_from,  float* __restrict__ array_into,  std::size_t array_sz);

// divides the array by a  uint16, must satisfy array_sz % SSE
void float_div_by_int( float* __restrict__ array,  std::uint16_t val,  std::size_t array_sz);

// divides the uint16 array by a  uint16, into floa, must satisfy (size(array) = array_sz) % SSE_sz = 0
void int_div_by_int_cast( std::uint16_t* __restrict__ array,  float* __restrict__ array_return,
                          std::uint16_t val,  std::size_t array_sz);

void memset_uint16_to_zero(std::uint16_t* __restrict__ array, std::size_t size);

// adjust dimensions to PBC, requires the array to be aligned but bit dim_array itself
void adjust_1dim(float* __restrict__ dim_array, float dim_length, const std::size_t arr_sz);

// normalizes g(r) by 4 pi r^2 dr rho
void adjust_gr(float* __restrict__ gr_vec, float* __restrict__ r_vec, float av_density, float dr, std::size_t array_length);

#endif //IONSCLUSTERS_SSEINSTRUCTIONS_HPP
