//
// Created by martin on 10/19/16.
//

#ifndef IONSCLUSTERS_XMLPARSER_HPP
#define IONSCLUSTERS_XMLPARSER_HPP

#include <vector>
#include <cstdio>
#include <iostream>

class HOOMD_XML_parser
{
    FILE* ptr_file;
    bool is_reading(){return ptr_file != nullptr;}
public:
    int open_XML(const char* file_name);
    std::vector<std::string> get_attr_list(std::string attr);
    int close_XML();
    ~HOOMD_XML_parser()
    {
        if(is_reading()){close_XML();}
    }
};

#endif //IONSCLUSTERS_XMLPARSER_HPP
