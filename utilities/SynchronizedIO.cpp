//
// Created by martin on 10/20/16.
//

#include "SynchronizedIO.hpp"
#include <x86intrin.h>
#include "SSEInstructions.hpp"
#include <cassert>
#include <thread>
#include <cstdint>
#include <limits>
#include <cstdio>
#include <iostream>
#include "clustering.h"

batch_cluster_handle::batch_cluster_handle(ClusterFile _file, std::size_t n_threads, cluster_options _opts, std::vector<IonClusters> *outptr)
{
    /*
     * Param : ClusterFile _file
     *
     * enum file_type MD_TYPE : indicates how to parse the topology and trajectory
     *
     * char* paths : paths to topology and trajectory files (only trajectory in the case of GSD files)
     *
     *
     *
     * Param : std::size_t n_threads : how many threads to use for this file calculation
     *
     *
     *
     * cluster_options _opts
     *
     * std::vector<std::string> ion_types : which types to use for cluster calculations
     *
     * std::uint32_t first_frame : how many frames to skip initially (to skip equilibration data if any)
     *
     * std::uint16_t grid_size[3] : number of grid decompositions in each dimension for the spatial distribution of clusters
     *
     * std::vector<float> dr_X_vector : bin positions for the g(r) calculation of X. X can be by cluster size, cluster charge or all clusters
     *
     * float unit_charge : unit charge in the system. Charge is calculated as integer by using std::lround(charge / unit_charge)
     *
     * std::uint8_t : max_cluster_charge : maximum cluster charge to consider. For instance setting this to 5 will
     * group all clusters of charge of 5 or greater together (and everything smaller or equal to -5)
     *
     * std::uint8_t : max_cluster_size : maximum cluster size to consider. cluster with size greater or equal to this
     * will be grouped together
     *
     * std::vector<std::string> update_on_coord_change_type : the code will watch whenever the positions associated with
     * these types change during simulation. On any change from a previous frame, it will calculate averages for the current
     * set of frames, push it out and reset the data. Will also skip N_skip_on_update frames when doing so
     *
     * std::uint32_t N_skip_on_update : number of frames to skip when the positions defined by update_on_coord_change_type
     * are changed.
     *
     *
     *
     * the object uses a large buffer as workspace for the calculation of clusters. Each memory pointer has nthreads + 1
     * frames of memory. The buffers use cyclic indexes. Since we want to calculate lifetimes we need an additional previous
     * frame which is why we have nthreads + 1 frames.
     *
     * For exemple, assuming we have 4 threds, charging the positions will set them as :
     *
     * frame 0, frame 1, frame 2, frame 3, empty
     *
     * and the second load will set them as :
     *
     * frame 5, frame 6, frame 7, frame 3, frame 4
     *
     * Calculation of cluster properties is done in parallel to the load from file. Reason behind this is that we may be
     * required to stop calculation and output the current averages in the case where some coordinates have changed,
     * as specified by _opts.update_on_coord_change
     *
     * positions in coord_workspace are laid out in DCD format, i.e., [x0, x1, ..., xN, y0, y1, ..., yN, z0, z1, ..., zN]
     * so that the value for index idx, dimension d and frame offset fo is given by
     *
     * *coord_workspace[fo * unit_coord_workspace_sz + d * const_vals.n_particles + idx]
     *
     * the use_index values are set to either true or false depending to whether they correspond to an ion type we are
     * interested in. They have corresponding charge_index array.
     *
     * all worspace data is 32-aligned to make use of AVX registers (with the exception of charge_index & use_index).
     *
     */

    std::cout << "checking array sizes, ";
    // to align multiple arrays of uint16 on 32 byte boundary will require a multiple of 16 ( 16 * 16b = 32B )
    prod_grid_size = _opts.grid_size[0] * _opts.grid_size[1] * _opts.grid_size[2];
    if((prod_grid_size%16))
    {
        valid_grid_sz = false;
        return;
    }
    else
    {
        valid_grid_sz = true;
    }
    std::cout << "found " << prod_grid_size << " points on grid ";

    grids[0] = _opts.grid_size[0];
    grids[1] = _opts.grid_size[1];
    grids[2] = _opts.grid_size[2];
	nthreads = n_threads;

    // all float grids are aligned too, 16 * 32b = 32B
    if(_opts.nbins_gofr_calculation % 16)
    {
        valid_dr_sz = false;
        return;
    }
    else
    {
        valid_dr_sz = true;
    }
    std::cout << "and multiple of 16 number of points on g(r) calculations " << std::endl;

    if(!outptr)
    {
        valid_files = false;
        last_error_code = -100;
        return;
    }

    cluster_cutoff = _opts.clustering_cutoff;
    if(cluster_cutoff < 0.0f){last_error_code = -101; return;}
    output_ptr = outptr;
    MD_type = _file.MD_type;
    transition_matrix_type = _opts.transition_matrix;

    std::cout << "Parsing topology file " << std::endl;

    int parsing_fail = parse_topology(_file, _opts);
    if(parsing_fail)
    {
        valid_files = false;
        return;
    }

	max_cluster_charge = _opts.max_cluster_charge;
	max_cluster_size = _opts.max_cluster_size;
	std::cout << "batch objects has limits of " << max_cluster_charge << " for |charge| and " << max_cluster_size << " for size " << std::endl;

	update_mode = _opts.update_coord_type;

    std::cout << "parsed files, found " << const_vals.n_particles << " coordinates per frame " << std::endl;
    std::cout << "allocating memory for gofr calculating and copying r arrays over all thread frames " << std::endl;

	/*
	 * MEMORY MANAGEMENT FOR GRIDS
	 */

	// grid charge
    unit_grid_charge_workspace_sz = prod_grid_size * (2*_opts.max_cluster_charge + 1);
    grid_charge_workspace = (std::uint16_t*) _mm_malloc(sizeof(std::uint16_t)*unit_grid_charge_workspace_sz * (nthreads + 1), 32);
    memset_uint16_to_zero(grid_charge_workspace, unit_grid_charge_workspace_sz * (nthreads+1));

    grid_charge_av = (std::uint16_t*) _mm_malloc(sizeof(std::uint16_t)*unit_grid_charge_workspace_sz, 32);
    memset_uint16_to_zero(grid_charge_av, unit_grid_charge_workspace_sz);

    // grid size
    unit_grid_sz_workspace_sz = prod_grid_size * (_opts.max_cluster_size);
    grid_sz_workspace = (std::uint16_t*) _mm_malloc(sizeof(std::uint16_t)*unit_grid_sz_workspace_sz * (nthreads + 1), 32);
    memset_uint16_to_zero(grid_sz_workspace, unit_grid_sz_workspace_sz * (nthreads+1));

    grid_sz_av = (std::uint16_t*) _mm_malloc(sizeof(std::uint16_t)*unit_grid_sz_workspace_sz, 32);
    memset_uint16_to_zero(grid_sz_av, unit_grid_sz_workspace_sz);


	/*
	 * MEMORY MANAGEMENT FOR PAIR CORRELATION FUNCTIONS
	 *  calculate vector of r from max r value and # of bins
	 */
	unit_dr_all_sz = _opts.nbins_gofr_calculation;
	dr_all_workspace = (float*) _mm_malloc(sizeof(float) * unit_dr_all_sz * (nthreads + 1), 32);
	float bin_sz = _opts.max_r_value_gofr_calculation / _opts.nbins_gofr_calculation;

	for(std::size_t th_idx = 0; th_idx != nthreads + 1; th_idx++)
	{
		dr_all_workspace[th_idx * unit_dr_all_sz] = bin_sz / 2.0f;
		for(std::size_t el_idx = 1; el_idx != unit_dr_all_sz; el_idx++)
		{
			dr_all_workspace[th_idx * unit_dr_all_sz + el_idx] = dr_all_workspace[th_idx * unit_dr_all_sz + el_idx - 1] + bin_sz;
		}
	}

	unit_gr_all_workspace_sz = unit_dr_all_sz;
	gr_all_workspace = (float*) _mm_malloc(sizeof(float) * unit_gr_all_workspace_sz * (nthreads+1), 32);
	gr_all_av = (float*) _mm_malloc(sizeof(float) * unit_gr_all_workspace_sz, 32);

    unit_gr_sz_workspace_sz = unit_dr_all_sz * _opts.max_cluster_size * (_opts.max_cluster_size + 1) / 2;
    gr_sz_workspace = (float*) _mm_malloc(sizeof(float) *unit_gr_sz_workspace_sz * (nthreads + 1), 32);
    gr_sz_av = (float*) _mm_malloc(sizeof(float) *unit_gr_sz_workspace_sz, 32);

    unit_gr_charge_workspace_sz = unit_dr_all_sz * (2*_opts.max_cluster_charge + 1) * (2*_opts.max_cluster_charge + 2) /2;
    gr_charge_workspace = (float*) _mm_malloc(sizeof(float) * unit_gr_charge_workspace_sz * (nthreads + 1), 32);
    gr_charge_av = (float*) _mm_malloc(sizeof(float) * unit_gr_charge_workspace_sz, 32);


	/*
	 * MEMORY MANAGEMENT FOR TRANSITION MATRIXES
	 *
	 * minimum size on individual grids is 16, if greater, raise to next power of 2, if single transition matrix
	 */

    if(transition_matrix_type == AVERAGE)
    {
        tr_matrix_charge_spatial_sz = 16;
        while(tr_matrix_charge_spatial_sz < (2*max_cluster_charge + 1) * (2*max_cluster_charge + 1)) tr_matrix_charge_spatial_sz *= 2;
        tr_matrix_sz_spatial_sz = 16;
        while(tr_matrix_sz_spatial_sz < max_cluster_size * max_cluster_size) tr_matrix_sz_spatial_sz *= 2;

        tr_matrix_charge_sz = tr_matrix_charge_spatial_sz;
        tr_matrix_sz_sz = tr_matrix_sz_spatial_sz;

        tr_matrix_charge_workspace = (std::uint16_t*) _mm_malloc(sizeof(std::uint16_t) * tr_matrix_charge_sz * (nthreads+1), 32);
        tr_matrix_sz_workspace = (std::uint16_t*) _mm_malloc(sizeof(std::uint16_t) * tr_matrix_sz_sz * (nthreads + 1), 32);

        tr_matrix_charge_av = (std::uint16_t*) _mm_malloc(sizeof(std::uint16_t) * tr_matrix_charge_sz, 32);
        tr_matrix_sz_av = (std::uint16_t*) _mm_malloc(sizeof(std::uint16_t) * tr_matrix_sz_sz, 32);
        std::cout << " transition matrix in average mode " << std::endl;

    }
    else if (transition_matrix_type == SPATIAL)
    {
        tr_matrix_charge_spatial_sz = 16;
        while(tr_matrix_charge_spatial_sz < (2*max_cluster_charge + 1) * (2*max_cluster_charge + 1)) tr_matrix_charge_spatial_sz *= 2;
        tr_matrix_sz_spatial_sz = 16;
        while(tr_matrix_sz_spatial_sz < max_cluster_size * max_cluster_size) tr_matrix_sz_spatial_sz *= 2;

        tr_matrix_charge_sz = tr_matrix_charge_spatial_sz * prod_grid_size;
        tr_matrix_sz_sz = tr_matrix_sz_spatial_sz * prod_grid_size;

        tr_matrix_charge_workspace = (std::uint16_t*) _mm_malloc(sizeof(std::uint16_t) * tr_matrix_charge_sz * (nthreads+1), 32);
        tr_matrix_sz_workspace = (std::uint16_t*) _mm_malloc(sizeof(std::uint16_t) * tr_matrix_sz_sz * (nthreads + 1), 32);

        tr_matrix_charge_av = (std::uint16_t*) _mm_malloc(sizeof(std::uint16_t) * tr_matrix_charge_sz, 32);
        tr_matrix_sz_av = (std::uint16_t*) _mm_malloc(sizeof(std::uint16_t) * tr_matrix_sz_sz, 32);
        std::cout << " transition matrix in spatial mode " << std::endl;
    }
    else
    {
        std::cout << "no transition matrix being computed" << std::endl;
    }

    /*
     * MANAGEMENT OF CLUSTERING OBJECTS
     * construct cluster calculation objects and assign frame numbers
     */

    cluster_calculators.resize(nthreads);
    std::cout << "creating clustering calculator objects " << std::endl;
    for (std::size_t thidx = 0; thidx != nthreads; thidx++)
    {
        cluster_calculators[thidx] = new Clusters::Clustering(cluster_cutoff, 1, 1, 1, 0, 1,
                                                              const_vals.n_particles, grids[0], grids[1], grids[2]);
        cluster_calculators[thidx]->set_buffers(
                grid_charge_workspace, unit_grid_charge_workspace_sz,
                grid_sz_workspace, unit_grid_sz_workspace_sz,
                dr_all_workspace, unit_dr_all_sz,
                gr_sz_workspace, unit_gr_sz_workspace_sz,
                gr_charge_workspace, unit_gr_charge_workspace_sz,
                gr_all_workspace, unit_gr_all_workspace_sz,
                coord_workspace, unit_coord_workspace_sz,
                box_workspace, use_index, charge_index
        );

        cluster_calculators[thidx]->set_limits(max_cluster_charge, max_cluster_size);
        if(transition_matrix_type == SPATIAL)
        {
            cluster_calculators[thidx]->set_extra_buffers(tr_matrix_sz_workspace, tr_matrix_sz_sz,
                                                          tr_matrix_charge_workspace, tr_matrix_charge_sz, 2);
        }
        else if(transition_matrix_type == AVERAGE)
        {
            cluster_calculators[thidx]->set_extra_buffers(tr_matrix_sz_workspace, tr_matrix_sz_sz,
                                                          tr_matrix_charge_workspace, tr_matrix_charge_sz, 1);
        }
    }

    // allocate frame # and set the last one to -1 (overflow uint to 0)
    frame_number = (std::uint32_t*) _mm_malloc(sizeof(std::uint32_t) * (nthreads+1), 32);
    file_identifier = _opts.ID;
    reset_state();
    std::cout << "finished initializing batch object for file " << _opts.ID << std::endl;
}

int batch_cluster_handle::parse_topology(ClusterFile _file, cluster_options _opts)
{
    if(MD_type == XML_FILE)
    {
        handle_xml = new HOOMD_XML_parser();
        if(last_error_code = handle_xml->open_XML(_file.topology_filepath.c_str()), last_error_code)
        {
            valid_files = false;
            return -10;
        }

        std::cout << "Reading particle types from XML" << std::endl;
        std::vector<std::string> _topology_types = handle_xml->get_attr_list("type");
        if (!_topology_types.size())
        {
            last_error_code = -11;
            valid_files = false;
            return -11;
        }

        std::cout << "Reading particle charges from XML" << std::endl;
        std::vector<std::string> _topology_charges = handle_xml->get_attr_list("charge");
        if(!_topology_charges.size() || _topology_charges.size() != _topology_types.size())
        {
            last_error_code = -12;
            valid_files = false;
            return -12;
        }
        const_vals.n_particles = _topology_charges.size();


        use_index = (bool*) malloc(sizeof(bool)*const_vals.n_particles);
        charge_index = (std::int16_t *) malloc(sizeof(std::int16_t) * const_vals.n_particles);

        /*
         * match the return  update checks with the topology string
         */
        std::cout << "matching updating values to parsed types" << std::endl;
        std::vector< std::size_t > update_idx;
        for(auto upt_idx = 0; upt_idx != _opts.update_on_coord_change_type.size(); upt_idx++)
        {
            for (std::size_t type_idx = 0; type_idx != _topology_types.size(); type_idx++)
            {
                if (_topology_types[type_idx] == _opts.update_on_coord_change_type[upt_idx])
                {
                    update_idx.push_back(type_idx);
                }
            }
        }
        n_updates_on_change = update_idx.size();

        std::cout << "found " << n_updates_on_change << " particles to check for data push " << std::endl;

        if(n_updates_on_change > 0)
        {
            m_p_update_on_change = (float *) _mm_malloc(n_updates_on_change*sizeof(float)*3, 32);
            update_on_change_idx = (std::uint32_t *) _mm_malloc(n_updates_on_change*sizeof(std::uint32_t), 32);
        }
        else
        {
            m_p_update_on_change = nullptr;
        }
        // fill the index array
        for(std::size_t idx = 0; idx != update_idx.size(); idx++)
        {
            update_on_change_idx[idx] = update_idx[idx];
        }

        /*
         *  Initialize vector of useful indexes for computation as well as their charges
         */

        std::cout << "matching supplied clustering types to parsed types" << std::endl;
        for(std::size_t idx = 0; idx != const_vals.n_particles; idx++)
        {
            bool check = false;
            for(std::size_t idxc = 0; idxc != _opts.ions_types.size(); idxc++)
            {
                check = check or _opts.ions_types[idxc] == _topology_types[idx];
            }
            if(check)
            {
                use_index[idx] = true;
                charge_index[idx] = (std::int16_t) std::lround(std::strtof(_topology_charges[idx].c_str(), nullptr)/_opts.unit_charge);
            }
            else
            {
                use_index[idx] = false;
            }
        }

        std::cout << "opening DCD file at " << _file.trajectory_filepath << std::endl;
        handle_dcd = new DCDHandle();
        if(last_error_code = handle_dcd->open_DCD(_file.trajectory_filepath.c_str()), last_error_code)
        {
            valid_files = false;
            return -20;
        }
        else
        {
            valid_files = true;
        }
        std::cout << "DCD file has " << handle_dcd->get_nframes() << " frames " << std::endl;
        const_vals.n_frames = handle_dcd->get_nframes();
        box_workspace = (float*) _mm_malloc(sizeof(float) * 6 * (nthreads + 1), 32);
        coord_workspace = (float*) _mm_malloc(sizeof(float) * const_vals.n_particles * 3 * (nthreads + 1), 32);
        unit_coord_workspace_sz = const_vals.n_particles * 3;

        //TODO : add checks for goto & get frame

        handle_dcd->get_frame(box_workspace, coord_workspace, coord_workspace + const_vals.n_particles, coord_workspace + 2 * const_vals.n_particles);
        handle_dcd->goto_frame(static_cast<std::size_t>(_opts.first_frame));
        handle_dcd->get_frame(box_workspace, coord_workspace, coord_workspace + const_vals.n_particles, coord_workspace + 2 * const_vals.n_particles);
        std::cout << " moved to supplied initial frame, found box " << box_workspace[0] << " X " << box_workspace[1] << " X " << box_workspace[2] << " at frame " << handle_dcd->get_current_frame() << std::endl;

        for(std::size_t idx = 0; idx != n_updates_on_change; idx++)
        {
            m_p_update_on_change[idx] = coord_workspace[update_on_change_idx[idx]];
            m_p_update_on_change[idx + n_updates_on_change] = coord_workspace[update_on_change_idx[idx] + const_vals.n_particles];
            m_p_update_on_change[idx + 2*n_updates_on_change] = coord_workspace[update_on_change_idx[idx] + 2*const_vals.n_particles];
        }

    }
    else if (MD_type == GSD_FILE)
    {
        // TODO : parse topology here from GSD and set read_frame function
    }
    else if(MD_type == LAMMPS_FILE)
    {
        // we have a dump file for topology and DCD for trajectory

        handle_lammps = new dumpParser();
        if(last_error_code = handle_lammps->open_dump(_file.topology_filepath.c_str()), last_error_code)
        {
            valid_files = false;
            return -21;
        }

        std::cout << "Reading attribute type from topology file" << std::endl;

        std::vector<std::string> _topology_types = handle_lammps->get_attr_list("type", last_error_code);
        if(last_error_code)
        {
            valid_files = false;
            std::cout << "error while getting particle types, code : " << last_error_code << std::endl;
            return -22;
        }
        if(_topology_types.size() == 0)
        {
            valid_files = false;
            std::cout << "got empty topology file ? " << std::endl;
            return -23;
        }

        std::vector<std::string> _topology_charges  = handle_lammps->get_attr_list("q", last_error_code);
        if(last_error_code == 1)
        {
            /*
            valid_files = false;
            std::cout << "error while getting particle charges, code : " << last_error_code << std::endl;
            return;
             */
            _topology_charges.resize(_topology_types.size());
            std::cout << "Warning : did not find charges in lammps dump file (searched for attribute q), undefined charges " << std::endl;
            last_error_code = 0;
        }

        std::vector<std::uint32_t> _absolute_index(_topology_types.size());
        std::vector<std::string> _topology_ID = handle_lammps->get_attr_list("ID", last_error_code);
        if(last_error_code == 1) // ID not supplied, assume linear indexing, set _abs_idx to iota
        {
            std::cout << "Warning : empty topology ID numbers, will assume linear indexing" << std::endl;
            last_error_code = 0;
            std::iota(_absolute_index.begin(), _absolute_index.end(), 0);
        }
        else // change all idx string to ints
        {
            for (auto i = 0; i != _absolute_index.size(); i++)
            {
                _absolute_index[i] = std::strtoul(_topology_ID[i].c_str(), nullptr, 10) - 1;
            }
            /*
             * double check that theres no crap in abs indexes
             */
            bool *_chk_vars = (bool *) malloc(sizeof(bool) * _topology_ID.size());
            std::fill_n(_chk_vars, _chk_vars + _topology_ID.size(), false);
            for (auto i = 0; i != _absolute_index.size(); i++)
            {
                if (_absolute_index[i] < _absolute_index.size() && !_chk_vars[_absolute_index[i]])
                {
                    _chk_vars[_absolute_index[i]] = true;
                }
                else
                {
                    free(_chk_vars);
                    valid_files = false;
                    std::cout << "Duplicate ID found in topology or ID larger than # particles " << std::endl;
                    return -30;
                }
            }
            free(_chk_vars);
        }

        const_vals.n_particles = _absolute_index.size();


        use_index = (bool*) malloc(sizeof(bool) * const_vals.n_particles);
        charge_index = (std::int16_t*) malloc(sizeof(std::int16_t) * const_vals.n_particles);

        for(auto i = 0; i != const_vals.n_particles; i++)
        {
            bool valid = false;
            for(auto ii = 0; ii != _opts.ions_types.size(); ii++)
            {
                if(!strcmp(_opts.ions_types[ii].c_str(), _topology_types[_absolute_index[i]].c_str()))
                {
                    valid = true;
                    break;
                }
            }
            if(valid)
            {
                use_index[i] = true;
                charge_index[i] = static_cast<std::int16_t>(std::roundl(std::strtof(_topology_charges[_absolute_index[i]].c_str(), nullptr) / _opts.unit_charge));
            }
            else
            {
                use_index[i] = false;
                charge_index[i] = 0;
            }
        }

        std::cout << " matching update types to parsed types" << std::endl;
        std::vector<std::size_t> update_idx;

        for(auto udt_idx = 0; udt_idx != _opts.update_on_coord_change_type.size(); udt_idx++)
        {
            for(auto t_idx = 0; t_idx != const_vals.n_particles; t_idx++)
            {
                if(!strcmp(_opts.update_on_coord_change_type[udt_idx].c_str(), _topology_types[_absolute_index[t_idx]].c_str()))
                {
                    update_idx.push_back(t_idx);
                }
            }
        }

        n_updates_on_change = update_idx.size();

        std::cout << "found " << n_updates_on_change << " particles to check for data push " << std::endl;

        if(n_updates_on_change > 0)
        {
            m_p_update_on_change = (float *) _mm_malloc(n_updates_on_change*sizeof(float)*3, 32);
            update_on_change_idx = (std::uint32_t *) _mm_malloc(n_updates_on_change*sizeof(std::uint32_t), 32);
        }
        else
        {
            m_p_update_on_change = nullptr;
        }
        // fill the index array
        for(std::size_t idx = 0; idx != update_idx.size(); idx++)
        {
            update_on_change_idx[idx] = update_idx[idx];
        }

        handle_dcd = new DCDHandle();
        if(last_error_code = handle_dcd->open_DCD(_file.trajectory_filepath.c_str()), last_error_code)
        {
            valid_files = false;
            return -31;
        }
        else
        {
            valid_files = true;
        }
        std::cout << "DCD file has " << handle_dcd->get_nframes() << " frames " << std::endl;
        const_vals.n_frames = handle_dcd->get_nframes();
        box_workspace = (float*) _mm_malloc(sizeof(float) * 6 * (nthreads + 1), 32);
        coord_workspace = (float*) _mm_malloc(sizeof(float) * const_vals.n_particles * 3 * (nthreads + 1), 32);
        unit_coord_workspace_sz = const_vals.n_particles * 3;

        //TODO : add checks for goto & get frame

        handle_dcd->get_frame(box_workspace, coord_workspace, coord_workspace + const_vals.n_particles, coord_workspace + 2 * const_vals.n_particles);
        handle_dcd->goto_frame(static_cast<std::size_t>(_opts.first_frame));
        handle_dcd->get_frame(box_workspace, coord_workspace, coord_workspace + const_vals.n_particles, coord_workspace + 2 * const_vals.n_particles);
        std::cout << " moved to supplied initial frame, found box " << box_workspace[0] << " X " << box_workspace[1] << " X " << box_workspace[2] << " at frame " << handle_dcd->get_current_frame() << std::endl;

        for(std::size_t idx = 0; idx != n_updates_on_change; idx++)
        {
            m_p_update_on_change[idx] = coord_workspace[update_on_change_idx[idx]];
            m_p_update_on_change[idx + n_updates_on_change] = coord_workspace[update_on_change_idx[idx] + const_vals.n_particles];
            m_p_update_on_change[idx + 2*n_updates_on_change] = coord_workspace[update_on_change_idx[idx] + 2*const_vals.n_particles];
        }
    }
    else
    {
        throw std::runtime_error("unimplemented parser");
    }
    return 0;
}

int batch_cluster_handle::memory_load_frames()
{
    std::thread threads[nthreads]; // thread object for parallel execution
    bool has_update = false; // need to update
    std::size_t max_thread = 0; // largest thread idx that sucessfully ran

    int dcd_status = 0; // value is 0 if the dcd file is still reading

    if(MD_type==XML_FILE || MD_type == LAMMPS_FILE)
    {
        for(std::size_t thidx = 0; thidx != nthreads; thidx++)
        {
            dcd_status = handle_dcd->get_frame(
                    box_workspace + (6 * cyclic_idx_memory_load),
                    coord_workspace + (unit_coord_workspace_sz * cyclic_idx_memory_load),
                    coord_workspace + (unit_coord_workspace_sz * cyclic_idx_memory_load + const_vals.n_particles),
                    coord_workspace + (unit_coord_workspace_sz * cyclic_idx_memory_load + const_vals.n_particles * 2));
            bool chk_updates = true;

            for(std::size_t chk_idx = 0; chk_idx != n_updates_on_change; chk_idx++)
            {
                chk_updates = chk_updates &&
                              m_p_update_on_change[chk_idx] == coord_workspace[update_on_change_idx[chk_idx]+unit_coord_workspace_sz * cyclic_idx_memory_load];
                chk_updates = chk_updates &&
                              m_p_update_on_change[chk_idx + n_updates_on_change] == coord_workspace[update_on_change_idx[chk_idx]+unit_coord_workspace_sz * cyclic_idx_memory_load + const_vals.n_particles];
                chk_updates = chk_updates &&
                              m_p_update_on_change[chk_idx + 2*n_updates_on_change] == coord_workspace[update_on_change_idx[chk_idx]+unit_coord_workspace_sz * cyclic_idx_memory_load + 2 * const_vals.n_particles];
            }

            /*
             * lammps box decomposition is not necessarily [-0.5, 0.5]. adjust this into first image [-0.5, 0.5];
             */



            if(chk_updates && !dcd_status)
            {
                n_avg_traj_frames++;
                cluster_calculators[thidx]->set_offsets(cyclic_idx_memory_load, (cyclic_idx_memory_load -1 + (nthreads+1)) % (nthreads+1));
                threads[thidx] = std::thread(Clusters::Clustering::wrapper_execute, cluster_calculators[thidx], thidx);
#ifdef DEBUGGING
                threads[thidx].join();
#endif
                max_thread++;
            }
            else // there's been a change in the update check, we have to finish current set of calculation and update values
            {
                if(verbose) std::cout << "detected change in tracked positions" << std::endl;
                has_update = true;
                break;
            }

            cyclic_idx_memory_load = (cyclic_idx_memory_load + 1) % (nthreads + 1);
        }

#ifndef DEBUGGING
        if(verbose) std::cout << "joining threads" << std::endl;
        for(std::size_t thidx = 0; thidx != max_thread; thidx++)
        {
            if(verbose) std::cout << "joining thread index : " << thidx << " out of " << max_thread << std::endl;
            threads[thidx].join();
        }
#endif

        if(verbose) std::cout << "joined threads " << std::endl;
    }
    else
    {
        // TODO : other parsers
    }

    if(handle_dcd)
    {
        current_traj_frame = handle_dcd->get_current_frame();
    }
    else if (handle_gsd)
    {
        current_traj_frame = static_cast<std::uint32_t>(handle_gsd->cur_frame);
    }

    if(has_update)
    {

        compile_current_data(max_thread);
        push_data_out();
        if(handle_dcd->get_current_frame() == handle_dcd->get_nframes()) // file is finished
        {
            if(verbose) std::cout << " end of DCD file reached" << std::endl;
            return -1;
        }
        if(dcd_status)
        {
            if(verbose) std::cout << "DCD file returned error # " << dcd_status << std::endl;
            return -2;
        }
	    if(update_mode == EXIT_FILE)
	    {
		    std::cout << "breaking out due to update mode" << std::endl;
		    return -3;
	    }
        reset_state(true);
        return 0;
    }
    else
    {
        return compile_current_data();
    }
}

int batch_cluster_handle::compile_current_data()
{
    // the current pointer of cyclic_idx_load points to the last value +1 we're interested in so we can use it directly, last used index + 2 = first index if weve done a full thread run
    std::size_t start_av_idx = (cyclic_idx_memory_load + 1) % (nthreads+1);
    for(std::size_t th_av = 0; th_av != nthreads; th_av++)
    {
        std::size_t float_addr_sum = (start_av_idx + th_av) % (nthreads + 1);
        if(nthreads < 7) // we can't perform everything at once
        {
            sumf_from_to(gr_charge_workspace + unit_gr_charge_workspace_sz * float_addr_sum, gr_charge_av, unit_gr_charge_workspace_sz);
            sumf_from_to(gr_sz_workspace + unit_gr_sz_workspace_sz * float_addr_sum, gr_sz_av, unit_gr_sz_workspace_sz);
            sumf_from_to(gr_all_workspace + unit_gr_all_workspace_sz * float_addr_sum, gr_all_av,unit_gr_all_workspace_sz);

            sumuint16_from_to(grid_charge_workspace + unit_grid_charge_workspace_sz * float_addr_sum, grid_charge_av, unit_grid_charge_workspace_sz);
            sumuint16_from_to(grid_sz_workspace + unit_grid_sz_workspace_sz * float_addr_sum, grid_sz_av, unit_grid_sz_workspace_sz);
            if((transition_matrix_type == AVERAGE || transition_matrix_type == SPATIAL) && !frame_number[float_addr_sum])
            {
                sumuint16_from_to(tr_matrix_charge_workspace + tr_matrix_charge_sz * float_addr_sum,
                                  tr_matrix_charge_av, tr_matrix_charge_sz);
                sumuint16_from_to(tr_matrix_sz_workspace + tr_matrix_sz_sz * float_addr_sum, tr_matrix_sz_av,
                                  tr_matrix_sz_sz);
            }
        }
        else
        {
#ifdef DEBUGGING
            std::cout << " checking data compilation, these should read equal : " << grid_charge_workspace[unit_grid_charge_workspace_sz * float_addr_sum + 6] + grid_charge_av[6];
#endif
            std::thread t1(sumf_from_to, gr_charge_workspace + unit_gr_charge_workspace_sz * float_addr_sum, gr_charge_av, unit_gr_charge_workspace_sz);
            std::thread t2(sumf_from_to, gr_sz_workspace + unit_gr_sz_workspace_sz * float_addr_sum, gr_sz_av, unit_gr_sz_workspace_sz);
            std::thread t3(sumf_from_to, gr_all_workspace + unit_gr_all_workspace_sz * float_addr_sum, gr_all_av,unit_gr_all_workspace_sz);
            std::thread t4(sumuint16_from_to, grid_charge_workspace + unit_grid_charge_workspace_sz * float_addr_sum, grid_charge_av, unit_grid_charge_workspace_sz);
            std::thread t5(sumuint16_from_to, grid_sz_workspace + unit_grid_sz_workspace_sz * float_addr_sum, grid_sz_av, unit_grid_sz_workspace_sz);

            if((transition_matrix_type == AVERAGE || transition_matrix_type == SPATIAL) && !frame_number[float_addr_sum])
            {
                std::thread t6(sumuint16_from_to, tr_matrix_charge_workspace + tr_matrix_charge_sz * float_addr_sum,
                               tr_matrix_charge_av, tr_matrix_charge_sz);
                std::thread t7(sumuint16_from_to, tr_matrix_sz_workspace + tr_matrix_sz_sz * float_addr_sum,
                               tr_matrix_sz_av, tr_matrix_sz_sz);
                t6.join();
                t7.join();
            }
            t1.join();
            t2.join();
            t3.join();
            t4.join();
            t5.join();
#ifdef DEBUGGING
            std::cout << ", " << grid_charge_av[6] << std::endl;
#endif
        }
    }
    return 0;
}

int batch_cluster_handle::compile_current_data(std::size_t max_thread)
{
    std::cout << "compiling data up to" << max_thread << std::endl;
    std::size_t start_av_idx = (cyclic_idx_memory_load - max_thread + (nthreads+1)) % (nthreads+1);
    for(std::size_t th_n = 0; th_n != max_thread; th_n ++)
    {
        std::size_t float_addr_sum = (start_av_idx + th_n) % (nthreads + 1);
        if(nthreads < 7) // we can't perform everything at once
        {
            sumf_from_to(gr_charge_workspace + unit_gr_charge_workspace_sz * float_addr_sum, gr_charge_av, unit_gr_charge_workspace_sz);
            sumf_from_to(gr_sz_workspace + unit_gr_sz_workspace_sz * float_addr_sum, gr_sz_av, unit_gr_sz_workspace_sz);
            sumf_from_to(gr_all_workspace + unit_gr_all_workspace_sz * float_addr_sum, gr_all_av,unit_gr_all_workspace_sz);

            sumuint16_from_to(grid_charge_workspace + unit_grid_charge_workspace_sz * float_addr_sum, grid_charge_av, unit_grid_charge_workspace_sz);
            sumuint16_from_to(grid_sz_workspace + unit_grid_sz_workspace_sz * float_addr_sum, grid_sz_av, unit_grid_sz_workspace_sz);
            if((transition_matrix_type == AVERAGE || transition_matrix_type == SPATIAL) && !frame_number[float_addr_sum])
            {
                sumuint16_from_to(tr_matrix_charge_workspace + tr_matrix_charge_sz * float_addr_sum,
                                  tr_matrix_charge_av, tr_matrix_charge_sz);
                sumuint16_from_to(tr_matrix_sz_workspace + tr_matrix_sz_sz * float_addr_sum, tr_matrix_sz_av,
                                  tr_matrix_sz_sz);
            }
        }
        else
        {
            std::thread t1(sumf_from_to, gr_charge_workspace + unit_gr_charge_workspace_sz * float_addr_sum, gr_charge_av, unit_gr_charge_workspace_sz);
            std::thread t2(sumf_from_to, gr_sz_workspace + unit_gr_sz_workspace_sz * float_addr_sum, gr_sz_av, unit_gr_sz_workspace_sz);
            std::thread t3(sumf_from_to, gr_all_workspace + unit_gr_all_workspace_sz * float_addr_sum, gr_all_av,unit_gr_all_workspace_sz);
            std::thread t4(sumuint16_from_to, grid_charge_workspace + unit_grid_charge_workspace_sz * float_addr_sum, grid_charge_av, unit_grid_charge_workspace_sz);
            std::thread t5(sumuint16_from_to, grid_sz_workspace + unit_grid_sz_workspace_sz * float_addr_sum, grid_sz_av, unit_grid_sz_workspace_sz);

            if((transition_matrix_type == AVERAGE || transition_matrix_type == SPATIAL) && !frame_number[float_addr_sum]) // can't sum the lifetime of frame #0
            {
                std::thread t6(sumuint16_from_to, tr_matrix_charge_workspace + tr_matrix_charge_sz * float_addr_sum, tr_matrix_charge_av, tr_matrix_charge_sz);
                std::thread t7(sumuint16_from_to, tr_matrix_sz_workspace + tr_matrix_sz_sz * float_addr_sum, tr_matrix_sz_av, tr_matrix_sz_sz);
                t6.join();
                t7.join();
            }
            t1.join();
            t2.join();
            t3.join();
            t4.join();
            t5.join();
        }
    }
    return 0;
}

int batch_cluster_handle::push_data_out()
{
/*
 * create an IonsCluster structure and output it to the stack, this is the high level slow stuff where optimization doesnt
 * really matter, though this could really be improved by using C arrays instead of vectors
 */
    std::cout << "outputting current data stack" << std::endl;
    IonClusters output;
    output.ID = file_identifier;

    // update coordinates
    auto thidx = (cyclic_idx_memory_load + 1)%(nthreads+1);
    for(auto idx = 0; idx != n_updates_on_change; idx++)
    {
        for(auto dim = 0; dim != 3; dim++)
        {
            output.update_coord_values.push_back(m_p_update_on_change[idx + (n_updates_on_change*dim)]);
            //coord_workspace[update_on_change_idx[idx] + const_vals.n_particles * (dim + thidx*3)]
            //);
        }
    }
	// TODO : POSTPROCESS g(r) FOR CLUSTER DENSITIES SO g(r) = 1 at r = inf



	if(verbose) std::cout << " assigning gr_all & dr_all " << std::endl;
    // r, g(r) for all
	adjust_gr(gr_all_av, dr_all_workspace, 1.0, dr_all_workspace[1] - dr_all_workspace[0], unit_dr_all_sz);
    output.dr_all.assign(dr_all_workspace, static_cast<float*>(dr_all_workspace + unit_dr_all_sz));
    output.gr_all.assign(gr_all_av, static_cast<float*>(gr_all_av + unit_gr_all_workspace_sz));

    if(verbose) std::cout << "r, g(r) for all sizes " << std::endl;

    // r, g(r) for sizes
    output.dr_size.assign(dr_all_workspace, static_cast<float*>(dr_all_workspace + unit_dr_all_sz));
    float_div_by_int(gr_sz_av, n_avg_traj_frames, unit_dr_all_sz * (max_cluster_size + 1) * max_cluster_size / 2); // averaging by SIMD instruction
    for(std::size_t sz = 0; sz != (max_cluster_size +1) * max_cluster_size / 2; sz++)
    {
	    adjust_gr(gr_sz_av + unit_dr_all_sz * sz, dr_all_workspace, 1.0, dr_all_workspace[1] - dr_all_workspace[0], unit_dr_all_sz);
	    std::vector<float> _v;
        _v.assign(static_cast<float*>(gr_sz_av + unit_dr_all_sz * sz), static_cast<float*>(gr_sz_av + unit_dr_all_sz * (sz+1)));
        output.gr_size.push_back(_v);
    }

    // r, g(r) for charges

    if(verbose) std::cout << " r, g(r) for charges " << std::endl;

    output.dr_charge.assign(dr_all_workspace, static_cast<float*>(dr_all_workspace + unit_dr_all_sz));
    float_div_by_int(gr_charge_av, n_avg_traj_frames, unit_gr_charge_workspace_sz); // average
    for(std::size_t charge = 0; charge != (2*max_cluster_charge + 1) * (2*max_cluster_charge + 2) / 2; charge++)
    {
	    adjust_gr(gr_charge_av + unit_dr_all_sz * charge, dr_all_workspace, 1.0, dr_all_workspace[1] - dr_all_workspace[0], unit_dr_all_sz);
	    std::vector<float> _v;
        _v.assign(static_cast<float*>(gr_charge_av + unit_dr_all_sz * charge),
                  static_cast<float*>(gr_charge_av + unit_dr_all_sz * (charge+1)));
        output.gr_charge.push_back(_v);
    }

    // output the grid points
    // TODO : check underlying calculator for ordering

    if(verbose) std::cout << "outputting grids" << std::endl;

    // output the grids
    float* tgrid_av = (float*) _mm_malloc(sizeof(float) * prod_grid_size , 32);
    std::vector<float> _gv;
    for(auto charge = 0; charge != 2 * max_cluster_charge + 1; charge++)
    {
        int_div_by_int_cast(grid_charge_av + prod_grid_size * charge, tgrid_av, n_avg_traj_frames, prod_grid_size);
        _gv.assign(tgrid_av, static_cast<float*>(tgrid_av + prod_grid_size));
#ifdef DEBUGGING
        std::cout << " checking average value tgrid_av : " << tgrid_av[6] << " hand division  : " << static_cast<float>(grid_charge_av[prod_grid_size * charge + 6]) / static_cast<float>(n_avg_traj_frames)
                    << "in output vector : " << _gv[6] << std::endl;
#endif
        output.grid_values_charge.push_back(_gv);
    }

    std::cout << " # of total clusters " << std::accumulate(grid_sz_av, grid_sz_av + prod_grid_size * max_cluster_size, 0) << std::endl;

    for(auto csz = 0; csz != max_cluster_size; csz++)
    {
        int_div_by_int_cast(grid_sz_av + prod_grid_size * csz, tgrid_av, n_avg_traj_frames, prod_grid_size);
        _gv.assign(tgrid_av, static_cast<float*>(tgrid_av + prod_grid_size));
#ifdef DEBUGGING
        std::cout << " checking average value tgrid_av : " << tgrid_av[6] << " hand division : " << static_cast<float>(grid_sz_av[prod_grid_size * csz + 6]) / static_cast<float>(n_avg_traj_frames)
                    << "in vector : " << _gv[6] << std::endl;
#endif
        output.grid_values_size.push_back(_gv);
    }

    // TODO : lifetimes averaging

    if(verbose)
    {
        std::cout << "pushing out " << std::endl;
        // output the averages
        std::cout << "output ptr : " << output_ptr << std::endl;
        std::cout << "output ID : " << output.ID << std::endl;
        std::cout << "output coord updt size " << output.update_coord_values.size() << std::endl;
        std::cout << "output dr_all size" << output.dr_all.size() << std::endl;
        std::cout << "output gr_all size " << output.gr_all.size() << std::endl;
        std::cout << "output gr_charge size : " << output.gr_charge.size() << std::endl;
        if (output.gr_charge.size() > 0)
        {
            std::cout << " subsize[0] : " << output.gr_charge[0].size() << std::endl;
        }

        std::cout << "output grid charge size  : " << output.grid_values_charge.size() << std::endl;
        if (output.grid_values_charge.size() > 0)
        {
            std::cout << " subsize[0] : " << output.grid_values_charge[0].size() << std::endl;
        }
        std::cout << "grid sz size : " << output.grid_values_size.size() << std::endl;
        if (output.grid_values_size.size() > 0)
        {
            std::cout << "subsize[0] : " << output.grid_values_size[0].size() << std::endl;
        }
    }
	_mm_free(tgrid_av);
    output_ptr->push_back(output);
    //reset_state();
    return 0;
}

int batch_cluster_handle::reset_state(bool chk_update)
{
    std::cout << "resetting state" << std::endl;
    frame_number[nthreads] = std::numeric_limits<std::uint32_t>::max();//UINT32_MAX; // trick so that the increment is zero

    if(chk_update)
    {
        // update the check index
        for (auto chk_idx = 0; chk_idx != n_updates_on_change; chk_idx++)
        {
            m_p_update_on_change[chk_idx] = coord_workspace[update_on_change_idx[chk_idx] +
                                                            unit_coord_workspace_sz * cyclic_idx_memory_load];
            m_p_update_on_change[chk_idx + n_updates_on_change] = coord_workspace[update_on_change_idx[chk_idx] +
                                                                                  unit_coord_workspace_sz * cyclic_idx_memory_load + const_vals.n_particles];

            m_p_update_on_change[chk_idx + 2 * n_updates_on_change] =
                    coord_workspace[update_on_change_idx[chk_idx] +
                                    unit_coord_workspace_sz * cyclic_idx_memory_load + 2 * const_vals.n_particles];
        }
    }

    cyclic_idx_memory_load = 0; // load into first element
    n_avg_traj_frames = 0;

    if(verbose)
    {
        std::cout << "g(r)_all_av : " << gr_all_av << std::endl;
        std::cout << "g(r)_charge_av : " << gr_charge_av << std::endl;
        std::cout << "g(r)_sz_av : " << gr_sz_av << std::endl;
        std::cout << "grid_charge_av : " << grid_charge_av << std::endl;
        std::cout << "grid_sz_av : " << grid_sz_av << std::endl;
    }


    memset(gr_all_av, 0, sizeof(float) * unit_dr_all_sz);
    memset(gr_charge_av, 0, sizeof(float) * unit_gr_charge_workspace_sz);
    memset(gr_sz_av, 0, sizeof(float) * unit_gr_sz_workspace_sz);


/*
    memset(grid_charge_av, 0, sizeof(std::uint16_t) * unit_grid_charge_workspace_sz);
    memset(grid_sz_av, 0, sizeof(std::uint16_t) * unit_grid_sz_workspace_sz);
*/

    memset_uint16_to_zero(grid_charge_av, unit_grid_charge_workspace_sz);
    memset_uint16_to_zero(grid_sz_av, unit_grid_sz_workspace_sz);
    memset_uint16_to_zero(grid_charge_workspace, unit_grid_charge_workspace_sz * (nthreads +1));
    memset_uint16_to_zero(grid_sz_workspace, unit_grid_sz_workspace_sz * (nthreads+1));

#ifdef DEBUGGING
    std::cout << "checking memset, these should read zero : " << grid_charge_av[6] << ", " << grid_sz_av[6] << std::endl;
#endif


    if(transition_matrix_type)
    {
        memset(tr_matrix_charge_av, 0, sizeof(std::uint16_t) * tr_matrix_charge_sz);
        memset(tr_matrix_sz_av, 0, sizeof(std::uint16_t) * tr_matrix_sz_sz);
    }
    return 0;
}

batch_cluster_handle::~batch_cluster_handle()
{
    if(verbose) std::cout << "freeing coord_workspace at " << coord_workspace << std::endl;
    _mm_free(coord_workspace);
    coord_workspace = nullptr;

    if(verbose) std::cout << "freeing box workspace at " << box_workspace << std::endl;
    _mm_free(box_workspace);
    box_workspace = nullptr;

    if(verbose) std::cout << "freeing dr workspaces at " << dr_all_workspace  << std::endl;
	_mm_free(dr_all_workspace);
    dr_all_workspace = nullptr;
	/*
    _mm_free(dr_charge_workspace);
    dr_charge_workspace = nullptr;
    _mm_free(dr_sz_workspace);
    dr_sz_workspace = nullptr;
	*/

	_mm_free(frame_number);

    if(verbose) std::cout << "freeing gr workspaces at " << gr_all_workspace << ", " << gr_charge_workspace << ", " << gr_sz_workspace << std::endl;
    _mm_free(gr_all_workspace);
    gr_all_workspace = nullptr;
    _mm_free(gr_charge_workspace);
    gr_charge_workspace = nullptr;
    _mm_free(gr_sz_workspace);
    gr_sz_workspace = nullptr;

    if(verbose) std::cout << "freeing gr averages at " << gr_all_av << ", " << gr_charge_av << ", " << gr_sz_av << std::endl;
    _mm_free(gr_all_av);
    gr_all_av = nullptr;
    _mm_free(gr_charge_av);
    gr_charge_av = nullptr;
    _mm_free(gr_sz_av);
    gr_sz_av = nullptr;

    if(verbose) std::cout << "freeing grid workspaces at " << grid_charge_workspace << ", " << grid_sz_workspace << std::endl;
    _mm_free(grid_charge_workspace);
    grid_charge_workspace = nullptr;
    _mm_free(grid_sz_workspace);
    grid_sz_workspace = nullptr;

    if(verbose) std::cout << "freeing grid averages at " << grid_charge_av << ", " << grid_sz_av << std::endl;
    _mm_free(grid_charge_av);
    grid_charge_av = nullptr;
    _mm_free(grid_sz_av);
    grid_sz_av = nullptr;

    if(verbose) std::cout << "freeing transition matrixes at " << tr_matrix_charge_av << ", " << tr_matrix_sz_av << ", " <<
                          tr_matrix_charge_workspace << ", " << tr_matrix_sz_workspace << std::endl;
    _mm_free(tr_matrix_charge_av);
    tr_matrix_charge_av = nullptr;
    _mm_free(tr_matrix_sz_workspace);
    tr_matrix_sz_av = nullptr;
    _mm_free(tr_matrix_charge_workspace);
    tr_matrix_charge_workspace = nullptr;
    _mm_free(tr_matrix_sz_av);
    tr_matrix_sz_av = nullptr;

    if(verbose) std::cout << "freeing use indexing at " << use_index << std::endl;
    free(use_index);
    use_index = nullptr;
    if(verbose) std::cout << "freeing charge indexing at " << charge_index << std::endl;
    free(charge_index);
    charge_index = nullptr;

    if(verbose) std::cout << "freeing update points at " << update_on_change_idx << ", " << m_p_update_on_change << std::endl;
    free(update_on_change_idx);
    update_on_change_idx = nullptr;
    free(m_p_update_on_change);
    m_p_update_on_change = nullptr;

    if(add_gofr_use_index)
    {
        free(add_gofr_use_index[0]);
    }
    free(add_gofr_use_index);

    if(handle_dcd)
    {
        if(verbose) std::cout << "freeing DCD handle at " << handle_dcd << std::endl;
        delete handle_dcd;
        handle_dcd = nullptr;
    }
    if(handle_xml)
    {
        if(verbose) std::cout << "freeing XML handle at " << handle_xml << std::endl;
        delete handle_xml;
        handle_xml = nullptr;
    }
    if(handle_gsd)
    {
        if(verbose) std::cout << "freeing GSD handle at " << handle_gsd << std::endl;
        delete handle_gsd;
        handle_gsd = nullptr;
    }
    if(handle_lammps)
    {
        if(verbose) std::cout << "freeing lammps topology handle at " << handle_lammps << std::endl;
        delete handle_lammps;
        handle_lammps = nullptr;
    }
    for(std::size_t thidx = 0; thidx != nthreads; thidx++)
    {
        if(cluster_calculators[thidx])
        {
            delete cluster_calculators[thidx];
            cluster_calculators[thidx] = nullptr;
        }
    }

}
