
#include <vector>
#include <string.h>
#include <cstdint>
#include "buildingblocks.h"

using namespace std;

namespace Clusters {

    void Particle::Dist2(float px, float py, float pz)
    {
        float dx, dy, dz;
        dx = xm - px;
        dy = ym - py;
        dz = zm - pz;
        dist2 = dx * dx + dy * dy + dz * dz;
    }

    bool operator< (const Particle& p1, const Particle& p2)
    {
        return (p1.dist2 < p2.dist2);
    }


    void Particle::find_center_of_mass()
    {
        xm = 0.0;
        ym = 0.0;
        zm = 0.0;

        for (auto i = 1; i < x_list.size(); i++)
        {
            float xt = x_list[i] - x_list[0];
            float yt = y_list[i] - y_list[0];
            float zt = z_list[i] - z_list[0];

            // apply PBC
            xt += xt >= box_ptr[0] / 2.0 ? -box_ptr[0] : xt < -box_ptr[0]/2.0 ?  box_ptr[0] : 0.0;
            yt += yt >= box_ptr[1] / 2.0 ? -box_ptr[1] : yt < -box_ptr[1]/2.0 ?  box_ptr[1] : 0.0;
            zt += zt >= box_ptr[2] / 2.0 ? -box_ptr[2] : zt < -box_ptr[2]/2.0 ?  box_ptr[2] : 0.0;

            xm += xt;
            ym += yt;
            zm += zt;
        }

        xm /= x_list.size();
        ym /= x_list.size();
        zm /= x_list.size();

        xm += x_list[0];
        ym += y_list[0];
        zm += z_list[0];

        // apply PBC
        xm += xm >= box_ptr[0] / 2.0 ? -box_ptr[0] : xm < -box_ptr[0]/2.0 ?  box_ptr[0] : 0.0;
        ym += ym >= box_ptr[1] / 2.0 ? -box_ptr[1] : ym < -box_ptr[1]/2.0 ?  box_ptr[1] : 0.0;
        zm += zm >= box_ptr[2] / 2.0 ? -box_ptr[2] : zm < -box_ptr[2]/2.0 ?  box_ptr[2] : 0.0;
    }

} // namespace Clusters
