mexa_exists = exist('matlab_IonCluster.mexa64') == 3;

if(mexa_exists)
    warning('current directory already had mexa file built, overwriting');
end

cc = mex.getCompilerConfigurations;
gpp_installed = 0;

for i = 1 : length(cc)
    if(strcmp(cc(i).Name, 'g++'))
       gpp_installed = 1;
       break;
    end
end

if(gpp_installed == 0)
   warning('did not find g++ in mex configuration, will try building anyway');
end
mex CXXOPTIM='-O3 -DNDEBUG' CXXFLAGS='-std=c++11 -fPIC -march=native -ffast-math -ftree-vectorize -funroll-loops' matlab_IonCluster.cpp buildingblocks.cpp clustering.cpp DCDReader.cpp SynchronizedIO.cpp SSEInstructions.cpp XMLParser.cpp dumpParser.cpp