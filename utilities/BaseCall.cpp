#include "SynchronizedIO.hpp"
#include <string>
#include <vector>
#include <cstdio>

/*
 *  exemple implementation of the multithread implementation for files called traj.dcd & topo.xml
 */

void linspace(float a, float b, float n, float* dst)
{
    float delta = (b-a)/(n-1);
    dst[0] = a;
    for(auto i = 1; i != n; i++)
    {
        dst[i] = dst[i-1] + delta;
    }
}

int main()
{

    ClusterFile _f;
    std::string tf("./traj.dcd");
    std::string to("./topo.xml");
    _f.trajectory_filepath = tf;
    _f.topology_filepath = to;
    _f.MD_type = XML_FILE;

    cluster_options opts;
    opts.clustering_cutoff = 0.5;

	opts.nbins_gofr_calculation = 128;
	opts.max_r_value_gofr_calculation = 2.0;

    opts.first_frame = 150;

    opts.grid_size[0] = 32;
    opts.grid_size[1] = 32;
    opts.grid_size[2] = 64;

    opts.ID = std::string(" ");

    std::vector<std::string> _it;
    const std::string na("Na");
    const std::string cl("Cl");
    _it.push_back(na);
    _it.push_back(cl);

    opts.ions_types = _it;

    std::vector<std::string> _updt;
    const std::string w("W");
    _updt.push_back(w);

    opts.update_on_coord_change_type = _updt;

    lifetime life = OFF;

    opts.transition_matrix = life;
    opts.unit_charge = 1.0;
    opts.max_cluster_charge = 2;
    opts.max_cluster_size = 2;

    std::vector<IonClusters> results;
    opts.ID = _f.topology_filepath;
    batch_cluster_handle* handle = new batch_cluster_handle(_f, 1, opts, &results); //outptr);
    std::cout << "handle pointer " << handle << std::endl;
    if(!handle->valid_dr_sz)
    {
        return 0;
    }
    if(!handle->valid_grid_sz)
    {
        return 0;
    }

    if(!handle->valid_files)
    {
        std::cout << "handle reported error # " << handle->last_error_code << std::endl;
        return 0;
    }
    std::cout << "batch object created, starting clustering" << std::endl;
    std::cout << "------------------------" << std::endl;
    while (!handle->memory_load_frames())
    {
        std::cout << "current file at " << std::endl;
        std::cout << "# of frames for current average : " << handle->get_current_nframe_avg() << std::endl;
        std::cout << "current frame position in trajectory : " << handle->get_current_traj_frame() << std::endl;
    }
    std::cout << " destroying batch handle" << std::endl;
    delete handle;
}