//
// Created by martin on 2/2/17.
//

#include "dumpParser.hpp"
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <string>
#include <cstring>

#define LINEBUF 4096

int dumpParser::open_dump(const char *filename)
{
    if(file_ptr)
    {
        close_file();
    }

    file_ptr = std::fopen(filename, "r");

    if(!file_ptr)
    {
        return -1;
    }
    return 0;
}

int dumpParser::close_file()
{
    attribute_values.clear();
    attribute_names.clear();
    if(!file_ptr)
    {
        return 0;
    }
    return std::fclose(file_ptr);
}

static void split_buffer(char* buffer, std::vector<std::string> &tokens, const char delim)
{
    char* ptr_begin = buffer;
    char* ptr_end = buffer;

    while(*ptr_begin != '\0')
    {
        if(*ptr_end == delim)
        {
            std::string token(ptr_begin, static_cast<std::size_t>(ptr_end - ptr_begin));
            tokens.push_back(token);
            ptr_end++;
            ptr_begin = ptr_end;
            continue;
        }
        else if(*ptr_end == '\0')
        {
            if(*ptr_begin != '\0' && *ptr_begin != '\n') // special case if the line ends with " \n".
            {
                std::string token(ptr_begin);
                tokens.push_back(token);
            }
            return;
        }
        ptr_end++;
    }

}

int dumpParser::parse_dump_file()
{
    /*
     * get a hold of the list of defined attributes on line #8 and then read matrix of all attributes
     */

    std::vector<std::string> list;
    if(!file_ptr)
    {
        return -1;
    }

    std::fseek(file_ptr, 0, SEEK_SET);
    char* ptr_test;
    bool is_valid = true;
    char buf[LINEBUF];
    for(auto i = 0; i!= 9; i++)
    {
        ptr_test = std::fgets(buf, LINEBUF, file_ptr);
        is_valid = is_valid && ptr_test;
    }
    if(!is_valid)
    {
        return -2;
    }

    // parse the line
    std::vector<std::string> attributes;
    //char delim = ' ';
    split_buffer(buf, attributes, ' ');

    if(attributes.size() == 0) // empty buffer ?
    {
        return -3;
    }
    if(attributes.size() < 3) // got a return from split but theres no item
    {
        return -4;
    }
    if(strcmp(attributes[0].c_str(), "ITEM:")) // first item of the split is not ok
    {
        return -5;
    }
    if(strcmp(attributes[1].c_str(), "ATOMS")) // no idea how you can actually get this, but better guard against it
    {
        return -6;
    }

    for(auto i = 2; i!= attributes.size(); i++)
    {
        attribute_names.push_back(attributes[i]);
    }

    attribute_values.resize(attribute_names.size());

    // get all lines afterwards
    while(ptr_test = std::fgets(buf, LINEBUF, file_ptr), ptr_test)
    {
        std::vector<std::string> current_line;
        split_buffer(buf, current_line, ' ');
        if(current_line.size() != 0 && !strcmp(current_line[0].c_str(), "ITEM:")) // this is a lammps trajectory instead of a topo file
        {
            break;
        }
        if(current_line.size() != attribute_names.size())
        {
            return -7;
        }
        for(auto i = 0; i!= current_line.size(); i++)
        {
            attribute_values[i].push_back(current_line[i]);
        }
    }
    return 0;
}

std::vector<std::string> dumpParser::get_attr_list(std::string dump_attr_name, int& err)
{
    std::vector<std::string> list_attr;
    if(attribute_names.size() == 0 || attribute_values.size() == 0) // we did not parse the file yet
    {
        err = parse_dump_file();
    }

    if(err)
    {
        return list_attr;
    }

    // find the attribute in attribte_names then return the corresponding vector
    bool found = false;

    int i;
    for(i = 0; i != attribute_names.size(); i++)
    {
        if(!strcmp(attribute_names[i].c_str(), dump_attr_name.c_str()))
        {
            found = true;
            break;
        }
    }

    if(!found)
    {
        err = 1;
        return list_attr;
    }
    return attribute_values[i];
}

int lammpstrjParser::open_traj(const char *filename)
{
    /*
     * open trajectory file, parse first frame and move back to frame start
     */
    if(file_ptr)
    {
        std::fclose(file_ptr);
    }
    file_ptr = std::fopen(filename, "r");
    if(!file_ptr)
    {
        return -1;
    }
    char buf[LINEBUF];
    char* ptr_test;

    while(1)
    {
        ptr_test = std::fgets(buf, LINEBUF, file_ptr);
        if(!ptr_test)
        {
            return -2;
        }
        //check for TIMESTEP, NUMBER, BOX BOUNDS
        if(!strncmp(buf, "ITEM: TIMESTEP", sizeof("ITEM: TIMESTEP")))
        {
            ptr_test = std::fgets(buf, LINEBUF, file_ptr);
            if(!ptr_test){return -2;}
            current_timestep = std::strtol(buf, nullptr, 10);
        }
        if(!strncmp(buf, "ITEM: NUMBER OF ATOMS", sizeof("ITEM: NUMBER OF ATOMS")))
        {
            ptr_test = std::fgets(buf, LINEBUF, file_ptr);
            if(!ptr_test){return -2;}
            nparticles = std::strtol(buf, nullptr, 10);
        }

    }

}