//
// Created by martin on 10/19/16.
//

#include "XMLParser.hpp"
#include <cstdint>
#include <iostream>
#include <vector>
#include <cstdio>
#include <cstring>
#include <stdexcept>
#define LINEBUF 1024
#define FILEBUF 1048576

int HOOMD_XML_parser::open_XML(const char *file_name)
{
    ptr_file = std::fopen(file_name, "r");
    if(!ptr_file)
    {
        return -1;
    }
    return 0;
}

int HOOMD_XML_parser::close_XML()
{
    return std::fclose(ptr_file);
}

std::vector<std::string> HOOMD_XML_parser::get_attr_list(std::string attr)
{
    std::vector<std::string> list;
    if (!ptr_file)
    {
        return list;
    }
    std::fseek(ptr_file, 0, SEEK_SET);
    char line[LINEBUF];

    std::string linecheck1 = std::string("<") + attr;// + std::string(">");
    std::string linecheck2 = std::string("</") + attr;// + std::string(">");

    const char* lchk1 = linecheck1.c_str();
    const char* lchk2 = linecheck2.c_str();

    char* pos;

    while(std::fgets(line, LINEBUF, ptr_file))
    {
        if(std::strncmp(line, lchk1, linecheck1.length()) == 0) // we hit the first <attr> bracket
        {
            while(std::fgets(line, LINEBUF, ptr_file) != NULL && std::strncmp(line, lchk2, linecheck1.length()) != 0) // till we hit </attr>
            {
                pos = std::strchr(line, '\n');
                if (pos == NULL){ throw std::out_of_range("cannot find end of line character in XML");}
                *pos = '\0';
                std::string str(line);
                list.push_back(str);
            }
            return list;
        }
    }

    return list;
}