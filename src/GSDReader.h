// Copyright (c) 2009-2019 The Regents of the University of Michigan
// This file is part of the HOOMD-blue project, released under the BSD 3-Clause License.
// Adopted from from GSDReader in HOOMD-Blue by Joshua Anderson (joaander)

#include <vector>
#include <string>
#include "gsd.h"


#ifndef __GSD_READER_H__
#define __GSD_READER_H__


//! Reads a GSD input file
/*! Read an input GSD file and generate a system snapshot. GSDReader can read any frame from a GSD
    file into the snapshot. For information on the GSD specification, see http://gsd.readthedocs.io/

*/

#include "io.h"

namespace Analysis {

class GSDReader : public IOHandle
{
public:
  //! Loads in the file and parses the data
  GSDReader(const std::string &name);
  
  //! Destructor
  ~GSDReader();

  //! Read in a frame
  int read_frame(const uint64_t frame, bool from_end=false);

  //! Returns the timestep of the simulation
  uint64_t getTimeStep() const  {
    return m_timestep;
  }

  //! Helper function to read a quantity from the file
  bool readChunk(void *data, uint64_t frame, const char *name, size_t expected_size, unsigned int cur_n=0);

  //! get handle
  gsd_handle getHandle(void) const  {
    return m_handle;
  }

  //! Returns the total number of frames
  uint64_t getTotalNumFrames() const {
    return m_nframes;
  }

  //! Returns the total number of particles in a frame
  uint64_t getTotalNumParticles() const {
    return m_nparticles;
  }

  void write_dump(const char* filename);

  const float* getBoxDim() const { return m_box; }

  unsigned int* getTypes() const { return type; }
  float* getMasses() const { return mass; }
  float* getDiameters() const { return diameter; }
  float* getCharges() const { return charge; }
  unsigned int* getBodies() const { return body; }
  float* getInertiaMoments() const { return inertia; }
  float* getPositions() const { return pos; }
  float* getOrientations() const { return orientation; }
  float* getVelocities() const { return vel; }
  float* getAngularMomenta() const { return angmom; }
  int* getImages() const { return image; }

private:
  uint64_t m_timestep;                         //!< Timestep at the selected frame
  std::string m_name;                          //!< Cached file name
  uint64_t m_frame;                            //!< Cached frame
  uint64_t m_nframes;                          //!< Total number of frames in the GSD file
  gsd_handle m_handle;                         //!< Handle to the file
  std::vector<std::string> m_type_mapping;
  int m_nparticles;                            //!< Number of particles in a frame
  unsigned int m_dimensions;                   //!< The dimensionality of the system
  float m_box[6];                              //!< Box dimensions: first 3 elements are Lx, Ly and Lz

  //!< particle properties in a frame (may or may not be stored)
  unsigned int* type;                          //!< size N 
  float* mass;                                 //!< size N  
  float* charge;                               //!< size N
  float* diameter;                             //!< size N 
  unsigned int* body;                          //!< size N  
  float* inertia;                              //!< size 3*N 
  float* pos;                                  //!< size 3*N
  float* orientation;                          //!< size 4*N
  float* vel;                                  //!< size 3*N
  float* angmom;                               //!< size 4*N
  int* image;                                  //!< size 3*N 

  void allocate(int N);

  //! Helper function to read a type list from the file
  std::vector<std::string> readTypes(uint64_t frame, const char *name);

  // helper functions to read sections of the file
  void readHeader();
  void readParticles();
  
};

} // namespace Analysis

#endif
