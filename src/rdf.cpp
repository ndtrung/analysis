/*
  Copyright (C) 2017  Trung D. Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*----------------------------------------------------------------------------
  Implementation for RDF g(r) and structure factor S(q) calculations

  TODO:
    1- Support GPU acceleration

  Trung Nguyen (ndactrung@gmail.com)
------------------------------------------------------------------------------*/

#include <algorithm>
#include <fstream>
#include <cmath>
#include <iostream>
#include <string>
#include <omp.h>

#include "DCDHandle.h"
#include "io.h"
#include "memory.h"
#include "rdf.h"

#ifdef _USE_FFTW
#include "fftw3.h"
#endif

using namespace std;
using namespace Analysis;

#define anint(x) ((x >= 0.5) ? (1.0) : (x < -0.5) ? (-1.0) : (0.0)) 
#define MAX_NEIGHBORS        64
#define MAX_BEADS_IN_CELL    128
#define EPSILON 0.00001

enum {XYZ=0, XML=1, DUMP=2, DCD=3};

RDF::RDF(double _dr, double _rmax, double _qmax, double _qmin, int _directsq, int _nthreads)
  : dr(_dr), rmax(_rmax), qmax(_qmax), qmin(_qmin), directsq(_directsq), nthreads(_nthreads)
{
  io_handle = NULL;
  g = g_rep = r = NULL;
  S = q = NULL;
  Sk = Sk_rep = NULL;
  nbins = nqbins = 0;
  nkx = nky = nkz = 1;
  Sradial = NULL;
  qradial = NULL;
  count = NULL;
  qbin = 0.001;
  ksetup = 0;
  use_template = 0;

  fft = 1;
  fft_dr = 0.01;
}

/*----------------------------------------------------------------------------*/

RDF::~RDF()
{
  if (io_handle) delete io_handle;

  if (g) delete [] g;
  if (g_rep) delete [] g_rep;
  if (r) delete [] r;
  if (S) delete [] S;
  if (q) delete [] q;
  if (Sk) delete [] Sk;
  if (Sk_rep) delete [] Sk_rep;
  if (Sradial) delete [] Sradial;
  if (qradial) delete [] qradial;
  if (count) delete [] count;

  waveVectors.clear();
  indexList1.clear();
  indexList2.clear();

  beadList1.clear();
  beadList2.clear();
}

/*----------------------------------------------------------------------------
  load the bead list from a file
  only pick the beads of specified types
------------------------------------------------------------------------------*/

int RDF::load_coord_list(int listidx, char* fileName, char** types, int ntypes, int nframe)
{
  std::vector<Bead> *beadList = NULL;
  if (listidx == 1) beadList = &beadList1;
  else if (listidx == 2) beadList = &beadList2;

  int success;
  string fullname(fileName);
  std::size_t found = fullname.find_last_of(".");
  base = fullname.substr(0,found);
  ext = fullname.substr(found+1);

  // guess the file type
  if (strcmp(ext.c_str(),"xyz") == 0) {
    if (io_handle == NULL) io_handle = new XYZHandle;
    success = io_handle->read(fileName, types, ntypes, nNumTotalBeads,
      box, *beadList, timestep);
    file_type = XYZ;
  } else if (strcmp(ext.c_str(),"xml") == 0) {
    if (io_handle == NULL) io_handle = new XMLHandle;
    success = io_handle->read(fileName, types, ntypes, nNumTotalBeads,
      box, *beadList, timestep);
    file_type = XML;
  } else if (strcmp(ext.c_str(),"dump") == 0 || strcmp(ext.c_str(),"txt") == 0) {
    if (io_handle == NULL) io_handle = new DUMPHandle;
    success = io_handle->read(fileName, types, ntypes, nNumTotalBeads,
      box, *beadList, timestep);
    file_type = DUMP;
  } else if (strcmp(ext.c_str(),"dcd") == 0) {
    if (io_handle == NULL) io_handle = new DCDHandle;
    char* file = new char [64];
    sprintf(file, "%s-%d", base.c_str(), nframe);
    base = std::string(file);
    timestep = nframe;

    if (use_template) {
      if (listidx == 1) 
        success = ((DCDHandle*)io_handle)->read(fileName, indexList1, nNumTotalBeads,
          box, *beadList, nframe);
      else if (listidx == 2)
        success = ((DCDHandle*)io_handle)->read(fileName, indexList2, nNumTotalBeads,
          box, *beadList, nframe);
    } else {
      success = io_handle->read(fileName, nNumTotalBeads, box, *beadList, nframe);
    }

    file_type = DCD;
    delete [] file;
  } else {
    std::cout << "Unknown file extension.\n";
    return 0;
  }

  return success;
}

/*----------------------------------------------------------------------------*/

void RDF::set_k_vectors(int _nkx, int _nky, int _nkz,
                        double _dkx, double _dky, double _dkz)
{
  nkx = _nkx;
  nky = _nky;
  nkz = _nkz;
  dkx = _dkx;
  dky = _dky;
  dkz = _dkz;
}

/*----------------------------------------------------------------------------*/

int RDF::execute()
{
  if (rmax == 0) rmax = box.Lx/2.0;                 // Lx/2.0 may be too large in most case
  if (qmax == 0) qmax = 8.0*atan(1.0)/dr;       // 2pi/dr
  if (qmin == 0) qmin = 8.0*atan(1.0)/(box.Lx/2.0); // 2pi/(L/2)

  int nmax = int(rmax / dr) + 1;
  if (nmax > nbins) {
    if (g) delete [] g;
    if (g_rep) delete [] g_rep;
    if (r) delete [] r;
    if (S) delete [] S;
    if (q) delete [] q;
    if (Sk) delete [] Sk;
    if (Sk_rep) delete [] Sk_rep;
    if (Sradial) delete [] Sradial;
    if (qradial) delete [] qradial;
    if (count) delete [] count;
    nbins = nqbins = nmax;
    g = new float [nbins];
    g_rep = new float [nthreads*nbins];
    r = new float [nbins];
    S = new float [nqbins];
    q = new float [nqbins];
    if (directsq) {
      numberOfWavevectors = nkx * nky * nkz;
      Sk = new double [numberOfWavevectors];
      Sk_rep = new double [nthreads*2*numberOfWavevectors];
      Sradial = new float [nqbins];
      qradial = new float [nqbins];
      count = new int [nqbins];
    }
  }

  if (directsq) {
    setup_waveVectors();
  }
  
  int n1 = beadList1.size();
  int n2 = beadList2.size();
  int success;
  if (n1 == 0 && n2 == 0) return 0;
  else if (n1 == 0 && n2 > 0) {
    success = compute(beadList2, beadList2);
  } else if (n1 > 0 && n2 == 0) {
    success = compute(beadList1, beadList1);
  } else {
    success = compute(beadList1, beadList2);
  }

  Sq_fft(beadList1, fft_dr, nthreads, (char*)"sq_fft.txt");

  return success;
}

/*----------------------------------------------------------------------------*/

void RDF::setup_waveVectors()
{
  if (ksetup == 1) return;

  cout << "Setting up wave vectors..\n";

  int nx, ny, nz, nk;
  int i, itop,i1;
  double rxi,ryi,rzi, mag2;
  int dkmultiplier = 1;
  double PI = 4.0*atan(1.0);

  waveVectors.resize(numberOfWavevectors);

  if (dkx < EPSILON) dkx = 2.0*PI / box.Lx * dkmultiplier;
  if (dky < EPSILON) dky = 2.0*PI / box.Ly * dkmultiplier;
  if (dkz < EPSILON) dkz = 2.0*PI / box.Lz * dkmultiplier;

  double start = omp_get_wtime();
  #pragma omp parallel
  {

  #pragma omp for schedule(static)
  for (int nk = 0; nk < numberOfWavevectors; nk++) {
    int ix = nk / (nky * nkz);
    int iy = (nk % (nky * nkz)) / nkz;
    int iz = (nk % (nky * nkz)) % nkz;

    double kx = ix * dkx;
    double ky = iy * dky;
    double kz = iz * dkz;

    waveVectors[nk].x = kx;
    waveVectors[nk].y = ky;
    waveVectors[nk].z = kz;
    waveVectors[nk].mag2 = kx * kx + ky * ky + kz * kz;
  }

  }

  // sort k's in the ascending order of their magnitude

  std::sort(waveVectors.begin(), waveVectors.end());

  printf("Wavevectors setup time = %0.3f secs\n", (double)(omp_get_wtime() - start));
  printf("Number of wavevectors =  %d %d %d\n", nkx, nky, nkz);
  printf("dk =  %f %f %f\n", dkx, dky, dkz);

  ksetup = 1;
}

/*----------------------------------------------------------------------------*/

int RDF::compute(const std::vector<Bead>& list1, const std::vector<Bead>& list2)
{
  std::cout << "Total number of beads in list 1:     " << list1.size() << std::endl;
  std::cout << "Total number of beads in list 2:     " << list2.size() << std::endl;
  std::cout << "Box dimensions, dist units:          " << box.Lx << " " << box.Ly << " " << box.Lz << "\n";

  omp_set_num_threads(nthreads);
  printf("\nCalculation of g(r) using %d threads..\n", nthreads);

  int i, j, n1, n2;
  double Lx2 = 0.5*box.Lx;
  double Ly2 = 0.5*box.Ly;
  double Lz2 = 0.5*box.Lz;

  n1 = list1.size();
  n2 = list2.size();

  memset(g, 0, sizeof(float)*nbins);
  memset(g_rep, 0, sizeof(float)*nthreads*nbins);

  double start = omp_get_wtime();

  #pragma omp parallel
  {
  int me = omp_get_thread_num();
  float *local_g = &g_rep[me*nbins];

  #pragma omp for schedule(static)
  for (int i = 0; i < n1; i++) {
    double xtmp = list1[i].x;
    double ytmp = list1[i].y;
    double ztmp = list1[i].z;
    for (int j = 0; j < n2; j++) {
      double dx = xtmp - list2[j].x;
      double dy = ytmp - list2[j].y;
      double dz = ztmp - list2[j].z;
      if (dx <= -Lx2) dx += box.Lx;
      else if (dx >= Lx2) dx -= box.Lx;
      if (dy <= -Ly2) dy += box.Ly;
      else if (dy >= Ly2) dy -= box.Ly;
      if (dz <= -Lz2) dz += box.Lz;
      else if (dz >= Lz2) dz -= box.Lz;
      double rsq = dx * dx + dy * dy + dz * dz;
      if (rsq < EPSILON) continue;
      unsigned int ibin = (unsigned int)(sqrt(rsq) / dr);
      if (ibin >= 0 && ibin < nbins) 
          local_g[ibin] += 1.0f;
    }
  }

  #pragma omp for schedule(static)
  for (int i = 0; i < nbins; i++) {
    int offset = nthreads/2;
    while (offset > 0) {
      for (int j = 0; j < offset; j++)
        if (j + offset < nthreads)
          g_rep[j*nbins+i] += g_rep[(j+offset)*nbins+i];
      offset >>= 1;
    }
    g[i] = g_rep[i];
  }

  } // end of omp parallel region


  printf("g(r) compute time = %0.3f secs\n\n", (double)(omp_get_wtime() - start));

  // non-overlapping sets
  int npairs = n1 * n2;
  double pi = 4.0*atan(1.0);
  double factor = 4.0/3.0*pi*pow((double)dr,3.0);
  double box_vol = box.Lx * box.Ly * box.Lz;
  double rho1 = n1 / box_vol;
  double rho2 = n2 / box_vol;
  double rho = sqrt(rho1 * rho2);

  string rdf_filename;
  rdf_filename = "g_r.txt";
  ofstream ofs(rdf_filename.c_str());
  ofs << "r g(r)\n";

  double sum = 0.0;
  for (i = 0; i < nbins; i++) {
    sum += g[i];
  }

  for (i = 0; i < nbins; i++) {
    double dv = factor * (pow((double)(i+1), 3.0) - pow((double)i, 3.0));
    double prob =  g[i]/sum;
    g[i] = g[i] / ((double)n1 * dv * (double)n2 / box_vol);
    r[i] = i * dr;
    ofs << r[i] << " " << g[i] << " " << prob << "\n";
  }
  ofs.close();

  // if direct calculation of S(q) is needed

  if (directsq) {
    char sq_filename[64];
    sprintf(sq_filename, "direct_s_q.txt", timestep);
    Sq_direct(list1, list2, nkx, nky, nkz, dkx, dky, dkz, nthreads, sq_filename);

  } else { // Fourier transform of [g(r) - 1]

    char sq_filename[64];
    sprintf(sq_filename, "s_q_%d.txt", timestep);

    // lim_{q \leftarrow 0} S(q) = \rho \kappa k_BT,
    // where \rho is number density, \kappa is isothermal compressibility
    // Fourier transform: k* = 2\pi/L, k < k* does not have physical length scale
    // IMPORTANT: q has unit of inverse length since we are evaluating sin(q*r) where r is the distance.
    ofs.open(sq_filename);
    ofs << "q S(q)\n";
/*
    // Linear scale in q
    double dq = (qmax - qmin) / nqbins;
    int n = 0;
    for (int m = 0; m < nqbins; m++) {
      double qn = qmin + m * dq;
      double Sn = 0;
      for (i = 0; i < nbins; i++) {
        Sn += (g[i] - 1) * r[i] * sin(qn*r[i]) * dr;
      }

      q[n] = qn;
      S[n] = 1.0 + 4.0 * pi * rho * Sn / qn;
      ofs << q[n] << " " << S[n] << "\n";
      n++;
    }
*/

    // Log scale in q

    int nintervals = (int)(log(qmax/qmin)/log(10.0));
    int npointsperinterval = ceil(nqbins / nintervals)+1;

    double s, e, dq;
    int done = 0;
    int n = 0;
    for (int m = 0; m < nintervals; m++) {
      s = qmin * pow(10.0,m);
      e = s * 10.0;
      dq = (e - s) / (double)npointsperinterval;
    
      for (int l = 0; l < npointsperinterval; l++) {
        double qn = s + l * dq;
        double Sn = 0;
        for (i = 0; i < nbins; i++) {
          Sn += (g[i] - 1) * r[i] * sin(qn*r[i]) * dr;
        }

        if (n == nqbins) {
          done = 1;
          break;
        }

        q[n] = qn;
        S[n] = 1.0 + 4.0 * pi * rho * Sn / qn;
        ofs << q[n] << " " << S[n] << "\n";
        n++;
      }
      if (done == 1) break;
    }

    ofs.close();
  }

  return 1;
}

/*----------------------------------------------------------------------------
  Implementing the structure factor from direct calculation
  Ref. K. Zhang, On the Concept of Static Structure Factor, arXiv:1606.03610 (2016)
  Lx, Ly, Lz = box dimensions
  dr   = distance bin size
  nkx, nky, nkz = number of k in each direction
  IMPORTANT: k also has unit of inverse length as we are evaluating exp(k dot x)
    where x is the position in some distance unit.
  For a peak showing up at k_m, the corresponding wavelength is 2*pi/k_m.
  For a box length of L, the minimum physical peak k* should be greater than 2*pi/L.
------------------------------------------------------------------------------*/

void RDF::Sq_direct(const BeadList& list1, const BeadList& list2,
                    int nkx, int nky, int nkz, double dkx, double dky, double dkz,
                    int nthreads, char* sq_filename)
{
  printf("S(q) from brute force calculation with wave vectors using %d threads ..\n", nthreads);

//  char* s = getenv("OMP_NUM_THREADS");
//  if (strlen(s) > 0) nthreads = atoi(s);
  omp_set_num_threads(nthreads);

  int i, nk, numberOfParticles;
  double dk = sqrt(dkx*dkx+dky*dky+dkz*dkz);
  double pi = 4.0*atan(1.0);
  double L = (box.Lx+box.Ly+box.Lz)/3.0;

  numberOfParticles = list1.size();

  memset(Sk, 0, numberOfWavevectors*sizeof(double));
  memset(Sradial, 0, nqbins*sizeof(float));
  memset(qradial, 0, nqbins*sizeof(float));
  memset(count, 0, nqbins*sizeof(int));

	double start = omp_get_wtime();

  if (numberOfParticles < numberOfWavevectors) {

    #pragma omp parallel default(shared)
    {
    int me = omp_get_thread_num();
    int num_k_per_thread = numberOfWavevectors / nthreads;
    if (num_k_per_thread * nthreads < numberOfWavevectors) num_k_per_thread++;
    int start = me * num_k_per_thread;
    int end = start + num_k_per_thread;

    for (int ii = start; ii < end; ii++) {
      double kx, ky, kz;
      kx = waveVectors[ii].x;
      ky = waveVectors[ii].y;
      kz = waveVectors[ii].z;
      double c = 0.0;
      double s = 0.0;
      for (int j = 0; j < numberOfParticles; j++) {
        double kdotr = kx*list1[j].x + ky*list1[j].y + kz*list1[j].z;
        c += cos(kdotr);
        s += sin(kdotr);
      } // end loop over particle j

      Sk[ii] = c * c + s * s;
    } // end loop over nk

    } // end of omp parallel region

  } else { // if numberOfParticles > numberOfWavevectors

    memset(Sk_rep, 0, nthreads*2*numberOfWavevectors*sizeof(double));

    #pragma omp parallel
    {
    int me = omp_get_thread_num();
    int stride = 2*numberOfWavevectors;
    double *local_Sk = &Sk_rep[me*stride];

    #pragma omp for schedule(static)
    for (int j = 0; j < numberOfParticles; j++) {
      double xtmp = list1[j].x;
      double ytmp = list1[j].y;
      double ztmp = list1[j].z;
      for (int ii = 0; ii < numberOfWavevectors; ii++) {
        double kdotr = waveVectors[ii].x*xtmp + waveVectors[ii].y*ytmp +
          waveVectors[ii].z*ztmp;
        local_Sk[2*ii+0] += cos(kdotr);
        local_Sk[2*ii+1] += sin(kdotr);
      } // end loop over ii
    } // end loop over particle j

    #pragma omp for schedule(static) nowait
    for (int ii = 0; ii < numberOfWavevectors; ii++) {
      int offset = nthreads/2;
      while (offset > 0) {
        for (int j = 0; j < offset; j++)
          if (j + offset < nthreads) {
            Sk_rep[j*stride+2*ii+0] += Sk_rep[(j+offset)*stride+2*ii+0];
            Sk_rep[j*stride+2*ii+1] += Sk_rep[(j+offset)*stride+2*ii+1];
          }
        offset >>= 1;
      }
      Sk[ii] = Sk_rep[2*ii]*Sk_rep[2*ii]+Sk_rep[2*ii+1]*Sk_rep[2*ii+1];
    }

    } // end of omp parallel region
  }

  printf("S(q) compute time = %0.3f seconds\n", (double)(omp_get_wtime() - start));

  ofstream ofs; 
  ofs.open(sq_filename);
  ofs << "k S(k)\n";

  // Radially average the 3-D Fourier data
/*
  int kcount, ibin;
  double knorm,skmean;
  double kminus,kplus,kmag2;

  kcount = 0;
  knorm = 0.0;
  skmean = 0.0;

  double BINSIZE = 0.001;
  std::vector<double> kvector, skvector;

  for (nk = 0; nk < numberOfWavevectors - 1; nk++) {
    kcount++;
    knorm += sqrt(waveVectors[nk].mag2);
    skmean += Sk[nk];
    kplus = waveVectors[nk+1].mag2;
    kminus = waveVectors[nk].mag2;
    if (fabs(kplus-kminus) > BINSIZE || nk + 1 == numberOfWavevectors) {
      knorm /= kcount;
      skmean /= kcount;

      kvector.push_back(knorm);
      skvector.push_back(skmean);

      kcount = 0;
      knorm = 0.0;
      skmean = 0.0;
    }
  }


  // sub-sampling S(k)
  int num_vectors = skvector.size();
  for (i = 0; i < num_vectors; i++) {
    int ibin = (int)(kvector[i] / qbin);
    if (ibin < nqbins) {
      Sradial[ibin] += skvector[i];
      qradial[ibin] += kvector[i];
      count[ibin] += 1;
    }

  }

  for (i = 0; i < nqbins; i++) {
    if (count[i] > 0)
      ofs << qradial[i]/count[i] << " " << Sradial[i]/count[i] << " " << count[i] << std::endl;
  }

*/

  for (nk = 0; nk < numberOfWavevectors; nk++) {
    int ibin = (int)(sqrt(waveVectors[nk].mag2)/qbin);
    if (ibin < nqbins) {
      Sradial[ibin] += Sk[nk];
      count[ibin]++;
    }
  }

  for (i = 0; i < nqbins; i++) {
    if (count[i] > 0) Sradial[i] /= count[i];
    qradial[i] = i * qbin;
  }
  double Nsq = (double)(numberOfParticles)*(double)(numberOfParticles);
  for (i = 0; i < nqbins; i++) {
    if (count[i] > 0) ofs << i*qbin/2.0 << " " << Sradial[i]/Nsq << std::endl;
  }


  ofs.close();
}

/*----------------------------------------------------------------------------
  Implementing the structure factor from Fourier transform of the particle density grid
------------------------------------------------------------------------------*/

void RDF::Sq_fft(const BeadList& list, double dr, int nthreads, char* sq_filename)
{
  printf("\nS(q) from Fourier transform of the density 3d grid using %d threads ..\n", nthreads);

  omp_set_num_threads(nthreads);

  int nx, ny, nz, numberOfParticles;
  double pi = 4.0*atan(1.0);
  numberOfParticles = list.size();
  nx = (int)(box.Lx / fft_dr);
  ny = (int)(box.Ly / fft_dr);
  nz = (int)(box.Lz / fft_dr);
  int nbins = nx * ny * nz;

  printf("FFT grid: %d %d %d\n", nx, ny, nz);

  ofstream ofs; 
  ofs.open(sq_filename);
  ofs << "q S(q)\n";

  double start = omp_get_wtime();

#ifdef _USE_FFTW

  fftw_complex* rho = (fftw_complex *) fftw_malloc(sizeof(fftw_complex)*nbins);
  fftw_complex *out = (fftw_complex *) fftw_malloc(sizeof(fftw_complex)*nbins);

  #pragma omp parallel for
  for (int i = 0; i < nbins; i++)
    rho[i][0] = rho[i][1] = 0;

  for (int i = 0; i < numberOfParticles; i++) {
    int ix = (int)((list[i].x - box.xlo) / fft_dr);
    int iy = (int)((list[i].y - box.ylo) / fft_dr);
    int iz = (int)((list[i].z - box.zlo) / fft_dr);
    int idx = iz + nz * iy + nz * ny * ix;
    if (idx >= 0 && idx < nbins) rho[idx][0] += 1.0;
  }

  // 3D DFT of the particle density grid
  fftw_plan plan = fftw_plan_dft_3d(nx, ny, nz, rho, out, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(plan);

  double* Sq = (double *) malloc(nbins * sizeof(double));
  #pragma omp parallel for
  for (int i = 0; i < nbins; i++) {
    Sq[i] = out[i][0]*out[i][0] + out[i][1]*out[i][1];
  }

  int nsq = nx*nx + ny*ny + nz*nz;
  double *Sq_ave = (double *) malloc(nsq * sizeof(double));
  double* qsq_ave = (double *) malloc(nsq * sizeof (double));
  double *qcount = (double *) malloc(nsq * sizeof(double));
  memset(Sq_ave, 0, nsq*sizeof(double));
  memset(qsq_ave, 0, nsq*sizeof(double));
  memset(qcount, 0, nsq*sizeof(double));

  #pragma omp parallel for shared(nx,ny,nz)
  for (int i = 0; i < nbins; i++) {
    int ix = i / (ny * nz);
    int iy = (i % (ny * nz)) / nz;
    int iz = (i % (ny * nz)) % nz;
    int qsq = ix*ix + iy*iy + iz*iz;
    int idx = iz + nz * iy + nz * ny * ix;
    
    Sq_ave[qsq] += Sq[idx];
    qsq_ave[qsq] += qsq;
    qcount[qsq] += 1.0;
  }

  double dq = pi/box.Lx;
  double Nsq = (double)(numberOfParticles)*(double)(numberOfParticles);
  int nvalues = (int)(nsq/10.0);
  for(int i = 0; i < nvalues; i++) {
    if (qcount[i] > 0) {
      double qval = dq*sqrt(qsq_ave[i]/qcount[i]);
      double rval = 2.0*pi/qval; // corresponding real-spaced value
      ofs << qval << " " << Sq_ave[i]/qcount[i]/Nsq << "\n";
    }
  }

  ofs.close(); 

  ofs.open("diffraction.txt");
  int cx = nx / 2;
  int cy = ny / 2;
  for (int ix = 0; ix < nx; ix++) {
    for (int iy = 0; iy < ny; iy++) {
      ofs << ix << " " << iy << " ";
      int idx = (ix - cx)*(ix - cx) + (iy - cy)*(iy - cy);
      if (qcount[idx] > 0) ofs << Sq_ave[idx]/qcount[idx]/Nsq;
      else ofs << "0";
      ofs << "\n";
    }
    ofs << "\n";
  }
  ofs.close();

  free(Sq_ave);
  free(qsq_ave);
  free(qcount);

  fftw_free(rho);
  fftw_free(out);
  fftw_destroy_plan(plan);
#endif

  printf("S(q) compute time = %0.3f seconds\n", (double)(omp_get_wtime() - start));

   
}

/*----------------------------------------------------------------------------
  load the bead list from a file
  only store the indices of beads of specified types into a template
------------------------------------------------------------------------------*/

int RDF::load_template(int listidx, char* fileName, char** types, int ntypes)
{
  std::vector<int> *indexList = NULL;
  if (listidx == 1) indexList = &indexList1;
  else if (listidx == 2) indexList = &indexList2;

  int success;
  string fullname(fileName);
  std::size_t found = fullname.find_last_of(".");
  base = fullname.substr(0,found);
  ext = fullname.substr(found+1);

  // temporary variables used for loading the template

  IOHandle* io_tmp = NULL;
  std::vector<Bead> list;
  int nNumBeads;

  // guess the file type

  if (strcmp(ext.c_str(),"xyz") == 0) {
    if (io_tmp == NULL) io_tmp = new XYZHandle;
    success = io_tmp->read(fileName, types, ntypes, nNumBeads, box, list, timestep);
  } else if (strcmp(ext.c_str(),"xml") == 0) {
    if (io_tmp == NULL) io_tmp = new XMLHandle;
    success = io_tmp->read(fileName, types, ntypes, nNumBeads, box, list, timestep);
  } else if (strcmp(ext.c_str(),"dump") == 0 || strcmp(ext.c_str(),"txt") == 0) {
    if (io_tmp == NULL) io_tmp = new DUMPHandle;
    success = io_tmp->read(fileName, types, ntypes, nNumBeads, box, list, timestep);
  } else {
    std::cout << "Unknown file extension.\n";
    return 0;
  }

  indexList->clear();
  for (int i = 0; i < nNumBeads; i++) {
    indexList->push_back(list[i].id);
  }

  delete io_tmp;

  return success;
}

