#ifndef __NORMAL_MODES
#define __NORMAL_MODES

#include <vector>
#include <string>
#include "buildingblocks.h"
#include "io.h"

namespace Analysis {

class Chain {
public:
  void project_on_modes(double** evectors, int nNumBeadsPerChain);
  void accumulate_correlation_matrix(double** matrix, int nNumBeadsPerChain);
  void remove_com_pbc(double Lx, double Ly, double Lz);
  std::vector<Bead> beadList;
};

class NormalModes
{
public:
  NormalModes(int nframes, int _nNumBeadsPerChain, int _periodic_wrapped=1);
  ~NormalModes();

  // load the bead list from a file
  int load_bead_list(char* fileName, char** types, int ntypes, int t=-1);

  // load the bead list from some where else: an array from DCDReader, XML Parser, etc.
  int load_bead_list(void*) { return 0; }

  // set fractions of beads of types[0] per chain within which the chains are considered 
  void set_fraction(double _fmin, double _fmax) { fmin = _fmin; fmax = _fmax; }

  //void analyze();
  void analyze_rouse();

protected:
  int nframes;              //! number of frames in the trajectory
  int periodic_wrapped;     //! 1 if PBC is used
  int nNumBeadsPerChain;    //! Number of beads per polymer chain (i.e. polymerization degree)
  double fmin, fmax;        //! fractions of beads of types[0] per chain within which the chains are considered
  int nNumTotalBeads;       //! Number of beads per snapshot
  Box box;                  //! Box dimensions
  int timestep;             //! Time step read from the config file
  int last_frame;

  int file_type;            //! Config file type: XYZ, DUMP (LAMMPS) or XML (HOOMD)
  string base, ext;         //! Config files are in the form: base.ext

  double** matrix;          //! Relative displacement matrix to COM, averaged over the chains
  double* evalues;          //! Eigenvalues
  double** evectors;        //! Eigenvectors = normal modes
  double* b;                //! Temporary variables for the eigenvalues
  double* z;                //! Temporary variables for the eigenvalues

  IOHandle* io_handle;      //! Handle to read/write data files
  class GSDReader* gsd;     //! Handle to read GSD files

protected:
  double*** X;              //! modes

  void compute_rouse_modes(std::vector<Chain>& chainList, int nmodes, int nframe);
  void rotate(double **matrix, int i, int j, int k, int l, double s, double tau);
  int diagonalize(double **matrix, int dim, double *evalues, double **evectors);
  void deep_copy(const Bead& src, Bead& dst);
  int compute_ncorr(int max_corr, int imax);
  void compute_log_points(int* points, int ncorr, int max_corr, int imax);
  void save_mode(int m, double** corr, int ncorr);
  void reorder_evectors(double* evalues, double** evectors, int n, int lo, int hi);
  int partition(double* evalues, double** evectors, int n, int lo, int hi);
  int load_from_gsd(char** types, int ntypes, int& nNumTotalBeads, Box& box,
    BeadList& beadList, int nframe);

  double* buffer;
};

} // namespace Polymers

#endif
