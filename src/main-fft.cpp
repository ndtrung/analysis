/*
  Fourier transforms

  This is an example of using the classes and API provided by libanalyzer.a.

  Compile:     make -f Makefile.cpu
  Clean-build: make -f Makefile.cpu clean

  Contact: Trung Nguyen, ndactrung@gmail.com

*/

#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

#include "io.h"
#include "memory.h"
#include "fourier.h"

using namespace std;
using namespace Analysis;

void test_1d();

int main(int argc, char** argv)
{
  test_1d();
  return 0;
}

void test_1d()
{
  int N = 2048;                      // number of data points, power of 2
  double dt = 0.02;                  // time interval
  double fs = 1.0 / dt;              // sampling frequency
  double fnyq = fs / 2.0;            // Nyquist frequency (freq max)
  double f1 = 0.05;                  // signal frequencies
  double f2 = 1.0;
  double df = fnyq / (N/2);          // frequency interval
  double pi = 4.0*atan(1.0);

  double* data = new double [N];     // calibrating signal

  double ave = 0;
  for (int i = 0; i < N; i++) {
    data[i] = 6.0*sin(2*pi*f1*i*dt) + 3.0*sin(2*pi*f2*i*dt);
    ave += data[i];
  }
  ave /= (double)N;

  for (int i = 0; i < N; i++)
    data[i] -= ave;

  Fourier fft;
  fft.setdata1d(data, N);

  fft.execute();

  int m = 0;
  for (int i = 0; i < N/2; i++) {
    double p = fft.output[m+0]*fft.output[m+0]+fft.output[m+1]*fft.output[m+1];
    printf("%f %f\n", i*df, 2.0*sqrt(p)/N);
    m += 2;
  }

  delete [] data;
}

