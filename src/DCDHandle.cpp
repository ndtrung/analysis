/*
  Copyright (C) 2017  Trung D. Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*----------------------------------------------------------------------------
  DCD file handler
  Created by Martin Girard on 10/19/16.
  Bugfixed and updated by Trung Nguyen (ndactrung@gmail.com) on 11/8/16.
------------------------------------------------------------------------------*/

#include <fstream>
#include <iostream>
#include <cmath>
#include "DCDHandle.h"
#include "memory.h"

using namespace Analysis;

DCDHandle::DCDHandle() : ptr_file(NULL), x(NULL), y(NULL), z(NULL), head_read(false)
{
  // initialize the header struct params
  head.ischarmm = false;
  head.ischarmm_xtra_block = false;
  head.ischarmm_4dims = false;
  head.double_delta = false;
  head.title = NULL;
}

/*----------------------------------------------------------------------------*/

DCDHandle::~DCDHandle()
{
  destroy(x);
  destroy(y);
  destroy(z);
  destroy(head.title);
  close_dcd();
}

/*----------------------------------------------------------------------------*/

int DCDHandle::read(const char* fileName, int& nNumTotalBeads,
    Box& box, std::vector<Bead>& beadList, const int nframe)
{
  int success = open_DCD(fileName);

  if (success < 0) return 0;

  if (nframe < 0 || nframe >= nframes) return 0;

  beadList.clear();

  goto_frame(nframe);
  get_frame(_box, x, y, z);

  nNumTotalBeads = n_particles;
  box.Lx = _box[0];
  box.Ly = _box[1];
  box.Lz = _box[2];
  box.xlo = -0.5*box.Lx;
  box.xhi = 0.5*box.Lx;
  box.ylo = -0.5*box.Ly;
  box.yhi = 0.5*box.Ly;
  box.zlo = -0.5*box.Lz;
  box.zhi = 0.5*box.Lz;
  for (int i = 0; i < nNumTotalBeads; i++) {
    Bead bead;
    bead.x = x[i];
    bead.y = y[i];
    bead.z = z[i];
    bead.q = 0;
    beadList.push_back(bead);
  }

  // write the coordinates in the frame into a XML file
  // note: there's no charge information from the DCD file
  string base, fullname(fileName);
  std::size_t found = fullname.find_last_of(".");
  base = fullname.substr(0,found);
  char* snapshot = new char [128];
  sprintf(snapshot, "%s-frame-%d.xml", base.c_str(), nframe);
  write_xml(snapshot);
  delete [] snapshot;

  return 1;
}

/*----------------------------------------------------------------------------*/

int DCDHandle::read(const char* fileName, std::vector<int>& indices, int& nNumTotalBeads,
    Box& box, std::vector<Bead>& beadList, const int nframe)
{
  int success = open_DCD(fileName);

  if (success < 0) return 0;

  if (nframe < 0 || nframe >= nframes) return 0;

  beadList.clear();

  goto_frame(nframe);
  get_frame(_box, x, y, z);

  nNumTotalBeads = indices.size();
  box.Lx = _box[0];
  box.Ly = _box[1];
  box.Lz = _box[2];
  box.xlo = -0.5*box.Lx;
  box.xhi = 0.5*box.Lx;
  box.ylo = -0.5*box.Ly;
  box.yhi = 0.5*box.Ly;
  box.zlo = -0.5*box.Lz;
  box.zhi = 0.5*box.Lz;

  for (int i = 0; i < nNumTotalBeads; i++) {
    Bead bead;
    int idx = indices[i];
    bead.x = x[idx];
    bead.y = y[idx];
    bead.z = z[idx];
    bead.q = 0;
    beadList.push_back(bead);
  }

  // write the coordinates in the frame into a XML file
  // note: there's no charge information from the DCD file
  string base, fullname(fileName);
  std::size_t found = fullname.find_last_of(".");
  base = fullname.substr(0,found);
  char* snapshot = new char [128];
  sprintf(snapshot, "%s-frame-%d.xml", base.c_str(), nframe);
  write_xml(snapshot);
  delete [] snapshot;

  return 1;
}

/*----------------------------------------------------------------------------*/

void DCDHandle::write(const Cluster& cluster, const std::vector<Particle>& particleList,
  const Box& box, const char* fileName, int cluster_id, int timestep)
{
  int i, j, nNumParticles;
  char* file = new char [256];
  ofstream ofs;

  sprintf(file, "%s-cluster_%d.xml", fileName, cluster_id);
  ofs.open(file);

  ofs << "<\?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
  ofs << "<hoomd_xml version=\"1.5\">\n";
  ofs << "<configuration time_step=\"" << timestep;
  ofs << "\" dimensions=\"3\" natoms=\"" << cluster.size() << "\" >\n";
  ofs << "<box lx=\"" << box.Lx << "\" ly=\"" << box.Ly << "\" lz=\"" << box.Lz;
  ofs << "\" xy=\"0\" xz=\"0\" yz=\"0\"/>\n";
  ofs << "<position num=\"" << cluster.size() << "\">\n";
  ofs.precision(15);

  nNumParticles = particleList.size();
  for (i = 0; i < nNumParticles; i++) {
    int nNumBeads = particleList[i].beadList.size();
    for (j = 0; j < nNumBeads; j++) {
      ofs << particleList[i].beadList[j].x << " "
          << particleList[i].beadList[j].y << " "
          << particleList[i].beadList[j].z << "\n";
    }
  }

  ofs << "</position><type>\n";
  for (i = 0; i < nNumParticles; i++) {
    int nNumBeads = particleList[i].beadList.size();
    for (j = 0; j < nNumBeads; j++)
      ofs << particleList[i].beadList[j].type << "\n";
  }

  ofs << "</type><charge>\n";
  for (i = 0; i < nNumParticles; i++) {
    int nNumBeads = particleList[i].beadList.size();
    for (j = 0; j < nNumBeads; j++)
      ofs << particleList[i].beadList[j].q << "\n";
  }
  ofs << "</charge>\n";

  ofs << "</configuration>\n";
  ofs << "</hoomd_xml>\n";

  ofs.close();

  delete [] file;
}

/*----------------------------------------------------------------------------
  a placeholder for writing out the current frame to a XML file given the DCD file is already opened
  before calling this function, the user should open the DCD file first, go to a specific frame
------------------------------------------------------------------------------*/

void DCDHandle::write_xml(const char* fileName)
{
  if (!is_reading()) return;

  ofstream ofs;
  ofs.open(fileName);

  ofs << "<\?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
  ofs << "<hoomd_xml version=\"1.5\">\n";
  ofs << "<configuration time_step=\"" << current_frame;
  ofs << "\" dimensions=\"3\" natoms=\"" << n_particles << "\" >\n";
  ofs << "<box lx=\"" << _box[0] << "\" ly=\"" << _box[1] << "\" lz=\"" << _box[2];
  ofs << "\" xy=\"0\" xz=\"0\" yz=\"0\"/>\n";
  ofs << "<position num=\"" << n_particles << "\">\n";
  ofs.precision(15);

  for (int i = 0; i < n_particles; i++) {
    ofs << x[i] << " " << y[i] << " " << z[i] << "\n";
  }

  ofs << "</position><type>\n";
  for (int i = 0; i < n_particles; i++)
    ofs << "1" << std::endl;

  ofs << "</type><charge>\n";
  for (int i = 0; i < n_particles; i++) {
    ofs << "0" << std::endl;
  }
  ofs << "</charge>\n";

  ofs << "</configuration>\n";
  ofs << "</hoomd_xml>\n";

  ofs.close();
}

/*----------------------------------------------------------------------------*/

int DCDHandle::open_DCD(const char *filename)
{
  // if already reading something, close it
  if (ptr_file) std::fclose(ptr_file);

  ptr_file = std::fopen(filename, "rb");
  // unable to open file, fopen returned NULL
  if (!ptr_file)
    return -1;

  // unable to seek end of file
  if (!std::fseek(ptr_file, 0, SEEK_END)) {
    filesize = std::ftell(ptr_file);
    if (std::fseek(ptr_file, 0, SEEK_SET)) { // unable to go back to beginning, fseek returns nonzero
      std::fclose(ptr_file);
      return -2;
    }
  } else {
    std::fclose(ptr_file);
    return -2;
  }

  std::size_t byteread, ncount;
  bool read_error = false;

  // freed returns number of objects, not number of bytes
  ncount = std::fread(&head.blocksize1, sizeof(head.blocksize1), 1, ptr_file);
  read_error = read_error || ncount != 1;

  if (head.blocksize1 != 84) { // bad magic number, one should try different endianess here
    std::fclose(ptr_file);
    return -4;
  }

  ncount = std::fread(&head.hdr, sizeof(char), 4, ptr_file);
  read_error = read_error || ncount != 4;

  ncount = std::fread(&head.nset, sizeof(head.nset), 1, ptr_file);
  read_error = read_error || ncount != 1;

  ncount = std::fread(&head.istart, sizeof(head.istart), 1, ptr_file);
  read_error = read_error || ncount != 1;

  ncount = std::fread(&head.nsavc, sizeof(head.nsavc), 1, ptr_file);
  read_error = read_error || ncount != 1;

  ncount = std::fread(&head.nstep, sizeof(head.nstep), 1, ptr_file);
  read_error = read_error || ncount != 1;

  ncount = std::fread(&head.null4, sizeof(head.null4[0]), 4, ptr_file);
  read_error = read_error || ncount != 4;

  ncount = std::fread(&head.nfreat, sizeof(head.nfreat), 1, ptr_file);
  read_error = read_error || ncount != 1;

  float buf;
  ncount = std::fread(&buf, sizeof(buf), 1, ptr_file);
  read_error = read_error || ncount != 1;
  head.delta = (double) buf;

  ncount = std::fread(&head.null9, sizeof(head.null9[0]), 9, ptr_file);
  read_error = read_error || ncount != 9;

  ncount = std::fread(&head.version, sizeof(head.version), 1, ptr_file);
  read_error = read_error || ncount != 1;

  if (read_error)
    return -5;

  // ensure version of DCD file is correct
  if (head.version > 0) {
    head.ischarmm = true;
  } else {
    long cof = std::ftell(ptr_file);
    if (std::fseek(ptr_file, 44, SEEK_SET)) {
      std::fclose(ptr_file);
      return -2;
    }
    ncount = std::fread(&head.delta, sizeof(head.delta), 1, ptr_file);
    read_error = read_error || ncount != 1;
    if (std::fseek(ptr_file, cof, SEEK_SET)) {
      std::fclose(ptr_file);
      return -2;
    }
  }

  if (head.ischarmm) {
    long cof = std::ftell(ptr_file);
    std::fseek(ptr_file, 48, SEEK_SET);
    std::int32_t n;
    std::fread(&n, sizeof(n), 1, ptr_file);
    if (n == 1) head.ischarmm_xtra_block = true;

    std::fseek(ptr_file, 52, SEEK_SET);
    std::fread(&n, sizeof(n), 1, ptr_file);
    if (n == 1) head.ischarmm_4dims = true;

    std::fseek(ptr_file, cof, SEEK_SET);
  }

  std::int32_t blocksize1;
  std::fread(&blocksize1, sizeof(blocksize1), 1, ptr_file);
  if (blocksize1 != head.blocksize1) {
    fclose(ptr_file); 
    return -5;
  }
  
  std::fread(&head.blocksize2, sizeof(head.blocksize2), 1, ptr_file);
  std::fread(&head.ntitle, sizeof(head.ntitle), 1, ptr_file);
  destroy(head.title);
  create(head.title, head.ntitle*80);
  std::fread(head.title, sizeof(char), 80*head.ntitle, ptr_file);

  std::int32_t blocksize2;
  std::fread(&blocksize2, sizeof(blocksize2), 1, ptr_file);
  if (blocksize2 != head.blocksize2) {
    fclose(ptr_file); return -5;
  }

  std::fread(&head.blocksize3, sizeof(head.blocksize3), 1, ptr_file);
  std::fread(&n_particles, sizeof(n_particles), 1, ptr_file);
  std::int32_t blocksize3;
  std::fread(&blocksize3, sizeof(blocksize3), 1, ptr_file);
  if (head.blocksize3 != blocksize3) {
    fclose(ptr_file);
    return -5;
  }

  headersize = std::ftell(ptr_file);

  extra_block_size = head.ischarmm_xtra_block ? 4*2 + 8*6 : 0;
  coord_block_size = (4*2 + 4*n_particles)*3;
  framesize = extra_block_size + coord_block_size;
  nframes = (filesize - headersize) / framesize;
  current_frame = 0;

  if (n_particles > 0 && x == NULL && y == NULL && z == NULL) {
    create(x, n_particles);
    create(y, n_particles);
    create(z, n_particles);
  }

  return 0;
}

/*----------------------------------------------------------------------------*/

int DCDHandle::get_frame(float *box, float *x, float *y, float *z)
{
  std::size_t objcount;
  if (current_frame == nframes)
    return -1;

  if (!ptr_file)
    return -2;

  long cof = std::ftell(ptr_file);
  std::int32_t blksize;
  if (head.ischarmm_xtra_block) {
    double dummy[6];
    std::fread(&blksize, sizeof(blksize), 1, ptr_file);
    std::fread(&dummy, sizeof(double), 6, ptr_file);
    std::fread(&blksize, sizeof(blksize), 1, ptr_file);
    float dummy2[6];

    for (int idx = 0; idx < 6; idx ++) {
      dummy2[idx] = (float) dummy[idx];
    }

    _box[0] = dummy2[0]; 
    _box[1] = dummy2[2];
    _box[2] = dummy2[5];
    _box[3] = std::acos(dummy2[4]);
    _box[4] = std::acos(dummy2[3]);
    _box[5] = std::acos(dummy2[1]); // charmm
  }

  // read x
  std::fread(&blksize, sizeof(blksize), 1, ptr_file);
  if (blksize/4 != (std::int32_t)n_particles)
    return -3;
  std::fread(x, sizeof(float), blksize/4, ptr_file);
  std::fread(&blksize, sizeof(blksize), 1, ptr_file);

  // read y
  std::fread(&blksize, sizeof(blksize), 1, ptr_file);
  if (blksize/4 != (std::int32_t)n_particles)
    return -3;
  std::fread(y, sizeof(float), blksize/4, ptr_file);
  std::fread(&blksize, sizeof(blksize), 1, ptr_file);

  // read z
  std::fread(&blksize, sizeof(blksize), 1, ptr_file);
  if (blksize/4 != (std::int32_t)n_particles)
    return -3;
  std::fread(z, sizeof(float), blksize/4, ptr_file);
  std::fread(&blksize, sizeof(blksize), 1, ptr_file);

  if (head.ischarmm_4dims) { // skip this stuff
    std::fread(&blksize, sizeof(blksize), 1 ,ptr_file);
    std::fseek(ptr_file, (long) blksize, SEEK_CUR);
    std::fread(&blksize, sizeof(blksize), 1, ptr_file);
  }

  if (std::ferror(ptr_file) != 0)
    return -4;

  // inconsistent frame size
  if (framesize != (std::ftell(ptr_file) - cof))
    return -5;

  std::cout << "Loaded frame " << current_frame << " from " << nframes << " frames" << std::endl;

  return 0;
}

/*----------------------------------------------------------------------------*/

int DCDHandle::goto_frame(std::size_t frame)
{
  if (!ptr_file || frame > nframes)
    return -1;

  long offset = framesize * frame + headersize;
  if (offset > filesize)
    return -2;

  std::fseek(ptr_file, offset, SEEK_SET);
  current_frame = frame;
  return 0;
}

/*----------------------------------------------------------------------------*/

bool DCDHandle::is_reading()
{
  if (!ptr_file) return false;
  else return true; 
}

