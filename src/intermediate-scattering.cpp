/*
  Copyright (C) 2017  Trung D. Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*----------------------------------------------------------------------------
  Implementation of the self-intermediate scattering function F(k,t)

  TODO:
    1- Support GPU acceleration

  Trung Nguyen (ndactrung@gmail.com)
------------------------------------------------------------------------------*/

#include <algorithm>
#include <fstream>
#include <cmath>
#include <iostream>
#include <string>
#include <omp.h>

#include "DCDHandle.h"
#include "GSDReader.h"
#include "io.h"
#include "memory.h"
#include "intermediate-scattering.h"

#ifdef _USE_FFTW
#include "fftw3.h"
#endif

using namespace std;
using namespace Analysis;

#define anint(x) ((x >= 0.5) ? (1.0) : (x < -0.5) ? (-1.0) : (0.0)) 
#define MAX_NEIGHBORS        64
#define MAX_BEADS_IN_CELL    128
#define EPSILON 0.00001

enum {XYZ=0, XML=1, DUMP=2, DCD=3, GSD=4};

IntermediateScattering::IntermediateScattering(int _nframes, int _nthreads, int _periodic_wrapped)
  : nthreads(_nthreads), periodic_wrapped(_periodic_wrapped)
{
  io_handle = NULL;
  gsd = NULL;
  ksetup = 0;
  nframes = _nframes;

  Fk_rep = NULL;
}

/*----------------------------------------------------------------------------*/

IntermediateScattering::~IntermediateScattering()
{
  if (io_handle) delete io_handle;
  if (gsd) delete gsd;
  beadList.clear();
  beadList0.clear();
  if (Fk_rep) delete [] Fk_rep;
}

/*----------------------------------------------------------------------------
  load the bead list from a file
  only pick the beads of specified types
------------------------------------------------------------------------------*/

int IntermediateScattering::load_bead_list(char* fileName, char** types, int ntypes, int nframe,
  int reference_point)
{
  if (last_frame == nframes) return 0;

  int success = 0;
  string fullname(fileName);
  std::size_t found = fullname.find_last_of(".");
  base = fullname.substr(0,found);
  ext = fullname.substr(found+1);
  
  char* filetime = new char [1024];

  // guess the file type
  if (strcmp(ext.c_str(),"xyz") == 0) {
    if (io_handle == NULL) io_handle = new XYZHandle;
    file_type = XYZ;
  } else if (strcmp(ext.c_str(),"xml") == 0) {
    if (io_handle == NULL) io_handle = new XMLHandle;
    success = io_handle->read(fileName, types, ntypes, nNumTotalBeads, box, beadList, timestep);
    file_type = XML;
  } else if (strcmp(ext.c_str(),"dump") == 0 || strcmp(ext.c_str(),"txt") == 0) {
    if (io_handle == NULL) io_handle = new DUMPHandle;
    timestep = nframe;
    sprintf(filetime, "%s%d.%s", base.c_str(), timestep, ext.c_str());
    success = io_handle->read(filetime, types, ntypes, nNumTotalBeads, box, beadList, timestep);
    file_type = DUMP;
  } else if (strcmp(ext.c_str(),"dcd") == 0) {
    if (io_handle == NULL) io_handle = new DCDHandle;
    char* file = new char [64];
    sprintf(file, "%s-%d", base.c_str(), nframe);
    base = std::string(file);
    timestep = nframe;
    success = io_handle->read(fileName, nNumTotalBeads, box, beadList, nframe);
    file_type = DCD;
    delete [] file;
  } else if (strcmp(ext.c_str(),"gsd") == 0) {
    if (gsd == NULL) gsd = new GSDReader(fileName);
    timestep = nframe;
    success = load_from_gsd(types, ntypes, nNumTotalBeads, box, beadList, timestep);
    file_type = GSD;
  } else {
    std::cout << "Unknown file extension.\n";
    delete [] filetime;
    return 0;
  }

  // store the reference state
  if (reference_point) {
    beadList0.clear();
    int n = beadList.size();
    for (int i = 0; i < n; i++) beadList0.push_back(beadList[i]);
  }

  delete [] filetime;
  return success;
}

/*! \param types Array of particle types to be selected
    \param ntypes Number of types to be selected
    \param box Box dimensions (output)
    \param nNumTotalBeads Total mumber of particles (output)
    \param beadList Array of particles received (output)
    \param nframe current frame in the GSD file

    Load the bead list from a file only pick the beads of specified types.
*/
int IntermediateScattering::load_from_gsd(char** types,
  int ntypes, int& nNumTotalBeads, Box& box, BeadList& list, int nframe)
{
  if (!gsd) return 0;

  gsd->read_frame(nframe);

  const float* _box = gsd->getBoxDim();
  box.Lx = _box[0];
  box.Ly = _box[1];
  box.Lz = _box[2];
  box.xlo = -0.5*box.Lx;
  box.xhi = 0.5*box.Lx;
  box.ylo = -0.5*box.Ly;
  box.yhi = 0.5*box.Ly;
  box.zlo = -0.5*box.Lz;
  box.zhi = 0.5*box.Lz;

  list.clear();

  int success = 1;
  int num_beads = gsd->getTotalNumParticles();
  unsigned int* beadtypes = gsd->getTypes();
  int* image = gsd->getImages();
  float* pos = gsd->getPositions();
  float* charge = gsd->getCharges();
  float* vel = gsd->getVelocities();
  int count1 = 0;
  int count2 = 0;
  for (int i = 0; i < num_beads; i++) {

    char stype[16];
    sprintf(stype, "%d", beadtypes[i]);
    int included = 0;
    for (int m = 0; m < ntypes; m++) {
      if (strcmp(stype, types[m]) == 0) {
        included = 1;
        break;
      }
    }
    
    if (included == 1) {
      Bead bead;
      bead.id = ++count1;
      sprintf(bead.type, "%d", beadtypes[i]);
      bead.x = pos[3*i+0];
      bead.y = pos[3*i+1];
      bead.z = pos[3*i+2];
      bead.ix = image[3*i];
      bead.iy = image[3*i+1];
      bead.iz = image[3*i+2];
      bead.vx = vel[3*i+0];
      bead.vy = vel[3*i+1];
      bead.vz = vel[3*i+2];
      bead.q = charge[i];
      list.push_back(bead);
    }
  }

  nNumTotalBeads = list.size();

  return success;
}

/*----------------------------------------------------------------------------*/

void IntermediateScattering::set_k_vectors(double _k, int _num)
{
  if (ksetup) return;

  k = _k;
  numberOfWavevectors = _num;
  setup_waveVectors();

  // each thread stores 2 double values (real and imaginary) per each wave vector
  Fk_rep = new double [nthreads*2*numberOfWavevectors];
}

/*----------------------------------------------------------------------------
  Compute the self part of the van Hove correlation function:
    G_s(r,t) = 1/N \langle sum_j (x_j(t) - x_j(0)) \rangle
------------------------------------------------------------------------------*/

double IntermediateScattering::compute_vanHove()
{
  int numberOfParticles = beadList.size();
  double G = 0;
  for (int j = 0; j < numberOfParticles; j++) {
    double delx = beadList[j].x-beadList0[j].x;
    double dely = beadList[j].y-beadList0[j].y;
    double delz = beadList[j].z-beadList0[j].z;
    box.minimum_image(delx, dely, delz);
    G += sqrt(delx*delx + dely*dely + delz*delz);
    
  } // end loop over particle j
  
  // averaged over the number of particles
  G /= (double)numberOfParticles;

  return G;
}

/*----------------------------------------------------------------------------
  Compute the self intermediate scattering function:
    S(k,t) = 1/N \langle sum_j i k \cdot (x_j(t) - x_j(0)) \rangle
------------------------------------------------------------------------------*/

double IntermediateScattering::compute()
{
  omp_set_num_threads(nthreads);

  int numberOfParticles = beadList.size();
  memset(Fk_rep, 0, nthreads*2*numberOfWavevectors*sizeof(double));

  #pragma omp parallel
  {
  int me = omp_get_thread_num();
  int stride = 2*numberOfWavevectors;
  double *local_Fk = &Fk_rep[me*stride];

  #pragma omp for schedule(static)
  for (int j = 0; j < numberOfParticles; j++) {
    double delx = beadList[j].x-beadList0[j].x;
    double dely = beadList[j].y-beadList0[j].y;
    double delz = beadList[j].z-beadList0[j].z;
    box.minimum_image(delx, dely, delz);
    for (int ii = 0; ii < numberOfWavevectors; ii++) {
      double kdotr = waveVectors[ii].x*delx + waveVectors[ii].y*dely +
        waveVectors[ii].z*delz;
      local_Fk[2*ii+0] += cos(kdotr);
      local_Fk[2*ii+1] += sin(kdotr);
    } // end loop over the wave vectors
  } // end loop over particle j
  
  } // end of omp parallel region

  // sum across the wave vectors via binary reduction
  double F = 0;
  int stride = 2*numberOfWavevectors;
  for (int ii = 0; ii < numberOfWavevectors; ii++) {
    int offset = nthreads/2;
    while (offset > 0) {
      for (int j = 0; j < offset; j++)
        if (j + offset < nthreads) {
          Fk_rep[j*stride+2*ii+0] += Fk_rep[(j+offset)*stride+2*ii+0];
          Fk_rep[j*stride+2*ii+1] += Fk_rep[(j+offset)*stride+2*ii+1];
        }
      offset >>= 1;
    }
    F += sqrt(Fk_rep[2*ii]*Fk_rep[2*ii]+Fk_rep[2*ii+1]*Fk_rep[2*ii+1]);
  }

  // averaged over the wave vectors and normalized by the number of particles
  F /= (double)numberOfWavevectors;
  F /= (double)numberOfParticles;

  return F;
}

/*----------------------------------------------------------------------------*/

void IntermediateScattering::setup_waveVectors()
{
  if (ksetup == 1) return;

  cout << "Setting up wave vectors..\n";

  double PI = 4.0*atan(1.0);
  waveVectors.resize(numberOfWavevectors);

  for (int nk = 0; nk < numberOfWavevectors; nk++) {

    // generate a random vector on a unit sphere
    double ran1, ran2;
    double rsq = 2.0;
    while (rsq >= 1.0) {
      ran1 = 1.0 - 2*drand48();
      ran2 = 1.0 - 2*drand48();
      rsq = ran1*ran1 + ran2*ran2;
    }
    double ranh = 2.0*sqrt(1.0 - rsq);
    double bx = ran1 * ranh;
    double by = ran2 * ranh;
    double bz = 1.0 - 2.0 * rsq;

    double kx = bx*k;
    double ky = by*k;
    double kz = bz*k;
    waveVectors[nk].x = kx;
    waveVectors[nk].y = ky;
    waveVectors[nk].z = kz;
    waveVectors[nk].mag2 = kx * kx + ky * ky + kz * kz;
  }

  ksetup = 1;
}

