/*
  Perform Fourier transforms for 1D, 2D and 3D data

  Prerequisites: FFTW

  Contact: Trung Nguyen (ndactrung@gmail.com)
*/

#include <algorithm>
#include <fstream>
#include <cmath>
#include <iostream>
#include <string>
#include <omp.h>

#include "memory.h"
#include "fourier.h"

#ifdef _USE_FFTW
#include "fftw3.h"
#endif

using namespace std;
using namespace Analysis;

#define anint(x) ((x >= 0.5) ? (1.0) : (x < -0.5) ? (-1.0) : (0.0)) 

Fourier::Fourier(int _mode) : data(NULL), output(NULL), mode(_mode)
{
  mode = 0;
  dim = 0;
}

/*----------------------------------------------------------------------------*/

Fourier::~Fourier()
{
  destroy(output);
}

/*----------------------------------------------------------------------------*/

void Fourier::setdata1d(void* _data, int _n1)
{
  data = _data;
  n1 = _n1;
  dim = 1;
}

/*----------------------------------------------------------------------------*/

void Fourier::setdata2d(void* _data, int _n1, int _n2)
{
  data = _data;
  n1 = _n1;
  n2 = _n2;
  dim = 2;
}

/*----------------------------------------------------------------------------*/

void Fourier::setdata3d(void* _data, int _n1, int _n2, int _n3)
{
  data = _data;
  n1 = _n1;
  n2 = _n2;
  n3 = _n3;
  dim = 3;
}

/*----------------------------------------------------------------------------*/

int Fourier::execute()
{
  int success = 1;

  preprocess();

  if (mode == 0) {
    if (dim == 1) success = forward_fft1d();
    else if (dim == 2) success = forward_fft2d();
    else if (dim == 3) success = forward_fft3d();
    else success = 0;
  } else {
    if (dim == 1) success = backward_fft1d();
    else if (dim == 2) success = backward_fft2d();
    else if (dim == 3) success = backward_fft3d();
    else success = 0;
  }
  return success;
}

/*----------------------------------------------------------------------------*/

void Fourier::preprocess()
{

}

/*----------------------------------------------------------------------------*/

int Fourier::forward_fft1d()
{
  int success = 1;
  if (data == NULL) return 0;

  int i,m;
  double* input = (double*)data;

  // output holds n1 complex numbers

  destroy(output);
  create(output, 2*n1);

#ifdef _USE_FFTW
  fftw_complex *in,*out;
  fftw_plan plan;
  in = (fftw_complex *) fftw_malloc(sizeof(fftw_complex)*n1);
  out = (fftw_complex *) fftw_malloc(sizeof(fftw_complex)*n1);
  plan = fftw_plan_dft_1d(n1, in, out, FFTW_FORWARD, FFTW_ESTIMATE);

  // populate the array in with n1 complex numbers
  for (i = 0; i < n1; i++) {
    in[i][0] = input[i];
    in[i][1] = 0;
  }

  // perform forward fft
  fftw_execute(plan);

  // output retrieves the complex numbers from out
  m = 0;
  for (i = 0; i < n1; i++) {
    output[m+0] = out[i][0];
    output[m+1] = out[i][1];
    m += 2;
  }

  fftw_destroy_plan(plan);
  fftw_free(out);
  fftw_free(in);
#endif

  return success;
}

/*----------------------------------------------------------------------------*/

int Fourier::backward_fft1d()
{
  int success = 1;

  return success;
}

/*----------------------------------------------------------------------------*/

int Fourier::forward_fft2d()
{
  int success = 1;
  if (data == NULL) return 0;

  int i,j,n2h,m;
  n2h = (n2 / 2) + 1;

  destroy(output);
  create(output, n1*n2h);

#ifdef _USE_FFTW
  fftw_complex *out;
  fftw_plan plan_forward;
  out = (fftw_complex *) fftw_malloc(sizeof(fftw_complex)*n1*n2h);
  plan_forward = fftw_plan_dft_r2c_2d(n1, n2, (double*)data, out, FFTW_ESTIMATE);
  fftw_execute(plan_forward);

  m = 0;
  for (i = 0; i < n1; i++) {
    for (j = 0; j < n2h; j++) {
      output[m+0] = out[i*n2h+j][0];
      output[m+1] = out[i*n2h+j][1];
      m += 2;
    }
  }

  fftw_destroy_plan(plan_forward);
  fftw_free(out);
#endif

  return success;
}

/*----------------------------------------------------------------------------*/

int Fourier::backward_fft2d()
{
  int success = 1;

  return success;
}

/*----------------------------------------------------------------------------*/

int Fourier::forward_fft3d()
{
  int success = 1;
  if (data == NULL) return 0;

  int i,j,k,n3h,m;
  n3h = (n3 / 2) + 1;

  destroy(output);
  create(output, n1*n2*n3h);

#ifdef _USE_FFTW
  fftw_complex *out;
  fftw_plan plan_forward;

  out = (fftw_complex *) fftw_malloc(sizeof(fftw_complex)*n1*n2*n3h);
  plan_forward = fftw_plan_dft_r2c_3d(n1, n2, n3, (double*)data, out, FFTW_ESTIMATE);
  fftw_execute(plan_forward);

  m = 0;
  for (i = 0; i < n1; i++) {
    for (j = 0; j < n2; j++) {
      for (k = 0; k < n3h; k++) {
        output[m+0] = out[i*n2*n3h+j*n3h+k][0];
        output[m+1] = out[i*n2*n3h+j*n3h+k][1];
        m += 2;
      }
    }
  }

  fftw_destroy_plan(plan_forward);
  fftw_free(out);
#endif

  return success;
}

/*----------------------------------------------------------------------------*/

int Fourier::backward_fft3d()
{
  int success = 1;

  return success;
}

