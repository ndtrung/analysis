#ifndef __FOURIER
#define __FOURIER

#include <vector>
#include <string>
#include "analyzer.h"
#include "buildingblocks.h"

namespace Analysis {

class Fourier : public Analyzer
{
public:
  Fourier(int _mode=0);
  ~Fourier();

  void setdata1d(void* data, int);
  void setdata2d(void* data, int, int);
  void setdata3d(void* data, int, int, int);

  int execute();

  double* output;    //! Output data storing real and imaginary parts as consecutive elements
  int n1,n2,n3;      //! Data dimensions 

protected:
  void preprocess(); //! padding zeros,
                     //! removing average, applying window function

  void*   data;

  int forward_fft1d();
  int forward_fft2d();
  int forward_fft3d();

  int backward_fft1d();
  int backward_fft2d();
  int backward_fft3d();

  int mode;          //! 0 = forward, 1 = backward
  int dim;           //! Data dimensionality
};

} // namespace Analysis

#endif
