#ifndef __COORD_H
#define __COORD_H

#include <vector>
#include <string>
#include "buildingblocks.h"
#include "io.h"

namespace Analysis {

struct Pair {
  int i, j;
};

class Coordination
{
public:
  Coordination(int nframes, double _cutoff, int _periodic_wrapped=1);
  ~Coordination();

  // load the bead list from a file
  int load_bead_list(class GSDReader* gsd, int nframe, char** types, int ntypes,
    BeadList& beadList);

  // load the bead list from some where else: an array from DCDReader, XML Parser, etc.
  int load_bead_list(void*) { return 0; }

  void get_box_dims(double& lx, double& ly, double& lz) {
    lx = box.Lx;
    ly = box.Ly;
    lz = box.Lz;
  }

  void analyze();

  double compute_coordination(const BeadList&, const BeadList&,
    std::vector<int>& ilist, int reference_point=0);
  double compute_lifetime(const BeadList&, const BeadList&,
    std::vector<Pair>& ijlist, int reference_point=0);

  void write_dump(const BeadList& beadList1, const BeadList& beadList2,
    double Lx, double Ly,  double Lz, ofstream& ofs, int timestep);

  void write_dump(const BeadList& beadList, double Lx, double Ly, double Lz, char* filename);

protected:


  int nframes;              //! number of frames in the trajectory
  int periodic_wrapped;     //! 1 if PBC is used
  double cutoff;            //! Distance between particles in list 1 to those in list 2
  Box box;                  //! Box dimensions
  int timestep;             //! Time step read from the config file
  int decorrelated_interval;//! Interval between two frames to be decorrelated
  int file_type;            //! Config file type: XYZ, DUMP (LAMMPS) or XML (HOOMD)
  string base, ext;         //! Config files are in the form: base.ext

  IOHandle* io_handle;

  std::vector<Bead> beadList1, beadList2;
  int first_frame;
};

} // namespace Analyis

#endif
