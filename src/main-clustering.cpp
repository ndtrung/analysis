/*
  Copyright (C) 2017  Trung D. Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
  Find clusters of particles/beads from a simulation snapshot
  using a simple recursive algorithm looping through individual beads' neighbor list

  This is an example of using the classes and API provided by libanalyzer.a.

  Compile:     make -f Makefile.cpu
  Clean-build: make -f Makefile.cpu clean

  Use:
  ./find_clusters config_file types M T1 .. TM [cutoff <arg>] [nbeadsperparticle <arg>] [threshold <arg>]
     [bin_size <arg>] [write_clusters <arg>] [frames <args>]
    
  Input:   config_file = an ASCII file in XYZ format, or LAMMPS dump or HOOMD XML
              for LAMMPS dump files, atom data contains 5 columns: id type x y z
              for HOOMD XML files, positions are listed first

           types 
              M     = number of bead types in the clusters to be found
              T1 .. TM = list of M bead types, as space-separated strings

           cutoff
              <arg> = cutoff distance within which beads are considered in the same cluster

           nbeadsperparticle (optional, default = 1)
              <arg> = number of beads per particle (for rigid bodies composed of a number of beads),

           threshold (optional, default = 5)
              <arg> = exclude the clusters that contain fewer than this number of particles,

           bin_size (optional, default = 5)
              <arg> = bin size used for the cluster size distribution (in N-dist.txt, see output below)
                        
           write_clusters (optional, default = -3)
              <arg> = -1 write all the clusters into separate files
                      -2 write all the clusters into a single file
                      non-negative value (>=0) write the specified cluster to a file


           frames (optional): see example for details
              <args> = start end interval
                 start    = start time
                 end      = end time
                 interval = inverval between consecutive frames (snapshots)
              A file named cluster_size_evol.txt will be generated showing the cluster size of each bead at the snapshots.

           exclude (optional):
              <args> = type1 type2
                 type1, type2: particle types that are excluded from each other's neighbor lists

           grid (optional): bin the clusters into a grid
              <args> = nx ny nz
                 nx, ny and nz are the number of bins along x, y and z directions to do spatial binning
 
           template (optional): when frames are read from a DCD file
              <arg> = template file (XYZ format, or LAMMPS dump or HOOMD XML)
              if not specified, then all the beads are included

  Examples: 1) run the following

             ./find_clusters final.xml cutoff 1.0 types 2 C D write_clusters 1

           to look for clusters composed of beads of two types C and D from final.xml
           using the cutoff distance of 1.0, and output the found clusters into separate files.
           
            2) run the following

             ./find_clusters snapshot..xml cutoff 1.2 types 2 A B write_clusters 1 frames 10000 20000 1000

           to look for clusters composed of beads of two types A and B from a trajectory containing
           files snapshot.10000.xml, snapshot.11000.xml,.., snapshot.20000.xml
           using the cutoff distance of 1.2, and output the found clusters into separate files.

            3) run the following

             ./find_clusters trajectory.dcd cutoff 1.2 types 1 1 write_clusters 1 frames 50 60 1

           to look for clusters composed of beads of type 1 from a DCD file
           using the cutoff distance of 1.2, using frame 50 to frame 60 (inclusive)
           and output the found clusters into separate files.

            4) run the follwing

             ./find_clusters final.xml cutoff 1.0 types 2 B C exclude C C

           to look for clusters composed of beads of two types C and D from final.xml,
           however excluding all the C-C pairs from the neighbor lists.

  Output:  config_file-Rg-dist.txt = size distribution in terms of radius of gyration
           config_file-N-dist.txt  = size distribution in terms of number of particles
           config_file-As.txt = list of the clusters found, their radius of gyration and principle axes
           config_file-plist.txt = list of the particles in individual clusters
              in each line, the first item is the cluster ID, as listed in config_file-As.txt,
              the second item is the number of particles/beads in the cluster
              the next three items are the center of mass of the cluster (xm, ym, zm)
              the rest are the bead IDs as arranged in the config_file (starting from 0)

  IMPORTANT NOTES:
    The beads in the config_file are assumed to be inside a box of [-Lx/2;Lx/2]x[-Ly/2;Ly/2]x[-Lz/2;Lz/2],
    which is the default setting for simulation boxes in HOOMD. For LAMMPS, the particles and box center will be
    shifted into this range. For general XYZ files, users should make sure this assumption holds.

  Contact: Trung Nguyen, ndactrung@gmail.com

*/

#include <iostream>
#include <fstream>
#include <vector>

#include "io.h"
#include "memory.h"
#include "clustering.h"

using namespace std;
using namespace Analysis;

double cluster_correlation(const std::vector<Cluster>&, const std::vector<Cluster>&, int, int);

int main(int argc, char** argv)
{
  if (argc < 2) {
    cout << "Arguments required: filename\n";
    cout << "Refer to README for optional arguments.\n";
    return 1;
  }

  char fileName[64], outfile[64];
  int ntypes = 0;
  char** types = NULL;
  double cutoff2;
  
  int nNumBeadsInParticle = 1;              //! number of beads per particle
  int periodic_wrapped = 1;                 //! periodic boundary condition
  double cutoff = 1.4;                      //! cutoff distance for clustering
  int bin_size = 5;                         //! bin size in particles (i.e. building blocks) used for size distribution
  int threshold = 10 * nNumBeadsInParticle; //! exclude clusters with fewer than this number of many beads
  int write_clusters = -3;                  //! -3 meaning not writing to clusters into files
  int write_plist = 0;                      //! 1 if list of the beads belong to individual clusters are written out
  double user_max_size = -1;
  vector<Exclude> exclusionList;

  sprintf(fileName, "%s", argv[1]);

  int trajectory = 0;
  int start = 0;
  int end = 0;
  int interval = 0;

  int spatial = 0;
  int nx = 1;
  int ny = 1;
  int nz = 1;

  char templatefile[256];
  int use_template = 0;

  int compute_correlation = 0;
  int correlation_max_size = -1;

  int check_percolation = 0;

  // parse the arguments

  int iarg = 2;
  while (iarg < argc) {
    if (strcmp(argv[iarg],"cutoff") == 0) {
      cutoff = atof(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"types") == 0) {
      ntypes = atoi(argv[iarg+1]);
      if (ntypes <= 0) {
        std::cout << "Invalid number of types\n";
        return 1;
      }
      iarg+=2;
      types = new char* [ntypes];
      for (int m = 0; m < ntypes; m++) {
        types[m] = new char [64];
        strcpy(types[m], argv[iarg++]);
      }
    } else if (strcmp(argv[iarg],"nbeadsperparticle") == 0) {
      nNumBeadsInParticle = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"threshold") == 0) {
      threshold = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"max_size") == 0) {
      user_max_size = atof(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"bin_size") == 0) {
      bin_size = atof(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"write_clusters") == 0) {
      write_clusters = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"frames") == 0) {
      start = atoi(argv[iarg+1]);
      end = atoi(argv[iarg+2]);
      interval = atoi(argv[iarg+3]);
      trajectory = 1;
      iarg += 4;
    } else if (strcmp(argv[iarg],"exclude") == 0) {
      Exclude excl;
      strcpy(excl.type1, argv[iarg+1]);
      strcpy(excl.type2, argv[iarg+2]);
      exclusionList.push_back(excl);
      iarg += 3;
    } else if (strcmp(argv[iarg],"grid")==0) {
      nx = atoi(argv[iarg+1]);
      ny = atoi(argv[iarg+2]);
      nz = atoi(argv[iarg+3]);
      spatial = 1;
      iarg += 4;
    } else if (strcmp(argv[iarg],"pbc") == 0) {
      if (strcmp(argv[iarg+1],"yes") == 0) periodic_wrapped = 1;
      else if (strcmp(argv[iarg+1],"no") == 0) periodic_wrapped = 0;
      else printf("Invalid value for the pbc argument\n");
      iarg += 2;
    } else if (strcmp(argv[iarg],"template") == 0) {
      strcpy(templatefile,argv[iarg+1]);
      use_template = 1;
      iarg += 2;
    } else if (strcmp(argv[iarg],"compute_correlation") == 0) {
      compute_correlation = 1;
      correlation_max_size = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"percolation") == 0) {
      if (strcmp(argv[iarg+1],"yes") == 0) check_percolation = 1;
      else if (strcmp(argv[iarg+1],"no") == 0) check_percolation = 0;
      else printf("Invalid value for the percolation argument\n");
      iarg += 2;
    } else {
      std::cout << "Invalid argument: " << argv[iarg] << "\n";
      return 1;
    }
  }

  // error checks
  
  if (ntypes < 1) {
    cout << "Needs to specify at least a bead type for the clusters.\n";
    return 1;
  }
  
  int success;
  int nclusters_counted;

  if (trajectory == 0) { // single snapshot
    Clustering c(cutoff, periodic_wrapped, nNumBeadsInParticle,
                 threshold, write_clusters, bin_size, spatial, nx, ny, nz);
    if (exclusionList.size() > 0) c.setExclusion(&exclusionList);
    if (user_max_size > 0) c.setMaxSize(user_max_size);

    c.load_bead_list(fileName, types, ntypes);
    success = c.find_clusters();

    nclusters_counted = c.nclusters_found;

    if (success == 0) std::cout << "Failed to find clusters for " << fileName << std::endl;

  } else { // multiple snapshots
    string fullname(fileName);
    std::size_t found = fullname.find_last_of(".");
    string base = fullname.substr(0,found);
    string ext = fullname.substr(found+1);

    char* filetime = new char [1024];
    int num_configs = (end - start)/interval + 1;
    int* clsize_time = NULL;
    int num_beads = 0;
    int iconfig = 0;

    Clustering c(cutoff, periodic_wrapped, nNumBeadsInParticle,
                 threshold, write_clusters, bin_size, spatial, nx, ny, nz);
    if (exclusionList.size() > 0) c.setExclusion(&exclusionList);
    if (user_max_size > 0) c.setMaxSize(user_max_size);
    if (use_template) c.load_template(templatefile, types, ntypes);

    std::vector<Cluster> list0;
    for (int t = start; t <= end; t += interval) {

      if (strcmp(ext.c_str(),"dcd") == 0) { // read from a DCD file
        c.load_bead_list(fileName, types, ntypes, t);

      } else { // read from an ASCII file
        sprintf(filetime, "%s%d.%s", base.c_str(), t, ext.c_str());
        c.load_bead_list(filetime, types, ntypes);
      }

      success = c.find_clusters();
      if (success == 0) {
        std::cout << "Failed to find clusters for config " << t << std::endl;
        continue;
      }

      if (t == start && clsize_time == NULL) {
        num_beads = c.beadList.size();
        // allocate for the first use
        clsize_time = new int [num_configs*num_beads];

        for (int i = 0; i < num_beads; i++) {
          clsize_time[iconfig*num_beads + i] = c.beadList[i].cluster_size;
        }
      } else {
        if (num_beads != c.beadList.size()) {
          std::cout << "Inconsistent number of beads in the configurations\n";
          break;
        }

        for (int i = 0; i < num_beads; i++) {
          clsize_time[iconfig*num_beads + i] = c.beadList[i].cluster_size;
        }
      }

      if (t == start) {
        list0 = c.clusterList;
      }

      std::vector<Cluster> list = c.clusterList;

      if (compute_correlation) {
        double m = cluster_correlation(list0, list, threshold, correlation_max_size);
      }

      iconfig++;
    }

    // output the cluster size over time for each bead
    ofstream ofs("cluster_size_evol.txt");
    ofs << "beadID ";
    for (int j = 0; j < num_configs; j++)
      ofs << "t" << j << " ";
    ofs << std::endl;
    for (int i = 0; i < num_beads; i++) {
      ofs << i << " ";
      for (int j = 0; j < num_configs; j++) {
        ofs << clsize_time[j*num_beads + i] << " ";
      }
      ofs << std::endl;
    }
    ofs.close();

    delete [] filetime;
    if (clsize_time) delete [] clsize_time;

  }

  // rare use case: percolation checks

  if (success && trajectory == 0 && check_percolation) {
    Clustering c(cutoff, periodic_wrapped, nNumBeadsInParticle,
                 threshold, write_clusters, bin_size, spatial, nx, ny, nz);
    if (exclusionList.size() > 0) c.setExclusion(&exclusionList);
    if (user_max_size > 0) c.setMaxSize(user_max_size);

    std::cout << "Replicating the system by 2 x 2 x 2 for percolation check ... \n";
    std::cout << "Find clusters for the replicated system:\n";
    c.load_bead_list(fileName, types, ntypes);
    int nx = 2;
    int ny = 2;
    int nz = 2;
    c.replicate_system(nx, ny, nz);

    success = c.find_clusters();
    if (success == 0) std::cout << "Failed to find clusters for " << fileName << std::endl;

    int nclusters_replicated = c.nclusters_found;
    double scale = (double)nclusters_replicated / (double)nclusters_counted;
    if (scale < nx * ny * nz)
      std::cout << "Percolated: 1\n";
    else
      std::cout << "Percolated: 0\n";
  }

  if (types) {
    for (int m = 0; m < ntypes; m++)
      delete [] types[m];
    delete [] types;
  }

  return 0;
}

/*----------------------------------------------------------------------
  rare use case: Find the cluster correlation
------------------------------------------------------------------------*/

double cluster_correlation(const std::vector<Cluster>& clusterList1,
  const std::vector<Cluster>& clusterList2, int threshold, int correlation_max_size)
{
  int i, j;
  int n1 = clusterList1.size();
  int n2 = clusterList2.size();

  double sum = 0;
  int ncount = 0;
  double num_pairs = 0;  // number of pairs in list 1

  // loop through all the cluster in list 1
  for (i = 0; i < n1; i++) {

    // loop through all the pairs in the cluster
    int num_beads = clusterList1[i].size();
    if (num_beads < threshold) continue;
    if (correlation_max_size > threshold) {
      if (num_beads > correlation_max_size) continue;
    }

    for (int m = 0; m < num_beads-1; m++) {
      for (int n = m+1; n < num_beads; n++) {
        int i1 = clusterList1[i][m];
        int i2 = clusterList1[i][n]; // i2 is always greater than i1

        num_pairs += 1.0;

        int found = 0;

        // loop through all the cluster in list 2        
        for (j = 0; j < n2; j++) {
          int num_beads2 = clusterList2[j].size();
          if (num_beads2 < threshold) continue;
          if (correlation_max_size > threshold) {
            if (num_beads2 > correlation_max_size) continue;
          }

          for (int m2 = 0; m2 < num_beads2-1; m2++) {
            for (int n2 = m2+1; n2 < num_beads2; n2++) {
              int j1 = clusterList2[j][m2];
              int j2 = clusterList2[j][n2]; // j2 is always greater than j1
              if (i1 == j1 && i2 == j2) {
                ncount++;
                sum += 1.0;
                found = 1;
                break;
              }

            }

            if (found == 1) break;
          }

          if (found == 1) break;

        } // end for j looping all the clusters in list 2

      } // end for n
    } 
  } // end for i

  printf("num pairs = %g; ncount = %d\n", num_pairs, ncount);
  return sum;
}


