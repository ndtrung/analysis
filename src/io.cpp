/*
  Copyright (C) 2017  Trung D. Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*----------------------------------------------------------------------------
  I/O implementation

  Trung Nguyen (ndactrung@gmail.com)
------------------------------------------------------------------------------*/

#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string.h>
#include <cmath>
#include "io.h"

using namespace std;

namespace Analysis {

enum {ID=0,MOL=1,TYPE=2,Q=3,X=4,Y=5,Z=6,VX=7,VY=8,VZ=9,IX=10,IY=11,IZ=12};

/*----------------------------------------------------------------------------*/

int XYZHandle::read(const char* fileName, char** types, int ntypes, int& nNumTotalBeads,
              Box& box, std::vector<Bead>& beadList, int& timestep)
{
  int i;
  Bead bead;
  ifstream ifs(fileName);
  if (!ifs.good()) {
    std::cout << "Cannot read file " << fileName << std::endl;
    return 0;
  }

  beadList.clear();

  ifs >> nNumTotalBeads;
  ifs >> box.Lx >> box.Ly >> box.Lz;

  box.xlo = -0.5*box.Lx;
  box.xhi = 0.5*box.Lx;
  box.ylo = -0.5*box.Ly;
  box.yhi = 0.5*box.Ly;
  box.zlo = -0.5*box.Lz;
  box.zhi = 0.5*box.Lz;

  for (i = 0; i < nNumTotalBeads; i++) {
    ifs >> bead.type >> bead.x >> bead.y >> bead.z;

    bead.id = i;

    int included = 0;
    for (int m = 0; m < ntypes; m++) {
      if (strcmp(bead.type, types[m]) == 0) {
        included = 1;
        break;
      }
    }

    if (included == 1)
      beadList.push_back(bead);
  }
  
  nNumTotalBeads = beadList.size();
  ifs.close();
  return 1;
}

/*----------------------------------------------------------------------------*/

void XYZHandle::write(const Cluster& cluster, const std::vector<Particle>& particleList,
               const Box& box, const char* fileName, int cluster_id,
               int timestep)
{
  int i, j, nNumParticles;
  char* file = new char [256];
  ofstream ofs;

  sprintf(file, "%s-cluster_%d.xyz", fileName, cluster_id);
  ofs.open(file);
  ofs << cluster.size() << std::endl;
  ofs << box.Lx << "\t" << box.Ly << "\t" << box.Lz << std::endl;
  nNumParticles = particleList.size();
  for (i = 0; i < nNumParticles; i++) {
    int nNumBeads = particleList[i].beadList.size();
    for (j = 0; j < nNumBeads; j++) {
      ofs << particleList[i].beadList[j].type << "\t"
          << particleList[i].beadList[j].x << "\t"
          << particleList[i].beadList[j].y << "\t"
          << particleList[i].beadList[j].z << std::endl;
    }
  }

  ofs.close();

  delete [] file;
}

/*----------------------------------------------------------------------------*/

int XMLHandle::read(const char* fileName, char** types, int ntypes, int& nNumTotalBeads,
             Box& box, std::vector<Bead>& beadList, int& timestep)
{
  int i, m;
  char* line = new char [1024];
  double xlo,xhi,ylo,yhi,zlo,zhi;
  Bead bead;
  vector<Bead> allBeads;
  
  ifstream ifs(fileName);
  if (!ifs.good()) {
    cout << "Cannot read file " << fileName << std::endl;
    return 0;
  }

  beadList.clear();

  int adding_beads = 0;
  int reading_pos = 0;
  int reading_type = 0;
  int reading_charge = 0;
  int added_beads = 0;
  int read_pos = 0;
  int read_type = 0;
  int read_charge = 0;
  while (!ifs.eof()) {
    ifs.getline(line, 1024);

    string s(line);
    s.erase(remove(s.begin(), s.end(), '\"'), s.end());
    s.erase(remove(s.begin(), s.end(), '<'), s.end());
    s.erase(remove(s.begin(), s.end(), '>'), s.end());

    istringstream iss(s);
    size_t pos;
    string word;
    while (iss >> word) {
      pos = word.find("natoms");
      if (pos!=std::string::npos) {
        string n = word.substr(pos+7);
        nNumTotalBeads = atoi(n.c_str());
      }
      pos = word.find("time_step");
      if (pos!=std::string::npos) {
        string n = word.substr(pos+10);
        timestep = atoi(n.c_str());
      }

      pos = word.find("lx=");
      if (pos!=std::string::npos) {
        string n = word.substr(pos+3);
        box.Lx = atof(n.c_str());
      }
      pos = word.find("ly=");
      if (pos!=std::string::npos) {
        string n = word.substr(pos+3);
        box.Ly = atof(n.c_str());
      }
      pos = word.find("lz=");
      if (pos!=std::string::npos) {
        string n = word.substr(pos+3);
        box.Lz = atof(n.c_str());
      }

      pos = word.find("position");
      if (pos!=std::string::npos && read_pos == 0)
        reading_pos = 1;
      pos = word.find("type");
      if (pos!=std::string::npos && read_type == 0) {
        reading_type = 1;
      }
      pos = word.find("charge");
      if (pos!=std::string::npos && read_charge == 0)
        reading_charge = 1;
    }    

    if (reading_pos == 1) {
      if (allBeads.size() == 0) {
        adding_beads = 1;
      }
      for (i = 0; i < nNumTotalBeads; i++) {
      ifs.getline(line, 1024);
        sscanf(line, "%lf %lf %lf", &bead.x, &bead.y, &bead.z);
       if (added_beads == 0) allBeads.push_back(bead);
        else {
          allBeads[i].x = bead.x;
          allBeads[i].y = bead.y;
          allBeads[i].z = bead.z;
        }
     }
      reading_pos = 0;
      read_pos = 1;
      added_beads = 1;
    }

    if (reading_type == 1) {
      if (allBeads.size() == 0) {
        adding_beads = 1;
      }
      for (i = 0; i < nNumTotalBeads; i++) {
      ifs.getline(line, 1024);
        sscanf(line, "%s", &bead.type);
        if (added_beads == 0) allBeads.push_back(bead);
        else {
          sprintf(allBeads[i].type, "%s", bead.type);
        }
     }
      reading_type = 0;
      read_type = 1;
      added_beads = 1;
    }

    if (reading_charge == 1) {    
      if (allBeads.size() == 0) {
        adding_beads = 1;
      }
      for (i = 0; i < nNumTotalBeads; i++) {
      ifs.getline(line, 1024);
        sscanf(line, "%lf", &bead.q);
        if (added_beads == 0) allBeads.push_back(bead);
        else {
          allBeads[i].q = bead.q;
        }
     }
     reading_charge = 0;
     read_charge = 1;
     added_beads = 1;
    }
  }

  int n = 0;
  for (i = 0; i < nNumTotalBeads; i++) {
    int included = 0;
    for (int m = 0; m < ntypes; m++) {
      if (strcmp(allBeads[i].type, types[m]) == 0) {
        included = 1;
        break;
      }
    }

    if (included == 1) {
      allBeads[i].id = n;
      beadList.push_back(allBeads[i]);
    }

    n++;
  }
  
  nNumTotalBeads = beadList.size();

  box.xlo = -0.5*box.Lx;
  box.xhi = 0.5*box.Lx;
  box.ylo = -0.5*box.Ly;
  box.yhi = 0.5*box.Ly;
  box.zlo = -0.5*box.Lz;
  box.zhi = 0.5*box.Lz;

  ifs.close();
  delete [] line;
  return 1;
}

/*----------------------------------------------------------------------------*/

void XMLHandle::write(const Cluster& cluster, const std::vector<Particle>& particleList,
               const Box& box, const char* fileName, int cluster_id,
               int timestep)
{
  int i, j, nNumParticles;
  char* file = new char [256];
  ofstream ofs;

  sprintf(file, "%s-cluster_%d.xml", fileName, cluster_id);
  ofs.open(file);

  ofs << "<\?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
  ofs << "<hoomd_xml version=\"1.5\">\n";
  ofs << "<configuration time_step=\"" << timestep;
  ofs << "\" dimensions=\"3\" natoms=\"" << cluster.size() << "\" >\n";
  ofs << "<box lx=\"" << box.Lx << "\" ly=\"" << box.Ly << "\" lz=\"" << box.Lz;
  ofs << "\" xy=\"0\" xz=\"0\" yz=\"0\"/>\n";
  ofs << "<position num=\"" << cluster.size() << "\">\n";
  ofs.precision(15);

  nNumParticles = particleList.size();
  for (i = 0; i < nNumParticles; i++) {
    int nNumBeads = particleList[i].beadList.size();
    for (j = 0; j < nNumBeads; j++) {
      ofs << particleList[i].beadList[j].x << " "
          << particleList[i].beadList[j].y << " "
          << particleList[i].beadList[j].z << "\n";
    }
  }

  ofs << "</position><type>\n";
  for (i = 0; i < nNumParticles; i++) {
    int nNumBeads = particleList[i].beadList.size();
    for (j = 0; j < nNumBeads; j++)
      ofs << particleList[i].beadList[j].type << "\n";
  }

  ofs << "</type><charge>\n";
  for (i = 0; i < nNumParticles; i++) {
    int nNumBeads = particleList[i].beadList.size();
    for (j = 0; j < nNumBeads; j++)
      ofs << particleList[i].beadList[j].q << "\n";
  }
  ofs << "</charge>\n";

  ofs << "</configuration>\n";
  ofs << "</hoomd_xml>\n";

  ofs.close();

  delete [] file;
}

/*----------------------------------------------------------------------------*/

int DUMPHandle::read(const char* fileName, char** types, int ntypes, int& nNumTotalBeads,
               Box& box, std::vector<Bead>& beadList, int& timestep)
{
  int i;
  char* line = new char[1024];
  Bead bead;
  
  ifstream ifs(fileName);
  if (!ifs.good()) {
    cout << "Cannot read file " << fileName << std::endl;
    return 0;
  }

  beadList.clear();

  int narg, MAXARGS=16;
  char *copy, **arg;
  copy = new char [1024];
  arg = new char* [MAXARGS];

  for (i = 0; i < 9; i++) {
   ifs.getline(line, 1024);
    if (i == 1) sscanf(line, "%d", &timestep);
    if (i == 3) sscanf(line, "%d", &nNumTotalBeads);
    if (i == 5) sscanf(line, "%lf %lf", &box.xlo, &box.xhi);
    if (i == 6) sscanf(line, "%lf %lf", &box.ylo, &box.yhi);
    if (i == 7) sscanf(line, "%lf %lf", &box.zlo, &box.zhi);
    if (i == 8) {
      parse(line, copy, narg, arg);
      if (strcmp(arg[1], "ATOMS") != 0) printf("Incorrect line!!\n");
      if (narg >= 3) {
        nfields = narg - 2;
        fields = new int [nfields];
        for (int m = 0; m < nfields; m++) fields[m] = 0;
      } else {
        printf("Invalid number of fields: %s\n", line);
      }
      
      int iarg = 2;
      int m = 0;
      while (iarg < narg) {
        if (strcmp(arg[iarg],"id") == 0) fields[m] = ID;
        else if (strcmp(arg[iarg],"mol") == 0) fields[m] = MOL;
        else if (strcmp(arg[iarg],"type") == 0) fields[m] = TYPE;
        else if (strcmp(arg[iarg],"q") == 0) fields[m] = Q;
        else if (strcmp(arg[iarg],"x") == 0) fields[m] = X;
        else if (strcmp(arg[iarg],"y") == 0) fields[m] = Y;
        else if (strcmp(arg[iarg],"z") == 0) fields[m] = Z;
        else if (strcmp(arg[iarg],"ix") == 0) fields[m] = IX;
        else if (strcmp(arg[iarg],"iy") == 0) fields[m] = IY;
        else if (strcmp(arg[iarg],"iz") == 0) fields[m] = IZ;
        else if (strcmp(arg[iarg],"vx") == 0) fields[m] = VX;
        else if (strcmp(arg[iarg],"vy") == 0) fields[m] = VY;
        else if (strcmp(arg[iarg],"vz") == 0) fields[m] = VZ;
        iarg++;
        m++;
      }
    }
  }

  box.Lx = box.xhi - box.xlo;
  box.Ly = box.yhi - box.ylo;
  box.Lz = box.zhi - box.zlo;
  
  // check if the box center is at (0,0,0)
  // otherwise shift everything to [-Lx/2;Lx/2]x[-Ly/2;Ly/2]x[-Lz/2;Lz/2]
  
  double EPSILON = 0.0001;
  int box_center_at_origin = 1;
  double delx, dely, delz, cx, cy, cz;
  cx = 0.5 * (box.xlo + box.xhi);
  cy = 0.5 * (box.ylo + box.yhi);
  cz = 0.5 * (box.zlo + box.zhi);
  if (fabs(cx) > EPSILON || fabs(cy) > EPSILON || fabs(cz) > EPSILON) {
    box_center_at_origin = 0;
    delx = -cx;
    dely = -cy;
    delz = -cz;
  }
  
  bead.q = 0;
  int n = 0;
  for (i = 0; i < nNumTotalBeads; i++) {
    ifs.getline(line, 1024);

    int mol;
    parse(line, copy, narg, arg);
    if (narg != nfields) printf("ERROR: Inconsistent number of fields\n");

    for (int m = 0; m < narg; m++) {
      if (fields[m] == ID) bead.id = atoi(arg[m]);
      else if (fields[m] == MOL) bead.mol = atoi(arg[m]);
      else if (fields[m] == TYPE) strcpy(bead.type, arg[m]);
      else if (fields[m] == Q) bead.q = atof(arg[m]);
      else if (fields[m] == X) bead.x = atof(arg[m]);
      else if (fields[m] == Y) bead.y = atof(arg[m]);
      else if (fields[m] == Z) bead.z = atof(arg[m]);
      else if (fields[m] == IX) bead.ix = atoi(arg[m]);
      else if (fields[m] == IY) bead.iy = atoi(arg[m]);
      else if (fields[m] == IZ) bead.iz = atoi(arg[m]);
      else if (fields[m] == VX) bead.vx = atof(arg[m]);
      else if (fields[m] == VY) bead.vy = atof(arg[m]);
      else if (fields[m] == VZ) bead.vz = atof(arg[m]);
    }
    // sscanf(line, "%d %d %s %lf %lf %lf", &bead.id, &mol, &bead.type, &bead.x, &bead.y, &bead.z);
    
    // atoms in LAMMPS dump files can be a little bit outside of the box
    
    if (bead.x < box.xlo) bead.x += box.Lx;
    else if (bead.x > box.xhi) bead.x -= box.Lx;
    if (bead.y < box.ylo) bead.y += box.Ly;
    else if (bead.y > box.yhi) bead.y -= box.Ly;
    if (bead.z < box.zlo) bead.z += box.Lz;
    else if (bead.z > box.zhi) bead.z -= box.Lz;
    
    // shift the bead positions so that the coordinates are in [-Lx/2;Lx/2]x[-Ly/2;Ly/2]x[-Lz/2;Lz/2]
/*
    if (box_center_at_origin == 0) {
      if (shift == 1) {
        bead.x += delx;
        bead.y += dely;
        bead.z += delz;
      }
    }
*/    
    int included = 0;
    for (int m = 0; m < ntypes; m++) {
      if (strcmp(bead.type, types[m]) == 0) {
        included = 1;
        break;
      }
    }

    if (included == 1) {
      //bead.id = n;
      beadList.push_back(bead);
      n++;
    }
  }

  nNumTotalBeads = beadList.size();
  ifs.close();

  delete [] arg;
  delete [] copy;
  delete [] line;
  return 1;
}

/*----------------------------------------------------------------------------*/

void DUMPHandle::write(const Cluster& cluster, const std::vector<Particle>& particleList,
               const Box& box, const char* fileName, int cluster_id,
               int timestep)
{
  int i, j, nNumParticles, id;
  char* file = new char [256];
  ofstream ofs;

  sprintf(file, "%s-cluster_%d.dump", fileName, cluster_id);
  ofs.open(file);
  ofs << "ITEM: TIMESTEP\n";
  ofs << timestep << "\n";
  ofs << "ITEM: NUMBER OF ATOMS\n";
  ofs << cluster.size() << "\n";
  ofs << "ITEM: BOX BOUNDS pp pp pp\n";
  ofs << box.xlo << " " << box.xhi << "\n";
  ofs << box.ylo << " " << box.yhi << "\n";
  ofs << box.zlo << " " << box.zhi << "\n";
  ofs << "ITEM: ATOMS id mol type q x y z\n";
  nNumParticles = particleList.size();
  id = 1;
  for (i = 0; i < nNumParticles; i++) {
    int nNumBeads = particleList[i].beadList.size();
    for (j = 0; j < nNumBeads; j++) {
      ofs << particleList[i].beadList[j].id << " ";
      ofs << particleList[i].beadList[j].mol << " ";
      ofs << particleList[i].beadList[j].type << " ";
      ofs << particleList[i].beadList[j].q << " ";
      ofs << particleList[i].beadList[j].x << " ";
      ofs << particleList[i].beadList[j].y << " ";
      ofs << particleList[i].beadList[j].z << "\n";
      id++;
    }
  }

  ofs.close();

  delete [] file;
}

/*----------------------------------------------------------------------------*/

// extract beads of a certain index locally to each particle
void extract(const char* fileName, int local, int nNumBeadsInParticle, char* outfile)
{
  ifstream ifs(fileName);
  vector<Bead> beadList;

  int i, nNumTotalBeads, nNumParticles;
  double Lx, Ly, Lz;
  Bead bead;

  ifs >> nNumTotalBeads;
  ifs >> Lx >> Ly >> Lz;
  for (i = 0; i < nNumTotalBeads; i++) {
    ifs >> bead.type >> bead.x >> bead.y >> bead.z;
    if (i % nNumBeadsInParticle == local)
      beadList.push_back(bead);
  }

  nNumParticles = nNumTotalBeads / nNumBeadsInParticle;

  ifs.close();

  ofstream ofs(outfile);
 
  ofs << beadList.size() << "\n";
  ofs << Lx << "\t" << Ly << "\t" << Lz << "\n";
  for (i = 0; i < beadList.size(); i++) {
    ofs << beadList[i].type << "\t" << beadList[i].x << "\t"
        << beadList[i].y << "\t" <<  beadList[i].z << "\n";
  }

  ofs.close();
}

} // namespace

