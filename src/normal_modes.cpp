/*
  Reference:
    J. T. Kalathi and S. K. Kumar and M. Rubinstein and G. S. Grest
     Rouse Mode Analysis of Chain Relaxation in Homopolymer Melts,
     Macromolecules, 2014, 47:6925--6931
*/
#include <algorithm>
#include <fstream>
#include <cmath>
#include <iostream>
#include <string>

#include "DCDHandle.h"
#include "GSDReader.h"
#include "io.h"
#include "memory.h"
#include "normal_modes.h"

using namespace std;
using namespace Analysis;

#define anint(x) ((x >= 0.5) ? (1.0) : (x < -0.5) ? (-1.0) : (0.0)) 

#define MAXJACOBI 50
#define EPSILON 1e-8
#define MAX_CORR 300

enum {XYZ=0, XML=1, DUMP=2, DCD=3, GSD=4};

NormalModes::NormalModes(int _nframes, int _nNumBeadsPerChain, int _periodic_wrapped)
{
  nframes = _nframes;
  periodic_wrapped = _periodic_wrapped;
  nNumBeadsPerChain = _nNumBeadsPerChain;
  timestep = 0;
  io_handle = NULL;
  gsd = NULL;
  matrix = NULL;
  evectors = NULL;
  evalues = NULL;
  b = NULL;
  z = NULL;

  create(matrix, nNumBeadsPerChain, nNumBeadsPerChain);
  create(evectors, nNumBeadsPerChain, nNumBeadsPerChain);
  create(evalues, nNumBeadsPerChain);
  create(b, nNumBeadsPerChain);
  create(z, nNumBeadsPerChain);

  X = NULL;
  create(X, nframes, nNumBeadsPerChain, 3);

  fmin = 0;
  fmax = 1;
  //frameList.reserve(nframes);
  last_frame = 0;
}

NormalModes::~NormalModes()
{
  if (io_handle) delete io_handle;
  if (gsd) delete gsd;
  destroy(matrix);
  destroy(evectors);
  destroy(evalues);
  destroy(b);
  destroy(z);
  destroy(X);
}

// load the bead list from a file
// only pick the beads of specified types
int NormalModes::load_bead_list(char* fileName, char** types, int ntypes, int nframe)
{
  if (last_frame == nframes) return 0;

  std::vector<Bead> beadList;

  int success = 0;
  string fullname(fileName);
  std::size_t found = fullname.find_last_of(".");
  base = fullname.substr(0,found);
  ext = fullname.substr(found+1);

  // guess the file type
  if (strcmp(ext.c_str(),"xyz") == 0) {
    if (io_handle == NULL) io_handle = new XYZHandle;
    success = io_handle->read(fileName, types, ntypes, nNumTotalBeads, box, beadList, timestep);
    file_type = XYZ;
  } else if (strcmp(ext.c_str(),"xml") == 0) {
    if (io_handle == NULL) io_handle = new XMLHandle;
    success = io_handle->read(fileName, types, ntypes, nNumTotalBeads, box, beadList, timestep);
    file_type = XML;
  } else if (strcmp(ext.c_str(),"dump") == 0 || strcmp(ext.c_str(),"txt") == 0) {
    if (io_handle == NULL) io_handle = new DUMPHandle;
    success = io_handle->read(fileName, types, ntypes, nNumTotalBeads, box, beadList, timestep);
    file_type = DUMP;
  } else if (strcmp(ext.c_str(),"dcd") == 0) {
    if (io_handle == NULL) io_handle = new DCDHandle;
    char* file = new char [64];
    sprintf(file, "%s-%d", base.c_str(), nframe);
    base = std::string(file);
    timestep = nframe;
    success = io_handle->read(fileName, nNumTotalBeads, box, beadList, nframe);
    file_type = DCD;
    delete [] file;
  } else if (strcmp(ext.c_str(),"gsd") == 0) {
    if (gsd == NULL) gsd = new GSDReader(fileName);
    timestep = nframe;
    success = load_from_gsd(types, ntypes, nNumTotalBeads, box, beadList, timestep);
    file_type = GSD;
  } else {
    std::cout << "Unknown file extension.\n";
    return 0;
  }

  std::vector<Chain> chainList;
  int num_chains = beadList.size() / nNumBeadsPerChain;
  int m = 0;
  for (int i = 0; i < num_chains; i++) {
    Chain chain;
    int n0 = 0;
    for (int j = 0; j < nNumBeadsPerChain; j++) {
      chain.beadList.push_back(beadList[m]);
      if (strcmp(beadList[m].type, types[0]) == 0) n0++;
      m++;
    }
    double f0 = (double)n0 / (double)nNumBeadsPerChain;
    if (f0 >= fmin && f0 < fmax)
      chainList.push_back(chain);
  }

  // compute the Rouse modes for the polymer chains in the current frame
  
  compute_rouse_modes(chainList, nNumBeadsPerChain, last_frame);
  last_frame++;

  return success;
}

/*! \param types Array of particle types to be selected
    \param ntypes Number of types to be selected
    \param box Box dimensions (output)
    \param nNumTotalBeads Total mumber of particles (output)
    \param beadList Array of particles received (output)
    \param nframe current frame in the GSD file

    Load the bead list from a file only pick the beads of specified types.
*/
int NormalModes::load_from_gsd(char** types,
  int ntypes, int& nNumTotalBeads, Box& box, BeadList& beadList, int nframe)
{
  if (!gsd) return 0;

  gsd->read_frame(nframe);

  const float* _box = gsd->getBoxDim();
  box.Lx = _box[0];
  box.Ly = _box[1];
  box.Lz = _box[2];
  box.xlo = -0.5*box.Lx;
  box.xhi = 0.5*box.Lx;
  box.ylo = -0.5*box.Ly;
  box.yhi = 0.5*box.Ly;
  box.zlo = -0.5*box.Lz;
  box.zhi = 0.5*box.Lz;

  beadList.clear();

  int success = 1;
  int num_beads = gsd->getTotalNumParticles();
  unsigned int* beadtypes = gsd->getTypes();
  int* image = gsd->getImages();
  float* pos = gsd->getPositions();
  float* charge = gsd->getCharges();
  float* vel = gsd->getVelocities();
  int count1 = 0;
  int count2 = 0;
  for (int i = 0; i < num_beads; i++) {

    char stype[16];
    sprintf(stype, "%d", beadtypes[i]);
    int included = 0;
    for (int m = 0; m < ntypes; m++) {
      if (strcmp(stype, types[m]) == 0) {
        included = 1;
        break;
      }
    }
    
    if (included == 1) {
      Bead bead;
      bead.id = ++count1;
      sprintf(bead.type, "%d", beadtypes[i]);
      bead.x = pos[3*i+0];
      bead.y = pos[3*i+1];
      bead.z = pos[3*i+2];
      bead.ix = image[3*i];
      bead.iy = image[3*i+1];
      bead.iz = image[3*i+2];
      bead.vx = vel[3*i+0];
      bead.vy = vel[3*i+1];
      bead.vz = vel[3*i+2];
      bead.q = charge[i];
      beadList.push_back(bead);
    }
  }

  nNumTotalBeads = beadList.size();

  return success;
}
/*
void NormalModes::analyze()
{
  int i, j, l, numChains;
  double Lx, Ly, Lz;

  Lx = box.Lx;
  Ly = box.Ly;
  Lz = box.Lz;

  // compute the correlation matrix from all the frames and all the chains in each frame

  for (i = 0; i < nNumBeadsPerChain; i++)
    for (j = 0; j < nNumBeadsPerChain; j++)
      matrix[i][j] = 0;
  
  for (i = 0; i < nframes; i++) {
    numChains = frameList[i].chainList.size();
    for (j = 0; j < numChains; j++) {
      frameList[i].chainList[j].remove_com_pbc(Lx, Ly, Lz);
      frameList[i].chainList[j].accumulate_correlation_matrix(matrix, nNumBeadsPerChain);
    }
  }

  for (i = 0; i < nNumBeadsPerChain; i++)
    for (j = 0; j < nNumBeadsPerChain; j++)
      matrix[i][j] /= ((double)numChains*(double)nframes);

  // diagonalize the correlation matrix to get the eigenvectors
  // NOTE: the eigenvalues (and the corresponding eigenvectors) are not in any order.

  diagonalize(matrix, nNumBeadsPerChain, evalues, evectors);

  buffer = new double [nNumBeadsPerChain];
  reorder_evectors(evalues, evectors, nNumBeadsPerChain, 0, nNumBeadsPerChain-1);
  delete [] buffer;
 
  printf("Eigenvalues:\n");
  for (i = 0; i < nNumBeadsPerChain; i++) 
    printf("%d: %f\n", i, evalues[i]);

  ofstream ofs;
  ofs.open("evectors.txt");
  for (i = 0; i < nNumBeadsPerChain; i++) {
    for (j = 0; j < nNumBeadsPerChain; j++) {
      ofs << evectors[i][j] << " ";
    }
    ofs << "\n"; 
  }
  ofs.close();


  // loop through the frames, project the chains on the eigenvectors

  for (i = 0; i < nframes; i++) {
    numChains = frameList[i].chainList.size();
    for (j = 0; j < numChains; j++) {
      frameList[i].chainList[j].project_on_modes(evectors, nNumBeadsPerChain);
    }
  }

  // compute relaxation of the modes

  Bead** nm_trj = new Bead* [numChains];
  for (i = 0; i < numChains; i++)
    nm_trj[i] = new Bead [nframes];

  Bead* avmodes = new Bead [numChains];

  int t0 = 0;
  int num_modes = nNumBeadsPerChain;

  int max_corr = MAX_CORR;
  int ncorr = compute_ncorr(max_corr, nframes-1);
  int* points = new int [ncorr];

  compute_log_points(points, ncorr, max_corr, nframes-1);

  // three rows, each with ncorr points
  double** corr = new double* [3];
  for (i = 0; i < 3; i++)
    corr[i] = new double [ncorr];

  double dt = 1.0; // time interval between consecutive frames
  for (i = 0; i < ncorr; i++)
    corr[0][i] = dt*points[i];

  // loop through the modes

  for (int m = 0; m < num_modes; m++) {

    // store beads m of each chain into nm_trj[l]

    for (l = 0; l < numChains; l++)
      for (j = 0; j < nframes; j++)
        deep_copy(frameList[j].chainList[l].beadList[m], nm_trj[l][j]);

    // compute the average coordinates over the frames

    for (l = 0; l < numChains; l++) {
      double avmx = 0;
      double avmy = 0;
      double avmz = 0;
      for (j = 0; j < nframes; j++) {
        avmx += nm_trj[l][j].x;
        avmy += nm_trj[l][j].y;
        avmz += nm_trj[l][j].z;
      }
      avmodes[l].x = avmx/(double)nframes;
      avmodes[l].y = avmy/(double)nframes;
      avmodes[l].z = avmz/(double)nframes;
    }

    double S, X, X2;
    double delx1,dely1,delz1,delx2,dely2,delz2;
    for (i = 0; i < ncorr; i++) {
      int k = points[i];
      X = X2 = 0.0;
      for (j = 0; j < nframes-k; j++) {
        double X0 = 0;
        double X20 = 0;
        for (l = 0; l < numChains; l++) {
          delx1 = nm_trj[l][j].x - avmodes[l].x;
          dely1 = nm_trj[l][j].y - avmodes[l].y;
          delz1 = nm_trj[l][j].z - avmodes[l].z;

          delx2 = nm_trj[l][j+k].x - avmodes[l].x;
          dely2 = nm_trj[l][j+k].y - avmodes[l].y;
          delz2 = nm_trj[l][j+k].z - avmodes[l].z;

          S = delx1*delx2 + dely1*dely2 + delz1*delz2;
          X0 += S;
          X20 += S * S;
        }
        X += X0;
        X2 += X20;
      }

      // SN is inverse of the total counts
      double SN = 1.0/((nframes-k)*numChains);
      X = X * SN;
      corr[1][i] = X;
      corr[2][i] = (X2*SN - X*X) * (k+1) * SN; // why includes the factor (k+1) here?
    }

    X = corr[1][0];
    for (i = 0; i < ncorr; i++) {
      corr[1][i] /= X;
      corr[2][i] = sqrt(corr[2][i]) / X;
    }

    // save the relaxtion to file

    save_mode(m, corr, ncorr);
  }

  // deallocate memory
  for (i = 0 ; i < 3; i++)
    delete [] corr[i];
  delete [] corr;
  for (i = 0; i < numChains; i++)
    delete [] nm_trj[i];
  delete [] nm_trj;
  delete [] avmodes;
  delete [] points;
}
*/

void NormalModes::compute_rouse_modes(std::vector<Chain>& chainList, int nmodes, int nframe)
{
  double pi = 4.0*atan(1.0);
  int numChains = chainList.size();
  double Lx, Ly, Lz;
  Lx = box.Lx;
  Ly = box.Ly;
  Lz = box.Lz;

  for (int p = 1; p <= nmodes; p++) {
    double prefactor_p = p*pi/(double)nNumBeadsPerChain;
    // average over the chains
    double Xk[3];
    Xk[0] = Xk[1] = Xk[2] = 0;
    for (int j = 0; j < numChains; j++) {
      double norm = sqrt(2.0/nNumBeadsPerChain);
      double xtmp, ytmp, ztmp;
      xtmp = ytmp = ztmp = 0;

      chainList[j].remove_com_pbc(Lx, Ly, Lz);

      for (int i = 0; i < nNumBeadsPerChain; i++) {
        const Bead* bead = &(chainList[j].beadList[i]);
        double r = cos(prefactor_p*(i + 1 - 1.0/2.0));
        xtmp += r*bead->x;
        ytmp += r*bead->y;
        ztmp += r*bead->z;
      }
      xtmp *= norm;
      ytmp *= norm;
      ztmp *= norm;

      Xk[0] += xtmp;
      Xk[1] += ytmp;
      Xk[2] += ztmp;
    }

    X[nframe][p-1][0] = Xk[0]/(double)numChains;
    X[nframe][p-1][1] = Xk[1]/(double)numChains;
    X[nframe][p-1][2] = Xk[2]/(double)numChains;
  }
  
}

void NormalModes::analyze_rouse()
{
  int i, j, p, numChains;
  double pi = 4.0*atan(1.0);
  int nmodes = nNumBeadsPerChain; // number of modes of interest

  // mode relaxation
  // lag max
  int tmax = nframes / 2;
  int uncorrelinterval = 5; // interval between two uncorrelated consecutive frames
  int nwindows = nframes / uncorrelinterval;

  char fileName[256];
  ofstream ofs;
  // loop over the modes
  for (p = 1; p <= 20; p++) {
    double normP = 1.0/(4.0*sin(p*pi/2.0/nNumBeadsPerChain)*sin(p*pi/2.0/nNumBeadsPerChain));
    
    sprintf(fileName, "rouse_%d.txt", p);
    ofs.open(fileName);
    // loop over the lags
    for (int t = 0; t < tmax; t++) {
      double vtv0 = 0;
      double v0v0 = 0;
      int nwindows = 0;
      // average over the reference points
      for (i = 0; i + t < nframes; i+=uncorrelinterval, nwindows++) {
        vtv0 += X[i][p-1][0]*X[i+t][p-1][0] +
                X[i][p-1][1]*X[i+t][p-1][1] +
                X[i][p-1][2]*X[i+t][p-1][2];
        v0v0 += X[i][p-1][0]*X[i][p-1][0] +
                X[i][p-1][1]*X[i][p-1][1] +
                X[i][p-1][2]*X[i][p-1][2];
      }

      vtv0 /= (double)(nwindows);
      v0v0 /= (double)(nwindows);
      ofs << t << " " << vtv0/v0v0 << " " << nwindows << "\n";
    }

    ofs.close();
  }  
}

/*
  Project on the modes, as eigenvectors arranged in columns
    CHAIN%X=MATMUL(CHAIN%X,MODES)
    CHAIN%Y=MATMUL(CHAIN%Y,MODES)
    CHAIN%Z=MATMUL(CHAIN%Z,MODES)
*/
void Chain::project_on_modes(double** evectors, int nNumBeadsPerChain)
{
  for (int i = 0; i < nNumBeadsPerChain; i++) {
    double xtmp = 0;
    double ytmp = 0;
    double ztmp = 0;
    for (int m = 0; m < nNumBeadsPerChain; m++) {
      double tmp = evectors[m][i];
      xtmp += beadList[m].x * tmp;
      ytmp += beadList[m].y * tmp;
      ztmp += beadList[m].z * tmp;
    }
    beadList[i].x = xtmp;
    beadList[i].y = ytmp;
    beadList[i].z = ztmp;
  }
}

/*
  Accumulate to a correlation matrix, already allocated
*/
void Chain::accumulate_correlation_matrix(double** matrix, int nNumBeadsPerChain)
{
  for (int i = 0; i < nNumBeadsPerChain; i++) {
    double xtmp = beadList[i].x;
    double ytmp = beadList[i].y;
    double ztmp = beadList[i].z;
    for (int j = 0; j < nNumBeadsPerChain; j++) {
      double ridotrj = xtmp*beadList[j].x + ytmp*beadList[j].y + ztmp*beadList[j].z;
      matrix[i][j] += ridotrj;
    }
  }
}

/*
  Compute the center of mass of the chain
  Remove center of mass of the chain from the bead coordinates
*/
void Chain::remove_com_pbc(double Lx, double Ly, double Lz)
{
  int i, nNumBeads;
  double dx, dy, dz, Lx2, Ly2, Lz2, xm[3];
  Bead ref;

  nNumBeads = beadList.size();

  Lx2 = Lx / 2.0;
  Ly2 = Ly / 2.0;
  Lz2 = Lz / 2.0;

  // select the middle monomer as the reference point - closest to the COM
  int midpoint = nNumBeads / 2;
  ref.x = beadList[midpoint].x;
  ref.y = beadList[midpoint].y;
  ref.z = beadList[midpoint].z;

  // Compute the center of mass
  //   release periodic boundary conditions for all beads in the chain

  xm[0] = xm[1] = xm[2] = 0.0;

  for (i = 0; i < nNumBeads; i++) {
    dx = beadList[i].x - ref.x;
    dy = beadList[i].y - ref.y;
    dz = beadList[i].z - ref.z;

    if (dx > Lx2) beadList[i].x -= Lx;
    else if (dx < -Lx2) beadList[i].x += Lx;
    if (dy > Ly2) beadList[i].y -= Ly;
    else if (dy < -Ly2) beadList[i].y += Ly;
    if (dz > Lz2) beadList[i].z -= Lz;
    else if (dz < -Lz2) beadList[i].z += Lz;

    xm[0] += beadList[i].x;
    xm[1] += beadList[i].y;
    xm[2] += beadList[i].z;
  }

  xm[0] /= nNumBeads;
  xm[1] /= nNumBeads;
  xm[2] /= nNumBeads;

  // Subtract from xm

  for (i = 0; i < nNumBeads; i++) {
    beadList[i].x -= xm[0];
    beadList[i].y -= xm[1];
    beadList[i].z -= xm[2];
  }
}

/*
  Diagonalize a matrix
  return the eigenvalues in evalues and eigenvectors in evectors
*/
int NormalModes::diagonalize(double **matrix, int dim, double *evalues, double **evectors)
{
  int i, j, k, iter;
  double tresh, theta, tau, t, sm, s, h, g, c;

  for (i = 0; i < dim; i++) {
    for (j = 0; j < dim; j++) 
      evectors[i][j] = 0.0;
    evectors[i][i] = 1.0;
  }
 
  for (i = 0; i < dim; i++) {
    b[i] = evalues[i] = matrix[i][i];
    z[i] = 0.0;
  }
 
  for (iter = 1; iter <= MAXJACOBI; iter++) {
    sm = 0.0;
    for (i = 0; i < dim - 1; i++)
      for (j = i + 1; j < dim; j++)
        sm += fabs(matrix[i][j]);
    if (fabs(sm) < EPSILON) // converged
      return 0;

    if (iter < 4)
      tresh = 0.2 * sm / (dim * dim);
    else
      tresh = 0.0;
   
    for (i = 0; i < dim - 1; i++) {
      for (j = i + 1; j < dim; j++) {
        g = 100.0*fabs(matrix[i][j]);
        if (iter > 4 && fabs(evalues[i])+g == fabs(evalues[i])
           && fabs(evalues[j])+g == fabs(evalues[j]))
           matrix[i][j] = 0.0;
        else if (fabs(matrix[i][j]) > tresh) {
          h = evalues[j] - evalues[i];
          if (fabs(h)+g == fabs(h)) 
            t = (matrix[i][j]) / h;
          else {
            theta = 0.5 * h / (matrix[i][j]);
            t = 1.0 / (fabs(theta) + sqrt(1.0 + theta * theta));
            if (theta < 0.0) 
              t = -t;
          }

          c = 1.0 / sqrt(1.0 + t * t);
          s = t * c;
          tau = s / (1.0 + c);
          h = t * matrix[i][j];
          z[i] -= h;
          z[j] += h;
          evalues[i] -= h;
          evalues[j] += h;
          matrix[i][j] = 0.0;

          for (k = 0; k < i; k++)
            rotate(matrix, k, i, k, j, s, tau);
          for (k = i + 1; k < j; k++) 
            rotate(matrix, i, k, k, j, s, tau);
          for (k = j + 1; k < dim; k++) 
            rotate(matrix, i, k, j, k, s, tau);
          for (k = 0; k < dim; k++) 
            rotate(evectors, k, i, k, j, s, tau);
        }
      }
    }
   
    for (i = 0; i < dim; i++) {
      evalues[i] = b[i] += z[i];
      z[i] = 0.0;
    }
  }

  return 1;
}

/*
  Perform a Jacobi rotation
*/
void NormalModes::rotate(double **matrix, int i, int j, int k, int l, double s, double tau)
{
  const double g = matrix[i][j];
  const double h = matrix[k][l];
  matrix[i][j] = g - s * (h + g * tau);
  matrix[k][l] = h + s * (g - h * tau);
}

/*
*/

void NormalModes::deep_copy(const Bead& src, Bead& dst)
{
  dst.id = src.id;
  strcpy(dst.type, src.type);
  dst.q = src.q; 
  dst.x = src.x;
  dst.y = src.y;
  dst.z = src.z;
}

/*
    ! NCORR = LCOUNT(MCORR,ITRJ-1): NN = MCORR (chosen as 300), ITRJ = number of frames, IMAX = ITRJ-1
    INTEGER FUNCTION LCOUNT(NN,IMAX) RESULT(I)
    INTEGER,INTENT(IN):: NN,IMAX
    INTEGER J,K,L
    REAL(8) :: XI
    I=0; J=0; L=0

    DO
        XI=REAL(IMAX,8)**(REAL(J,8)/REAL(NN,8))
        K=FLOOR (XI)
        IF (K>IMAX) EXIT
        IF(L<K) THEN ; L=K ; I=I+1 ;END IF
        J=J+1
    END DO

    END FUNCTION LCOUNT
*/

int NormalModes::compute_ncorr(int max_corr, int imax)
{
  int i, j, l;
  i = 0;
  j = 0;
  l = 0;
  while (1) {
    double m = (double)j / (double)max_corr;
    double xi = pow((double)imax, m);
    int k = floor(xi);
    if (k > imax) break;
    if (l < k) {
      l = k;
      i++;
    }
    j++;
  }

  return i;
}

/*
    ! COUNT_LOG(POINTS(1:NCORR),NCORR,MCORR,ITRJ-1)
    SUBROUTINE COUNT_LOG(P,LC,NN,IMAX)
    INTEGER I,J,K,L,LC,NN,P(LC),IMAX
    REAL(8) :: XI
    INTENT(OUT) :: P
    INTENT(IN):: NN,IMAX,LC
    I=0; J=0; L=0

    DO
        XI=REAL(IMAX,8)**(REAL(J,8)/REAL(NN,8))
        K=FLOOR (XI)
        IF (K>IMAX) EXIT
        IF(L<K) THEN ; L=K ; I=I+1 ; P(I)=L
        END IF
        J=J+1
    END DO
    END SUBROUTINE COUNT_LOG
*/
void NormalModes::compute_log_points(int* points, int ncorr, int max_corr, int imax)
{
  int i, j, l;
  i = 0;
  j = 0;
  l = 0;

  points[0] = 0;
  while (1) {
    double m = (double)j / (double)max_corr;
    double xi = pow((double)imax, m);
    int k = floor(xi);
    if (k > imax) break;
    if (l < k) {
      l = k;
      i++;
      if (i < ncorr) points[i] = l;
    }
    j++;
  }
}

void NormalModes::save_mode(int m, double** corr, int ncorr)
{
  char fileName[256];
  sprintf(fileName, "mode_%d.txt", m);
  ofstream ofs;
  ofs.open(fileName);
  for (int i = 0; i < ncorr; i++) {
    ofs << corr[0][i] << " "  << corr[1][i] << " "  << corr[2][i] << "\n";
  }
  ofs.close();
}

/*
  Sort the evalues and vectors in the descending order using quicksort algorithm
*/
void NormalModes::reorder_evectors(double* evalues, double** evectors, int n, int lo, int hi)
{
  if (lo < hi) {
    int p = partition(evalues, evectors, n, lo, hi);
    reorder_evectors(evalues, evectors, n, lo, p-1);
    reorder_evectors(evalues, evectors, n, p+1, hi);
  }
}

/*
  Partition the array evalues into two subsets, one with elements that are smaller than pivot
  the other with those that are greater than pivot.
*/
int NormalModes::partition(double* evalues, double** evectors, int n, int lo, int hi)
{
  int i, j, m;
  double pivot, t;

  pivot = evalues[hi];
  i = lo - 1;
  for (j = lo; j <= hi-1; j++) {
    if (evalues[j] > pivot) {
      i = i + 1;

      // swap evalues[i] with evalues[j]      
      t = evalues[i];
      evalues[i] = evalues[j];
      evalues[j] = t;
      
      // also swap evectors[][i] with evectors[][j]
      for (m = 0; m < n; m++)
        buffer[m] = evectors[m][i];
      for (m = 0; m < n; m++)
        evectors[m][i] = evectors[m][j];
      for (m = 0; m < n; m++)
        evectors[m][j] = buffer[m];
    }
  }
  if (evalues[hi] > evalues[i+1]) {
    // swap evalues[i+1] with evalues[hi]      
    t = evalues[i+1];
    evalues[i+1] = evalues[hi];
    evalues[hi] = t;

    // also swap evectors[][i+1] with evectors[][hi]
    for (m = 0; m < n; m++)
      buffer[m] = evectors[m][i+1];
    for (m = 0; m < n; m++)
      evectors[m][i+1] = evectors[m][hi];
    for (m = 0; m < n; m++)
      evectors[m][hi] = buffer[m];
  }

  return (i+1);
}

