/*
  Copyright (C) 2017  Trung D. Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __RDF
#define __RDF

#include <vector>
#include <string>
#include "analyzer.h"
#include "buildingblocks.h"

namespace Analysis {

class RDF : public Analyzer
{
public:
  RDF(double _dr, double _rmax, double _qmax, double _qmin, int _directsq=0, int _nthreads=1);
  ~RDF();

  // load the bead list from a file
  int load_coord_list(int listidx, char* fileName, char** types, int ntypes, int t=-1);
   
  int execute();

  // set the number of wavevectors and dk's in three dimensions
  void set_k_vectors(int _nkx, int _nky, int _nkz, double _dkx, double _dky, double _dkz);

  // set the bin size of the wavevector magnitude
  void set_q_bin(double binsize) { qbin = binsize; }

  // set the size of the particle density grid
  void set_fft_dr(double binsize) { fft_dr = binsize; }

  // load the bead indinces of specified types from a file (XYZ, HOOMD XML or LAMMPS dump files)
  int load_template(int listidx, char* fileName, char** types, int ntypes);

  // Two bead lists
  std::vector<Bead> beadList1, beadList2;

  // two index lists
  std::vector<int> indexList1, indexList2;

  float* g;                 //! g(r)
  float* g_rep;             //! replicated g(r) for multiple threads
  float* r;                 //! distances
  int nbins;                //! number of distance bins
  double dr;                //! distance bin size for g(r)
  double fft_dr;            //! grid size for particle density for FFT

  float* S;                 //! S(k) computed from Fourier transform of g(r)
  float* q;                 //! wavevectors (q or k)
  int nqbins;               //! number of wavevectors

  double* Sk;               //! S(k) computed from direct calculation
  double*Sk_rep;            //! replicated S(k) for multiple threads
  int numberOfWavevectors;  //! number of wavevectors for direct calculation

  float* Sradial;           //! radially averaged S(k) from direct calculation
  float* qradial;           //! radially averaged k from direct calculation
  int* count;               //! number of values per bin of k (or q)
  double qbin;              //! bin size in q for averaging over the wave vectors
                            //! whose magnitude is within the bin
  int use_template;         //! 1 if using a template for DCD files

protected:

  int nNumTotalBeads;       //! Number of beads per snapshot/configuration
  Box box;        //! Box dimensions
  int timestep;             //! Time step read from the config file
  int nkx, nky, nkz;        //! Number of k vectors in 3 dimensions
  double dkx, dky, dkz;

  double rmax;              //! Maximum range of distance for g(r)
  double qmin,qmax;         //! Range of q

  int file_type;            //! Config file type: XYZ, DUMP (LAMMPS) or XML (HOOMD)
  string base, ext;         //! Config files are in the form: base.ext

  int nthreads;             //! Number of threads in use
  int directsq;             //! 1 if direct calculation of S(q) is needed, 0 otherwise (default)
  int fft;                  //! 1 if FFT is used

  int compute(const std::vector<Bead>& list1, const std::vector<Bead>& list2);

  // workhorse for direct calculation of S(q) = < exp(i k dot r) >
  void Sq_direct(const BeadList& list1, const BeadList& list2,
       int nkx, int nky, int nkz, double dkx, double dky, double dkz,
       int nthreads, char* sq_filename);

  void Sq_fft(const BeadList& list, double dr, int nthreads, char* sq_filename);

  int ksetup;
  std::vector<class KVector> waveVectors;
  void setup_waveVectors();

  IOHandle* io_handle;
};

class KVector {
public:
  #if (__GNUC__ < 6)
  friend  bool operator < (const KVector& v1, const KVector& v2) { return (v1.mag2 < v2.mag2); }
  #else
  bool operator < (const KVector& v) { return (mag2 < v.mag2); }
  #endif

  double x, y, z, mag2;
};

} // namespace Analysis

#endif
