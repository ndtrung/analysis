/*
  Copyright (C) 2017  Trung D. Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <vector>
#include <string.h>
#include "buildingblocks.h"

using namespace std;

namespace Analysis {

void Particle::Dist2(double px, double py, double pz)
{
  double dx, dy, dz;
  dx = xm - px;
  dy = ym - py;
  dz = zm - pz;
  dist2 = dx * dx + dy * dy + dz * dz;
}

bool operator< (const Particle& p1, const Particle& p2)
{
  return (p1.dist2 < p2.dist2);
}

Particle::Particle(const Particle& p)
{
  for (int i = 0; i < p.beadList.size(); i++) {
    Bead bead;
    bead.id = p.beadList[i].id;
    strcpy(bead.type, p.beadList[i].type);
    bead.q = p.beadList[i].q;
    bead.x = p.beadList[i].x;
    bead.y = p.beadList[i].y;
    bead.z = p.beadList[i].z;
    bead.cluster_size = p.beadList[i].cluster_size;

    beadList.push_back(bead);
  }

  xm = p.xm;
  ym = p.ym;
  zm = p.zm;
  dist2 = p.dist2;
}

Particle& Particle::operator= (const Particle& p)
{
  if (&p == this)
    return *this;
 
  beadList.clear();
 
  for (int i=0; i<p.beadList.size(); i++) {
    Bead bead;
    bead.id = p.beadList[i].id;
    strcpy(bead.type, p.beadList[i].type);
    bead.q = p.beadList[i].q;
    bead.x = p.beadList[i].x;
    bead.y = p.beadList[i].y;
    bead.z = p.beadList[i].z;
    bead.cluster_size = p.beadList[i].cluster_size;

    beadList.push_back(bead);
  }

  xm = p.xm;
  ym = p.ym;
  zm = p.zm;
  dist2 = p.dist2;

  return *this;
}

void Particle::find_center_of_mass()
{
  int i, nNumBeads;

  nNumBeads = beadList.size();

  xm = 0.0;
  ym = 0.0;
  zm = 0.0;
  for (i = 0; i < nNumBeads; i++) {
    xm += beadList[i].x;
    ym += beadList[i].y;
    zm += beadList[i].z;
  }

  xm /= nNumBeads;
  ym /= nNumBeads;
  zm /= nNumBeads;
}

} // namespace Analysis

