/*
  
  Contact: Trung Nguyen, trung.nguyen@northwestern.edu

*/

#include <stdlib.h>
#include <cmath>
#include <fstream>
#include <iostream>

#include "memory.h"
#include "autocorrelation.h"

using namespace Analysis;
using namespace std;

enum {ALL=0,MSD=1,VACF=2,SQT=3};

void compute_msd(int start, int end, int interval, int decorrelated, int periodic_wrapped,
 char* fileName, char** types, int ntypes, int nthreads, int mode);

int main(int argc, char** argv)
{
  int periodic_wrapped = 1;
  int trajectory = 0;
  int start, end, interval;
  char* fileName = new char [1024];
  int ntypes = 0;
  int nthreads = 1;
  char** types = NULL;
  int mode = ALL;
  int decorrelated = 1;

  sprintf(fileName, "%s", argv[1]);

  // parse the arguments
  
  int iarg = 2;
  while (iarg < argc) {
    if (strcmp(argv[iarg],"types") == 0) {
      ntypes = atoi(argv[iarg+1]);
      if (ntypes <= 0) {
        std::cout << "Invalid number of types\n";
        return 1;
      }
      iarg+=2;
      types = new char* [ntypes];
      for (int m = 0; m < ntypes; m++) {
        types[m] = new char [64];
        strcpy(types[m], argv[iarg++]);
      }
    } else if (strcmp(argv[iarg],"frames") == 0) {
      start = atoi(argv[iarg+1]);
      end = atoi(argv[iarg+2]);
      interval = atoi(argv[iarg+3]);
      trajectory = 1;
      iarg += 4;
    } else if (strcmp(argv[iarg],"decorrelated") == 0) {
      decorrelated = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"nthreads") == 0) {
      nthreads = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"mode") == 0) {
      if (strcmp(argv[iarg+1],"all") == 0) mode = ALL;
      else if (strcmp(argv[iarg+1],"msd") == 0) mode = MSD;
      else if (strcmp(argv[iarg+1],"vacf") == 0) mode = VACF;
      else if (strcmp(argv[iarg+2],"sqt") == 0) mode = SQT;
      iarg += 2;
    } else {
      std::cout << "Invalid argument: " << argv[iarg] << "\n";
      return 1;
    }
  }

  // error checks
  
  if (ntypes < 1) {
    cout << "Needs to specify at least a bead type\n";
    return 1;
  }

  compute_msd(start, end, interval, decorrelated, periodic_wrapped,
    fileName, types, ntypes, nthreads, mode);

  delete [] fileName;
  if (types) {
    for (int m = 0; m < ntypes; m++)
      delete [] types[m];
    delete [] types;
  }
}

void compute_msd(int start, int end, int interval, int decorrelated, int periodic_wrapped,
 char* fileName, char** types, int ntypes, int nthreads, int mode)
{
  clock_t tstart, telapsed;
  tstart = clock();

  ofstream logfile;

  int nframes = (end - start)/interval + 1;
  int nwindows = nframes / decorrelated - 1;
  int tmax = nframes*interval / 2;

  Autocorrelation autocorel(nthreads, periodic_wrapped);
  
  double** h_data = NULL;
  int num_entries = tmax/interval + 1;
  create(h_data, nwindows, num_entries);
  for (int i = 0; i < nwindows; i++) 
    for (int t = 0; t < num_entries; t++) h_data[i][t] = 0;

  double* ave_h = NULL;
  double* ave_h2 = NULL;
  int* count = NULL;
  create(ave_h, num_entries);
  create(ave_h2, num_entries);
  create(count, num_entries);
  for (int t = 0; t < num_entries; t++) {
    ave_h[t] = 0;
    ave_h2[t] = 0;
    count[t] = 0;
  }

  for (int i = 0; i < nwindows; i++) {
    int origin = start + i*decorrelated*interval;
    std::cout << "  window " << i+1 << " / " << nwindows << "\n";
    for (int t = 0; t < tmax; t+=interval) {
      int t_idx = t/interval;
      if (origin + t < end) {

        // if t == 0, save the particle coordinates to the reference
        int reference_point = 0;
        if (t == 0) reference_point = 1;

        // load the particle positions at t
        autocorel.load_bead_list(fileName, types, ntypes, origin+t, reference_point);

        double h = autocorel.compute(mode);
        h_data[i][t_idx] = h;

        ave_h[t_idx] += h;
        ave_h2[t_idx] += h*h;
        count[t_idx]++;

      } else {
        h_data[i][t_idx] = 0;  
      }
    }

    // running averages
    if (i % 10 == 0) {
      logfile.open("msd_ave.txt");
      for (int t = 0; t < num_entries; t++) {
        if (count[t] > 0) {
          double avg = ave_h[t] / (double)count[t];
          double avg2 = ave_h2[t] / (double)count[t];
          logfile << t*interval << " ";
          logfile << avg << " ";
          logfile << sqrt(avg2 - avg*avg) << " ";
          logfile << count[t] << std::endl;
        }
      }
      logfile.close();
    }

  }

  telapsed = clock()-start;      
  std::cout << "Elapsed time (seconds): " << (double)telapsed/CLOCKS_PER_SEC << "\n";

  for (int t = 0; t < num_entries; t++) {
    ave_h[t] = 0;
    ave_h2[t] = 0;
    count[t] = 0;
  }

  // average over the windows
  logfile.open("msd_all.txt");
  for (int t = 0; t < num_entries; t++) {
    logfile << t << " "; 
    for (int i = 0; i < nwindows; i++) {
      logfile << h_data[i][t] << " ";
      if (h_data[i][t] > 0) {
        ave_h[t] += h_data[i][t];
        ave_h2[t] += h_data[i][t]*h_data[i][t];
        count[t]++;
      }
    }
    logfile << std::endl;
  }
  logfile.close();

  
  logfile.open("msd_ave.txt");
  for (int t = 0; t < num_entries; t++) {
    if (count[t] > 0) {
      double avg = ave_h[t] / (double)count[t];
      double avg2 = ave_h2[t] / (double)count[t];
      logfile << t*interval << " ";
      logfile << avg << " ";
      logfile << sqrt(avg2 - avg*avg) << " ";
      logfile << count[t] << std::endl;
    }
  }
  logfile.close();

  destroy(h_data);
  destroy(ave_h);
  destroy(count);
}


