/*
  Reference:
    
*/
#include <algorithm>
#include <fstream>
#include <cmath>
#include <iostream>
#include <string>
#include <omp.h>

#include "DCDHandle.h"
#include "io.h"
#include "memory.h"
#include "autocorrelation.h"

using namespace std;
using namespace Analysis;

#define anint(x) ((x >= 0.5) ? (1.0) : (x < -0.5) ? (-1.0) : (0.0)) 

#define MAXJACOBI 50
#define EPSILON 1e-8
#define MAX_CORR 300

enum {ALL=0,MSD=1,VACF=2,SQT=3};
enum {XYZ=0, XML=1, DUMP=2, DCD=3};

Autocorrelation::Autocorrelation(int _nthreads, int _periodic_wrapped)
{
  nthreads = _nthreads;
  periodic_wrapped = _periodic_wrapped;
  timestep = 0;
  io_handle = NULL;
}

Autocorrelation::~Autocorrelation()
{
  if (io_handle) delete io_handle;
}

// load the bead list from a file
// only pick the beads of specified types
int Autocorrelation::load_bead_list(char* fileName, char** types, int ntypes, int nframe,
  int reference_point)
{
  int success;
  string fullname(fileName);
  std::size_t found = fullname.find_last_of(".");
  base = fullname.substr(0,found);
  ext = fullname.substr(found+1);

  char* filetime = new char [1024];

  // guess the file type
  if (strcmp(ext.c_str(),"xyz") == 0) {
    if (io_handle == NULL) io_handle = new XYZHandle;
    success = io_handle->read(fileName, types, ntypes, nNumTotalBeads,
      box, beadList, timestep);
    file_type = XYZ;
  } else if (strcmp(ext.c_str(),"xml") == 0) {
    if (io_handle == NULL) io_handle = new XMLHandle;
    success = io_handle->read(fileName, types, ntypes, nNumTotalBeads,
      box, beadList, timestep);
    file_type = XML;
  } else if (strcmp(ext.c_str(),"dump") == 0 || strcmp(ext.c_str(),"txt") == 0) {
    if (io_handle == NULL) io_handle = new DUMPHandle;
    timestep = nframe;
    sprintf(filetime, "%s%d.%s", base.c_str(), timestep, ext.c_str());
    success = io_handle->read(filetime, types, ntypes, nNumTotalBeads,
      box, beadList, timestep);
    file_type = DUMP;
  } else if (strcmp(ext.c_str(),"dcd") == 0) {
    if (io_handle == NULL) io_handle = new DCDHandle;
    char* file = new char [64];
    sprintf(file, "%s-%d", base.c_str(), nframe);
    base = std::string(file);
    timestep = nframe;
    success = io_handle->read(fileName, nNumTotalBeads,
      box, beadList, nframe);
    file_type = DCD;
    delete [] file;
  } else {
    std::cout << "Unknown file extension.\n";
    return 0;
  }

  // store the reference state
  if (reference_point) {
    beadList0.clear();
    int n = beadList.size();
    for (int i = 0; i < n; i++) beadList0.push_back(beadList[i]);
    com_remove(beadList0);
  }

  com_remove(beadList);

  return success;
}

double Autocorrelation::compute(int mode)
{
  double m = 0;
  if (mode == MSD) m = compute_msd();
  else if (mode == VACF) m = compute_vacf();
  return m;
}

double Autocorrelation::compute_msd()
{
  int num_beads = beadList0.size();
  double msd = 0;

  // loop through the beads in the two frames
  omp_set_num_threads(nthreads);

  #pragma omp parallel for reduction(+:msd)
  for (int m = 0; m < num_beads; m++) {
    double x0 = beadList0[m].x + box.Lx * beadList0[m].ix;
    double y0 = beadList0[m].y + box.Ly * beadList0[m].iy;
    double z0 = beadList0[m].z + box.Lz * beadList0[m].iz;
    double x  = beadList[m].x + box.Lx * beadList[m].ix;
    double y  = beadList[m].y + box.Ly * beadList[m].iy;
    double z  = beadList[m].z + box.Lz * beadList[m].iz;
    double rsq = (x - x0)*(x - x0) + (y - y0)*(y - y0) + (z - z0)*(z - z0);
    msd += rsq;
  }
  
  msd /= (double)num_beads;
   

  return msd; 
}

double Autocorrelation::compute_vacf()
{
  int num_beads = beadList0.size();
  double sum_v0vt = 0;
  double sum_v0v0 = 0;
  // loop through the beads in the two frames
  for (int m = 0; m < num_beads; m++) {
    double v0x = beadList0[m].vx;
    double v0y = beadList0[m].vy;
    double v0z = beadList0[m].vz;
    double vtx  = beadList[m].vx;
    double vty  = beadList[m].vy;
    double vtz  = beadList[m].vz;
    double v0dotvt = v0x * vtx + v0y * vty + v0z * vtz;
    double v0dotv0 = v0x * v0x + v0y * v0y + v0z * v0z;
    sum_v0vt += v0dotvt;
    sum_v0v0 += v0dotv0;
  }

  double vacf = sum_v0vt/sum_v0v0;      
  return vacf;
}


void Autocorrelation::com_remove(BeadList& list)
{
  int num_beads = list.size();
  double cm[3];

  cm[0] = cm[1] = cm[2] = 0;
  for (int i = 0; i < num_beads; i++) {
    cm[0] += list[i].x+list[i].ix*box.Lx;
    cm[1] += list[i].y+list[i].iy*box.Ly;
    cm[2] += list[i].z+list[i].iz*box.Lz;
  }

  cm[0] /= (double)num_beads;
  cm[1] /= (double)num_beads;
  cm[2] /= (double)num_beads;

  for (int i = 0; i < num_beads; i++) {
    list[i].x = list[i].x+list[i].ix*box.Lx - cm[0];
    list[i].y = list[i].y+list[i].iy*box.Ly - cm[1];
    list[i].z = list[i].z+list[i].iz*box.Lz - cm[2];
  }
}
