/*
  Find the coordination correlation function of
    the particles in GSD file 1 with those in GSD file 2
  and the MSD of those in file 1  

  References:
  1. Webb et al., ACS Marco Lett. 2018, 7:734--738 (coordination correlation function)
  2. Solano et al., J. Chem. Phys. 2013, 139_034502 (ion conductivity)
  3. (life time correlation function)

  Build:

     make -f Makefile.cpu

  Run:

    ./mobility file1.gsd file2.gsd types1 1 1 types2 1 2 cutoff 1.2 frames 30000 40000 10 decorrelated 100

  file1.gsd, file2.gsd
  types1 [number of types] [list of types] from file1.gsd
  types2 [number of types] [list of types] from file2.gsd
  cutoff                          = distance between particles between two particle groups
  frames [start] [end] [interval] = frames from start to end (inclusive) with interval (in number of frames)
  decorrelated                    = number of frames for data to be decorrelated
  comflag                         = 1/0 if COM is removed from the MSD and conductivity calculations
  write_dump                      = 1/0 if a combined LAMMPS dump files generated for each calculated frame

  Contact: Trung Nguyen, trung.nguyen@northwestern.edu

*/

#include <stdlib.h>
#include <cmath>
#include <fstream>
#include <iostream>
#include <ctime>
#include <omp.h>
#include "GSDReader.h"
#include "coord.h"
#include "memory.h"

using namespace Analysis;
using namespace std;

#define EPSILON 0.0001

void coordination(GSDReader* gsd1, GSDReader* gsd2, int start, int end,
  int interval, int decorrelated, int periodic_wrapped,
  char** types1, int ntypes1, char** types2, int ntypes2, double cutoff,
  int write_dump);

void lifetime(GSDReader* gsd1, GSDReader* gsd2, int start, int end,
  int interval, int decorrelated, int periodic_wrapped,
  char** types1, int ntypes1, char** types2, int ntypes2, double cutoff,
  int write_dump);

void msd(GSDReader* gsd, int start, int end, int interval, int decorrelated,
  int periodic_wrapped, int comflag, char** types, int ntypes, const char* filename, int nthreads);

void conductivity(GSDReader* gsd1, GSDReader* gsd2, int start, int end, int interval,
  int decorrelated, int periodic_wrapped, int comflag, char** types1, int ntypes1,
  char** types2, int ntypes2, const char* filenam, int nthreads);

int main(int argc, char** argv)
{
  int periodic_wrapped = 1;
  int trajectory = 0;
  int start, end, interval;
  char *fileName = new char [1024];
  int ntypes1 = 0;
  char** types1 = NULL;
  int ntypes2 = 0;
  char** types2 = NULL;
  double cutoff = 1.2;
  int decorrelated = 1;
  int comflag = 1;
  int write_dump = 0;
  int msdflag = 1;
  int coordflag = 1;
  int lifetimeflag = 1;
  int conductivityflag = 1;
  int nthreads = 8;

  // parse the arguments
  
  int iarg = 3;
  while (iarg < argc) {
    if (strcmp(argv[iarg],"types1") == 0) {
      ntypes1 = atoi(argv[iarg+1]);
      if (ntypes1 <= 0) {
        std::cout << "Invalid number of types\n";
        return 1;
      }
      iarg+=2;
      types1 = new char* [ntypes1];
      for (int m = 0; m < ntypes1; m++) {
        types1[m] = new char [64];
        strcpy(types1[m], argv[iarg++]);
      }
    } else if (strcmp(argv[iarg],"types2") == 0) {
      ntypes2 = atoi(argv[iarg+1]);
      if (ntypes2 <= 0) {
        std::cout << "Invalid number of types\n";
        return 1;
      }
      iarg+=2;
      types2 = new char* [ntypes2];
      for (int m = 0; m < ntypes2; m++) {
        types2[m] = new char [64];
        strcpy(types2[m], argv[iarg++]);
      }
    } else if (strcmp(argv[iarg],"frames") == 0) {
      start = atoi(argv[iarg+1]);
      end = atoi(argv[iarg+2]);
      interval = atoi(argv[iarg+3]);
      trajectory = 1;
      iarg += 4;
    } else if (strcmp(argv[iarg],"cutoff") == 0) {
      cutoff = atof(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"comflag") == 0) {
      comflag = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"decorrelated") == 0) {
      decorrelated = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"msdflag") == 0) {
      msdflag = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"coordflag") == 0) {
      coordflag = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"lifetimeflag") == 0) {
      lifetimeflag = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"conductivityflag") == 0) {
      conductivityflag = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"nthreads") == 0) {
      nthreads = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"write_dump") == 0) {
      write_dump = atoi(argv[iarg+1]);
      iarg += 2;
    } else {
      std::cout << "Invalid argument: " << argv[iarg] << "\n";
      return 1;
    }
  }

  GSDReader* gsd1=NULL,*gsd2=NULL;
  gsd1 = new GSDReader(argv[1]);
  if (strcmp(argv[2], "NULL") == 0) gsd2 = new GSDReader(argv[1]);
  else gsd2 = new GSDReader(argv[2]);

  // error checks
  
  if (ntypes1 < 1 || ntypes2 < 1) {
    std::cout << "Needs to specify at least a bead type\n";
    return 1;
  }

  int ntotalframes1 = gsd1->getTotalNumFrames();
  int ntotalframes2 = gsd2->getTotalNumFrames();
  if (ntotalframes1 != ntotalframes2)
    std::cout << "WARNING: Two trajectories have different numbers of frames\n";
  std::cout << "Total number of frames: " << ntotalframes1 << std::endl;

  if (msdflag) {
    std::cout << "Computing mean squared displacement of particles of types1 ...\n";
    msd(gsd1, start, end, interval,
      decorrelated, periodic_wrapped, comflag, types1, ntypes1, (char*)"msd.txt", nthreads);
  }
  
  if (coordflag) {
    std::cout << "\nComputing coordination correlation ...\n";
    coordination(gsd1, gsd2, start, end, interval,
      decorrelated, periodic_wrapped, types1, ntypes1, types2, ntypes2, cutoff, write_dump);
    
  }

  if (lifetimeflag) {
    std::cout << "\nComputing life time correlation ...\n";
    lifetime(gsd1, gsd2, start, end, interval,
      decorrelated, periodic_wrapped, types1, ntypes1, types2, ntypes2, cutoff, write_dump);
    
  }

  if (conductivityflag) {
    std::cout << "Computing conductivity of particles of types1 with respect to those of types2 ...\n";
    conductivity(gsd1, gsd2, start, end,  interval,
      decorrelated,  periodic_wrapped, comflag, types1, ntypes1, types2, ntypes2,
       (char*)"conductivity.txt", nthreads);
  }

  delete gsd1;
  delete gsd2;

  if (types1) {
    for (int m = 0; m < ntypes1; m++)
      delete [] types1[m];
    delete [] types1;
  }

  if (types2) {
    for (int m = 0; m < ntypes2; m++)
      delete [] types2[m];
    delete [] types2;
  }

  delete [] fileName;
}

void coordination(GSDReader* gsd1, GSDReader* gsd2, int start, int end,
 int interval, int decorrelated, int periodic_wrapped,
 char** types1, int ntypes1, char** types2, int ntypes2, double cutoff, int write_dump)
{
  clock_t tstart, telapsed;
  tstart = clock();

  ofstream logfile;

  int nframes = (end - start)/interval + 1;
  int nwindows = nframes / decorrelated - 1;
  int tmax = nframes*interval / 2;

  Coordination coord(nframes, cutoff, periodic_wrapped);
  std::vector<int> ilist;

  double** h_data = NULL;
  int num_entries = tmax/interval + 1;
  create(h_data, nwindows, num_entries);
  for (int i = 0; i < nwindows; i++) 
    for (int t = 0; t < num_entries; t++) h_data[i][t] = 0;

  BeadList beadList;
  coord.load_bead_list(gsd1, 0, types1, ntypes1, beadList);
  coord.load_bead_list(gsd2, 0, types2, ntypes2, beadList);
  std::cout << "Number of particles selected from file 1: " << beadList.size() << " ";
  std::cout << " and file 2: " << beadList.size() << std::endl;

  for (int i = 0; i < nwindows; i++) {
    int origin = start + i*decorrelated;
    std::cout << "  window " << i+1 << " / " << nwindows << "\n";
    for (int t = 0; t < tmax; t+=interval) {
      int t_idx = t/interval;
      if (origin + t < end) {
        // load the particle positions at t from both files
        BeadList beadList1, beadList2;
        coord.load_bead_list(gsd1, origin + t, types1, ntypes1, beadList1);
        coord.load_bead_list(gsd2, origin + t, types2, ntypes2, beadList2);

        int num_beads1 = beadList1.size();
        // if t == 0, save he particle indices in list 1 that have neighbors in list 2 into ilist
        // otherwise, loop over the particles only in ilist to check with those in list 2
        // ilist is the set C in Ref. Webb et al. ACS Macro Lett. 2018
        int reference_point = 0;
        if (t == 0) reference_point = 1;
        double h = coord.compute_coordination(beadList1, beadList2, ilist, reference_point);

        double h0;
        if (reference_point) h0 = h;

        h_data[i][t_idx] = h/h0;

      } else {
        h_data[i][t_idx] = 0;  
      }
    }
  }

  telapsed = clock()-start;      
  std::cout << "Coordination time (seconds): " << (double)telapsed/CLOCKS_PER_SEC << "\n";

  double* ave_h = NULL;
  double* ave_h2 = NULL;
  int* count = NULL;
  create(ave_h, num_entries);
  create(ave_h2, num_entries);
  create(count, num_entries);
  for (int t = 0; t < num_entries; t++) {
    ave_h[t] = 0;
    ave_h2[t] = 0;
    count[t] = 0;
  }

  // average over different windows
  logfile.open("coord_all.txt");
  for (int t = 0; t < num_entries; t++) {
    logfile << t << " "; 
    for (int i = 0; i < nwindows; i++) {
      logfile << h_data[i][t] << " ";
      if (h_data[i][t] > 0) {
        ave_h[t] += h_data[i][t];
        ave_h2[t] += h_data[i][t]*h_data[i][t];
        count[t]++;
      }
    }
    logfile << std::endl;
  }
  logfile.close();

  logfile.open("coord_ave.txt");
  for (int t = 0; t < num_entries; t++) {
    if (count[t] > 0) {
      ave_h[t] /= (double)count[t];
      ave_h2[t] /= (double)count[t];
      logfile << t*interval << " ";
      logfile << ave_h[t] << " ";
      logfile << sqrt(ave_h2[t] - ave_h[t]*ave_h[t]) << " ";
      logfile << count[t] << std::endl;
    }
  }
  logfile.close();

  if (write_dump) {
    const float* box = gsd1->getBoxDim();
    double Lx = box[0];
    double Ly = box[1];
    double Lz = box[2];
    logfile.open("trajectory.dump");
    for (int t = start; t < end; t+=interval) {
      BeadList beadList1, beadList2;
      coord.load_bead_list(gsd1, t, types1, ntypes1, beadList1);
      coord.load_bead_list(gsd2, t, types2, ntypes2, beadList2);
      coord.write_dump(beadList1, beadList2, Lx, Ly, Lz, logfile, t);
    }
    logfile.close();
  }

  destroy(h_data);
  destroy(ave_h);
  destroy(count);
}

void lifetime(GSDReader* gsd1, GSDReader* gsd2, int start, int end,
 int interval, int decorrelated, int periodic_wrapped,
 char** types1, int ntypes1, char** types2, int ntypes2, double cutoff, int write_dump)
{
  clock_t tstart, telapsed;
  tstart = clock();

  ofstream logfile;

  int nframes = (end - start)/interval + 1;
  int nwindows = nframes / decorrelated - 1;
  int tmax = nframes*interval / 2;

  Coordination coord(nframes, cutoff, periodic_wrapped);
  std::vector<Pair> ijlist;

  double** h_data = NULL;
  int num_entries = tmax/interval + 1;
  create(h_data, nwindows, num_entries);
  for (int i = 0; i < nwindows; i++) 
    for (int t = 0; t < num_entries; t++) h_data[i][t] = 0;

  BeadList beadList;
  coord.load_bead_list(gsd1, 0, types1, ntypes1, beadList);
  coord.load_bead_list(gsd2, 0, types2, ntypes2, beadList);
  std::cout << "Number of particles selected from file 1: " << beadList.size() << " ";
  std::cout << " and file 2: " << beadList.size() << std::endl;

  for (int i = 0; i < nwindows; i++) {
    int origin = start + i*decorrelated;
    std::cout << "  window " << i+1 << " / " << nwindows << "\n";
    for (int t = 0; t < tmax; t+=interval) {
      int t_idx = t/interval;
      if (origin + t < end) {
        // load the particle positions at t from both files
        BeadList beadList1, beadList2;
        coord.load_bead_list(gsd1, origin + t, types1, ntypes1, beadList1);
        coord.load_bead_list(gsd2, origin + t, types2, ntypes2, beadList2);

        int num_beads1 = beadList1.size();

        int reference_point = 0;
        if (t == 0) reference_point = 1;
        double h = coord.compute_lifetime(beadList1, beadList2, ijlist, reference_point);

        double h0;
        if (reference_point) h0 = h;

        h_data[i][t_idx] = h; // h/h0
      } else {
        h_data[i][t_idx] = 0;  
      }
    }
  }

  telapsed = clock()-start;      
  std::cout << "Lifetime time (seconds): " << (double)telapsed/CLOCKS_PER_SEC << "\n";

  double* ave_h = NULL;
  double* ave_h2 = NULL;
  int* count = NULL;
  create(ave_h, num_entries);
  create(ave_h2, num_entries);
  create(count, num_entries);
  for (int t = 0; t < num_entries; t++) {
    ave_h[t] = 0;
    ave_h2[t] = 0;
    count[t] = 0;
  }

  // average over different windows
  logfile.open("lifetime_all.txt");
  for (int t = 0; t < num_entries; t++) {
    logfile << t << " "; 
    for (int i = 0; i < nwindows; i++) {
      logfile << h_data[i][t] << " ";
      if (h_data[i][t] > 0) {
        ave_h[t] += h_data[i][t];
        ave_h2[t] += h_data[i][t]*h_data[i][t];
        count[t]++;
      }
    }
    logfile << std::endl;
  }
  logfile.close();

  logfile.open("lifetime_ave.txt");
  for (int t = 0; t < num_entries; t++) {
    if (count[t] > 0) {
      ave_h[t] /= (double)count[t];
      ave_h2[t] /= (double)count[t];
      logfile << t*interval << " ";
      logfile << ave_h[t] << " ";
      logfile << sqrt(ave_h2[t] - ave_h[t]*ave_h[t]) << " ";
      logfile << count[t] << std::endl;
    }
  }
  logfile.close();

  if (write_dump) {
    const float* box = gsd1->getBoxDim();
    double Lx = box[0];
    double Ly = box[1];
    double Lz = box[2];
    logfile.open("trajectory.dump");
    for (int t = start; t < end; t+=interval) {
      BeadList beadList1, beadList2;
      coord.load_bead_list(gsd1, t, types1, ntypes1, beadList1);
      coord.load_bead_list(gsd2, t, types2, ntypes2, beadList2);
      coord.write_dump(beadList1, beadList2, Lx, Ly, Lz, logfile, t);
    }
    logfile.close();
  }

  destroy(h_data);
  destroy(ave_h);
  destroy(count);
}
// remove the COM of beadList
void com_remove(BeadList& beadList, double* cm, double Lx, double Ly, double Lz)
{
  int num_beads = beadList.size();
  cm[0] = cm[1] = cm[2] = 0;
  for (int i = 0; i < num_beads; i++) {
    cm[0] += beadList[i].x+beadList[i].ix*Lx;
    cm[1] += beadList[i].y+beadList[i].iy*Ly;
    cm[2] += beadList[i].z+beadList[i].iz*Lz;
  }
  cm[0] /= (double)num_beads;
  cm[1] /= (double)num_beads;
  cm[2] /= (double)num_beads;
  for (int i = 0; i < num_beads; i++) {
    beadList[i].x = beadList[i].x+beadList[i].ix*Lx - cm[0];
    beadList[i].y = beadList[i].y+beadList[i].iy*Ly - cm[1];
    beadList[i].z = beadList[i].z+beadList[i].iz*Lz - cm[2];
  }
}

void msd(GSDReader* gsd, int start, int end, int interval, int decorrelated,
 int periodic_wrapped, int comflag, char** types, int ntypes, const char* filename, int nthreads)
{
  double tstart, telapsed;
  tstart = omp_get_wtime();

  ofstream logfile;
  int nframes = (end - start)/interval + 1;
  int nwindows = nframes / decorrelated - 1;
  int tmax = nframes*interval / 2;
  double cutoff = 1.2;

  Coordination coord(nframes, cutoff, periodic_wrapped);
  std::vector<int> ilist;

  double** d_data = NULL;
  int num_entries = tmax/interval + 1;
  create(d_data, nwindows, num_entries);
  for (int i = 0; i < nwindows; i++) 
    for (int t = 0; t < num_entries; t++) d_data[i][t] = 0;

  const float* box = gsd->getBoxDim();
  double Lx = box[0];
  double Ly = box[1];
  double Lz = box[2];

  omp_set_num_threads(nthreads);

  for (int i = 0; i < nwindows; i++) {
    int origin = start + i*decorrelated;
    if (origin + tmax >= end) break;

    BeadList beadList1;
    coord.load_bead_list(gsd, origin,     types, ntypes, beadList1);
    int num_beads = beadList1.size();

    double cm[3];
    if (comflag) com_remove(beadList1, cm, Lx, Ly, Lz);

    for (int t = 0; t < tmax; t+=interval) {
      
      int t_idx = t/interval;
      if (origin + t < end) {
        BeadList beadList2;
        coord.load_bead_list(gsd, origin + t, types, ntypes, beadList2);
        if (comflag) com_remove(beadList2, cm, Lx, Ly, Lz);

        double d = 0;

        #pragma omp parallel shared(num_beads) reduction(+:d)
        {

        #pragma omp for schedule(static)
        for (int n = 0; n < num_beads; n++) {
          double delx = (beadList1[n].x+beadList1[n].ix*Lx) - (beadList2[n].x+beadList2[n].ix*Lx);
          double dely = (beadList1[n].y+beadList1[n].iy*Ly) - (beadList2[n].y+beadList2[n].iy*Ly);
          double delz = (beadList1[n].z+beadList1[n].iz*Lz) - (beadList2[n].z+beadList2[n].iz*Lz);
          double rsq = delx*delx + dely*dely + delz*delz;
          d += rsq;
        }

        } // end of omp parallel

        d_data[i][t_idx] = d/(double)num_beads;
        
      } else {
        d_data[i][t_idx] = 0;
      }
    }
  }

  telapsed = omp_get_wtime()-tstart;
  std::cout << "MSD time (seconds): " << (double)telapsed << " for " << nthreads << " threads\n";

  logfile.open("msd_all.txt");
  for (int t = 0; t < num_entries; t++) {
    logfile << t << " ";
    for (int i = 0; i < nwindows; i++) {
      logfile << d_data[i][t] << " ";
    }
    logfile << "\n";
  }
  logfile.close();

  double* ave_d = NULL;
  double* ave_d2 = NULL;
  int* count = NULL;
  create(ave_d, num_entries);
  create(ave_d2, num_entries);
  create(count, num_entries);
  for (int t = 0; t < num_entries; t++) {
    ave_d[t] = 0;
    ave_d2[t] = 0;
    count[t] = 0;
  }

  // average over different windows
  for (int t = 0; t < num_entries; t++) {
    for (int i = 0; i < nwindows; i++) {
      if (d_data[i][t] > 0) {
        ave_d[t] += d_data[i][t];
        ave_d2[t] += d_data[i][t]*d_data[i][t];
        count[t]++;
      }
    }
  }

  logfile.open("msd_ave.txt");
  for (int t = 0; t < num_entries; t++) {
    if (count[t] > 0) {
      ave_d[t] /= (double)count[t];
      ave_d2[t] /= (double)count[t];
      logfile << t*interval << " ";
      logfile << ave_d[t] << " ";
      logfile << sqrt(ave_d2[t] - ave_d[t]*ave_d[t]) << " ";
      logfile << count[t] << std::endl;
    }
  }
  logfile.close();

  destroy(d_data);
  destroy(ave_d);
  destroy(count);
}

void conductivity(GSDReader* gsd1, GSDReader* gsd2, int start, int end, int interval,
  int decorrelated, int periodic_wrapped, int comflag, char** types1, int ntypes1,
  char** types2, int ntypes2, const char* filename, int nthreads)
{
  double tstart, telapsed;
  tstart = omp_get_wtime();

  ofstream logfile;
  int nframes = (end - start)/interval + 1;
  int nwindows = nframes / decorrelated - 1;
  int tmax = nframes*interval / 2;
  double cutoff = 1.2;

  Coordination coord(nframes, cutoff, periodic_wrapped);
  std::vector<int> ilist;

  double** d_data = NULL;
  int num_entries = tmax/interval + 1;
  create(d_data, nwindows, num_entries);
  for (int i = 0; i < nwindows; i++) 
    for (int t = 0; t < num_entries; t++) d_data[i][t] = 0;

  const float* box = gsd1->getBoxDim();
  double Lx = box[0];
  double Ly = box[1];
  double Lz = box[2];

  omp_set_num_threads(nthreads);

  for (int i = 0; i < nwindows; i++) {
    int origin = start + i*decorrelated;
    if (origin + tmax >= end) break;

    BeadList beadList1,beadList2;
    coord.load_bead_list(gsd1, origin,     types1, ntypes1, beadList1);
    coord.load_bead_list(gsd2, origin,     types2, ntypes2, beadList2);
    int num_beads1 = beadList1.size();

    double cm1[3], cm2[3];
    if (comflag) {
      com_remove(beadList1, cm1, Lx, Ly, Lz);
      com_remove(beadList2, cm2, Lx, Ly, Lz);
    }

    for (int t = 0; t < tmax; t+=interval) {

      int t_idx = t/interval;
      if (origin + t < end) {
        BeadList beadList1t,beadList2t;
        coord.load_bead_list(gsd1, origin + t, types1, ntypes1, beadList1t);
        coord.load_bead_list(gsd2, origin + t, types2, ntypes2, beadList2t);
        if (comflag) {
          com_remove(beadList1, cm1, Lx, Ly, Lz);
          com_remove(beadList2, cm2, Lx, Ly, Lz);
        }

        double d = 0;
        int num_beads2 = beadList2.size();

        #pragma omp parallel shared(num_beads1,num_beads2)
        {

        #pragma omp for schedule(static) reduction(+: d)
        for (int n = 0; n < num_beads1; n++) {
          double delx1 = (beadList1t[n].x+beadList1t[n].ix*Lx) - (beadList1[n].x+beadList1[n].ix*Lx);
          double dely1 = (beadList1t[n].y+beadList1t[n].iy*Ly) - (beadList1[n].y+beadList1[n].iy*Ly);
          double delz1 = (beadList1t[n].z+beadList1t[n].iz*Lz) - (beadList1[n].z+beadList1[n].iz*Lz);
          for (int m = 0; m < num_beads2; m++) {
            double delx2 = (beadList2t[m].x+beadList2t[m].ix*Lx) - (beadList2[m].x+beadList2[m].ix*Lx);
            double dely2 = (beadList2t[m].y+beadList2t[m].iy*Ly) - (beadList2[m].y+beadList2[m].iy*Ly);
            double delz2 = (beadList2t[m].z+beadList2t[m].iz*Lz) - (beadList2[m].z+beadList2[m].iz*Lz);
            double r1dotr2 = beadList1t[n].q*beadList2t[m].q*(delx1*delx2 + dely1*dely2 + delz1*delz2);
            d += r1dotr2;
          }
        }

        } // end of omp parallel

        d_data[i][t_idx] = d;
        
      } else {
        d_data[i][t_idx] = 0;
      }
    }
  }

  telapsed = omp_get_wtime()-tstart;
  std::cout << "Conductivity time (seconds): " << (double)telapsed << " for " << nthreads << " threads\n";

  logfile.open("conductivity_all.txt");
  for (int t = 0; t < num_entries; t++) {
    logfile << t << " ";
    for (int i = 0; i < nwindows; i++) {
      logfile << d_data[i][t] << " ";
    }
    logfile << "\n";
  }
  logfile.close();

  double* ave_d = NULL;
  double* ave_d2 = NULL;
  int* count = NULL;
  create(ave_d, num_entries);
  create(ave_d2, num_entries);
  create(count, num_entries);
  for (int t = 0; t < num_entries; t++) {
    ave_d[t] = 0;
    ave_d2[t] = 0;
    count[t] = 0;
  }

  // average over different windows
  for (int t = 0; t < num_entries; t++) {
    for (int i = 0; i < nwindows; i++) {
      if (fabs(d_data[i][t]) > EPSILON) {
        ave_d[t] += d_data[i][t];
        ave_d2[t] += d_data[i][t]*d_data[i][t];
        count[t]++;
      }
    }
  }

  logfile.open("conductivity_ave.txt");
  for (int t = 0; t < num_entries; t++) {
    if (count[t] > 0) {
      ave_d[t] /= (double)count[t];
      ave_d2[t] /= (double)count[t];
      logfile << t*interval << " ";
      logfile << ave_d[t] << " ";
      logfile << sqrt(ave_d2[t] - ave_d[t]*ave_d[t]) << " ";
      logfile << count[t] << std::endl;
    }
  }
  logfile.close();

  destroy(d_data);
  destroy(ave_d);
  destroy(count);
}
