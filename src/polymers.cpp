/*
  Copyright (C) 2017  Trung D. Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*----------------------------------------------------------------------------
  Polymer analysis:
  - Compute end-to-end and radius of gyration histograms
  - Asphericity parameter
  - Bond orientation correlation used to coompute persistent length L_p
   by fitting against the following function:
      G(l) = a exp(-l/L_p) + (1-a) exp(-l/L_s)

  Trung Nguyen (ndactrung@gmail.com)
------------------------------------------------------------------------------*/

#include <algorithm>
#include <fstream>
#include <cmath>
#include <iostream>
#include <string>
#include <omp.h>

#include "DCDHandle.h"
#include "io.h"
#include "memory.h"
#include "polymers.h"

using namespace std;
using namespace Analysis;

#define anint(x) ((x >= 0.5) ? (1.0) : (x < -0.5) ? (-1.0) : (0.0)) 
#define MAX_NEIGHBORS        64
#define MAX_BEADS_IN_CELL    128
#define EPSILON 0.00001

enum {XYZ=0, XML=1, DUMP=2, DCD=3};

/*----------------------------------------------------------------------------*/

Polymers::Polymers(int _nbeadsperchain, int _nthreads) : nthreads(_nthreads)
{
  io_handle = NULL;
  use_template = 0;
  user_range = 0;

  e2e_data = Rg_data = Gl_data = NULL;
  nbeadsperchain = _nbeadsperchain;
}

/*----------------------------------------------------------------------------*/

Polymers::~Polymers()
{
  if (io_handle) delete io_handle;

  destroy(e2e_data);
  destroy(Rg_data);
  destroy(Gl_data);

  indexList.clear();
  beadList.clear();
}

/*----------------------------------------------------------------------------*/
// load the bead list from a file
// only pick the beads of specified types

int Polymers::load_coord_list(char* fileName, char** types, int ntypes, int nframe)
{
  int success;
  string fullname(fileName);
  std::size_t found = fullname.find_last_of(".");
  base = fullname.substr(0,found);
  ext = fullname.substr(found+1);

  // guess the file type
  if (strcmp(ext.c_str(),"xyz") == 0) {
    if (io_handle == NULL) io_handle = new XYZHandle;
    success = io_handle->read(fileName, types, ntypes, nNumTotalBeads,
      box, beadList, timestep);
    file_type = XYZ;
  } else if (strcmp(ext.c_str(),"xml") == 0) {
    if (io_handle == NULL) io_handle = new XMLHandle;
    success = io_handle->read(fileName, types, ntypes, nNumTotalBeads,
      box, beadList, timestep);
    file_type = XML;
  } else if (strcmp(ext.c_str(),"dump") == 0 || strcmp(ext.c_str(),"txt") == 0) {
    if (io_handle == NULL) io_handle = new DUMPHandle;
    success = io_handle->read(fileName, types, ntypes, nNumTotalBeads,
      box, beadList, timestep);
    file_type = DUMP;
  } else if (strcmp(ext.c_str(),"dcd") == 0) {
    if (io_handle == NULL) io_handle = new DCDHandle;
    char* file = new char [64];
    sprintf(file, "%s-%d", base.c_str(), nframe);
    base = std::string(file);
    timestep = nframe;

    if (use_template) {
      success = ((DCDHandle*)io_handle)->read(fileName, indexList, nNumTotalBeads,
        box, beadList, nframe);
    } else {
      success = io_handle->read(fileName, nNumTotalBeads, box, beadList, nframe);
    }

    file_type = DCD;
    delete [] file;
  } else {
    std::cout << "Unknown file extension.\n";
    return 0;
  }

  return success;
}

/*----------------------------------------------------------------------------*/

int Polymers::execute(int e2e_flag, int Rg_flag, int Lp_flag)
{
  if (e2e_flag) {
    nbins_e2e = nbins;
    if (e2e_data == NULL) create(e2e_data, nbins_e2e);
  }

  if (Rg_flag) {
    nbins_Rg = nbins;
    if (Rg_data == NULL) create(Rg_data, nbins_Rg);
  }

  if (Lp_flag) {
    int nbondsperchain = nbeadsperchain - 1;
    nbins_segments = nbondsperchain / 2;
    if (Gl_data == NULL) create(Gl_data, nbins_segments);
  }  
  

  int success = compute(e2e_flag, Rg_flag, Lp_flag);

  return success;
}

/*----------------------------------------------------------------------------*/

int Polymers::compute(int e2e_flag, int Rg_flag, int Lp_flag)
{
  int i, j;
  int nchains = beadList.size() / nbeadsperchain;
  //cout << "Number of chains " << nchains << std::endl;

  write_dump("out.dump");

  std::vector<Particle> particleList;
  int m = 0;
  for (i = 0; i < nchains; i++) {
    Particle particle;
    for (j = 0; j < nbeadsperchain; j++) {
      particle.beadList.push_back(beadList[m + j]);
    }
    particleList.push_back(particle);
    m += nbeadsperchain;
  }

  // end-to-end calculation
  int periodic_wrapped = 1;
  char filename[256];

  if (e2e_flag) {
    std::vector<double> e2eList;
    for (i = 0; i < nchains; i++) {
      double e2e = end_to_end(particleList[i]);
      e2eList.push_back(e2e);
    }
    sprintf(filename, "e2e_hist_%d.txt", timestep);
    histogram(e2eList, filename, nbins, e2e_data);    
  }

  // Rg calculation

  if (Rg_flag) {
    std::vector<double> RgList;
    for (i = 0; i < nchains; i++) {
      double As, xm[3];
      double Rg = radius_gyration(particleList[i], As, xm, periodic_wrapped);
      RgList.push_back(Rg);
    }
    sprintf(filename, "Rg_hist_%d.txt", timestep);
    histogram(RgList, filename, nbins, Rg_data);
  }
  
  // persistent length calculation

  if (Lp_flag) {
    int nbondsperchain = nbeadsperchain - 1;
    int nsegments = nbondsperchain / 2;
    double* Gl = new double [nsegments];
    for (i = 0; i < nsegments; i++) Gl[i] = 0;

    // accumulate into G
    for (i = 0; i < nchains; i++)
      bond_orientation_correlation(particleList[i], Gl, nsegments);

    // normalize by the number of chains
    for (i = 0; i < nsegments; i++)
      Gl[i] /= (double)nchains;

    // save the data to the output histogram for the given configuration
    for (i = 0; i < nbins_segments; i++)
      Gl_data[i] = Gl[i];

    sprintf(filename, "Gl_%d.txt", timestep);
    ofstream ofs(filename);
    for (i = 0; i < nsegments; i++)
      ofs << i << " " << Gl[i] << std::endl;

    delete [] Gl;
  }
  

  return 1;
}

/*----------------------------------------------------------------------------*/

double Polymers::end_to_end(const Particle& particle)
{
  int nNumBeads = particle.beadList.size();
  double xtmp, ytmp, ztmp, delx, dely, delz, Lx2, Ly2, Lz2;
  Lx2 = box.Lx / 2.0;
  Ly2 = box.Ly / 2.0;
  Lz2 = box.Lz / 2.0;

  xtmp = particle.beadList[0].x;
  ytmp = particle.beadList[0].y;
  ztmp = particle.beadList[0].z;
  
  delx = particle.beadList[nNumBeads-1].x - xtmp;
  dely = particle.beadList[nNumBeads-1].y - ytmp;
  delz = particle.beadList[nNumBeads-1].z - ztmp;
  box.minimum_image(delx, dely, delz); 
  double rsq = delx * delx + dely * dely + delz * delz;

  return sqrt(rsq);
}

/*----------------------------------------------------------------------------*/

void Polymers::bond_orientation_correlation(const Particle& particle,
  double* G, int nsegments)
{
  int nNumBeads = particle.beadList.size();
  double Lx2, Ly2, Lz2;
  int nNumBonds = nNumBeads - 1;
  Lx2 = box.Lx / 2.0;
  Ly2 = box.Ly / 2.0;
  Lz2 = box.Lz / 2.0;

  for (int l = 0; l < nsegments; l++) {
    int n = 0;
    double nsdotnsl = 0;
    for (int s = 0; s < nNumBeads-l-1; s++){
      double n1x, n1y, n1z, n2x, n2y, n2z;

      // get the bond unit vector s
      get_unit_bond_vector(particle, s, n1x, n1y, n1z);

      // get the bond unit vector (s + l)
      get_unit_bond_vector(particle, s+l, n2x, n2y, n2z);
      
      nsdotnsl += n1x*n2x + n1y*n2y + n1z*n2z;
      n++;
    }
    nsdotnsl /= (double)n;
    G[l] += nsdotnsl;
  }

}

/*----------------------------------------------------------------------------*/

void Polymers::get_unit_bond_vector(const Particle& particle, int s,
  double& nx, double& ny, double& nz)
{
  double xtmp = particle.beadList[s].x;
  double ytmp = particle.beadList[s].y;
  double ztmp = particle.beadList[s].z;
  double delx = particle.beadList[s+1].x - xtmp;
  double dely = particle.beadList[s+1].y - ytmp;
  double delz = particle.beadList[s+1].z - ztmp;

  box.minimum_image(delx, dely, delz);
  double r = sqrt(delx * delx + dely * dely + delz * delz);
  nx = delx / r;
  ny = dely / r;
  nz = delz / r;
}

/*----------------------------------------------------------------------------*/

void Polymers::center_of_mass(Particle& particle, double* xm, int period_wrapped)
{
  int i, mid, nNumBeads;
  double dx, dy, dz, Lx2, Ly2, Lz2;
  Bead ref;

  nNumBeads = particle.beadList.size();
  mid = nbeadsperchain / 2;

  if (period_wrapped == 1) {
    Lx2 = box.Lx / 2.0;
    Ly2 = box.Ly / 2.0;
    Lz2 = box.Lz / 2.0;

    ref.x = particle.beadList[mid].x;
    ref.y = particle.beadList[mid].y;
    ref.z = particle.beadList[mid].z;

    // make all the contituent beads in the particle close to the reference bead
    // within half box lengths
    for (i = 0; i < nNumBeads; i++) {
      dx = particle.beadList[i].x - ref.x;
      dy = particle.beadList[i].y - ref.y;
      dz = particle.beadList[i].z - ref.z;

      if (dx > Lx2) particle.beadList[i].x -= box.Lx;
      else if (dx < -Lx2) particle.beadList[i].x += box.Lx;
      if (dy > Ly2) particle.beadList[i].y -= box.Ly;
      else if (dy < -Ly2) particle.beadList[i].y += box.Ly;
      if (dz > Lz2) particle.beadList[i].z -= box.Lz;
      else if (dz < -Lz2) particle.beadList[i].z += box.Lz;
    }
  }

  // Compute the center of mass
  xm[0] = xm[1] = xm[2] = 0.0;
  for (i = 0; i < nNumBeads; i++) {
    xm[0] += particle.beadList[i].x;
    xm[1] += particle.beadList[i].y;
    xm[2] += particle.beadList[i].z;
  }

  xm[0] /= nNumBeads;
  xm[1] /= nNumBeads;
  xm[2] /= nNumBeads;
}

/*----------------------------------------------------------------------------*/

void Polymers::gyration_tensor(Particle& particle, double* xm,
  int periodic_wrapped, double** gyration, double* R2, double* major_eigenvector)
{
  int i, j, k, nNumBeads;
  nNumBeads = particle.beadList.size();

  for (j = 0; j < 3; j++)
    for (k = 0; k < 3; k++)
      gyration[j][k] = 0.0;
 
  double xi[3], L[3];
  L[0] = box.Lx; L[1] = box.Ly; L[2] = box.Lz;
  for (i = 0; i < nNumBeads; i++) {
    xi[0] = particle.beadList[i].x;
    xi[1] = particle.beadList[i].y;
    xi[2] = particle.beadList[i].z;
    for (j = 0; j < 3; j++) {
      double rj = xi[j] - xm[j];
      if (rj > L[j]/2)
        rj -= L[j];
      else if (rj < -L[j]/2)
        rj += L[j];
       
      for (k = 0; k < 3; k++) {
        double rk = xi[k] - xm[k];
        if (rk > L[k]/2)
          rk -= L[k];
        else if (rk < -L[k]/2)
          rk += L[k];
     
        gyration[j][k] += rj * rk; //(xi[j] - xm[j]) * (xi[k] - xm[k]);
      }
    }
  }

  for (j = 0; j < 3; j++)
    for (k = 0; k < 3; k++)
      gyration[j][k] /= nNumBeads;

  // Dummy eigen_vectors
  double** eigen_vectors = new double* [3];
  for (i = 0; i < 3; i++)
    eigen_vectors[i] = new double [3];

  // Diagonalize the gyration tensor, yielding the eigenvalues
  // as squares of the radii of gyration
  diagonalize(gyration, R2, eigen_vectors);

  // find the maximum eignvalue
  double max = R2[0];
  int index = 0;
  for (i = 1; i < 3; i++) {
    if (max < R2[i]) {
      max = R2[i];
      index = i;
    }
  }

  major_eigenvector[0] = eigen_vectors[0][index];
  major_eigenvector[1] = eigen_vectors[1][index];
  major_eigenvector[2] = eigen_vectors[2][index];

  for (i = 0; i < 3; i++)
    delete [] eigen_vectors[i];
  delete [] eigen_vectors;
}

/*----------------------------------------------------------------------------
 Compute radius of gyration (Rg), center of mass (xm)
------------------------------------------------------------------------------*/

double Polymers::radius_gyration(Particle& particle, double& As, double* xm,
  int periodic_wrapped)
{
  int i;
  double R2[3], major_eigenvector[3];
  double **gyration;

  gyration = new double* [3];
  for (i = 0; i < 3; i++)
    gyration[i] = new double [3];
 
  // Find the center of mass
  center_of_mass(particle, xm, periodic_wrapped);

  // Compute the gyration tensor
  gyration_tensor(particle, xm, periodic_wrapped,
    gyration, R2, major_eigenvector);

  As = ((R2[2] - R2[1]) * (R2[2] - R2[1]) +
        (R2[1] - R2[0]) * (R2[1] - R2[0]) +
        (R2[2] - R2[0]) * (R2[2] - R2[0])) /
        (2 * (R2[0] + R2[1] + R2[2]) * (R2[0] + R2[1] + R2[2]));

  double Rg = sqrt(R2[0] + R2[1] + R2[2]);

  if (Rg > 20) {
    cout << "i = " << i << " " << Rg;
    cout << " xm : " << xm[0] << " " << xm[1] << " " << xm[2];
    cout << " R2 : " << R2[0] << " " << R2[1] << " " << R2[2];
    cout << "\n";
  }

  for (i = 0; i < 3; i++)
    delete [] gyration[i];
  delete [] gyration;

  return Rg;
}

/*----------------------------------------------------------------------------*/

void Polymers::write_dump(const char* filename)
{
  int i, mol, id;
  ofstream ofs;

  ofs.open(filename);
  ofs << "ITEM: TIMESTEP\n";
  ofs << timestep << "\n";
  ofs << "ITEM: NUMBER OF ATOMS\n";
  ofs << beadList.size() << "\n";
  ofs << "ITEM: BOX BOUNDS pp pp pp\n";
  ofs << box.xlo << " " << box.xhi << "\n";
  ofs << box.ylo << " " << box.yhi << "\n";
  ofs << box.zlo << " " << box.zhi << "\n";
  ofs << "ITEM: ATOMS id mol type q x y z\n";

  id = 1;
  mol = 1;
  int nNumBeads = beadList.size();
  for (i = 0; i < nNumBeads; i++) {
    beadList[i].mol = mol;
    ofs << id << " ";
    ofs << beadList[i].mol << " ";
    ofs << beadList[i].type << " ";
    ofs << beadList[i].q << " ";
    ofs << beadList[i].x << " ";
    ofs << beadList[i].y << " ";
    ofs << beadList[i].z << "\n";
    id++;
    if (id % nbeadsperchain == 0) mol++;
  }

  ofs.close();
}

/*----------------------------------------------------------------------------
 load the bead list from a file
 only store the indices of beads of specified types into a template
------------------------------------------------------------------------------*/

int Polymers::load_template(char* fileName, char** types, int ntypes)
{
  int success;
  string fullname(fileName);
  std::size_t found = fullname.find_last_of(".");
  base = fullname.substr(0,found);
  ext = fullname.substr(found+1);

  // temporary variables used for loading the template

  IOHandle* io_tmp = NULL;
  std::vector<Bead> list;
  int nNumBeads;

  // guess the file type

  if (strcmp(ext.c_str(),"xyz") == 0) {
    if (io_tmp == NULL) io_tmp = new XYZHandle;
    success = io_tmp->read(fileName, types, ntypes, nNumBeads, box, list, timestep);
  } else if (strcmp(ext.c_str(),"xml") == 0) {
    if (io_tmp == NULL) io_tmp = new XMLHandle;
    success = io_tmp->read(fileName, types, ntypes, nNumBeads, box, list, timestep);
  } else if (strcmp(ext.c_str(),"dump") == 0 || strcmp(ext.c_str(),"txt") == 0) {
    if (io_tmp == NULL) io_tmp = new DUMPHandle;
    success = io_tmp->read(fileName, types, ntypes, nNumBeads, box, list, timestep);
  } else {
    std::cout << "Unknown file extension.\n";
    return 0;
  }

  indexList.clear();
  for (int i = 0; i < nNumBeads; i++) {
    indexList.push_back(list[i].id);
  }

  delete io_tmp;

  return success;
}

/*----------------------------------------------------------------------------
  Gyration tensor and asphericity parameter helper functions
------------------------------------------------------------------------------*/

void Polymers::rotate(double **matrix, int i, int j, int k, int l, double s, double tau)
{
  double g = matrix[i][j];
  double h = matrix[k][l];
  matrix[i][j] = g - s * (h + g * tau);
  matrix[k][l] = h + s * (g - h * tau);
}

/*----------------------------------------------------------------------------
  diagonalize a matrix
  return the eigenvalues in evalues and eigenvectors in evectors
------------------------------------------------------------------------------*/
#define MAXJACOBI 50
int Polymers::diagonalize(double **matrix, double *evalues, double **evectors)
{
  int i, j, k, iter;
  double tresh, theta, tau, t, sm, s, h, g, c, b[3], z[3];
 
  for (i = 0; i < 3; i++) {
    for (j = 0; j < 3; j++) 
      evectors[i][j] = 0.0;
    evectors[i][i] = 1.0;
  }
 
  for (i = 0; i < 3; i++) {
    b[i] = evalues[i] = matrix[i][i];
    z[i] = 0.0;
  }
 
  for (iter = 1; iter <= MAXJACOBI; iter++) {
    sm = 0.0;
    for (i = 0; i < 2; i++)
      for (j=i+1; j<3; j++)
        sm += fabs(matrix[i][j]);
    if (sm == 0.0) 
      return 0;
   
    if (iter < 4) 
      tresh = 0.2 * sm / (3 * 3);
    else 
      tresh = 0.0;
   
    for (i = 0; i < 2; i++) {
      for (j = i+1; j < 3; j++) {
        g = 100.0*fabs(matrix[i][j]);
        if (iter > 4 && fabs(evalues[i])+g == fabs(evalues[i])
           && fabs(evalues[j])+g == fabs(evalues[j]))
           matrix[i][j] = 0.0;
        else if (fabs(matrix[i][j]) > tresh) {
          h = evalues[j] - evalues[i];
          if (fabs(h)+g == fabs(h)) 
            t = (matrix[i][j]) / h;
          else {
            theta = 0.5 * h / (matrix[i][j]);
            t = 1.0 / (fabs(theta) + sqrt(1.0 + theta * theta));
            if (theta < 0.0) 
              t = -t;
          }

          c = 1.0 / sqrt(1.0 + t * t);
          s = t * c;
          tau = s / (1.0 + c);
          h = t * matrix[i][j];
          z[i] -= h;
          z[j] += h;
          evalues[i] -= h;
          evalues[j] += h;
          matrix[i][j] = 0.0;

          for (k = 0; k < i; k++)
            rotate(matrix, k, i, k, j, s, tau);
          for (k = i+1; k < j; k++) 
            rotate(matrix, i, k, k, j, s, tau);
          for (k = j+1; k < 3; k++) 
            rotate(matrix, i, k, j, k, s, tau);
          for (k = 0; k < 3; k++) 
            rotate(evectors, k, i, k, j, s, tau);
        }
      }
    }
   
    for (i = 0; i < 3; i++) {
      evalues[i] = b[i] += z[i];
      z[i] = 0.0;
    }
  }

  return 1;
}

/*----------------------------------------------------------------------------
  generate a generic histogram for a double array
------------------------------------------------------------------------------*/

void Polymers::histogram(const std::vector<double>& array, const char* filename,
  int num_bins, double* output_hist)
{
  int i, ibin, array_size;
  double bin_size;
  double* bin = 0x0;

  array_size = array.size();
  if (array_size == 0) return;

  // if the histogram range is not specified by the user, find them from the array data

  if (!user_range) {
    min_val = max_val = array[0];
    for (i = 0; i < array_size; i++) {
      if (min_val > array[i]) min_val = array[i];
      if (max_val < array[i]) max_val = array[i];
    } 
  }

  bin_size = (max_val - min_val) / num_bins;
  bin = new double [num_bins];

  for (i = 0; i < num_bins; i++) bin[i] = 0.0;

  int ncount = 0;
  for (i = 0; i < array_size; i++) {
    if (fabs(bin_size) > EPSILON)
      ibin = (int)((array[i] - min_val) / bin_size);
    else ibin = 0;

    if (ibin >= 0 && ibin < num_bins) {
      bin[ibin] += 1.0;
      ncount++;
    }
  }

  ofstream ofs(filename);

  // weighted mean and variance 

  double ave = 0;
  for (i = 0; i < num_bins; i++) {
    ofs << min_val + i * bin_size << "\t" << bin[i] << " " << bin[i] / ncount << std::endl;
    output_hist[i] = bin[i] / ncount;
    ave += bin[i] * (min_val + i * bin_size);
  }
  double var = 0;
  for (i = 0; i < num_bins; i++) {
    var += bin[i] * (min_val + i * bin_size - ave) * (min_val + i * bin_size - ave);
  }
  var = sqrt(var);

  delete [] bin;
}

