#ifndef __AUTOCORRELATION
#define __AUTOCORRELATION

#include <vector>
#include <string>
#include "buildingblocks.h"
#include "io.h"

namespace Analysis {

class Frame {
public:
  std::vector<Bead> beadList;
};

class Autocorrelation
{
public:
  Autocorrelation(int _nthreads, int _periodic_wrapped=1);
  ~Autocorrelation();

  // load the bead list from a file
  int load_bead_list(char* fileName, char** types, int ntypes, int t=-1, int reference_point=1);

  // load the bead list from some where else: an array from DCDReader, XML Parser, etc.
  int load_bead_list(void*) { return 0; }

  double compute(int mode);

  // bead lists
  std::vector<Bead> beadList0, beadList;

protected:
  double compute_msd();
  double compute_vacf();

  void com_remove(BeadList& beadList);

  int nthreads;             //! number of threads
  int periodic_wrapped;     //! 1 if PBC is used

  int nNumTotalBeads;       //! Number of beads per snapshot
  Box box;                  //! Box dimensions
  int timestep;             //! Time step read from the config file

  int file_type;            //! Config file type: XYZ, DUMP (LAMMPS) or XML (HOOMD)
  string base, ext;         //! Config files are in the form: base.ext

  IOHandle* io_handle;

protected:

  
};

} // namespace Polymers

#endif
