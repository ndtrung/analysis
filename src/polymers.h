/*
  Copyright (C) 2017  Trung D. Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __POLYMERS
#define __POLYMERS

#include <vector>
#include <string>
#include "analyzer.h"
#include "buildingblocks.h"

namespace Analysis {

class Polymers : public Analyzer
{
public:
  Polymers(int _nbeadsperchain, int _nthreads=1);
  ~Polymers();

  // load the bead list from a file
  int load_coord_list(char* fileName, char** types, int ntypes, int t=-1);

  int execute(int e2e_flag, int Rg_flag, int Lp_flag);

  void set_range(double _min_val, double _max_val) { 
    min_val = _min_val; max_val = _max_val;
    user_range = 1;
  }

  void set_nbins(int _nbins) { nbins = _nbins; }

  // load the bead indinces of specified types from a file (XYZ, HOOMD XML or LAMMPS dump files)
  int load_template(char* fileName, char** types, int ntypes);

  // Bead list
  std::vector<Bead> beadList;

  // two index lists
  std::vector<int> indexList;

  int use_template;         //! 1 if using a template for DCD files
  double* e2e_data;
  int nbins_e2e;
  double* Rg_data;
  int nbins_Rg;
  double* Gl_data;
  int nbins_segments;

protected:

  int nNumTotalBeads;       //! Number of beads per snapshot/configuration
  int nbeadsperchain;       //! Number of beads per chain 
  Box box;                  //! Box dimensions
  int timestep;             //! Time step read from the config file
  double min_val, max_val;  //! Range for the histogram
  int user_range;           //! 1/0 if user set ranges or not (0 by default)
  int nbins;                //! Number of histogram bins
  int file_type;            //! Config file type: XYZ, DUMP (LAMMPS) or XML (HOOMD)
  string base, ext;         //! Config files are in the form: base.ext

  int nthreads;             //! Number of threads in use

  int compute(int e2e_flag, int Rg_flag, int Lp_flag);

  void center_of_mass(Particle&, double*, int);

  double end_to_end(const Particle&);

  void bond_orientation_correlation(const Particle&, double*, int);

  double radius_gyration(Particle& particle,
    double& As, double* xm, int periodic_wrapped);

  void get_unit_bond_vector(const Particle& particle, int s,
    double& nx, double& ny, double& nz);

  void gyration_tensor(Particle& particle, double* xm,
    int periodic_wrapped, double** gyration, double* R2, double* major_eigenvector);

  int diagonalize(double **matrix, double *evalues, double **evectors);
  void rotate(double **matrix, int i, int j, int k, int l, double s, double tau);

  void histogram(const std::vector<double>& array, const char* filename, int nbins, double*);
  void write_dump(const char* filename);
  IOHandle* io_handle;
};

} // namespace Analysis

#endif
