/*
  Copyright (C) 2017  Trung D. Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*

  This is an example of using the classes and API provided by libanalyzer.a.

  Compile:     make -f Makefile.cpu
  Clean-build: make -f Makefile.cpu clean

  Use:   ./analyze-polymers types 1 M T_1 .. T_M [frames <args>]
    
  Input:   config_file = an ASCII file in XYZ format, or LAMMPS dump or HOOMD XML
              for LAMMPS dump files, atom data contains at least 5 columns: id type x y z
              for HOOMD XML files, positions are listed first

           types 
              M     = number of bead types in the first group
              T_1 .. T_M = list of M bead types, as space-separated strings

           range 
             <args> = min_val max_val
                 min_val  = minimum value for the histograms
                 max_val  = maximum value for the histograms

           nbins  = number of bins for end-to-end and radius of gyration histograms

           frames
              <args> = start end interval
                 start    = start time
                 end      = end time
                 interval = inverval between consecutive frames (snapshots)

           e2e = 1/0 turn on/off end-to-end calculation
           Rg  = 1/0 turn on/off radius of gyration calculation
           Lp  = 1/0 turn on/off persistent legnth calculation

           template (optional): when frames are read from a DCD file
              <arg> = template file (XYZ format, or LAMMPS dump or HOOMD XML)
              if not specified, then all the beads are included


  Examples: 1) run the following

            ./analyze-polymers final.xml types 2 1 2 range 0 10 nbins 100

            for polymers composed of beads of two types 1 and 2.
         
            2) run the following

            ./analyze-polymers trajectory.dcd types 2 A B frames 20 60 1 range 0 10 nbins 100

            for polymers composed of beads of two types A and B from a trajectory
            from frame 20 through frame 60 with step 1.

            3) run the following

            ./analyze-polymers snapshot..xml types 2 A B frames 10000 20000 1000

            for polymers composed of beads of two types A and B from a series of
            files snapshot.10000.xml, snapshot.11000.xml,.., snapshot.20000.xml.

  Output:  Rg-hist-*.txt and e2e-hist-*.txt are the histograms
              the 1st column are the bins,
              the 2nd column number of counts in each bin and
              the 3rd column the normalized histogram

  Contact: Trung Nguyen, ndactrung@gmail.com

*/

#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

#include "io.h"
#include "memory.h"
#include "polymers.h"

using namespace std;
using namespace Analysis;

int main(int argc, char** argv)
{
  if (argc < 2) {
    cout << "Arguments required: filename\n";
    cout << "Refer to README for optional arguments.\n";
    return 1;
  }

  char fileName[64], outfile[64];
  int ntypes = 0;
  char** types = NULL;
  int nbeadsperchain = 40;

  sprintf(fileName, "%s", argv[1]);

  int trajectory = 0;
  int start = 0;
  int end = 0;
  int interval = 0;
  int nbins = 100;
  double min_val = 0;
  double max_val = -1;
  int nthreads = 1;
  int e2e = 1;
  int Rg = 1;
  int Lp = 1;
  char templatefile[256];
  int use_template = 0;

  // parse the arguments
  
  int iarg = 2;
  while (iarg < argc) {
    if (strcmp(argv[iarg],"types") == 0) {
      ntypes = atoi(argv[iarg+1]);
      if (ntypes <= 0) {
        std::cout << "Invalid number of types\n";
        return 1;
      }
      iarg+=2;
      types = new char* [ntypes];
      for (int m = 0; m < ntypes; m++) {
        types[m] = new char [64];
        strcpy(types[m], argv[iarg++]);
      }
    } else if (strcmp(argv[iarg],"nbeadsperchain") == 0) {
      nbeadsperchain = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"nthreads") == 0) {
      nthreads = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"frames") == 0) {
      start = atoi(argv[iarg+1]);
      end = atoi(argv[iarg+2]);
      interval = atoi(argv[iarg+3]);
      trajectory = 1;
      iarg += 4;
    } else if (strcmp(argv[iarg],"nbins") == 0) {
      nbins = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"e2e") == 0) {
      e2e = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"Rg") == 0) {
      Rg = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"Lp") == 0) {
      Lp = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"range") == 0) {
      min_val = atof(argv[iarg+1]);
      max_val = atof(argv[iarg+2]);
      iarg += 3;
    } else if (strcmp(argv[iarg],"template") == 0) {
      strcpy(templatefile,argv[iarg+1]);
      use_template = 1;
      iarg += 2;
    } else {
      std::cout << "Invalid argument: " << argv[iarg] << "\n";
      return 1;
    }
  }

  // error checks
  
  if (ntypes == 0) {
    cout << "Needs to specify at least a bead type.\n";
    return 1;
  }
 
  int success;

  if (trajectory == 0) { // single snapshot

    Polymers polymers(nbeadsperchain, nthreads);
    if (min_val < max_val) polymers.set_range(min_val, max_val);
    polymers.set_nbins(nbins);
    if (ntypes > 0) polymers.load_coord_list(fileName, types, ntypes);

    success = polymers.execute(e2e, Rg, Lp);
    if (success == 0) std::cout << "Failed to analyze " << fileName << std::endl;

  } else { // multiple snapshots

    string fullname(fileName);
    std::size_t found = fullname.find_last_of(".");
    string base = fullname.substr(0,found);
    string ext = fullname.substr(found+1);

    char* filetime = new char [1024];
    int num_configs = (end - start)/interval + 1;
    int num_beads = 0;
    int iconfig = 0;

    Polymers polymers(nbeadsperchain, nthreads);
    if (min_val < max_val) polymers.set_range(min_val, max_val);
    polymers.set_nbins(nbins);
    polymers.use_template = use_template;
    if (use_template) {
      if (ntypes > 0) polymers.load_template(templatefile, types, ntypes);
    }

    double** ave_e2e = NULL;
    double** ave_Rg = NULL;
    double** ave_Gl = NULL;
    int nbins_segments = (nbeadsperchain - 1) / 2;
    if (e2e) {
      create(ave_e2e, nbins, 2);
      for (int i = 0; i < nbins; i++)
        ave_e2e[i][0] = ave_e2e[i][1] = 0;
    }

    if (Rg) {
      create(ave_Rg, nbins, 2);
      for (int i = 0; i < nbins; i++)
        ave_Rg[i][0] = ave_Rg[i][1] = 0;
    }

    if (Lp) {
      create(ave_Gl, nbins_segments, 2);
      for (int i = 0; i < nbins_segments; i++)
        ave_Gl[i][0] = ave_Gl[i][1] = 0;
    }

    for (int t = start; t <= end; t += interval) {

      if (strcmp(ext.c_str(),"dcd") == 0) { // read from a DCD file
        if (ntypes > 0) polymers.load_coord_list(fileName, types, ntypes, t);
      } else { // read from an ASCII file
        sprintf(filetime, "%s%d.%s", base.c_str(), t, ext.c_str());
        if (ntypes > 0) polymers.load_coord_list(filetime, types, ntypes);
      }

      success = polymers.execute(e2e, Rg, Lp);

      if (e2e) {
        for (int i = 0; i < nbins; i++) {
          double Ree = polymers.e2e_data[i];
          ave_e2e[i][0] += Ree;
          ave_e2e[i][1] += Ree*Ree;
        }
      }

      if (Rg) {
        for (int i = 0; i < nbins; i++) {
          double r = polymers.Rg_data[i];
          ave_Rg[i][0] += r;
          ave_Rg[i][1] += r*r;
        }
      }

      if (Lp) {
        for (int i = 0; i < nbins_segments; i++) {
          double gl = polymers.Gl_data[i];
          ave_Gl[i][0] += gl;
          ave_Gl[i][1] += gl*gl;
        }
      }

      iconfig++;
    }

    ofstream ofs;
    if (e2e) {
      ofs.open("ave_e2e.txt");
      for (int i = 0; i < nbins; i++) {
        double ave = ave_e2e[i][0] / (double)num_configs;
        double stdev = sqrt(ave_e2e[i][1]/(double)num_configs - ave*ave);
        ofs << i << " " << ave << " " << stdev << std::endl;
      }
      ofs.close();
    }

    if (Rg) {
      ofs.open("ave_Rg.txt");
      for (int i = 0; i < nbins; i++) {
        double ave = ave_Rg[i][0] / (double)num_configs;
        double stdev = sqrt(ave_Rg[i][1]/(double)num_configs - ave*ave);
        ofs << i << " " << ave << " " << stdev << std::endl;
      }
      ofs.close();
    }

    if (Lp) {
      ofs.open("ave_Gl.txt");
      for (int i = 0; i < nbins_segments; i++) {
        double ave = ave_Gl[i][0] / (double)num_configs;
        double stdev = sqrt(ave_Gl[i][1]/(double)num_configs - ave*ave);
        ofs << i << " " << ave << " " << stdev << std::endl;
      }
      ofs.close();
    }
  
    destroy(ave_e2e);
    destroy(ave_Rg);
    destroy(ave_Gl);
  }

  if (types) {
    for (int m = 0; m < ntypes; m++)
      delete [] types[m];
    delete [] types;
  }


  return 0;
}
