/*
  Copyright (C) 2017  Trung D. Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
  Compute pair correlation function g(r) and structure factor S(q) from simulation snapshots.

  This is an example of using the classes and API provided by libanalyzer.a.

  Compile:     make -f Makefile.cpu
  Clean-build: make -f Makefile.cpu clean

  Use:   ./make-rdf types1 M T_1 .. T_M types2 N T_1 .. T_N [frames <args>]
    
  Input:   config_file = an ASCII file in XYZ format, or LAMMPS dump or HOOMD XML
              for LAMMPS dump files, atom data contains at least 5 columns: id type x y z
              for HOOMD XML files, positions are listed first

           types1 
              M     = number of bead types in the first group
              T_1 .. T_M = list of M bead types, as space-separated strings

           types2
              N     = number of bead types in the second group
              T_1 .. T_N = list of N bead types, as space-separated strings

           rmax (optional, default = 10.0)
              <arg> = maximum range for RDF calculation

           dr (optional, default = 0.5)
              <arg> = bin size used for RDF

           qmin (optional) - used for Fourier transform of g(r)
              <arg> = minimum wavenumber

           qmax (optional) - used for Fourier transform of g(r)
              <arg> = maximum wavenumber

           nthreads (optional, default = 1)
              <arg> = number of threads

           direct (optional, default = no)
              <arg> = yes/no: computing S(q) directly, i.e. not from Fourier transforming g(r)

           kgrid (optional)
              <args> = nkx nky nkz: numbers of wavevectors in each direction, default nkx = nky = nkz = 32

           dk (optional)
              <args> = dkx dky dkz: dk in each direction, default dkx = dky = dkz = 2pi/L (1/unit length)

           fft (optional)
              <arg> = dr : bin size for the particle density grid

           frames (optional): see example for details
              <args> = start end interval
                 start    = start time
                 end      = end time
                 interval = inverval between consecutive frames (snapshots)

           template (optional): when frames are read from a DCD file
              <arg> = template file (XYZ format, or LAMMPS dump or HOOMD XML)
              if not specified, then all the beads are included

  Examples: 1) run the following

             ./make-rdf final.xml types1 2 1 2 nthreads 2

           to compute the RDF and S(q) of beads of types 1 and 2 from final.xml using 2 threads.
           
            2) run the following

             ./make-rdf trajectory.dcd types1 2 A B frames 50 60 1

           to compute the RDF and S(q) for beads of two types A and B from a trajectory
           from frame 50 through frame 60 with step 1 and averaged output.

            3) run the following

             ./make-rdf snapshot..xml types1 2 A B types2 2 A B frames 10000 20000 1000

           to compute the RDF and S(q) composed of beads of two types A and B from a series of
           files snapshot.10000.xml, snapshot.11000.xml,.., snapshot.20000.xml, and averaged output.

  Output:  For single configuration:
              rdf.txt contains 2 columns, r and g(r)
              s_q.txt contains 2 columns, q and S(q)
              If specify 'direct yes', s_q_direct.txt contains 2 columns, q and S(q)
           For multiple configurations (i.e. a trajectory):
              g_r_*.txt and s_q_*.txt for each configuration
              ave_rdf.txt contains 3 columns, r, ave g(r) and stdev
              ave_sq.txt contains 3 columns, q, ave S(q) and stdev
              If specify 'direct yes', ave_sq_direct.txt contains 3 columns, q, ave S(q) and stdev

  Contact: Trung Nguyen, ndactrung@gmail.com

*/

#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

#include "io.h"
#include "memory.h"
#include "rdf.h"

using namespace std;
using namespace Analysis;

int main(int argc, char** argv)
{
  if (argc < 2) {
    cout << "Arguments required: filename\n";
    cout << "Refer to README for optional arguments.\n";
    return 1;
  }

  char fileName[64], outfile[64];
  int ntypes1 = 0;
  char** types1 = NULL;
  int ntypes2 = 0;
  char** types2 = NULL;
  double dr = 0.01;
  double rmax = 0;
  double qmin = 0;
  double qmax = 0;
  double qbin = 0.01;

  sprintf(fileName, "%s", argv[1]);

  int trajectory = 0;
  int start = 0;
  int end = 0;
  int interval = 0;

  int nkx = 32;
  int nky = 32;
  int nkz = 32;
  double dkx = 0;
  double dky = 0;
  double dkz = 0;
  double fft_dr = 0.1;
  int nthreads = 1;
  int directsq = 0;

  char templatefile[256];
  int use_template = 0;

  // parse the arguments
  
  int iarg = 2;
  while (iarg < argc) {
    if (strcmp(argv[iarg],"types1") == 0) {
      ntypes1 = atoi(argv[iarg+1]);
      if (ntypes1 <= 0) {
        std::cout << "Invalid number of types\n";
        return 1;
      }
      iarg+=2;
      types1 = new char* [ntypes1];
      for (int m = 0; m < ntypes1; m++) {
        types1[m] = new char [64];
        strcpy(types1[m], argv[iarg++]);
      }
    } else if (strcmp(argv[iarg],"types2") == 0) {
      ntypes2 = atoi(argv[iarg+1]);
      if (ntypes2 <= 0) {
        std::cout << "Invalid number of types\n";
        return 1;
      }
      iarg+=2;
      types2 = new char* [ntypes2];
      for (int m = 0; m < ntypes2; m++) {
        types2[m] = new char [64];
        strcpy(types2[m], argv[iarg++]);
      }
    } else if (strcmp(argv[iarg],"dr") == 0) {
      dr = atof(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"rmax") == 0) {
      rmax = atof(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"qmin") == 0) {
      qmin = atof(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"qmax") == 0) {
      qmax = atof(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"qbin") == 0) {
      qbin = atof(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"fft") == 0) {
      fft_dr = atof(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"nthreads") == 0) {
      nthreads = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"frames") == 0) {
      start = atoi(argv[iarg+1]);
      end = atoi(argv[iarg+2]);
      interval = atoi(argv[iarg+3]);
      trajectory = 1;
      iarg += 4;
    } else if (strcmp(argv[iarg],"direct") == 0) {
      if (strcmp(argv[iarg+1],"yes") == 0) directsq = 1;
      else if (strcmp(argv[iarg+1],"no") == 0) directsq = 0;
      else printf("Invalid value for the direct argument\n");
      iarg += 2;
    } else if (strcmp(argv[iarg],"kgrid")==0) {
      nkx = atoi(argv[iarg+1]);
      nky = atoi(argv[iarg+2]);
      nkz = atoi(argv[iarg+3]);
      iarg += 4;
    } else if (strcmp(argv[iarg],"dk")==0) {
      dkx = atof(argv[iarg+1]);
      dky = atof(argv[iarg+2]);
      dkz = atof(argv[iarg+3]);
      iarg += 4;
    } else if (strcmp(argv[iarg],"template") == 0) {
      strcpy(templatefile,argv[iarg+1]);
      use_template = 1;
      iarg += 2;
    } else {
      std::cout << "Invalid argument: " << argv[iarg] << "\n";
      return 1;
    }
  }

  // error checks
  
  if (ntypes1 == 0 && ntypes2 == 0) {
    cout << "Needs to specify at least a bead type for computing RDF.\n";
    return 1;
  }
 
  int success;

  if (trajectory == 0) { // single snapshot

    RDF rdf(dr, rmax, qmax, qmin, directsq, nthreads);
    rdf.set_k_vectors(nkx, nky, nkz, dkx, dky, dkz);
    rdf.set_q_bin(qbin);
    rdf.set_fft_dr(fft_dr);
    if (ntypes1 > 0) rdf.load_coord_list(1, fileName, types1, ntypes1);
    if (ntypes2 > 0) rdf.load_coord_list(2, fileName, types2, ntypes2);
    success = rdf.execute();
    if (success == 0) std::cout << "Failed to compute the RDF for " << fileName << std::endl;

  } else { // multiple snapshots

    string fullname(fileName);
    std::size_t found = fullname.find_last_of(".");
    string base = fullname.substr(0,found);
    string ext = fullname.substr(found+1);

    char* filetime = new char [1024];
    int num_configs = (end - start)/interval + 1;
    float* rdf_time = NULL;
    float* sq_time = NULL;
    float* sq_direct_time = NULL;
    int num_beads = 0;
    int iconfig = 0;
    int nbins, nqbins;

    RDF rdf(dr, rmax, qmax, qmin, directsq, nthreads);
    rdf.set_k_vectors(nkx, nky, nkz, dkx, dky, dkz);
    rdf.set_q_bin(qbin);
    rdf.set_fft_dr(fft_dr);
    rdf.use_template = use_template;
    if (use_template) {
      if (ntypes1 > 0) rdf.load_template(1, templatefile, types1, ntypes1);
      if (ntypes2 > 0) rdf.load_template(2, templatefile, types2, ntypes2);
    }
    for (int t = start; t <= end; t += interval) {

      if (strcmp(ext.c_str(),"dcd") == 0) { // read from a DCD file
        if (ntypes1 > 0) rdf.load_coord_list(1, fileName, types1, ntypes1, t);
        if (ntypes2 > 0) rdf.load_coord_list(2, fileName, types2, ntypes2, t);
      } else { // read from an ASCII file
        sprintf(filetime, "%s%d.%s", base.c_str(), t, ext.c_str());
        if (ntypes1 > 0) rdf.load_coord_list(1, filetime, types1, ntypes1);
        if (ntypes2 > 0) rdf.load_coord_list(2, filetime, types2, ntypes2);
      }

      std::cout << "Frame " << iconfig+1 << " of " << num_configs << std::endl;

      success = rdf.execute();

      if (success == 0) {
        std::cout << "Failed to compute the RDF for config " << t << std::endl;
        continue;
      }

      if (t == start && rdf_time == NULL) {
        nbins = rdf.nbins;
        nqbins = rdf.nqbins;
        rdf_time = new float [3*nbins];
        memset(rdf_time, 0, sizeof(float)*3*nbins);
      }

      if (directsq == 1) {
        if (t == start && sq_direct_time == NULL) {
          sq_direct_time = new float [3*nqbins];
          memset(sq_direct_time, 0, sizeof(float)*3*nqbins);
        }
      } else {
        if (t == start && sq_time == NULL) {
          sq_time = new float [3*nqbins];
          memset(sq_time, 0, sizeof(float)*3*nqbins);
        }
      }

      for (int i = 0; i < nbins; i++) {
        double r = rdf.r[i];
        double g = rdf.g[i];
        rdf_time[i] = r;
        rdf_time[i+nbins] += g;
        rdf_time[i+2*nbins] += g * g;
      }

      if (directsq) {
        double dk = sqrt(dkx*dkx+dky*dky+dkz*dkz);
        for (int i = 0; i < nqbins; i++) {
          double q = rdf.qradial[i];
          double S = rdf.Sradial[i];
          sq_direct_time[i] = q;
          sq_direct_time[i+nqbins] += S;
          sq_direct_time[i+2*nqbins] += S * S;
        }
      } else {

        for (int i = 0; i < nqbins; i++) {
          double q = rdf.q[i];
          double S = rdf.S[i];
          sq_time[i] = q;
          sq_time[i+nqbins] += S;
          sq_time[i+2*nqbins] += S * S;
        }
      }

      iconfig++;
    }

    double ave, avesq;
    
    for (int i = 0; i < nbins; i++) {
      ave   = rdf_time[i+nbins]/(float)iconfig;
      avesq = rdf_time[i+2*nbins]/(float)iconfig;
      rdf_time[i+nbins] = ave;
      rdf_time[i+2*nbins] = sqrt(avesq-ave*ave);
    }


    if (directsq) {
      for (int i = 0; i < nqbins; i++) {
        ave   = sq_direct_time[i+nqbins]/(float)iconfig;
        avesq = sq_direct_time[i+2*nqbins]/(float)iconfig;
        sq_direct_time[i+nqbins] = ave;
        sq_direct_time[i+2*nqbins] = sqrt(avesq-ave*ave);
      }
    } else {

      for (int i = 0; i < nqbins; i++) {
        ave   = sq_time[i+nqbins]/(float)iconfig;
        avesq = sq_time[i+2*nqbins]/(float)iconfig;
        sq_time[i+nqbins] = ave;
        sq_time[i+2*nqbins] = sqrt(avesq-ave*ave);
      }

    }

    ofstream ofs;
    ofs.open("ave_rdf.txt");
    ofs << "r g(r) stdev\n";
    for (int i = 0; i < nbins; i++) {
      ofs << rdf_time[i] << " ";
      ofs << rdf_time[i+nbins] << " ";
      ofs << rdf_time[i+2*nbins] << "\n";
    }
    ofs.close();

    if (directsq) {
      ofs.open("ave_sq_direct.txt");
      ofs << "k S(k) stdev\n";
      for (int i = 0; i < nqbins; i++) {
        ofs << sq_direct_time[i] << " ";
        ofs << sq_direct_time[i+nqbins] << " ";
        ofs << sq_direct_time[i+2*nqbins] << "\n";
      }
      ofs.close();
    } else {

      ofs.open("ave_sq.txt");
      ofs << "q S(q) stdev\n";
      for (int i = 0; i < nqbins; i++) {
        ofs << sq_time[i] << " ";
        ofs << sq_time[i+nqbins] << " ";
        ofs << sq_time[i+2*nqbins] << "\n";
      }
      ofs.close();

    }

    delete [] filetime;
    delete [] rdf_time;
    delete [] sq_time;
    if (sq_direct_time) delete [] sq_direct_time;
  }

  if (types1) {
    for (int m = 0; m < ntypes1; m++)
      delete [] types1[m];
    delete [] types1;
  }

  if (types2) {
    for (int m = 0; m < ntypes2; m++)
      delete [] types2[m];
    delete [] types2;
  }

  return 0;
}
