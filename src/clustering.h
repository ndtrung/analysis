/*
  Copyright (C) 2017  Trung D. Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __CLUSTERING
#define __CLUSTERING

#include <vector>
#include <string>
#include "analyzer.h"
#include "buildingblocks.h"

namespace Analysis {

struct Exclude 
{
  char type1[8];
  char type2[8];
};

class Clustering : public Analyzer
{
public:
  Clustering(double _cutoff, int _periodic_wrapped=1, int _nNumBeadsInParticle=1,
             int _threshold=5, int _write_clusters=0, int _bin_size=5,
             int _spatial_binning=0, int _nx=1, int _ny=1, int _nz=1);
  ~Clustering();

  int execute();

  // load the bead list from a file
  int load_bead_list(char* fileName, char** types, int ntypes, int t=-1);

  // load the bead list from some where else: an array from DCDReader, XML Parser, etc.
  int load_coord_list(void*) { return 0; }

  // set the exclusion list
  void setExclusion(vector<Exclude>* _exclusionList) { exclusionList = _exclusionList; }

  // set the max cluster size to bin
  void setMaxSize(double m) { user_max_size = m; }

  int find_clusters();

  ///! Bead list for clustering
  std::vector<Bead> beadList;

  ///! Returned list of clusters
  std::vector<Cluster> clusterList;

  ///! Neighbor list info for other accesses
  int** neighbors;   ///! neighbors[i][j] gives the index of the jth neighbor of bead i
  int* num_neigh;    ///! num_neigh[i] gives the number of neighbors of bead i

  ///! Eexclusion list
  vector<Exclude>* exclusionList;

  void replicate_system(int nx, int ny, int nz);

  // load the bead indinces of specified types from a file (XYZ, HOOMD XML or LAMMPS dump files)
  int load_template(char* fileName, char** types, int ntypes);

  // the list of particles to read in from the template
  std::vector<Bead> templateBeadList;
  std::vector<int> indexList;

  int nclusters_found;

protected:

  void make_cluster(int i, Cluster& cluster, std::vector<Bead>& beadList,
                    int nNumBeadsInParticle);

  double find_max_dist(Cluster& cluster, double* xm);

  void arrange(std::vector<Particle>& particleList, BeadList& beadList,
               Cluster& cluster, int nNumBeadsPerParticle);

  int excluded(const char* type1, const char* type2);

  ///! cluster analysis
  void Ndistribution(std::vector<Cluster>& clusterList,
                     int bin_size, int threshold, double max_size, int nNumBeadsInParticle);

  double asphericity_param(Cluster& cluster, double* major_eigenvector,
                           double* R2, double& Rg, double* xm);

  int binning(double* xm, int numBeads, int* cells, int nx, int ny, int nz);

  void center_of_mass(BeadList& beadList, double* xm, int period_wrapped);

  ///! cluster shapes
  void rotate(double **matrix, int i, int j, int k, int l, double s, double tau);
  int diagonalize(double **matrix, double *evalues, double **evectors);
  void gyration_tensor(BeadList& beadList, double* xm,
                       int periodic_wrapped, double** gyration, double* R2, double* major_eigenvector);

  ///! create a histogram for array with bin_size and write to filename
  void histogram(vector<double>& array, double bin_size, char* filename);

  ///! create neighbor lists
  void create_cell_list(int ncellx, int ncelly, int ncellz,
                        int** neighbor_cells, int* num_neighbor_cells);

  void neighboring(std::vector<Bead>& beadList, double cutoff2, int periodic_wrapped);

protected:

  double cutoff;            //! Maximum distance between two beads to be in a cluster
  int periodic_wrapped;     //! 1 if the clusters are wrapped by the PBC
  int nNumBeadsInParticle;  //! Number of beads per particle/molecule
  int threshold;            //! Exclude clusters with fewer than threshold particles
  int write_clusters;       //! 1 if the clusters are written to files, 0 otherwise
  int bin_size;             //! Bin size for the cluster size histogram (number of beads)
  
  int nNumTotalBeads;       //! Number of beads per snapshot for clustering
  Box box;
  int timestep;             //! Time step read from the config file

  int *grid;                //! grid for spatial binning
  int nx, ny, nz;           //! Number of bins in x, y and z directions
  int nbins;
  int spatial_binning;      //! 1 if do the spatial binning, 0 otherwise
 
  int file_type;            //! Config file type: XYZ, DUMP (LAMMPS) or XML (HOOMD)
  string base, ext;         //! Config files are in the form: base.ext
  int max_neigh;            //! maximum number of neighbors per particle (used to grow the array neighbors)
  int max_beads_in_cell;    //! maximum number of particles per cell (used to grow the array cells)
  double user_max_size;     //! maximum cluster size used for the size distribution histogram (-1 if not specified)

  int use_template;         //! 1 if using a template for DCD files

  int *visited;
  
  void combine_cluster(const Cluster&,  Cluster&);
  void write(const Cluster& cluster, const std::vector<Bead>& beadList,
    const char* fileName, int timestep);


  IOHandle* io_handle;
};

} // namespace Analysis

#endif
