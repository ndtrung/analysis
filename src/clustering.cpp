/*
  Copyright (C) 2017  Trung D. Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*----------------------------------------------------------------------------
  Clustering analysis

  Trung Nguyen (ndactrung@gmail.com)
------------------------------------------------------------------------------*/

#include <algorithm>
#include <fstream>
#include <cassert>
#include <cmath>
#include <iostream>
#include <string>

#include "DCDHandle.h"
#include "io.h"
#include "memory.h"
#include "clustering.h"

using namespace std;
using namespace Analysis;

#define anint(x) ((x >= 0.5) ? (1.0) : (x < -0.5) ? (-1.0) : (0.0)) 
#define MAX_NEIGHBORS        64
#define MAX_BEADS_IN_CELL    128
#define DELTA 1000

enum {XYZ=0, XML=1, DUMP=2, DCD=3};

/*----------------------------------------------------------------------------*/

Clustering::Clustering(double _cutoff, int _periodic_wrapped, int _nNumBeadsInParticle,
                       int _threshold, int _write_clusters, int _bin_size,
                       int _spatial_binning, int _nx, int _ny, int _nz)
{
  cutoff = _cutoff;
  periodic_wrapped = _periodic_wrapped;
  nNumBeadsInParticle = _nNumBeadsInParticle;
  threshold = _threshold;
  write_clusters = _write_clusters;
  bin_size = _bin_size;
  spatial_binning = _spatial_binning;
  nx = _nx;
  ny = _ny;
  nz = _nz;
  grid = NULL;
  neighbors = NULL;
  num_neigh = NULL;
  io_handle = NULL;
  max_neigh = MAX_NEIGHBORS;
  max_beads_in_cell = MAX_BEADS_IN_CELL;
  timestep = 0;
  user_max_size = -1;
  if (spatial_binning) {
    nbins = nx * ny * nz;
    create(grid, nbins);
    memset(grid, 0, nbins*sizeof(int));
  }

  visited = NULL;
  exclusionList = NULL;
  use_template = 0;
}

/*----------------------------------------------------------------------------*/

Clustering::~Clustering()
{
  if (io_handle) delete io_handle;
  destroy(grid);
  destroy(num_neigh);
  destroy(neighbors);
}

/*----------------------------------------------------------------------------
  load the bead list from a file
   only pick the beads of specified types
------------------------------------------------------------------------------*/

int Clustering::load_bead_list(char* fileName, char** types, int ntypes, int nframe)
{
  int success;
  string fullname(fileName);
  std::size_t found = fullname.find_last_of(".");
  base = fullname.substr(0,found);
  ext = fullname.substr(found+1);

  // guess the file type
  if (strcmp(ext.c_str(),"xyz") == 0) {
    if (io_handle == NULL) io_handle = new XYZHandle;
    beadList.clear();
    success = io_handle->read(fileName, types, ntypes, nNumTotalBeads, box, beadList, timestep);
    file_type = XYZ;
  } else if (strcmp(ext.c_str(),"xml") == 0) {
    if (io_handle == NULL) io_handle = new XMLHandle;
    beadList.clear();
    success = io_handle->read(fileName, types, ntypes, nNumTotalBeads, box, beadList, timestep);
    file_type = XML;
  } else if (strcmp(ext.c_str(),"dump") == 0 || strcmp(ext.c_str(),"txt") == 0) {
    if (io_handle == NULL) io_handle = new DUMPHandle;
    beadList.clear();
    success = io_handle->read(fileName, types, ntypes, nNumTotalBeads, box, beadList, timestep);
    file_type = DUMP;
  } else if (strcmp(ext.c_str(),"dcd") == 0) {
    if (io_handle == NULL) io_handle = new DCDHandle;
    char* file = new char [64];
    sprintf(file, "%s-%d", base.c_str(), nframe);
    base = std::string(file);
    timestep = nframe;

    if (use_template) {
      if (beadList.size() == 0) { // first time read in from the DCD file
        success = ((DCDHandle*)io_handle)->read(fileName, indexList, nNumTotalBeads,
          box, beadList, nframe);
        int n = templateBeadList.size();
        for (int i = 0; i < n; i++)
          strcpy(beadList[i].type, templateBeadList[i].type);
      } else {
        BeadList _list;
        success = ((DCDHandle*)io_handle)->read(fileName, indexList, nNumTotalBeads,
            box, _list, nframe);
        assert(nNumTotalBeads == _list.size());
        for (int i = 0; i < nNumTotalBeads; i++) {
          beadList[i].x = _list[i].x;
          beadList[i].y = _list[i].y;
          beadList[i].z = _list[i].z;
        }
      }
        
    } else {
      beadList.clear();
      success = io_handle->read(fileName, nNumTotalBeads, box, beadList, nframe);
    }

    file_type = DCD;
    delete [] file;
  } else {
    std::cout << "Unknown file extension.\n";
    return 0;
  }

  return success;
}

/*----------------------------------------------------------------------------*/

int Clustering::execute()
{
  int success = find_clusters();
  return success;
}

/*----------------------------------------------------------------------------
  Clustering workhorse
  return a vector of clusters, essentially a vector of bead indices in beadList
------------------------------------------------------------------------------*/

int Clustering::find_clusters()
{
  if (beadList.size() == 0) return 0;

  int i, nNumBeads, nNumParticles;
  double cutoff2 = cutoff * cutoff;
  double binsize_Rg = 0.1;
  ofstream ofs, ofs2;

  nNumParticles = nNumTotalBeads / nNumBeadsInParticle;

  // reset the cluster list
  clusterList.clear();

  // Update the new number of beads
  nNumBeads = beadList.size();
  std::cout << "Total number of beads:               "
    << nNumBeads << std::endl;
  std::cout << "Box dimensions, dist units:          "
    << box.Lx << " " << box.Ly << " " << box.Lz << "\n";

  destroy(visited);
  create(visited, nNumBeads);
  for (i = 0; i < nNumBeads; i++) visited[i] = 0;

  // Finding neighbors
  // using the cell-list algorithm
  neighboring(beadList, cutoff2, periodic_wrapped);

  // Loop through all the beads of the specified type
  int nNumBeadsInParticleAfterFiltered = nNumBeads / nNumParticles;
  for (i = 0; i < nNumBeads; i++) {

    vector<int> cluster;
    make_cluster(i, cluster, beadList, nNumBeadsInParticleAfterFiltered);

    if (cluster.size() > 0)
      clusterList.push_back(cluster);
  }

  char fileAs[128], fileList[128];
  sprintf(fileAs, "%s-As.txt", base.c_str());
 
  ofs.open(fileAs);
  ofs << "Cluster   # beads    Rg    As   Rx   Ry   Rz  xcm ycm zcm Major axis (x-, y-, z- comp.)\n";
  ofs.close();

  sprintf(fileList, "%s-plist.txt", base.c_str());
  ofs2.open(fileList);
  ofs2 << "Cluster num_beads xm ym zm bead_IDs\n";
  ofs2.close();
  
  double As, major[3], R2[3], xm[3], Rg;
  int nNumParticlesInCluster;
  int nTotalParticlesinCluster = 0;
  int numClusters = clusterList.size(); // total number of clusters
  vector<double> Rg_list, max_dist_list;

  Cluster clusterAll;
  nclusters_found = 0; // number of clusters bigger than threshold

  for (i = 0; i < numClusters; i++) {

    int numBeads = clusterList[i].size();
    nNumParticlesInCluster =  numBeads / nNumBeadsInParticleAfterFiltered;
    
    for (int j = 0; j < numBeads; j++) {
      beadList[clusterList[i][j]].cluster_size = numBeads;
      beadList[clusterList[i][j]].mol = i;
    }

    // NOTE: threshold is the number of particles, not beads
    // exclude the clusters smaller than threshold
    if (nNumParticlesInCluster < threshold) continue;

    vector<Particle> particleList;
    
    // Ascending sorting is required to ensure beads in a particle sit next to each other
    sort(clusterList[i].begin(), clusterList[i].end());

    As = asphericity_param(clusterList[i], major, R2, Rg, xm);
    Rg_list.push_back(Rg);

    double max_dist = find_max_dist(clusterList[i], xm);
    max_dist_list.push_back(max_dist);

    // Arrange the particles based on the distance from their COM to the bottom left corner of the box
    arrange(particleList, beadList, clusterList[i], nNumBeadsInParticleAfterFiltered);

    nTotalParticlesinCluster += nNumParticlesInCluster;
    nclusters_found++;

    // write the clusters to separate files
    if (write_clusters >= 0) {
      if (i == write_clusters)
        io_handle->write(clusterList[write_clusters], particleList, box, base.c_str(), i, timestep);
    } else {
      if (write_clusters == -1)
        io_handle->write(clusterList[i], particleList, box, base.c_str(), i, timestep);
      if (write_clusters == -2)
        combine_cluster(clusterList[i], clusterAll);
    }

    if (grid && spatial_binning)
      binning(xm, numBeads, grid, nx, ny, nz);

    // store the geometrical paramaters of this cluster to file

    ofs.open(fileAs, ios::app);

    ofs << i << "\t" << clusterList[i].size() << "\t" << Rg << "\t" << As;
    ofs << "\t" << sqrt(R2[0]) << "\t" <<  sqrt(R2[1]) << "\t" << sqrt(R2[2]);
    ofs << "\t" << xm[0] << "\t" <<  xm[1] << "\t" << xm[2];
    ofs << "\t" << major[0] << "\t" << major[1] << "\t" << major[2] << " " << max_dist << std::endl;

    ofs.close();
    
    // store the bead list of this cluster to another file
    
    ofs2.open(fileList, ios::app);
    
    int num_beads = clusterList[i].size();
    ofs2 << i << "\t" << num_beads << "\t" << xm[0] << " " << xm[1] << " " << xm[2] << "\t";
    for (int k = 0; k < num_beads; k++) {
      ofs2 << clusterList[i][k] << " ";
    }
    ofs2 << "\n";
    
    ofs2.close();
  }

  // write the combined cluster into a single file
  if (write_clusters == -2)
    write(clusterAll, beadList, base.c_str(), timestep);

  // build the histogram for the radius of gyration of the clusters
  sprintf(fileList, "%s-Rg-hist.txt", base.c_str());
  histogram(Rg_list, binsize_Rg, fileList);
  std::cout << "Number of clusters with >= " << threshold << " particles: " << nclusters_found << std::endl;

  sprintf(fileList, "%s-max-dist-hist.txt", base.c_str());
  histogram(max_dist_list, binsize_Rg, fileList);

  // build the histogram for the number of beads in the clusters
  Ndistribution(clusterList, bin_size, threshold, user_max_size, nNumBeadsInParticle);
  
  // output the size of the cluster each bead belongs to
  sprintf(fileList, "%s-clsize.txt", base.c_str());
  ofs2.open(fileList);
  ofs2 << "i cluster_size\n";

  for (i = 0; i < nNumBeads; i++) {
    ofs2 << i << " " << beadList[i].cluster_size << "\n";
  }
  ofs2.close();

  destroy(visited);
  
  return 1;
}

/*----------------------------------------------------------------------------*/

void Clustering::combine_cluster(const Cluster& cluster,  Cluster& clusterAll)
{
  int num_beads = cluster.size();
  for (int i = 0; i < num_beads; i++) {
    clusterAll.push_back(cluster[i]);
  }
}

/*----------------------------------------------------------------------------*/

void Clustering::write(const Cluster& cluster, const std::vector<Bead>& beadList,
               const char* fileName, int timestep)
{
  int i, j, nNumParticles, id;
  char* file = new char [256];
  ofstream ofs;

  sprintf(file, "%s-cluster-combined.dump", fileName);
  ofs.open(file);
  ofs << "ITEM: TIMESTEP\n";
  ofs << timestep << "\n";
  ofs << "ITEM: NUMBER OF ATOMS\n";
  ofs << cluster.size() << "\n";
  ofs << "ITEM: BOX BOUNDS pp pp pp\n";
  ofs << box.xlo << " " << box.xhi << "\n";
  ofs << box.ylo << " " << box.yhi << "\n";
  ofs << box.zlo << " " << box.zhi << "\n";
  ofs << "ITEM: ATOMS id mol type q x y z\n";
  id = 1;
  int nNumBeads = cluster.size();
  for (i = 0; i < nNumBeads; i++) {
    int idx = cluster[i];
    ofs << beadList[idx].id << " ";
    ofs << beadList[idx].mol << " ";
    ofs << beadList[idx].type << " ";
    ofs << beadList[idx].q << " ";
    ofs << beadList[idx].x << " ";
    ofs << beadList[idx].y << " ";
    ofs << beadList[idx].z << "\n";
    id++;

  }

  ofs.close();

  delete [] file;
}
/*----------------------------------------------------------------------------*/

void Clustering::create_cell_list(int ncellx, int ncelly, int ncellz,
                      int** neighbor_cells, int* num_neighbor_cells)
{
  int i, j, k, c, cn, x, y, z, ncell2, ncells;
 
  ncell2 = ncellx * ncelly;
  ncells = ncell2 * ncellz;

  for (c = 0; c < ncells; c++)
    num_neighbor_cells[c] = 0;
 
  // loop over all cells 
  for (i = 0; i < ncellz; i++) {
    for (j = 0; j < ncelly; j++) {
      for (k = 0; k < ncellx; k++) {
  
        // identify current cell 
        c = i*ncell2 + j*ncellx + k;
    
        // always interact within cell 
        neighbor_cells[c][num_neighbor_cells[c]] = c;
        num_neighbor_cells[c]++;
    
        // collect near neighbors of cell c 
        x = 1;
        for (y = -1; y <= 1; y++) {
          for (z = -1; z <= 1; z++) {       
            cn = ((i + z + ncellz) % ncellz) * ncell2 +
             ((j + y + ncelly) % ncelly) * ncellx + 
             (k + x + ncellx) % ncellx;
            neighbor_cells[c][num_neighbor_cells[c]] = cn;
            num_neighbor_cells[c]++;
          }
        }

        x = 0;
        y = 1;
        for (z = -1; z <= 1; z++) {       
          cn = ((i + z + ncellz) % ncellz) * ncell2 +
                 ((j + y + ncelly) % ncelly) * ncellx +
                  (k + x + ncellx) % ncellx;
          neighbor_cells[c][num_neighbor_cells[c]] = cn;
          num_neighbor_cells[c]++;
        }
    
        y = 0;   
        z = 1;
        cn = ((i + z + ncellz) % ncellz) * ncell2 +
               ((j + y + ncelly) % ncelly) * ncellx +
                (k + x + ncellx) % ncellx;
        neighbor_cells[c][num_neighbor_cells[c]] = cn;
        num_neighbor_cells[c]++;
      }
    }
  } // end loop over cells
}

/*----------------------------------------------------------------------------
  Build the neighbor list of the beads
  Note: To restrict the clusters to contain the specified types only,
       modify how the beads are added to the neighbor list
------------------------------------------------------------------------------*/

void Clustering::neighboring(std::vector<Bead>& beadList, double cutoff2,
  int periodic_wrapped)
{
  int i, j, ncellx, ncelly, ncellz, ncellxy, ncells, n, cn;
  int HALF_NEIGH=14;
  int nNumBeads, indexi, indexj;
  int** neighbor_cells = NULL;
  int* num_neighbor_cells = NULL;
  int** cells = NULL;
  int* num_beads_in_cell = NULL;
  double xtmp, ytmp, ztmp, dx, dy, dz, r2, cutoff;
  char typei[8], typej[8];

  nNumBeads = beadList.size();

  cutoff = sqrt(cutoff2);

  // Divide the box into cells of which the size is cutoff
  ncellx = ceil(box.Lx / cutoff);
  ncelly = ceil(box.Ly / cutoff);
  ncellz = ceil(box.Lz / cutoff);
  ncellxy = ncellx * ncelly;
  ncells = ncellx * ncelly * ncellz;

  create(cells, ncells, max_beads_in_cell);
  create(num_beads_in_cell, ncells);
  create(neighbor_cells, ncells, HALF_NEIGH);
  create(num_neighbor_cells, ncells);

  memset(num_beads_in_cell, 0, ncells*sizeof(int));

  if (num_neigh == NULL) create(num_neigh, nNumBeads);

  memset(num_neigh, 0, nNumBeads*sizeof(int));

  if (neighbors == NULL) create(neighbors, nNumBeads, max_neigh);

  int nexcluded = 0;
  if (exclusionList)
    nexcluded = exclusionList->size();

  // Create the cell list
  create_cell_list(ncellx, ncelly, ncellz, neighbor_cells, num_neighbor_cells);

  // Bin the beads into cells
  int c;
  double cellSizex, cellSizey, cellSizez, halfboxx, halfboxy, halfboxz;
  cellSizex = box.Lx / ncellx;
  cellSizey = box.Ly / ncelly;
  cellSizez = box.Lz / ncellz;
 
  for (i = 0; i < nNumBeads; i++) {
    xtmp = beadList[i].x;
    ytmp = beadList[i].y;
    ztmp = beadList[i].z;
  
    // apply pbc to put everything in the box
    box.wrap(xtmp, ytmp, ztmp);

    c = (int)((ztmp - box.xlo) / cellSizez) * ncellxy + 
     (int)((ytmp - box.ylo) / cellSizey) * ncellx + 
     (int)((xtmp - box.zlo) / cellSizex);

    if (num_beads_in_cell[c] == max_beads_in_cell) {
      max_beads_in_cell += DELTA;
      grow(cells, ncells, max_beads_in_cell);
    }

    // put the index of the bead into the list of molecules in this cell
    cells[c][num_beads_in_cell[c]] = i;
    num_beads_in_cell[c]++;
  }

  // Neighboring
  // build the neighbor list of each bead of the specified type
  for (c = 0; c < ncells; c++) {

    if (num_beads_in_cell[c] == 0) continue;

    // find neighbors within a cell 
    for (i = 0; i < num_beads_in_cell[c] - 1; i++) {    
      indexi = cells[c][i];

      xtmp = beadList[indexi].x;
      ytmp = beadList[indexi].y;
      ztmp = beadList[indexi].z;

      if (nexcluded > 0) strcpy(typei, beadList[indexi].type);

      for (j = i + 1; j<num_beads_in_cell[c]; j++) {
        indexj = cells[c][j];

        if (nexcluded > 0) {
          if (excluded(typei, beadList[indexj].type)) continue;
        }

        // find the distance between bead i and bead j
        dx  = xtmp - beadList[indexj].x;
        dy  = ytmp - beadList[indexj].y;
        dz  = ztmp - beadList[indexj].z;   
        if (periodic_wrapped == 1) box.minimum_image(dx, dy, dz);

        r2 = dx * dx + dy * dy + dz * dz;
    
        if (r2 <= cutoff2) {
          // add bead indexj into the neighbor list of bead indexi and vice versa
          
          if (num_neigh[indexi] == max_neigh || num_neigh[indexj] == max_neigh) {
            max_neigh += DELTA;
            grow(neighbors, nNumBeads, max_neigh);
          }
          
          neighbors[indexi][num_neigh[indexi]++] = indexj;
          neighbors[indexj][num_neigh[indexj]++] = indexi;
        }    
      }
    }  // end within a cell
   
    // now find neighbors in bordering cells  NBRS = 14;
    for (n = 1; n < num_neighbor_cells[c]; n++) {
      cn = neighbor_cells[c][n];

      // this is similar to that above
      for (i = 0; i < num_beads_in_cell[c]; i++) { 
        indexi = cells[c][i];
        xtmp = beadList[indexi].x;
        ytmp = beadList[indexi].y;
        ztmp = beadList[indexi].z;

        if (nexcluded > 0) strcpy(typei, beadList[indexi].type);

        // here is the loop through each bead in the cell[nbr]
        for (j = 0; j < num_beads_in_cell[cn]; j++) {
          indexj = cells[cn][j];
 
          if (nexcluded > 0) {
            if (excluded(typei, beadList[indexj].type)) continue;
          }

          // find the distance between bead i and bead j
          dx  = xtmp - beadList[indexj].x;
          dy  = ytmp - beadList[indexj].y;
          dz  = ztmp - beadList[indexj].z;
          if (periodic_wrapped == 1) box.minimum_image(dx, dy, dz);
     
          r2 = dx * dx + dy * dy + dz * dz;
     
          if (r2 <= cutoff2) {
            // add bead indexj into the neighbor list of bead indexi and vice versa
            
            if (num_neigh[indexi] == max_neigh || num_neigh[indexj] == max_neigh) {
              max_neigh += DELTA;
              grow(neighbors, nNumBeads, max_neigh);
            }
             
            neighbors[indexi][num_neigh[indexi]++] = indexj;
            neighbors[indexj][num_neigh[indexj]++] = indexi;
          }
        }
      }
    }   // end loops over neighbor cells 
  }  // end loop over cells 


  // deallocate
  destroy(cells);
  destroy(num_beads_in_cell);
  destroy(neighbor_cells);
  destroy(num_neighbor_cells);
}

/*----------------------------------------------------------------------------
  recursively find clusters for bead i by iterating through i's neighbor list
------------------------------------------------------------------------------*/

void Clustering::make_cluster(int i, Cluster& cluster, 
                              std::vector<Bead>& beadList, int nNumBeadsInParticle)
{
  int j, start, end;
  if (visited[i] == 0) {
    start = i - i % nNumBeadsInParticle; 
    end = start + nNumBeadsInParticle;
    for (j = start; j < end; j++) {
      visited[j] = 1;
      cluster.push_back(j);
    }
    int nNeighbors = num_neigh[i];
    for (j = 0; j < nNeighbors; j++)
      make_cluster(neighbors[i][j], cluster, beadList, nNumBeadsInParticle);

  }
}

/*----------------------------------------------------------------------------
  return 1 if the two beads are excluded from each other's neighbor list
  return 0 otherwise
------------------------------------------------------------------------------*/

int Clustering::excluded(const char* type1, const char* type2)
{
  int nexcluded = exclusionList->size();
  int match = 0;
  for (int i = 0; i < nexcluded; i++) {
    if ((strcmp(type1, (*exclusionList)[i].type1) == 0 &&
         strcmp(type2, (*exclusionList)[i].type2) == 0) ||
        (strcmp(type1, (*exclusionList)[i].type2) == 0 &&
         strcmp(type2, (*exclusionList)[i].type1) == 0) ) {
      match = 1;
      break;
    }
  }
  return match;
}

/*----------------------------------------------------------------------------
  each particle (body) is composed of nNumBeadsPerParticle beads
  Since each cluster holds the list of bead indices,
  which are not in any order with respect to the particles,
  we need to first re-arrange (group) the beads into particles (for visualization purposes).
  Then the particles are arranged based on their distance to some reference point,
  e.g. the bottom corner of the box or the cluster center of mass,
  so that a loop through the particle list has some spatial order
------------------------------------------------------------------------------*/

void Clustering::arrange(std::vector<Particle>& particleList,
  BeadList& beadList, Cluster& cluster, int nNumBeadsPerParticle)
{
  int i, j, nNumBeads, nNumParticles;
 
  // group beads from the cluster into particles

  nNumBeads = cluster.size();
  nNumParticles = nNumBeads / nNumBeadsPerParticle;
  for (i = 0; i < nNumParticles; i++) {
    Particle particle;
    for (j = 0; j < nNumBeadsPerParticle; j++)
      particle.beadList.push_back(beadList[cluster[i * nNumBeadsPerParticle + j]]); 

    particleList.push_back(particle);
  }

  // re-arrange the particles based on their distance to some reference point (optional)

  double px, py, pz;
  px = box.xlo;
  py = box.ylo;
  pz = box.zlo;
 
  for (i = 0; i < nNumParticles; i++) {
    particleList[i].find_center_of_mass();
    particleList[i].Dist2(px, py, pz);
  }

  sort(particleList.begin(), particleList.end()); 
}

/*----------------------------------------------------------------------------
  cluster size distribution in terms of number of particles
------------------------------------------------------------------------------*/

void Clustering::Ndistribution(std::vector<Cluster>& clusterList,
        int bin_size, int threshold, double user_max_size, int nNumBeadsInParticle)
{
  int i, nbins, min_size, max_size, nclusters, cluster_size, cluster_max;
  int ibin, nNumParticles;
  double* bin = 0x0;
  double* bin_nbeads = 0x0;

  max_size = 0;
  min_size = bin_size;
  cluster_max = -1;
  nclusters = clusterList.size();
  for (i = 0; i < nclusters; i++) {
    cluster_size = clusterList[i].size();
    nNumParticles = cluster_size / nNumBeadsInParticle;
    if (max_size < nNumParticles) {
      max_size = nNumParticles;
      cluster_max = i;
    }
    if (min_size > nNumParticles)
      min_size = nNumParticles;
  }

  // maximum number of particles in a cluster
  double max_found = max_size;

  // find the max value for the histogram
  if (user_max_size < 0) max_size = (int)(1.5 * max_size);
  else max_size = user_max_size;

//  if (bin_size > min_size) bin_size = min_size;

  nbins = (int)(max_size / bin_size);
  bin = new double [nbins+1];
  bin_nbeads = new double [nbins+1];
 
  for (i = 0; i < nbins+1; i++) {
    bin[i] = 0.0;
    bin_nbeads[i] = 0.0;
  }

  // ncount = number of clusters whose the number of consitutent beads is bigger than threshold
  int ncount = 0;
  for (i = 0; i < nclusters; i++) {
    cluster_size = clusterList[i].size(); // number of beads
    nNumParticles = cluster_size / nNumBeadsInParticle;

    if (nNumParticles >= threshold && nNumParticles <= max_size) {     
      ibin = (int)((double)nNumParticles / bin_size);
      bin[ibin] += 1.0;
      bin_nbeads[ibin] += nNumParticles;
      ncount++;
    }
  }

  // weighted average: s = p(s)*s/sum(p(s)) 
  ofstream ofs;
  char fileName[128];

  sprintf(fileName, "%s-N-dist.txt", base.c_str());
  ofs.open(fileName);
  double ave_size = 0;
  double sum_p = 0;
  for (i = 0; i < nbins+1; i++) {
    if (ncount > 0) bin[i] /= ncount;
    else bin[i] = 0;
    ave_size += bin[i] * (i * bin_size);
    sum_p += bin[i];
    ofs << i * bin_size << "\t" << bin[i] << std::endl;
  }
  ofs.close();

  // bin_nbeads[i] = number of beads that belong to clusters of size i

  sprintf(fileName, "%s-prob-N.txt", base.c_str());
  ofs.open(fileName);
  for (i = 0; i < nbins+1; i++) {
    ofs << i * bin_size << "\t" << bin_nbeads[i] << std::endl;
  }
  ofs.close();


  if (nclusters > 0) {
    if (sum_p > 0) ave_size /= sum_p;
    else ave_size = 0;
    printf("Expected cluster size, beads:        %g\n", ave_size);
    printf("Maximum cluster size, beads:         %g\n", max_found);
  } 

  delete [] bin;
  delete [] bin_nbeads;
}

/*----------------------------------------------------------------------------
  generate a generic histogram for a double array
------------------------------------------------------------------------------*/

void Clustering::histogram(vector<double>& array, double bin_size, char* filename)
{
  int i, nbins, ibin, size;
  double min_val, max_val;
  double* bin = 0x0;

  size = array.size();
  if (size == 0) return;

  min_val = max_val = array[0];
  for (i = 0; i < size; i++) {
    if (max_val < array[i]) max_val = array[i];
    if (min_val > array[i]) min_val = array[i];
  }

  nbins = (int)((max_val - min_val) / bin_size);
  bin = new double [nbins+1];
 
  for (i = 0; i < nbins+1; i++) bin[i] = 0.0;

  int ncount = 0;
  for (i = 0; i < size; i++) {
    ibin = (int)((array[i] - min_val) / bin_size);
    bin[ibin] += 1.0;
    ncount++;
  }

  ofstream ofs(filename);

  // weighted mean and variance 
  double w = 0;
  double ave = 0;
  for (i = 0; i < nbins+1; i++) {
    if (ncount > 0) bin[i] /= ncount;
    else bin[i] = 0;
    ofs << min_val + i * bin_size << "\t" << bin[i] << std::endl;
    ave += bin[i] * (min_val + i * bin_size);
  }
  double var = 0;
  for (i = 0; i < nbins+1; i++) {
    var += bin[i] * (min_val + i * bin_size - ave) * (min_val + i * bin_size - ave);
  }
  var = sqrt(var);
//  printf("Radius of gyration, dist units: %0.2f +/- %0.2f\n", ave, var);
  delete [] bin;
}

/*----------------------------------------------------------------------------
  Gyration tensor and Asphericity parameter helper functions
------------------------------------------------------------------------------*/

void Clustering::rotate(double **matrix, int i, int j, int k, int l, double s, double tau)
{
  double g = matrix[i][j];
  double h = matrix[k][l];
  matrix[i][j] = g - s * (h + g * tau);
  matrix[k][l] = h + s * (g - h * tau);
} 

/*----------------------------------------------------------------------------
  diagonalize a matrix
  return the eigenvalues in evalues and eigenvectors in evectors
------------------------------------------------------------------------------*/

#define MAXJACOBI 50
int Clustering::diagonalize(double **matrix, double *evalues, double **evectors)
{
  int i, j, k, iter;
  double tresh, theta, tau, t, sm, s, h, g, c, b[3], z[3];
 
  for (i = 0; i < 3; i++) {
    for (j = 0; j < 3; j++) 
      evectors[i][j] = 0.0;
    evectors[i][i] = 1.0;
  }
 
  for (i = 0; i < 3; i++) {
    b[i] = evalues[i] = matrix[i][i];
    z[i] = 0.0;
  }
 
  for (iter = 1; iter <= MAXJACOBI; iter++) {
    sm = 0.0;
    for (i = 0; i < 2; i++)
      for (j=i+1; j<3; j++)
        sm += fabs(matrix[i][j]);
    if (sm == 0.0) 
      return 0;
   
    if (iter < 4) 
      tresh = 0.2 * sm / (3 * 3);
    else 
      tresh = 0.0;
   
    for (i = 0; i < 2; i++) {
      for (j = i+1; j < 3; j++) {
        g = 100.0*fabs(matrix[i][j]);
        if (iter > 4 && fabs(evalues[i])+g == fabs(evalues[i])
           && fabs(evalues[j])+g == fabs(evalues[j]))
           matrix[i][j] = 0.0;
        else if (fabs(matrix[i][j]) > tresh) {
          h = evalues[j] - evalues[i];
          if (fabs(h)+g == fabs(h)) 
            t = (matrix[i][j]) / h;
          else {
            theta = 0.5 * h / (matrix[i][j]);
            t = 1.0 / (fabs(theta) + sqrt(1.0 + theta * theta));
            if (theta < 0.0) 
              t = -t;
          }

          c = 1.0 / sqrt(1.0 + t * t);
          s = t * c;
          tau = s / (1.0 + c);
          h = t * matrix[i][j];
          z[i] -= h;
          z[j] += h;
          evalues[i] -= h;
          evalues[j] += h;
          matrix[i][j] = 0.0;

          for (k = 0; k < i; k++)
            rotate(matrix, k, i, k, j, s, tau);
          for (k = i+1; k < j; k++) 
            rotate(matrix, i, k, k, j, s, tau);
          for (k = j+1; k < 3; k++) 
            rotate(matrix, i, k, j, k, s, tau);
          for (k = 0; k < 3; k++) 
            rotate(evectors, k, i, k, j, s, tau);
        }
      }
    }
   
    for (i = 0; i < 3; i++) {
      evalues[i] = b[i] += z[i];
      z[i] = 0.0;
    }
  }

  return 1;
}

/*----------------------------------------------------------------------------*/

void Clustering::center_of_mass(BeadList& beadList, double* xm, int period_wrapped)
{
  int i, nNumBeads;
  double dx, dy, dz, Lx2, Ly2, Lz2;
  Bead pivot;

  nNumBeads = beadList.size();

  if (period_wrapped == 1) {
    Lx2 = 0.5*box.Lx;
    Ly2 = 0.5*box.Ly;
    Lz2 = 0.5*box.Lz;

    pivot.x = beadList[0].x;
    pivot.y = beadList[0].y;
    pivot.z = beadList[0].z;

    // Release periodic boundary conditions for all beads in cluster
    for (i = 1; i < nNumBeads; i++) {
      dx = beadList[i].x - pivot.x;
      dy = beadList[i].y - pivot.y;
      dz = beadList[i].z - pivot.z;

      if (dx > Lx2) beadList[i].x -= box.Lx;
      else if (dx < -Lx2) beadList[i].x += box.Lx;
      if (dy > Ly2) beadList[i].y -= box.Ly;
      else if (dy < -Ly2) beadList[i].y += box.Ly;
      if (dz > Lz2) beadList[i].z -= box.Lz;
      else if (dz < -Lz2) beadList[i].z += box.Lz;
    }
  }

  // Compute the center of mass
  xm[0] = xm[1] = xm[2] = 0.0;
  for (i = 0; i < nNumBeads; i++) {
    xm[0] += beadList[i].x;
    xm[1] += beadList[i].y;
    xm[2] += beadList[i].z;
  }

  xm[0] /= nNumBeads;
  xm[1] /= nNumBeads;
  xm[2] /= nNumBeads;
}

/*----------------------------------------------------------------------------*/

void Clustering::gyration_tensor(BeadList& beadList, double* xm, int periodic_wrapped,
  double** gyration, double* R2, double* major_eigenvector)
{
  int i, j, k, nNumBeads;
  nNumBeads = beadList.size();

  for (j = 0; j < 3; j++)
    for (k = 0; k < 3; k++)
      gyration[j][k] = 0.0;
 
  double xi[3], L[3];
  L[0] = box.Lx; L[1] = box.Ly; L[2] = box.Lz;
  for (i = 0; i < nNumBeads; i++) {
    xi[0] = beadList[i].x;
    xi[1] = beadList[i].y;
    xi[2] = beadList[i].z;
    for (j = 0; j < 3; j++) {
      double rj = xi[j] - xm[j];
      if (rj > L[j]/2)
        rj -= L[j];
      else if (rj < -L[j]/2)
        rj += L[j];
       
      for (k = 0; k < 3; k++) {
        double rk = xi[k] - xm[k];
        if (rk > L[k]/2) rk -= L[k];
        else if (rk < -L[k]/2) rk += L[k];
     
        gyration[j][k] += rj * rk; //(xi[j] - xm[j]) * (xi[k] - xm[k]);
      }
    }
  }

  for (j = 0; j < 3; j++)
    for (k = 0; k < 3; k++)
      gyration[j][k] /= nNumBeads;

  // Dummy eigen_vectors
  double** eigen_vectors = new double* [3];
  for (i = 0; i < 3; i++)
    eigen_vectors[i] = new double [3];

  // Diagonalize the gyration tensor, yielding the eigenvalues as squares of the radii of gyration
  diagonalize(gyration, R2, eigen_vectors);

  // find the maximum eignvalue
  double max = R2[0];
  int index = 0;
  for (i = 1; i < 3; i++) {
    if (max < R2[i]) {
      max = R2[i];
      index = i;
    }
  }

  major_eigenvector[0] = eigen_vectors[0][index];
  major_eigenvector[1] = eigen_vectors[1][index];
  major_eigenvector[2] = eigen_vectors[2][index];

  for (i = 0; i < 3; i++)
    delete [] eigen_vectors[i];
  delete [] eigen_vectors;
}

/*----------------------------------------------------------------------------
  Compute the asphericity parameter of a cluster
  return the asphericity paramter (As), radius of gyration (Rg), center of mass (xm)
------------------------------------------------------------------------------*/

double Clustering::asphericity_param(Cluster& cluster, double* major_eigenvector,
                   double* R2, double& Rg, double* xm)
{
  int i;
  double As;
  double **gyration;

  gyration = new double* [3];
  for (i = 0; i < 3; i++)
    gyration[i] = new double [3];
 
  BeadList beadsInCluster;
  for (i = 0; i < cluster.size(); i++)
    beadsInCluster.push_back(beadList[cluster[i]]);

  // Find the center of mass
  center_of_mass(beadsInCluster, xm, periodic_wrapped);

  // Compute the gyration tensor
  gyration_tensor(beadsInCluster, xm, periodic_wrapped,
    gyration, R2, major_eigenvector);

  As = ((R2[2] - R2[1]) * (R2[2] - R2[1]) +
        (R2[1] - R2[0]) * (R2[1] - R2[0]) +
        (R2[2] - R2[0]) * (R2[2] - R2[0])) /
        (2 * (R2[0] + R2[1] + R2[2]) * (R2[0] + R2[1] + R2[2]));

  Rg = sqrt(R2[0] + R2[1] + R2[2]);

  for (i = 0; i < 3; i++)
    delete [] gyration[i];
  delete [] gyration;

  return As;
}

/*----------------------------------------------------------------------------
  Compute the maximum distance of the particles from the cluster center of mass (xm) 
------------------------------------------------------------------------------*/

double Clustering::find_max_dist(Cluster& cluster, double* xm)
{
  int i, nNumBeads;
  double max_dist;
  double dx, dy, dz, Lx2, Ly2, Lz2;

  BeadList beadsInCluster;
  for (i = 0; i < cluster.size(); i++)
    beadsInCluster.push_back(beadList[cluster[i]]);

  nNumBeads = beadsInCluster.size();

  Lx2 = box.Lx / 2.0;
  Ly2 = box.Ly / 2.0;
  Lz2 = box.Lz / 2.0;

  // Release periodic boundary conditions for all beads in cluster
  max_dist = 0;
  for (i = 0; i < nNumBeads; i++) {
    dx = beadList[i].x - xm[0];
    dy = beadList[i].y - xm[1];
    dz = beadList[i].z - xm[2];

    if (dx > Lx2) dx -= box.Lx;
    else if (dx < -Lx2) dx += box.Lx;
    if (dy > Ly2) dy -= box.Ly;
    else if (dy < -Ly2) dy += box.Ly;
    if (dz > Lz2) dz -= box.Lz;
    else if (dz < -Lz2) dz += box.Lz;

    double r = sqrt(dx*dx + dy*dy + dz*dz);
    if (r > max_dist) max_dist = r;
  }
  //printf("  numBeads = %d: max dist = %g\n", nNumBeads, max_dist);
  return max_dist;
}


/*----------------------------------------------------------------------------
  binning the clusters into the cells
------------------------------------------------------------------------------*/

int Clustering::binning(double* xm, int numBeads, int* grid, int nx, int ny, int nz)
{
  int nxy = nx * ny;
  double gridSizex, gridSizey, gridSizez;
  gridSizex = box.Lx / nx;
  gridSizey = box.Ly / ny;
  gridSizez = box.Lz / nz;

  int c = (int)((xm[2] - box.zlo) / gridSizez) * nxy + 
          (int)((xm[1] - box.ylo) / gridSizey) * nx + 
          (int)((xm[0] - box.xlo) / gridSizex);

  grid[c] += numBeads;
  return c;
}

/*----------------------------------------------------------------------------
  replicate
------------------------------------------------------------------------------*/

void Clustering::replicate_system(int nx, int ny, int nz)
{
  int ix, iy, iz;
  int m = 0;
  int num_beads = beadList.size();
  int num_mol = beadList[num_beads-1].mol;
  int mol_shift = num_mol;

  int idx = num_beads + 1;
  for (ix = 0; ix < nx; ix++) {
    for (iy = 0; iy < ny; iy++) {
      for (iz = 0; iz < nz; iz++) {

        if (ix == 0 && iy == 0 && iz == 0) continue;

        for (int i = 0; i < num_beads; i++) {
          Bead bead = beadList[i];
          bead.id = idx++;
          bead.mol += mol_shift;
          bead.x = bead.x + ix*box.Lx;
          bead.y = bead.y + iy*box.Ly;
          bead.z = bead.z + iz*box.Lz;
          beadList.push_back(bead);
        }

        mol_shift += num_mol;
      }
    }
  }  

  box.Lx *= nx;
  box.Ly *= ny;
  box.Lz *= nz;
  box.xhi = box.xlo + box.Lx;
  box.yhi = box.ylo + box.Ly;
  box.zhi = box.zlo + box.Lz;
  nNumTotalBeads = beadList.size();

  int debugging = 0;
  if (debugging) {
    char* file = new char [256];
    ofstream ofs;

    sprintf(file, "replicated-%d.dump", timestep);
    ofs.open(file);
    ofs << "ITEM: TIMESTEP\n";
    ofs << timestep << "\n";
    ofs << "ITEM: NUMBER OF ATOMS\n";
    ofs << beadList.size() << "\n";
    ofs << "ITEM: BOX BOUNDS pp pp pp\n";
    ofs << box.xlo << " " << box.xhi << "\n";
    ofs << box.ylo << " " << box.yhi << "\n";
    ofs << box.zlo << " " << box.zhi << "\n";
    ofs << "ITEM: ATOMS id mol type q x y z\n";
    int nNumBeads = beadList.size();
    for (int i = 0; i < nNumBeads; i++) {
      ofs << beadList[i].id << " ";
      ofs << beadList[i].mol << " ";
      ofs << beadList[i].type << " ";
      ofs << beadList[i].q << " ";
      ofs << beadList[i].x << " ";
      ofs << beadList[i].y << " ";
      ofs << beadList[i].z << "\n";
    }
    ofs.close();
    delete [] file;
  }
}

/*----------------------------------------------------------------------------
  load the bead list from a file
  only store the indices of beads of specified types into a template
------------------------------------------------------------------------------*/

int Clustering::load_template(char* fileName, char** types, int ntypes)
{
  int success;
  string fullname(fileName);
  std::size_t found = fullname.find_last_of(".");
  base = fullname.substr(0,found);
  ext = fullname.substr(found+1);

  // temporary variables used for loading the template

  IOHandle* io_tmp = NULL;
  int nNumBeads;

  // guess the file type

  if (strcmp(ext.c_str(),"xyz") == 0) {
    if (io_tmp == NULL) io_tmp = new XYZHandle;
    success = io_tmp->read(fileName, types, ntypes, nNumBeads, box, templateBeadList, timestep);
  } else if (strcmp(ext.c_str(),"xml") == 0) {
    if (io_tmp == NULL) io_tmp = new XMLHandle;
    success = io_tmp->read(fileName, types, ntypes, nNumBeads, box, templateBeadList, timestep);
  } else if (strcmp(ext.c_str(),"dump") == 0 || strcmp(ext.c_str(),"txt") == 0) {
    if (io_tmp == NULL) io_tmp = new DUMPHandle;
    success = io_tmp->read(fileName, types, ntypes, nNumBeads, box, templateBeadList, timestep);
  } else {
    std::cout << "Unknown file extension.\n";
    return 0;
  }

  delete io_tmp;

  int n = templateBeadList.size();
  indexList.clear();
  for (int i = 0; i < n; i++)
    indexList.push_back(templateBeadList[i].id);

  use_template = 1;
  return success;
}
