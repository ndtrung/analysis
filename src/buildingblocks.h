/*
  Copyright (C) 2017  Trung D. Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __BUILDING_BLOCKS
#define __BUILDING_BLOCKS

#include <stdio.h>
#include <string.h>
#include <vector>
using namespace std;

namespace Analysis {

class Bead
{
public:
  Bead()
  {
    id = 0;
    mol = 0;
    ix = iy = iz = 0;
    sprintf(type, "1");
  }

  Bead(const Bead& b)
  {
    id = b.id;
    mol = b.mol;
    strcpy(type, b.type);
    q = b.q; 
    x = b.x;
    y = b.y;
    z = b.z;
    ix = b.ix;
    iy = b.iy;
    iz = b.iz;
    vx = b.vx;
    vy = b.vy;
    vz = b.vz;
    cluster_size = b.cluster_size;
  }

  Bead& operator = (const Bead& b)
  {
    if (this == &b) return *this;
    id = b.id;
    mol = b.mol;
    strcpy(type, b.type);
    q = b.q; 
    x = b.x;
    y = b.y;
    z = b.z;
    ix = b.ix;
    iy = b.iy;
    iz = b.iz;
    vx = b.vx;
    vy = b.vy;
    vz = b.vz;
    cluster_size = b.cluster_size;
    return *this;
  }

  ~Bead() {}

public:
  // 32 bytes
  int id;
  int mol;
  char type[8];
  double q;
  double x;
  double y;
  double z;
  int ix, iy, iz;
  double vx;
  double vy;
  double vz;
  int cluster_size;
};

typedef vector<Bead> BeadList;
typedef vector<int> Cluster;

class Particle
{
  friend bool operator< (const Particle& p1, const Particle& p2);
public:
  Particle() {}
  Particle(const Particle& p);
  Particle& operator= (const Particle& p);

  void Dist2(double px, double py, double pz);
  void find_center_of_mass();

  BeadList beadList;
  double xm;
  double ym;
  double zm;
  double dist2;
};

struct Box {
  double Lx, Ly, Lz;
  double xlo, xhi, ylo, yhi, zlo, zhi;

  void wrap(double& x, double& y, double& z) {
    if (x < xlo) x += Lx;
    else if (x > xhi) x -= Lx;
    if (y < ylo) y += Ly;
    else if (y > yhi) y -= Ly;
    if (z < zlo) z += Lz;
    else if (z > zhi) z -= Lz;
  }

  void minimum_image(double& delx, double& dely, double& delz) {
    if (delx < -0.5*Lx) delx += Lx;
    else if (delx > 0.5*Lx) delx -= Lx;
    if (dely < -0.5*Ly) dely += Ly;
    else if (dely > 0.5*Ly) dely -= Ly;
    if (delz < -0.5*Lz) delz += Lz;
    else if (delz > 0.5*Lz) delz -= Lz;
  }
};

} // namespace Analysis

#endif
