/*
  Polymer Rouse mode and normal mode relaxation analysis

  Compile:     make -f Makefile.cpu
  Clean-build: make -f Makefile.cpu clean

  Use:   ./analyze config_file types M T1 .. TM [frames <args>]
    
  Input:   config_file = 
              for LAMMPS dump files, atom data contains at least 5 columns: id type x y z

           types 
              M     = number of bead types in the polymer beads
              T1 .. TM = list of M bead types, as space-separated strings

           nbeadsperchain
              <args> = polymerization degree of individual chains (assuming to be the same for all the chains)
             
           frames
              <args> = start end interval
                 start    = start time
                 end      = end time
                 interval = inverval between consecutive frames (snapshots)

  Examples: ./analyze dump..txt types 2 1 2 nbeadsperchain 10 frames 0 20000 100

           to compute the normal modes of the polymer chains composed of beads of two types 1 and 2
           from a trajectory containing files dump.0.txt, dump.100.txt,.., dump.20000.txt

  Output:  rouse_*.txt where * is the mode index. There are three columns in each file:
           the first are the lags, the second the average correlation values, the third the standard deviations.

  IMPORTANT NOTES:

  Contact: Trung Nguyen, trung.nguyen@northwestern.edu

*/

#include <stdlib.h>
#include <cmath>
#include <fstream>
#include <iostream>
#include "normal_modes.h"

using namespace Analysis;
using namespace std;

int main(int argc, char** argv)
{
  int periodic_wrapped = 1;
  int numBeadsPerChain = 10;
  int trajectory = 0;
  int start, end, interval;
  char* fileName = new char [1024];
  int ntypes = 0;
  char** types = NULL;
  double fmin = 0.0;
  double fmax = 1.0;

  // parse the arguments
  
  int iarg = 2;
  while (iarg < argc) {
    if (strcmp(argv[iarg],"types") == 0) {
      ntypes = atoi(argv[iarg+1]);
      if (ntypes <= 0) {
        std::cout << "Invalid number of types\n";
        return 1;
      }
      iarg+=2;
      types = new char* [ntypes];
      for (int m = 0; m < ntypes; m++) {
        types[m] = new char [64];
        strcpy(types[m], argv[iarg++]);
      }
    } else if (strcmp(argv[iarg],"nbeadsperchain") == 0) {
      numBeadsPerChain = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"fraction") == 0) {
      fmin = atof(argv[iarg+1]);
      fmax = atof(argv[iarg+2]);
      iarg += 3;
    } else if (strcmp(argv[iarg],"frames") == 0) {
      start = atoi(argv[iarg+1]);
      end = atoi(argv[iarg+2]);
      interval = atoi(argv[iarg+3]);
      trajectory = 1;
      iarg += 4;
    } else {
      std::cout << "Invalid argument: " << argv[iarg] << "\n";
      return 1;
    }
  }

  // error checks
  
  if (ntypes < 1) {
    cout << "Needs to specify at least a bead type\n";
    return 1;
  }

  sprintf(fileName, "%s", argv[1]);

  string fullname(fileName);
  std::size_t found = fullname.find_last_of(".");
  string base = fullname.substr(0,found);
  string ext = fullname.substr(found+1);

  char* filetime = new char [1024];
  int num_configs = (end - start)/interval + 1;

  int iframe = 0;
  int nframes = (end - start)/interval + 1;
  NormalModes n(nframes, numBeadsPerChain, periodic_wrapped);
  n.set_fraction(fmin, fmax);

  for (int t = start; t <= end; t += interval) {

    if (strcmp(ext.c_str(),"dcd") == 0) { // read from a DCD file
      n.load_bead_list(fileName, types, ntypes, t);

    } if (strcmp(ext.c_str(),"gsd") == 0) { // read from a GSD file
      n.load_bead_list(fileName, types, ntypes, t);

    } else { // read from an ASCII file (LAMMPS dump files for now
      sprintf(filetime, "%s%d.%s", base.c_str(), t, ext.c_str());
      n.load_bead_list(filetime, types, ntypes);
    }
  
    iframe++;
  }

  //n.analyze();
  n.analyze_rouse();

  delete [] fileName;
  if (types) {
    for (int m = 0; m < ntypes; m++)
      delete [] types[m];
    delete [] types;
  }
}


