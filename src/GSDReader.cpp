// Copyright (c) 2009-2019 The Regents of the University of Michigan
// This file is part of the HOOMD-blue project, released under the BSD 3-Clause License.
// Adopted from GSDReader in HOOMD-Blue by Joshua Anderson (joaander)

/*! \file GSDReader.cc
    \brief Defines the GSDReader class
*/

#include <iostream>
#include <fstream>
#include <stdexcept>
#include <string.h>
#include "gsd.h"
#include "GSDReader.h"
#include "memory.h"

using namespace Analysis;
using namespace std;

/*! \param name File name to read
    \param frame Frame index to read from the file
    \param from_end Count frames back from the end of the file

    The GSDReader constructor opens the GSD file, initializes an empty snapshot, and reads the file into
    memory (on the root rank).
*/
GSDReader::GSDReader(const std::string &name) : m_timestep(0), m_name(name), m_frame(0)
{
  // open the GSD file in read mode
  std::cerr << "gsd: open gsd file " << name << endl;
  int retval = gsd_open(&m_handle, name.c_str(), GSD_OPEN_READONLY);
  if (retval == -1) {
    std::cerr << "gsd: " << strerror(errno) << " - " << name << endl;
  } else if (retval == -2) {
    std::cerr << "gsd: " << name << " is not a valid GSD file" << endl;
  } else if (retval == -3) {
    std::cerr << "gsd: " << "Invalid GSD file version in " << name << endl;
  } else if (retval == -4) {
    std::cerr << "gsd: " << "Corrupt GSD file: " << name << endl;
  } else if (retval == -5) {
    std::cerr << "gsd: " << "Out of memory opening: " << name << endl;
  } else if (retval != 0) {
    std::cerr << "gsd: " << "Unknown error opening: " << name << endl;
  }

  // validate schema
  if (string(m_handle.header.schema) != string("hoomd")) {
    std::cerr << "gsd: " << "Invalid schema in " << name << endl;
  }

  if (m_handle.header.schema_version >= gsd_make_version(2,0)) {
    std::cerr<< "gsd: " << "Invalid schema version in " << name << endl;
  }

  // set frame from the end of the file if requested
  m_nframes = gsd_get_nframes(&m_handle);
  
  std::cerr << "gsd: Total number of frames is " << m_nframes << std::endl;

  m_nparticles = 0;
  type = NULL;
  mass = NULL;
  charge = NULL;
  diameter = NULL;
  body = NULL;
  inertia = NULL;
  pos = NULL;
  orientation = NULL;
  vel = NULL;
  angmom = NULL;
  image = NULL;

  // read header to allocate memory for the arrays
  readHeader();
}

GSDReader::~GSDReader()
{
  destroy(type);
  destroy(mass);
  destroy(charge);
  destroy(diameter);
  destroy(body);
  destroy(inertia);
  destroy(pos);
  destroy(orientation);
  destroy(vel);
  destroy(angmom);
  destroy(image);

  gsd_close(&m_handle);
}

/*!
    \param frame Frame index to read from the file
    \param from_end Count frames back from the end of the file
*/
int GSDReader::read_frame(const uint64_t frame, bool from_end)
{
  m_frame = frame;
  if (from_end && frame <= m_nframes)
    m_frame = m_nframes - frame;

  // validate number of frames
  if (m_frame >= m_nframes) {
    std::cerr << "gsd: " << "Cannot read frame " << m_frame << " " << m_name << " only has " << gsd_get_nframes(&m_handle) << " frames" << endl;
  }
  
  readParticles();
  return 0;
}

/*! \param data Pointer to data to read into
    \param frame Frame index to read from
    \param name Name of the data chunk
    \param expected_size Expected size of the data chunk in bytes.
    \param cur_n N in the current frame.

    Attempts to read the data chunk of the given name at the given frame. If it is not present at this
    frame, attempt to read from frame 0. If it is also not present at frame 0, return false.
    If the found data chunk is not the expected size, throw an exception.

    Per the GSD spec, keep the default when the frame 0 N does not match the current N.

    Return true if data is actually read from the file.
*/
bool GSDReader::readChunk(void *data, uint64_t frame, const char *name, size_t expected_size, unsigned int cur_n)
{
  const struct gsd_index_entry* entry = gsd_find_chunk(&m_handle, frame, name);
  if (entry == NULL && frame != 0)
    entry = gsd_find_chunk(&m_handle, 0, name);

  if (entry == NULL || (cur_n != 0 && entry->N != cur_n)) {
    std::cerr << "gsd: chunk not found " << name << endl;
    return false;
  } else {
    std::cerr << "gsd: reading chunk " << name << endl;
    size_t actual_size = entry->N * entry->M * gsd_sizeof_type((enum gsd_type)entry->type);
    if (actual_size != expected_size) {
      std::cerr << "gsd: " << "Expecting " << expected_size << " bytes in " << name << " but found " << actual_size << endl;
    }
    int retval = gsd_read_chunk(&m_handle, data, entry);

    if (retval == -1) {
      std::cerr << "gsd: " << strerror(errno) << " - " << m_name << endl;
    } else if (retval == -2) {
      std::cerr << "gsd: " << "Unknown error reading: " << m_name << endl;
    } else if (retval == -3) {
      std::cerr << "gsd: " << "Invalid GSD file " << m_name << endl;
    } else if (retval != 0) {
      std::cerr << "gsd: " << "Unknown error reading: " << m_name << endl;
    }

    return true;
  }
}

/*! \param frame Frame index to read from
    \param name Name of the data chunk

    Attempts to read the data chunk of the given name at the given frame. If it is not present at this
    frame, attempt to read from frame 0. If it is also not present at frame 0, return an empty list.

    If the data chunk is found in the file, return a vector of string type names.
*/
std::vector<std::string> GSDReader::readTypes(uint64_t frame, const char *name)
{
  std::cerr << "gsd: reading chunk " << name << endl;

  std::vector<std::string> type_mapping;

  // set the default particle type mapping per the GSD HOOMD Schema
  if (std::string(name) == "particles/types")
     type_mapping.push_back("A");

  const struct gsd_index_entry* entry = gsd_find_chunk(&m_handle, frame, name);
  if (entry == NULL && frame != 0)
    entry = gsd_find_chunk(&m_handle, 0, name);

  if (entry == NULL)
    return type_mapping;
  else {
    size_t actual_size = entry->N * entry->M * gsd_sizeof_type((enum gsd_type)entry->type);
    std::vector<char> data(actual_size);
    int retval = gsd_read_chunk(&m_handle, &data[0], entry);

    if (retval == -1) {
      std::cerr << "gsd: " << strerror(errno) << " - " << m_name << endl;
    } else if (retval == -2) {
      std::cerr << "gsd: " << "Unknown error reading: " << m_name << endl;
    } else if (retval == -3) {
      std::cerr << "gsd: " << "Invalid GSD file " << m_name << endl;
    } else if (retval != 0) {
      std::cerr << "gsd: " << "Unknown error reading: " << m_name << endl;
    }

    type_mapping.clear();
    for (unsigned int i = 0; i < entry->N; i++) {
      size_t l = strnlen(&data[i*entry->M], entry->M);
      type_mapping.push_back(std::string(&data[i*entry->M], l));
    }

    return type_mapping;
  }
}

/*! Read the same data chunks written by GSDDumpWriter::writeFrameHeader
*/
void GSDReader::readHeader()
{
  readChunk(&m_timestep, m_frame, "configuration/step", 8);

  uint8_t dim = 3;
  readChunk(&dim, m_frame, "configuration/dimensions", 1);
  m_dimensions = dim;

  readChunk(&m_box, m_frame, "configuration/box", 6*4);
  //m_snapshot->global_box = BoxDim(box[0], box[1], box[2]);
  //m_snapshot->global_box.setTiltFactors(box[3], box[4], box[5]);

  //m_snapshot->particle_data.resize(N);
  unsigned int N = 0;
  readChunk(&N, m_frame, "particles/N", 4);
  if (N == 0) {
    std::cerr << "gsd: " << "cannot read a file with 0 particles" << endl;
  }
  if (N > m_nparticles) {
    m_nparticles = N;
    allocate(m_nparticles);
  }
}

/*! Read the same data chunks for particles
*/
void GSDReader::readParticles()
{
  unsigned int N = m_nparticles;
  std::vector<std::string> type_mapping = readTypes(m_frame, "particles/types");

  // the snapshot already has default values, if a chunk is not found, the value
  // is already at the default, and the failed read is not a problem
  readChunk(&type[0], m_frame, "particles/typeid", N*4, N);
  readChunk(&mass[0], m_frame, "particles/mass", N*4, N);
  readChunk(&charge[0], m_frame, "particles/charge", N*4, N);
  readChunk(&diameter[0], m_frame, "particles/diameter", N*4, N);
  readChunk(&body[0], m_frame, "particles/body", N*4, N);
  readChunk(&inertia[0], m_frame, "particles/moment_inertia", N*12, N);
  readChunk(&pos[0], m_frame, "particles/position", N*12, N);
  readChunk(&orientation[0], m_frame, "particles/orientation", N*16, N);
  readChunk(&vel[0], m_frame, "particles/velocity", N*12, N);
  readChunk(&angmom[0], m_frame, "particles/angmom", N*16, N);
  readChunk(&image[0], m_frame, "particles/image", N*12, N);
}

/*! Allocate memory for particles
*/

void GSDReader::allocate(int N)
{
  grow(type, N);
  grow(mass, N);
  grow(charge, N);
  grow(diameter, N);
  grow(body, N);
  grow(inertia, 3*N);
  grow(pos, 3*N);
  grow(orientation, 4*N);
  grow(vel, 3*N);
  grow(angmom, 4*N);
  grow(image, 3*N);
}

/*! Write the current frame to a LAMMPS dump file
*/

void GSDReader::write_dump(const char* filename)
{
  ofstream ofs;
  ofs.open(filename);

  ofs << "ITEM: TIMESTEP\n";
  ofs << m_timestep << "\n";
  ofs << "ITEM: NUMBER OF ATOMS\n";
  ofs << m_nparticles << "\n";
  ofs << "ITEM: BOX BOUNDS pp pp pp\n";
  ofs << -m_box[0]/2.0 << " " << m_box[0]/2.0 << "\n";
  ofs << -m_box[1]/2.0 << " " << m_box[1]/2.0 << "\n";
  ofs << -m_box[2]/2.0 << " " << m_box[2]/2.0 << "\n";
  ofs << "ITEM: ATOMS id type q x y z\n";

  int id = 1;
  for (int i = 0; i < m_nparticles; i++) {
    ofs << id << " ";
    ofs << type[i] << " ";
    ofs << charge[i] << " ";
    ofs << pos[3*i] << " " << pos[3*i+1] << " " << pos[3*i+2] << "\n";
    id++;
  }

  ofs.close();
}