/*
  Fourier transforms

  This is an example of using the classes and API provided by libanalyzer.a.

  Compile:     make -f Makefile.cpu
  Clean-build: make -f Makefile.cpu clean

  Contact: Trung Nguyen, ndactrung@gmail.com

*/

#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <cstring>

#include "io.h"
#include "memory.h"
#include "fourier.h"

using namespace std;
using namespace Analysis;

void parse(char* line, char* copy, char*& command, int& narg, char** arg);
void get_data_1d(const char* filename, const int col, vector<double>& data);
void compute_autocorrelation(vector<double>& input);

int main(int argc, char** argv)
{
  if (argc < 2) {
    std::cout << "Missing the data file\n";
    return 1;
  }

  vector<double> data;
  int col = 2;

  int iarg = 2;
  while (iarg < argc) {
    if (strcmp(argv[iarg],"col") == 0) {
      col = atoi(argv[iarg+1]);
      iarg+=2;
    } else {
      std::cout << "Invalid argument: " << argv[iarg] << "\n";
      return 1;
    }
  }

  get_data_1d(argv[1], col, data);

  for (int i =0; i < data.size(); i++) {
    std::cout << data[i] << "\n";
  }
  return 0;
}

void compute_autocorrelation(vector<double>& input)
{
  int N = input.size();

  // find the closest power of 2 for the size

  int m = 1;
  while (m < N) {
    m <<= 1;
  }

  double* data = new double [N];     // calibrating signal

  // remove the average value

  double ave = 0;
  for (int i = 0; i < N; i++) {
    data[i] = input[i];
    ave += data[i];
  }
  ave /= (double)N;

  for (int i = 0; i < N; i++)
    data[i] -= ave;

  // pad with zeros 

  for (int i = N; i < m; i++)
    data[i] = 0;

  // create FFT

  Fourier fft;
  fft.setdata1d(data, m);
  fft.execute();

  // point-wise multiplication fft[i] * conj(fft[i])
  m = 0;
  for (int i = 0; i < N/2; i++) {
    double p = fft.output[m+0]*fft.output[m+0]+fft.output[m+1]*fft.output[m+1];

    m += 2;
  }


  delete [] data;
}

void get_data_1d(const char* filename, const int col, vector<double>& data)
{
  int maxarg=64, narg=0;
  int MAXLINE=1024;
  char* line = new char [MAXLINE];
  char* copy = new char [MAXLINE];
  char* entry;
  char** arg = new char* [maxarg];

  ifstream ifs;
  ifs.open(filename);

  while (!ifs.eof()) {
    ifs.getline(line, MAXLINE);
    parse(line, copy, entry, narg, arg);

    if (entry == NULL) continue;

    double value = atof(arg[0]);

    data.push_back(value);
    std::cout << value << "\n";
  }

  ifs.close();

  delete [] copy;
	delete [] line;
	delete [] arg;
}

void parse(char* line, char* copy, char*& command, int& narg, char** arg)
{
	// make a copy to work on
	
	strcpy(copy, line);
	
	// strip any # comment by resetting string terminator
	// do not strip # inside double quotes
	
	int level = 0;
	char *ptr = copy;
	while (*ptr) 
	{
		if (*ptr == '#' && level == 0) 
		{
			*ptr = '\0';
			break;
		}
		
		ptr++;
	}
	
	// entry = 1st arg
	command = strtok(copy," \t\n\r\f");
	if (command == NULL) return;
	
	// point arg[] at each subsequent arg
	// treat text between double quotes as one arg
	// insert string terminators in copy to delimit args
	
	narg = 0;
	while (1) 
	{
		arg[narg] = strtok(NULL," \t\n\r\f");
		if (arg[narg]) 
			narg++;
		else 
			break;
	}
}
