#ifndef __ANALYZER
#define __ANALYZER

namespace Analysis {

class Analyzer
{
public:
  Analyzer() {}
  ~Analyzer() {}

  virtual int execute() { return 0; }
};

} // namespace Analysis

#endif
