/*
  Find the coordination correlation function and bonding life time correlation function
  of the particles in GSD file 1 with those in GSD file 2

  References:
  1. Webb et al., ACS Marco Lett. 2018, 7:734--738 (coordination correlation function)
  2. Solano et al., J. Chem. Phys. 2013, 139_034502 (ion conductivity and bonding life time correlation function)
    
*/

#include <algorithm>
#include <fstream>
#include <cmath>
#include <iostream>
#include <string>

#include "DCDHandle.h"
#include "GSDReader.h"
#include "io.h"
#include "memory.h"
#include "coord.h"

using namespace std;
using namespace Analysis;

#define anint(x) ((x >= 0.5) ? (1.0) : (x < -0.5) ? (-1.0) : (0.0)) 

#define EPSILON 1e-8
#define MAX_CORR 300

enum {XYZ=0, XML=1, DUMP=2, DCD=3, GSD=4};

/*---------------------------------------------------------------------------*/

Coordination::Coordination(int _nframes, double _cutoff, int _periodic_wrapped)
{
  nframes = _nframes;
  periodic_wrapped = _periodic_wrapped;
  timestep = 0;
  io_handle = NULL;
  cutoff = _cutoff;
}

/*---------------------------------------------------------------------------*/

Coordination::~Coordination()
{
  if (io_handle) delete io_handle;
}

/*! \param gsd GSDReader handle that already opens a GSD file to read
    \param types Array of particle types to be selected
    \param ntypes Number of types to be selected
    \param beadList Array of particles received (output)
    \param nframe current frame in the GSD file

    Load the bead list from a file only pick the beads of specified types.
*/
int Coordination::load_bead_list(class GSDReader* gsd, int nframe, char** types,
  int ntypes, BeadList& beadList)
{
  if (!gsd) return 0;

  gsd->read_frame(nframe);

  const float* _box = gsd->getBoxDim();
  box.Lx = _box[0];
  box.Ly = _box[1];
  box.Lz = _box[2];
  box.xlo = -0.5*box.Lx;
  box.xhi = 0.5*box.Lx;
  box.ylo = -0.5*box.Ly;
  box.yhi = 0.5*box.Ly;
  box.zlo = -0.5*box.Lz;
  box.zhi = 0.5*box.Lz;

  beadList.clear();

  int success = 1;
  int num_beads = gsd->getTotalNumParticles();
  unsigned int* beadtypes = gsd->getTypes();
  int* image = gsd->getImages();
  float* pos = gsd->getPositions();
  float* charge = gsd->getCharges();
  float* vel = gsd->getVelocities();
  int count1 = 0;
  int count2 = 0;
  for (int i = 0; i < num_beads; i++) {

    char stype[16];
    sprintf(stype, "%d", beadtypes[i]);
    int included = 0;
    for (int m = 0; m < ntypes; m++) {
      if (strcmp(stype, types[m]) == 0) {
        included = 1;
        break;
      }
    }
    
    if (included == 1) {
      Bead bead;
      bead.id = ++count1;
      sprintf(bead.type, "%d", beadtypes[i]);
      bead.x = pos[3*i+0];
      bead.y = pos[3*i+1];
      bead.z = pos[3*i+2];
      bead.ix = image[3*i];
      bead.iy = image[3*i+1];
      bead.iz = image[3*i+2];
      bead.vx = vel[3*i+0];
      bead.vy = vel[3*i+1];
      bead.vz = vel[3*i+2];
      bead.q = charge[i];
      beadList.push_back(bead);
    }
  }
  
  return success;
}

/*---------------------------------------------------------------------------*/

void Coordination::analyze()
{
  
}

/*! \param l1 Reference bead list
    \param l2 Coordinated bead list
    \param ilist1 List of indices from the referece bead list
    \param reference_point = 1 if list 1 is used as the reference point and ilist is rebuilt
                           = 0 if looping over ilist of list 1 only
    return the number of particles in list l1 that have a neighbor in list l2 in the present frame
    Compute the coordination number of particles in list l1 with those in list l2.
*/

double Coordination::compute_coordination(const BeadList& l1, const BeadList& l2,
  std::vector<int>& ilist1, int reference_point)
{
  double cutoff2 = cutoff * cutoff;
  double h = 0;

  if (reference_point) {
    ilist1.clear();

    int n1 = l1.size();
    int n2 = l2.size();
    for (int i = 0; i < n1; i++) {
      double xtmp = l1[i].x;
      double ytmp = l1[i].y;
      double ztmp = l1[i].z;

      int has_neighbors = 0;
      for (int j = 0; j < n2; j++) {
        double delx = xtmp - l2[j].x;
        double dely = ytmp - l2[j].y;
        double delz = ztmp - l2[j].z;
        box.minimum_image(delx, dely, delz);
        double rsq = delx*delx + dely*dely + delz*delz;
        if (rsq <= cutoff2) {
          has_neighbors = 1;
          break;
        }
      }
      // store the index of list 1 if it has a neighbor within cutoff from list 2
      if (has_neighbors) {
        ilist1.push_back(i);
        h += 1.0;
      }
    }
  } else {

    int n1 = ilist1.size();
    int n2 = l2.size();
    for (int ii = 0; ii < n1; ii++) {
      int i = ilist1[ii];
      double xtmp = l1[i].x;
      double ytmp = l1[i].y;
      double ztmp = l1[i].z;

      int has_neighbors = 0;
      for (int j = 0; j < n2; j++) {
        double delx = xtmp - l2[j].x;
        double dely = ytmp - l2[j].y;
        double delz = ztmp - l2[j].z;
        box.minimum_image(delx, dely, delz);
        double rsq = delx*delx + dely*dely + delz*delz;
        if (rsq <= cutoff2) {
          has_neighbors = 1;
          break;
        }
      }
      if (has_neighbors) h += 1.0;
    }
    
  }
  
  return h;
}

/*! \param l1 Reference bead list
    \param l2 Coordinated bead list
    \param ilist1 List of indices from the reference bead list
    \param ilist12List of indices from the coordinated bead list
    \param reference_point = 1 if list 1 is used as the reference point and ilist is rebuilt
                           = 0 if looping over ilist of list 1 only
    return the number of particles in list l1 that have a neighbor in list l2 in the present frame
    Compute the coordination number of particles in list l1 with those in list l2.
*/

double Coordination::compute_lifetime(const BeadList& l1, const BeadList& l2,
  std::vector<Pair>& ijlist, int reference_point)
{
  double cutoff2 = cutoff * cutoff;
  double h = 0;

  if (reference_point) {
    ijlist.clear();

    int n1 = l1.size();
    int n2 = l2.size();
    for (int i = 0; i < n1; i++) {
      double xtmp = l1[i].x;
      double ytmp = l1[i].y;
      double ztmp = l1[i].z;

      int has_neighbors = 0;
      for (int j = 0; j < n2; j++) {
        double delx = xtmp - l2[j].x;
        double dely = ytmp - l2[j].y;
        double delz = ztmp - l2[j].z;
        box.minimum_image(delx, dely, delz);
        double rsq = delx*delx + dely*dely + delz*delz;
        if (rsq <= cutoff2) {
          h += 1.0;

          // store the pair
          Pair pij;
          pij.i = i;
          pij.j = j;
          ijlist.push_back(pij);
        }
      }
    }
  } else {

    int n = ijlist.size();
    for (int ii = 0; ii < n; ii++) {
      int i = ijlist[ii].i;
      int j = ijlist[ii].j;
      
      double delx = l1[i].x - l2[j].x;
      double dely = l1[i].y - l2[j].y;
      double delz = l1[i].z - l2[j].z;
      box.minimum_image(delx, dely, delz);
      double rsq = delx*delx + dely*dely + delz*delz;
      if (rsq <= cutoff2) {
        h += 1.0;
      }
    }
    
  }
  
  return h;
}

/*---------------------------------------------------------------------------*/

void Coordination::write_dump(const BeadList& beadList, double Lx, double Ly,
  double Lz, char* filename)
{
  int nNumBeads = beadList.size();
  ofstream ofs(filename);
  ofs << "ITEM: TIMESTEP\n0\n";
  ofs << "ITEM: NUMBER OF ATOMS\n";
  ofs << nNumBeads << "\n";
  ofs << "ITEM: BOX BOUNDS pp pp pp\n";
  ofs << "0 " << Lx << "\n";
  ofs << "0 " << Ly << "\n";
  ofs << "0 " << Lz << "\n";
  ofs << "ITEM: ATOMS id mol type q x y z ix iy iz vx vy vz\n";

  for (int i = 0; i < nNumBeads; i++) {
    ofs << beadList[i].id << " ";
    ofs << beadList[i].mol << " ";
    ofs << beadList[i].type << " ";
    ofs << beadList[i].q << " ";
    ofs << beadList[i].x << " ";
    ofs << beadList[i].y << " ";
    ofs << beadList[i].z << " ";
    ofs << beadList[i].ix << " ";
    ofs << beadList[i].iy << " ";
    ofs << beadList[i].iz << " ";
    ofs << beadList[i].vx << " ";
    ofs << beadList[i].vy << " ";
    ofs << beadList[i].vz << "\n";
  }

  ofs.close();
}

/*---------------------------------------------------------------------------*/

void Coordination::write_dump(const BeadList& beadList1, const BeadList& beadList2,
  double Lx, double Ly,  double Lz, ofstream& ofs, int timestep)
{
  int nNumBeads1 = beadList1.size();
  int nNumBeads2 = beadList2.size();
  
  ofs << "ITEM: TIMESTEP\n";
  ofs << timestep << "\n";
  ofs << "ITEM: NUMBER OF ATOMS\n";
  ofs << nNumBeads1+nNumBeads2 << "\n";
  ofs << "ITEM: BOX BOUNDS pp pp pp\n";
  ofs << -Lx/2 << " " << Lx/2 << "\n";
  ofs << -Ly/2 << " " << Ly/2 << "\n";
  ofs << -Lz/2 << " " << Lz/2 << "\n";
  ofs << "ITEM: ATOMS id mol type q x y z ix iy iz vx vy vz\n";
  int id = 1;
  for (int i = 0; i < nNumBeads1; i++) {
    ofs << id << " ";
    ofs << beadList1[i].mol << " ";
    ofs << beadList1[i].type << " ";
    ofs << beadList1[i].q << " ";
    ofs << beadList1[i].x << " ";
    ofs << beadList1[i].y << " ";
    ofs << beadList1[i].z << " ";
    ofs << beadList1[i].ix << " ";
    ofs << beadList1[i].iy << " ";
    ofs << beadList1[i].iz << " ";
    ofs << beadList1[i].vx << " ";
    ofs << beadList1[i].vy << " ";
    ofs << beadList1[i].vz << "\n";
    id++;
  }

  for (int i = 0; i < nNumBeads2; i++) {
    ofs << id << " ";
    ofs << beadList2[i].mol << " ";
    ofs << beadList2[i].type << " ";
    ofs << beadList2[i].q << " ";
    ofs << beadList2[i].x << " ";
    ofs << beadList2[i].y << " ";
    ofs << beadList2[i].z << " ";
    ofs << beadList2[i].ix << " ";
    ofs << beadList2[i].iy << " ";
    ofs << beadList2[i].iz << " ";
    ofs << beadList2[i].vx << " ";
    ofs << beadList2[i].vy << " ";
    ofs << beadList2[i].vz << "\n";
    id++;
  }

}
