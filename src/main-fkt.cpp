/*
  Copyright (C) 2017  Trung D. Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
  Compute self-intermediate scattering function F(q,t) from simulation snapshots.

  This is an example of using the classes and API provided by libanalyzer.a.

  Compile:     make -f Makefile.cpu
  Clean-build: make -f Makefile.cpu clean

  Use:   ./make-fqt types M T_1 .. T_M [frames <args>]
    
  Input:   config_file = an ASCII file in XYZ format, or LAMMPS dump or HOOMD XML
              for LAMMPS dump files, atom data contains at least 5 columns: id type x y z
              for HOOMD XML files, positions are listed first

           types 
              M     = number of bead types in the group
              T_1 .. T_M = list of M bead types, as space-separated strings

           frames (optional): see example for details
              <args> = start end interval
                 start    = start time
                 end      = end time
                 interval = inverval between consecutive frames (snapshots)

  Examples: 1) run the following

             ./make-fqt final.xml types 2 1 2 nthreads 2 k 1.0 nsamples 300

           to compute the F(k,t) of beads of types 1 and 2 from final.xml using 2 threads
           for 300 wave vectors of magnitude k = 1.0.
           
            2) run the following

             ./make-fkt trajectory.dcd types 1 1 frames 50 60 1 k 5.0 nsamples 200

           to compute F(k,t) for beads of two types A and B from a trajectory
           from frame 50 through frame 60 with step 1 and averaged output.

            3) run the following

             ./make-fkt dump..txt types 2 1 2 frames 10000 20000 1000 k 0.5 nsamples 200
            
            to compute F(k,t) for beads of two types A and B from a series of
            files dump.10000.txt, dump.11000.txt,.., dump.20000.txt,
            for 200 wave vectors of magnitude k = 0.5.

  Output:  

  Contact: Trung Nguyen, ndactrung@gmail.com

*/

#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

#include "io.h"
#include "memory.h"
#include "intermediate-scattering.h"

using namespace std;
using namespace Analysis;

void compute_fkt(int start, int end, int interval, int decorrelated, int periodic_wrapped,
 char* fileName, char** types, int ntypes, int nthreads, double k, int nsamples);

int main(int argc, char** argv)
{
  if (argc < 2) {
    cout << "Arguments required: filename\n";
    cout << "Refer to README for optional arguments.\n";
    return 1;
  }

  char fileName[64], outfile[64];
  int ntypes = 0;
  char** types = NULL;
  double k = 1.0;
  int nsamples = 500;

  sprintf(fileName, "%s", argv[1]);

  int trajectory = 0;
  int start = 0;
  int end = 0;
  int interval = 0;

  int nthreads = 1;

  char templatefile[256];
  int use_template = 0;

  int periodic_wrapped = 1;
  int decorrelated = 1;

  // parse the arguments
  
  int iarg = 2;
  while (iarg < argc) {
    if (strcmp(argv[iarg],"types") == 0) {
      ntypes = atoi(argv[iarg+1]);
      if (ntypes <= 0) {
        std::cout << "Invalid number of types\n";
        return 1;
      }
      iarg+=2;
      types = new char* [ntypes];
      for (int m = 0; m < ntypes; m++) {
        types[m] = new char [64];
        strcpy(types[m], argv[iarg++]);
      }
    } else if (strcmp(argv[iarg],"nthreads") == 0) {
      nthreads = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"k") == 0) {
      k = atof(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"nsamples") == 0) {
      nsamples = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"decorrelated") == 0) {
      decorrelated = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"frames") == 0) {
      start = atoi(argv[iarg+1]);
      end = atoi(argv[iarg+2]);
      interval = atoi(argv[iarg+3]);
      trajectory = 1;
      iarg += 4;
    } else {
      std::cout << "Invalid argument: " << argv[iarg] << "\n";
      return 1;
    }
  }

  // error checks
  
  if (ntypes == 0) {
    cout << "Needs to specify at least a bead type for computing S(q,t).\n";
    return 1;
  }

  int success;
  
  compute_fkt(start, end, interval, decorrelated, periodic_wrapped,
    fileName, types, ntypes, nthreads, k, nsamples);
  
  
  if (types) {
    for (int m = 0; m < ntypes; m++)
      delete [] types[m];
    delete [] types;
  }

  return 0;
}

void compute_fkt(int start, int end, int interval, int decorrelated, int periodic_wrapped,
 char* fileName, char** types, int ntypes, int nthreads, double k, int nsamples)
{
  clock_t tstart, telapsed;
  tstart = clock();

  ofstream logfile;

  int nframes = (end - start)/interval + 1;
  int nwindows = nframes / decorrelated - 1;
  int tmax = nframes*interval / 2;

  IntermediateScattering scattering(nframes, nthreads, periodic_wrapped);
  scattering.set_k_vectors(k, nsamples);

  double** h_data = NULL;
  int num_entries = tmax/interval + 1;
  create(h_data, nwindows, num_entries);
  for (int i = 0; i < nwindows; i++) 
    for (int t = 0; t < num_entries; t++) h_data[i][t] = 0;

  double* ave_h = NULL;
  double* ave_h2 = NULL;
  int* count = NULL;
  create(ave_h, num_entries);
  create(ave_h2, num_entries);
  create(count, num_entries);
  for (int t = 0; t < num_entries; t++) {
    ave_h[t] = 0;
    ave_h2[t] = 0;
    count[t] = 0;
  }

  for (int i = 0; i < nwindows; i++) {
    int origin = start + i*decorrelated*interval;
    std::cout << "  window " << i+1 << " / " << nwindows << "\n";
    for (int t = 0; t < tmax; t+=interval) {
      int t_idx = t/interval;
      if (origin + t < end) {

        // if t == 0, save the particle coordinates to the reference
        int reference_point = 0;
        if (t == 0) reference_point = 1;

        // load the particle positions at t
        scattering.load_bead_list(fileName, types, ntypes, origin+t, reference_point);

        double h = scattering.compute();
        h_data[i][t_idx] = h;

        ave_h[t_idx] += h;
        ave_h2[t_idx] += h*h;
        count[t_idx]++;

      } else {
        h_data[i][t_idx] = 0;  
      }
    }

    // running averages
    if (i % 10 == 0) {
      logfile.open("fkt_ave.txt");
      for (int t = 0; t < num_entries; t++) {
        if (count[t] > 0) {
          double avg = ave_h[t] / (double)count[t];
          double avg2 = ave_h2[t] / (double)count[t];
          logfile << t*interval << " ";
          logfile << avg << " ";
          logfile << sqrt(avg2 - avg*avg) << " ";
          logfile << count[t] << std::endl;
        }
      }
      logfile.close();
    }

  }

  telapsed = clock()-start;      
  std::cout << "Scattering time (seconds): " << (double)telapsed/CLOCKS_PER_SEC << "\n";

  for (int t = 0; t < num_entries; t++) {
    ave_h[t] = 0;
    ave_h2[t] = 0;
    count[t] = 0;
  }

  // average over the windows
  logfile.open("fkt_all.txt");
  for (int t = 0; t < num_entries; t++) {
    logfile << t << " "; 
    for (int i = 0; i < nwindows; i++) {
      logfile << h_data[i][t] << " ";
      if (h_data[i][t] > 0) {
        ave_h[t] += h_data[i][t];
        ave_h2[t] += h_data[i][t]*h_data[i][t];
        count[t]++;
      }
    }
    logfile << std::endl;
  }
  logfile.close();

  
  logfile.open("fkt_ave.txt");
  for (int t = 0; t < num_entries; t++) {
    if (count[t] > 0) {
      double avg = ave_h[t] / (double)count[t];
      double avg2 = ave_h2[t] / (double)count[t];
      logfile << t*interval << " ";
      logfile << avg << " ";
      logfile << sqrt(avg2 - avg*avg) << " ";
      logfile << count[t] << std::endl;
    }
  }
  logfile.close();

  destroy(h_data);
  destroy(ave_h);
  destroy(count);
}
