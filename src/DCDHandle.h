/*
  Copyright (C) 2017  Trung Dac Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*----------------------------------------------------------------------------
  DCD file handler
  Created by Martin Girard on 10/19/16.
  Bugfixed and updated by Trung Nguyen (ndactrung@gmail.com) on 11/8/16.
------------------------------------------------------------------------------*/

#ifndef __DCDHANDLE
#define __DCDHANDLE

#include <cstdint>
#include <iostream>
#include <vector>
#include "io.h"

namespace Analysis {

struct DCDHeader
{
  bool ischarmm;
  bool ischarmm_xtra_block;
  bool ischarmm_4dims;
  bool double_delta;
  std::int32_t blocksize1, blocksize2, blocksize3;
  std::int32_t nset, istart, nsavc, nstep, null4[4],
               nfreat, null9[9], version, ntitle;
  double delta;
  char* title;
  char hdr[4];
};

class DCDHandle : public IOHandle
{
public:
  DCDHandle();
  ~DCDHandle();

  int open_DCD(const char* filename);
  int get_frame(float* box, float* x, float* y, float* z);
  int goto_frame(std::size_t frame);

  int read(const char* fileName, int& nNumTotalBeads,
           Box& box, std::vector<Bead>& beadList, const int nframe);
  int read(const char* fileName, std::vector<int>& indices, int& nNumTotalBeads,
           Box& box, std::vector<Bead>& beadList, const int nframe);
  void write(const Cluster& cluster, const std::vector<Particle>& particleList,
             const Box& box, const char* fileName, int cluster_id,
             int timestep);
  void write_xml(const char* fileName);

  long get_frame_size() { return framesize; }
  int get_current_frame() { return current_frame; }
  DCDHeader get_head() { return head; }
  std::size_t get_natoms() { return (std::size_t) n_particles; }
  long fptr_tell() { return std::ftell(ptr_file); }
  std::size_t get_nframes() { return (std::size_t) nframes; }
  bool is_reading();
  int close_dcd() { return std::fclose(ptr_file); }

protected:
  std::int32_t n_particles;
  long filesize, headersize, framesize;
  DCDHeader head;
  bool head_read;
  FILE* ptr_file;
  int extra_block_size, coord_block_size;
  int current_frame, nframes;
  float *x, *y, *z;
  float _box[6];

};

}

#endif // DCDHANDLE
