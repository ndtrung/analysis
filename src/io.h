#ifndef __INPUT_OUTPUT
#define __INPUT_OUTPUT

#include <vector>
#include "buildingblocks.h"

namespace Analysis {

class IOHandle
{
public:
  IOHandle() {}
  virtual ~IOHandle() {}

  virtual int read(const char* fileName, char** types, int ntypes, int& nNumTotalBeads,
            Box& box, std::vector<Bead>& beadList, int& timestep)
  {
    return 0;
  }

  virtual int read(const char* fileName, int& nNumTotalBeads,
            Box& box, std::vector<Bead>& beadList, const int nframe)
  {
    return 0;
  }

  virtual void write(const Cluster& cluster, const std::vector<Particle>& particleList,
             const Box& box, const char* fileName, int cluster_id, int timestep)
  {
  }

  int shift;  //! 1 if box center should be shifted to the origin (0, 0, 0)

  void parse(char* line, char* copy, int& narg, char** arg) {

  	// make a copy to work on

	  strcpy(copy, line);

  	// strip any # comment by resetting string terminator
	  // do not strip # inside double quotes
	
	  int level = 0;
  	char *ptr = copy;
	  while (*ptr) {
  		if (*ptr == '#' && level == 0) {
		  	*ptr = '\0';
		  	break;
      }  
		
		  ptr++;
	  }

  	// entry = 1st arg
  	arg[0] = strtok(copy," \t\n\r\f");
  	if (arg[0] == NULL) return;

  	// point arg[] at each subsequent arg
  	// treat text between double quotes as one arg
  	// insert string terminators in copy to delimit args

    narg = 1;
    while (1) {
      arg[narg] = strtok(NULL," \t\n\r\f");
      if (arg[narg])
  		  narg++;
      else 
        break;
    }
  }

};

/*----------------------------------------------------------------------------*/

//! XYZ files
class XYZHandle : public IOHandle
{
public:
  XYZHandle() : IOHandle() {}
  ~XYZHandle() {}

  int read(const char* fileName, char** types, int ntypes, int& nNumTotalBeads,
           Box& box, std::vector<Bead>& beadList, int& timestep);

  void write(const Cluster& cluster, const std::vector<Particle>& particleList,
               const Box& box, const char* fileName, int cluster_id,
               int timestep);
};

/*----------------------------------------------------------------------------*/

//! HOOMD XML files
class XMLHandle : public IOHandle
{
public:
  XMLHandle() : IOHandle() {}
  ~XMLHandle() {}

  int read(const char* fileName, char** types, int ntypes, int& nNumTotalBeads,
           Box& box, std::vector<Bead>& beadList, int& timestep);

  void write(const Cluster& cluster, const std::vector<Particle>& particleList,
             const Box& box, const char* fileName, int cluster_id,
                 int timestep);
};

/*----------------------------------------------------------------------------*/

//! LAMMPS DUMP files
class DUMPHandle : public IOHandle
{
public:
  DUMPHandle() : IOHandle()
  {
    nfields = 0; 
    fields = NULL;
    shift = 0;
  }

  ~DUMPHandle()
  {
    if (fields) delete [] fields;
  }

  int read(const char* fileName, char** types, int ntypes, int& nNumTotalBeads,
           Box& box, std::vector<Bead>& beadList, int& timestep);

  void write(const Cluster& cluster, const std::vector<Particle>& particleList,
             const Box& box, const char* fileName, int cluster_id,
             int timestep);
  int* fields;
  int nfields;
};

void extract(const char* fileName, int local, int nNumBeadsInParticle, char* outfile);

}

#endif
