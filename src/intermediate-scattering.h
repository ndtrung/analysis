/*
  Copyright (C) 2017  Trung D. Nguyen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __INTERMEDIATE_SCATTERING
#define __INTERMEDIATE_SCATTERING

#include <vector>
#include <string>
#include "analyzer.h"
#include "buildingblocks.h"

namespace Analysis {

class IntermediateScattering : public Analyzer
{
public:
  IntermediateScattering(int nframes, int _nthreads=1, int _periodic_wrapped=1);
  ~IntermediateScattering();

  // load the bead list from a file
  int load_bead_list(char* fileName, char** types, int ntypes, int t=-1, int reference_point=1);

  // set the k vector magnitude and number of samples
  void set_k_vectors(double k, int num);

  // workhorse for direct calculation of G_s(r,t) = < |r(0) - r(t)| >
  double compute_vanHove();

  // workhorse for direct calculation of F(k,t) = < exp(i k dot (r(0) - r(t))) >
  double compute();

  // bead lists
  std::vector<Bead> beadList0, beadList;

protected:
  double k;                 //! k vector magnitude
  int numberOfWavevectors;  //! number of samples (vectors) with the same magnitude 
  int nNumTotalBeads;       //! Number of beads per snapshot/configuration
  Box box;                  //! Box dimensions
  int timestep;             //! Time step read from the config file
  int nframes;              //! number of frames in the trajectory
  int last_frame;           //! store the most recently loaded frame
  int file_type;            //! Config file type: XYZ, DUMP (LAMMPS) or XML (HOOMD)
  string base, ext;         //! Config files are in the form: base.ext

  int nthreads;             //! Number of threads in use
  int periodic_wrapped;     //! 1 if periodic boundary conditions are used
  double *Fk_rep;

  int ksetup;                //! 1 if the wave vectors are already setup

  std::vector<class KVector> waveVectors;
  void setup_waveVectors();
  int load_from_gsd(char** types, int ntypes, int& nNumTotalBeads, Box& box,
    BeadList& beadList, int nframe);

  IOHandle* io_handle;
  class GSDReader* gsd;     //! Handle to read GSD files
};

class KVector {
public:
  #if (__GNUC__ < 6)
  friend  bool operator < (const KVector& v1, const KVector& v2) { return (v1.mag2 < v2.mag2); }
  #else
  bool operator < (const KVector& v) { return (mag2 < v.mag2); }
  #endif

  double x, y, z, mag2;
};

} // namespace Analysis

#endif
