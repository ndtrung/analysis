#!/usr/bin/env python

import sys, os, argparse
from math import *
import numpy as np

def create_parser():
  parser = argparse.ArgumentParser(description='Finding particle clusters within a cutoff distance')

  parser.add_argument('-input' , dest='input',  default=None, help = 'Name of file with data inputs')
  parser.add_argument('-cutoff', dest='cutoff', default="1.4", help = 'Neighbor cutoff')
  parser.add_argument('-types' , dest='types',  default="1 1",   help = 'Particle types')
  parser.add_argument('-threshold' , dest='threshold', default="5",   help = 'Minimum cluster size')
  parser.add_argument('-binsize' , dest='binsize',  default="1",   help = 'Cluster bin size')
  parser.add_argument('-frames' , dest='frames',  default=None,   help = 'Frames including start, end inteval')
  parser.add_argument('-writeClusters' , dest='writeClusters',  default="-3",   help = 'Writing clusters to files')

  return parser

def main(argv):
  parser     = create_parser()
  args       = parser.parse_args()

  # Assemble the command line to execute
  cmd = "../src/find_clusters "
  cmd += args.input
  cmd += " types " + args.types
  cmd += " threshold " + args.threshold
  cmd += " cutoff " + args.cutoff
  if args.frames is not None:
    cmd += " frames " + args.frames

  print(cmd)
  os.system(cmd)

if __name__ == "__main__":
  main(sys.argv[1:])