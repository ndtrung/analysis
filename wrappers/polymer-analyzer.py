#!/usr/bin/env python

import sys, os, argparse
from math import *
import numpy as np

def create_parser():
  parser = argparse.ArgumentParser(description='Analyzing polymer statistics over the trajectory')

  parser.add_argument('-input' , dest='input',  default=None, help = 'Name of file with data inputs')
  parser.add_argument('-types' , dest='types',  default="1 1",   help = 'Monomer types')
  parser.add_argument('-nbins' , dest='nbins',  default="50",   help = 'Number of bins for the histograms') 
  parser.add_argument('-range' , dest='range',  default="0 10",   help = 'Range for the histograms')
  parser.add_argument('-nthreads' , dest='nthreads',  default="4",   help = 'Number of threads')
  parser.add_argument('-frames' , dest='frames',  default=None,   help = 'Frames including start, end inteval')

  return parser

def main(argv):
  parser     = create_parser()
  args       = parser.parse_args()

  # Assemble the command line to execute
  cmd = "../src/analyze-polymers "
  cmd += args.input
  cmd += " types " + args.types
  cmd += " nbins " + args.nbins
  cmd += " range " + args.range

  if args.frames is not None:
    cmd += " frames " + args.frames

  print(cmd)
  os.system(cmd)

if __name__ == "__main__":
  main(sys.argv[1:])