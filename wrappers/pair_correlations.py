#!/usr/bin/env python

import sys, os, argparse
from math import *
import numpy as np

def create_parser():
  parser = argparse.ArgumentParser(description='Finding pair correlation functions and structure factor')

  parser.add_argument('-input' , dest='input',  default=None, help = 'Name of file with data inputs')
  parser.add_argument('-types1' , dest='types1',  default="1 1",   help = 'Particle types 1')
  parser.add_argument('-types2' , dest='types2',  default="1 1",   help = 'Particle types 2')
  parser.add_argument('-dr' , dest='dr',  default="0.1",   help = 'Bin size for g(r)') 
  parser.add_argument('-rmax' , dest='rmax',  default="10",   help = 'Distance range for g(r)')
  parser.add_argument('-direct' , dest='direct',  default=False,   help = 'Direct computation of S(q)')
  parser.add_argument('-nthreads' , dest='nthreads',  default="4",   help = 'Number of threads')
  parser.add_argument('-frames' , dest='frames',  default=None,   help = 'Frames including start, end inteval')

  return parser

def main(argv):
  parser     = create_parser()
  args       = parser.parse_args()

  # Assemble the command line to execute
  cmd = "../src/make-rdf "
  cmd += args.input
  cmd += " types1 " + args.types1
  cmd += " types2 " + args.types2
  cmd += " dr " + args.dr
  cmd += " rmax " + args.rmax
  if args.direct == False:
    cmd += " direct no"
  else:
    cmd += " direct yes"
  cmd += " nthreads " + args.nthreads

  if args.frames is not None:
    cmd += " frames " + args.frames

  print(cmd)
  os.system(cmd)

if __name__ == "__main__":
  main(sys.argv[1:])