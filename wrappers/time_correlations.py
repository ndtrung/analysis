#!/usr/bin/env python

import sys, os, argparse
from math import *
import numpy as np

def create_parser():
  parser = argparse.ArgumentParser(description='Finding time correlation functions over the trajectory')

  parser.add_argument('-input' , dest='input',  default=None, help = 'Name of file with data inputs')
  parser.add_argument('-types' , dest='types',  default="1 1",   help = 'Particle types 1')
  parser.add_argument('-decorrelated' , dest='decorrelated',  default="10",   help = 'Number of snapshots that presumes decorrelation')
  parser.add_argument('-mode' , dest='mode',  default="msd",   help = 'Quantity of interest: msd, vacf or sqt') 
  parser.add_argument('-nthreads' , dest='nthreads',  default="4",   help = 'Number of threads')
  parser.add_argument('-frames' , dest='frames',  default=None,   help = 'Frames including start, end inteval')

  return parser

def main(argv):
  parser     = create_parser()
  args       = parser.parse_args()

  # Assemble the command line to execute
  cmd = "../src/correlation "
  cmd += args.input
  cmd += " types " + args.types
  cmd += " decorrelated " + args.decorrelated
  cmd += " mode " + args.mode
  cmd += " nthreads " + args.nthreads

  if args.frames is not None:
    cmd += " frames " + args.frames

  print(cmd)
  os.system(cmd)

if __name__ == "__main__":
  main(sys.argv[1:])